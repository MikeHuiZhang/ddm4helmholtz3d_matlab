%helmholtz_sol.m -- femdirect solution

clear all;

f= 2;
k= 2*pi*f; k2= k^2; a= 1i*k;
% $$$ k2= @ksquare;
% $$$ a= @arobin;                                   % Robin parameter
Sa= 0;
% $$$ scpoints= [6760,6760,10,1];
scpoints= [.5 .5 .5 1];
uD= 0;
uR= 0;
xl= 0; xr= 1;
yl= 0; yr= 1;
zl= 0; zr= 1;
GammaD= [];
GammaR= [1,2,3,4,5,6];
nx= 20; ny= 20; nz= 20;
[coordinates,elements,dirichlet,robin]= qubemesh(xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR);
[udirect,A,b]= femdirect(k2,Sa,scpoints,uD,a,uR,coordinates,elements, ...
                         dirichlet,robin);
udirect= reshape(udirect,nx+1,ny+1,nz+1);

uu= permute(udirect,[3 2 1]);
save('~/Codes/Helmholtz/3D/u.mat','uu');

% $$$ [y,x,z]= meshgrid(yl:(yr-yl)/ny:yr,xl:(xr-xl)/nx:xr,zl:(zr-zl)/nz:zr);
% $$$ figure, slice(y,x,z,imag(udirect),6720,6700,10);
% $$$ set(findobj(gca,'Type','Surface'),'EdgeColor','none'), axis off
% $$$ figure, plot(y(26,:,46), real(udirect(26,:,46)),'r');
% $$$ figure, plot(y(41,:,73), real(udirect(41,:,73)),'Color',[0 .4 .6]);