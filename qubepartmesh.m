function [scoord,selem,srobin,ii,sid,sir,sib,sic,sbsend,sbnb,scsend, ...
    scnb,bdintelems,bdintelemslocal,nbdintnodes,bdintnodes,bfnodes,bfelems,bfelemslocal] ...
    = qubepartmesh(xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR,Nx,Ny,Nz)
% INPUT
% OUTPUT
%    scoord{s}  each row is coordinates for a node in the s-th subdomain,
%               theses nodes come in order of 
%               (interior,dirichlet,robin\cross,boundary,cross)
%     selem{s}  each row is 8 node indices for a qube element
%    srobin{s}  part of robin boundary elements for the whole domain and 
%               owned by the s-th subdomain
%          ii   column,indices of the interior nodes in each subdomain
%      sid{s}   column, indices of the dirichlet nodes in subdomain s
%      sir{s}   column, indices of robin\cross nodes in subdomain s
%      sib{s}   column, indices of the inner boundary nodes in subdomain s
%               inner boundary nodes are partitioned into segments, each 
%               segment is shared with one and only one neighbor, and the 
%               orders of nodes in segments are the same between neighbors
%      sic{s}   column vector, indices of the cross points in subdomain s
% sbsend{s}(:)  row, end positions of boundary segments in sib{s}
%   sbnb{s}(:)  row, neighbors sharing segments with the s-th subdomain
% scsend{s}(:)  row, relative end positions of cross segments in the cross
% scnb{s}(j,:)  matrix with 7 columns, neighbors sharing the j-th cross 
%               segment with subdomain s, 0 means a null neighbor
%
%! Warning: 'uint32' allow 1624^3 grid cells  

% % input check, matlab always hints error
% allfaces= 1:6;
% if ~(union(GammaD,GammaR)==allfaces)
%     display('the union of GammD and GammaR must be 1:6, a row vector');
%     return;
% end

% initialization
Ns= Nx*Ny*Nz; % assume ordered in x first, y second,z last
Hx= (xr-xl)/Nx; Hy= (yr-yl)/Ny; Hz= (zr-zl)/Nz;
scoord= cell(Ns,1); selem= cell(Ns,1); srobin= cell(Ns,1); 
sid= cell(Ns,1); sir= cell(Ns,1); sib= cell(Ns,1); sic= cell(Ns,1);
sbsend= cell(Ns,1);  sbnb= cell(Ns,1); 
scsend= cell(Ns,1);  scnb= cell(Ns,1);
snx= nx/Nx; sny= ny/Ny; snz= nz/Nz;


% preprocessing
% 
% % mesh for the 1st subdomain, coordinates and elements
[scoord{1},selem{1}]= qubemesh(xl,xl+Hx,yl,yl+Hy,zl,zl+Hz,snx,sny,snz);
nnodes= size(scoord{1},1);
% 
% % find ii, the indices of interior nodes 
iz= kron((2:snz)',ones((snx-1)*(sny-1),1)); % 3d indices of interior nodes
iy= repmat(kron((2:sny)',ones(snx-1,1)),snz-1,1);
ix= repmat((2:snx)',(sny-1)*(snz-1),1);
ii= ix+(iy-1)*(snx+1)+(iz-1)*(snx+1)*(sny+1); % 1d indices of interior nodes
%
% % find nodes on boundary(outer and inner) - 6 faces, 12 edges and
% % 8 vertices, and construct bounary elements
[bfnodes,bfelems,benodes,bvnodes,bdintelems,bdintelemslocal,nbdintnodes,...
    bdintnodes,bfelemslocal]= qubebdmesh(snx,sny,snz);

% treat interior subdomains, begin with the (2,2,2) subdomain 
% then translate to other interior subdomains
%
% % translante scoord{1} and selem{1} to the canonical subdomain (2,2,2),
% % then generate all the outputs for this subdomain 
if Nx>2 && Ny>2 && Nz>2  % if interior subdomains exist
    s= 2+Nx+Nx*Ny;
    scoord{s}= scoord{1} + repmat([Hx,Hy,Hz],nnodes,1);
    selem{s}= selem{1};
    [srobin{s},sid{s},sir{s},sib{s},sic{s},sbsend{s},sbnb{s},scsend{s},scnb{s}] ...
        = seekibc(bfnodes,bfelems,benodes,bvnodes,s,Nx,Ny,Nz,GammaD,GammaR);
end
%
% % translate outputs for subdomain (2,2,2) to other interior subdomains
for sx= 2:Nx-1
    for sy= 2:Ny-1
        for sz= 2:Nz-1
            s1= sx+(sy-1)*Nx+(sz-1)*Nx*Ny;
            if (s1==s)
                continue;
            end
            scoord{s1}= scoord{s}+repmat([Hx*(sx-2),Hy*(sy-2),Hz*(sz-2)],nnodes,1);
            selem{s1}= selem{s};
            srobin{s1}= [];
            sid{s1}= [];
            sir{s1}= [];
            sib{s1}= sib{s};  sic{s1}= sic{s};
            sbsend{s1}= sbsend{s};
            sbnb{s1}= sbnb{s}+(sx-2)+(sy-2)*Nx+(sz-2)*Nx*Ny;
            scsend{s1}= scsend{s};
            scnb{s1}= zeros(size(scnb{s},1),7);
            nb= find(scnb{s}(:));
            scnb{s1}(nb)= scnb{s}(nb)+(sx-2)+(sy-2)*Nx+(sz-2)*Nx*Ny;
        end
    end
end

% treat subdomains along the faces excluding those along edges
%
% % 'left' face (1,sy,sz)
% % % canonical subdomain (1,2,2)
if Ny>2 && Nz>2
    s= 1+Nx+Nx*Ny;
    scoord{s}= scoord{1} + repmat([0,Hy,Hz],nnodes,1);
    selem{s}= selem{1};
    [srobin{s},sid{s},sir{s},sib{s},sic{s},sbsend{s},sbnb{s},scsend{s},scnb{s}] ...
        = seekibc(bfnodes,bfelems,benodes,bvnodes,s,Nx,Ny,Nz,GammaD,GammaR);
% % % translation to (1,sy,sz)
for sy= 2:Ny-1
    for sz= 2:Nz-1
        s1= 1+(sy-1)*Nx+(sz-1)*Nx*Ny;
        if s1==s
            continue;
        end
        scoord{s1}= scoord{s}+repmat([0,Hy*(sy-2),Hz*(sz-2)],nnodes,1);
        selem{s1}= selem{s};
        srobin{s1}= srobin{s};
        sid{s1}= sid{s};
        sir{s1}= sir{s};
        sib{s1}= sib{s};  sic{s1}= sic{s};
        sbsend{s1}= sbsend{s};
        sbnb{s1}= sbnb{s}+(sy-2)*Nx+(sz-2)*Nx*Ny;
        scsend{s1}= scsend{s};
        scnb{s1}= zeros(size(scnb{s},1),7);
        nb= find(scnb{s}(:));
        scnb{s1}(nb)= scnb{s}(nb)+(sy-2)*Nx+(sz-2)*Nx*Ny;
    end
end        
end
%
% % 'right' face (Nx,sy,sz)
% % % canonical subdomain (Nx,2,2)
if Ny>2 && Nz>2 && Nx>1
    s= Nx+Nx+Nx*Ny;
    scoord{s}= scoord{1} + repmat([(Nx-1)*Hx,Hy,Hz],nnodes,1);
    selem{s}= selem{1};
    [srobin{s},sid{s},sir{s},sib{s},sic{s},sbsend{s},sbnb{s},scsend{s},scnb{s}] ...
        = seekibc(bfnodes,bfelems,benodes,bvnodes,s,Nx,Ny,Nz,GammaD,GammaR);
% % % translation to (Nx,sy,sz)
for sy= 2:Ny-1
    for sz= 2:Nz-1
        s1= Nx+(sy-1)*Nx+(sz-1)*Nx*Ny;
        if s1==s
            continue;
        end
        scoord{s1}= scoord{s}+repmat([0,Hy*(sy-2),Hz*(sz-2)],nnodes,1);
        selem{s1}= selem{s};
        srobin{s1}= srobin{s};
        sid{s1}= sid{s};
        sir{s1}= sir{s};
        sib{s1}= sib{s};  sic{s1}= sic{s};
        sbsend{s1}= sbsend{s};
        sbnb{s1}= sbnb{s}+(sy-2)*Nx+(sz-2)*Nx*Ny;
        scsend{s1}= scsend{s};
        scnb{s1}= zeros(size(scnb{s},1),7);
        nb= find(scnb{s}(:));
        scnb{s1}(nb)= scnb{s}(nb)+(sy-2)*Nx+(sz-2)*Nx*Ny;
    end
end        
end
%
% % 'front' face
% % % canonical subdomain (2,1,2)
if Nx>2 && Nz>2
    s= 2+Nx*Ny;
    scoord{s}= scoord{1} + repmat([Hx,0,Hz],nnodes,1);
    selem{s}= selem{1};
    [srobin{s},sid{s},sir{s},sib{s},sic{s},sbsend{s},sbnb{s},scsend{s},scnb{s}] ...
        = seekibc(bfnodes,bfelems,benodes,bvnodes,s,Nx,Ny,Nz,GammaD,GammaR);
% % % translation to (sx,1,sz)
for sx= 2:Nx-1
    for sz= 2:Nz-1
        s1= sx+(sz-1)*Nx*Ny;
        if s1==s
            continue;
        end
        scoord{s1}= scoord{s}+repmat([Hx*(sx-2),0,Hz*(sz-2)],nnodes,1);
        selem{s1}= selem{s};
        srobin{s1}= srobin{s};
        sid{s1}= sid{s};
        sir{s1}= sir{s};
        sib{s1}= sib{s};  sic{s1}= sic{s};
        sbsend{s1}= sbsend{s};
        sbnb{s1}= sbnb{s}+(sx-2)+(sz-2)*Nx*Ny;
        scsend{s1}= scsend{s};
        scnb{s1}= zeros(size(scnb{s},1),7);
        nb= find(scnb{s}(:));
        scnb{s1}(nb)= scnb{s}(nb)+(sx-2)+(sz-2)*Nx*Ny;
    end
end        
end
%
% % 'back' face
% % % canonical subdomain (2,Ny,2)
if Nx>2 && Nz>2 && Ny>1
    s= 2+(Ny-1)*Nx+Nx*Ny;
    scoord{s}= scoord{1} + repmat([Hx,(Ny-1)*Hy,Hz],nnodes,1);
    selem{s}= selem{1};
    [srobin{s},sid{s},sir{s},sib{s},sic{s},sbsend{s},sbnb{s},scsend{s},scnb{s}] ...
        = seekibc(bfnodes,bfelems,benodes,bvnodes,s,Nx,Ny,Nz,GammaD,GammaR);
% % % translation to (sx,Ny,sz)
for sx= 2:Nx-1
    for sz= 2:Nz-1
        s1= sx+(Ny-1)*Nx+(sz-1)*Nx*Ny;
        if s1==s
            continue;
        end
        scoord{s1}= scoord{s}+repmat([Hx*(sx-2),0,Hz*(sz-2)],nnodes,1);
        selem{s1}= selem{s};
        srobin{s1}= srobin{s};
        sid{s1}= sid{s};
        sir{s1}= sir{s};
        sib{s1}= sib{s};  sic{s1}= sic{s};
        sbsend{s1}= sbsend{s};
        sbnb{s1}= sbnb{s}+(sx-2)+(sz-2)*Nx*Ny;
        scsend{s1}= scsend{s};
        scnb{s1}= zeros(size(scnb{s},1),7);
        nb= find(scnb{s}(:));
        scnb{s1}(nb)= scnb{s}(nb)+(sx-2)+(sz-2)*Nx*Ny;
    end
end        
end
% % 'bottom' face
% % % canonical subdomain (2,2,1)
if Nx>2 && Ny>2
    s= 2+Nx;
    scoord{s}= scoord{1} + repmat([Hx,Hy,0],nnodes,1);
    selem{s}= selem{1};
    [srobin{s},sid{s},sir{s},sib{s},sic{s},sbsend{s},sbnb{s},scsend{s},scnb{s}] ...
        = seekibc(bfnodes,bfelems,benodes,bvnodes,s,Nx,Ny,Nz,GammaD,GammaR);
% % % translation to (sx,sy,1)
for sx= 2:Nx-1
    for sy= 2:Ny-1
        s1= sx+(sy-1)*Nx;
        if s1==s
            continue;
        end
        scoord{s1}= scoord{s}+repmat([Hx*(sx-2),Hy*(sy-2),0],nnodes,1);
        selem{s1}= selem{s};
        srobin{s1}= srobin{s};
        sid{s1}= sid{s};
        sir{s1}= sir{s};
        sib{s1}= sib{s};  sic{s1}= sic{s};
        sbsend{s1}= sbsend{s};
        sbnb{s1}= sbnb{s}+(sx-2)+(sy-2)*Nx;
        scsend{s1}= scsend{s};
        scnb{s1}= zeros(size(scnb{s},1),7);
        nb= find(scnb{s}(:));
        scnb{s1}(nb)= scnb{s}(nb)+(sx-2)+(sy-2)*Nx;
    end
end        
end
% % 'top' face
% % % canonical subdomain (2,2,Nz)
if Nx>2 && Ny>2 && Nz>1
    s= 2+Nx+(Nz-1)*Nx*Ny;
    scoord{s}= scoord{1} + repmat([Hx,Hy,(Nz-1)*Hz],nnodes,1);
    selem{s}= selem{1};
    [srobin{s},sid{s},sir{s},sib{s},sic{s},sbsend{s},sbnb{s},scsend{s},scnb{s}] ...
        = seekibc(bfnodes,bfelems,benodes,bvnodes,s,Nx,Ny,Nz,GammaD,GammaR);
% % % translation to (sx,sy,Nz)
for sx= 2:Nx-1
    for sy= 2:Ny-1
        s1= sx+(sy-1)*Nx+(Nz-1)*Nx*Ny;
        if s1==s
            continue;
        end
        scoord{s1}= scoord{s}+repmat([Hx*(sx-2),Hy*(sy-2),0],nnodes,1);
        selem{s1}= selem{s};
        srobin{s1}= srobin{s};
        sid{s1}= sid{s};
        sir{s1}= sir{s};
        sib{s1}= sib{s};  sic{s1}= sic{s};
        sbsend{s1}= sbsend{s};
        sbnb{s1}= sbnb{s}+(sx-2)+(sy-2)*Nx;
        scsend{s1}= scsend{s};
        scnb{s1}= zeros(size(scnb{s},1),7);
        nb= find(scnb{s}(:));
        scnb{s1}(nb)= scnb{s}(nb)+(sx-2)+(sy-2)*Nx;
    end
end        
end

% treat bounary subdomains along the 12 edges excluding corners
%
% % the first edge (1,1,sz)
% % % canonical subdomain (1,1,2)
if Nz>2
    s= 1+Nx*Ny;
    scoord{s}= scoord{1} + repmat([0,0,Hz],nnodes,1);
    selem{s}= selem{1};
    [srobin{s},sid{s},sir{s},sib{s},sic{s},sbsend{s},sbnb{s},scsend{s},scnb{s}] ...
        = seekibc(bfnodes,bfelems,benodes,bvnodes,s,Nx,Ny,Nz,GammaD,GammaR);
% % % translation to (1,1,sz)
for sz= 3:Nz-1
    s1= 1+(sz-1)*Nx*Ny;
    scoord{s1}= scoord{s}+repmat([0,0,Hz*(sz-2)],nnodes,1);
    selem{s1}= selem{s};
    srobin{s1}= srobin{s};
    sid{s1}= sid{s};
    sir{s1}= sir{s};
    sib{s1}= sib{s};  sic{s1}= sic{s};
    sbsend{s1}= sbsend{s};
    sbnb{s1}= sbnb{s}+(sz-2)*Nx*Ny;
    scsend{s1}= scsend{s};
    scnb{s1}= zeros(size(scnb{s},1),7);
    nb= find(scnb{s}(:));
    scnb{s1}(nb)= scnb{s}(nb)+(sz-2)*Nx*Ny;
end
end
%
% % the second edge (Nx,1,sz)
% % % canonical subdomain (Nx,1,2)
if Nz>2 && Nx>1
    s= Nx+Nx*Ny;
    scoord{s}= scoord{1} + repmat([(Nx-1)*Hx,0,Hz],nnodes,1);
    selem{s}= selem{1};
    [srobin{s},sid{s},sir{s},sib{s},sic{s},sbsend{s},sbnb{s},scsend{s},scnb{s}] ...
        = seekibc(bfnodes,bfelems,benodes,bvnodes,s,Nx,Ny,Nz,GammaD,GammaR);
% % % translation to (Nx,1,sz)
for sz= 3:Nz-1
    s1= Nx+(sz-1)*Nx*Ny;
    scoord{s1}= scoord{s}+repmat([0,0,Hz*(sz-2)],nnodes,1);
    selem{s1}= selem{s};
    srobin{s1}= srobin{s};
    sid{s1}= sid{s};
    sir{s1}= sir{s};
    sib{s1}= sib{s};  sic{s1}= sic{s};
    sbsend{s1}= sbsend{s};
    sbnb{s1}= sbnb{s}+(sz-2)*Nx*Ny;
    scsend{s1}= scsend{s};
    scnb{s1}= zeros(size(scnb{s},1),7);
    nb= find(scnb{s}(:));
    scnb{s1}(nb)= scnb{s}(nb)+(sz-2)*Nx*Ny;
end
end
%
% % the third edge (1,Ny,sz)
% % % canonical subdomain (1,Ny,2)
if Nz>2 && Ny>1
    s= 1+(Ny-1)*Nx+Nx*Ny;
    scoord{s}= scoord{1} + repmat([0,(Ny-1)*Hy,Hz],nnodes,1);
    selem{s}= selem{1};
    [srobin{s},sid{s},sir{s},sib{s},sic{s},sbsend{s},sbnb{s},scsend{s},scnb{s}] ...
        = seekibc(bfnodes,bfelems,benodes,bvnodes,s,Nx,Ny,Nz,GammaD,GammaR);
% % % translation to (1,Ny,sz)
for sz= 3:Nz-1
    s1= 1+(Ny-1)*Nx+(sz-1)*Nx*Ny;
    scoord{s1}= scoord{s}+repmat([0,0,Hz*(sz-2)],nnodes,1);
    selem{s1}= selem{s};
    srobin{s1}= srobin{s};
    sid{s1}= sid{s};
    sir{s1}= sir{s};
    sib{s1}= sib{s};  sic{s1}= sic{s};
    sbsend{s1}= sbsend{s};
    sbnb{s1}= sbnb{s}+(sz-2)*Nx*Ny;
    scsend{s1}= scsend{s};
    scnb{s1}= zeros(size(scnb{s},1),7);
    nb= find(scnb{s}(:));
    scnb{s1}(nb)= scnb{s}(nb)+(sz-2)*Nx*Ny;
end
end
%
% % the fourth edge (Nx,Ny,sz)
% % % canonical subdomain (Nx,Ny,2)
if Nz>2 && Ny>1 && Nx>1
    s= Nx+(Ny-1)*Nx+Nx*Ny;
    scoord{s}= scoord{1} + repmat([(Nx-1)*Hx,(Ny-1)*Hy,Hz],nnodes,1);
    selem{s}= selem{1};
    [srobin{s},sid{s},sir{s},sib{s},sic{s},sbsend{s},sbnb{s},scsend{s},scnb{s}] ...
        = seekibc(bfnodes,bfelems,benodes,bvnodes,s,Nx,Ny,Nz,GammaD,GammaR);
% % % translation to (Nx,Ny,sz)
for sz= 3:Nz-1
    s1= Nx+(Ny-1)*Nx+(sz-1)*Nx*Ny;
    scoord{s1}= scoord{s}+repmat([0,0,Hz*(sz-2)],nnodes,1);
    selem{s1}= selem{s};
    srobin{s1}= srobin{s};
    sid{s1}= sid{s};
    sir{s1}= sir{s};
    sib{s1}= sib{s};  sic{s1}= sic{s};
    sbsend{s1}= sbsend{s};
    sbnb{s1}= sbnb{s}+(sz-2)*Nx*Ny;
    scsend{s1}= scsend{s};
    scnb{s1}= zeros(size(scnb{s},1),7);
    nb= find(scnb{s}(:));
    scnb{s1}(nb)= scnb{s}(nb)+(sz-2)*Nx*Ny;
end
end
%
% % the fifth edge (1,sy,1)
% % % canonical subdomain (1,2,1)
if Ny>2
    s= 1+Nx;
    scoord{s}= scoord{1} + repmat([0,Hy,0],nnodes,1);
    selem{s}= selem{1};
    [srobin{s},sid{s},sir{s},sib{s},sic{s},sbsend{s},sbnb{s},scsend{s},scnb{s}] ...
        = seekibc(bfnodes,bfelems,benodes,bvnodes,s,Nx,Ny,Nz,GammaD,GammaR);
% % % translation to (1,sy,1)
for sy= 3:Ny-1
    s1= 1+(sy-1)*Nx;
    scoord{s1}= scoord{s}+repmat([0,Hy*(sy-2),0],nnodes,1);
    selem{s1}= selem{s};
    srobin{s1}= srobin{s};
    sid{s1}= sid{s};
    sir{s1}= sir{s};
    sib{s1}= sib{s};  sic{s1}= sic{s};
    sbsend{s1}= sbsend{s};
    sbnb{s1}= sbnb{s}+(sy-2)*Nx;
    scsend{s1}= scsend{s};
    scnb{s1}= zeros(size(scnb{s},1),7);
    nb= find(scnb{s}(:));
    scnb{s1}(nb)= scnb{s}(nb)+(sy-2)*Nx;
end
end
%
% % the sixth edge (Nx,sy,1)
% % % canonical subdomain (Nx,2,1)
if Ny>2 && Nx>1
    s= Nx+Nx;
    scoord{s}= scoord{1} + repmat([(Nx-1)*Hx,Hy,0],nnodes,1);
    selem{s}= selem{1};
    [srobin{s},sid{s},sir{s},sib{s},sic{s},sbsend{s},sbnb{s},scsend{s},scnb{s}] ...
        = seekibc(bfnodes,bfelems,benodes,bvnodes,s,Nx,Ny,Nz,GammaD,GammaR);
% % % translation to (Nx,sy,1)
for sy= 3:Ny-1
    s1= Nx+(sy-1)*Nx;
    scoord{s1}= scoord{s}+repmat([0,Hy*(sy-2),0],nnodes,1);
    selem{s1}= selem{s};
    srobin{s1}= srobin{s};
    sid{s1}= sid{s};
    sir{s1}= sir{s};
    sib{s1}= sib{s};  sic{s1}= sic{s};
    sbsend{s1}= sbsend{s};
    sbnb{s1}= sbnb{s}+(sy-2)*Nx;
    scsend{s1}= scsend{s};
    scnb{s1}= zeros(size(scnb{s},1),7);
    nb= find(scnb{s}(:));
    scnb{s1}(nb)= scnb{s}(nb)+(sy-2)*Nx;
end
end
%
% % the seventh edge (1,sy,Nz)
% % % canonical subdomain (1,2,Nz)
if Ny>2 && Nz>1
    s= 1+Nx+(Nz-1)*Nx*Ny;
    scoord{s}= scoord{1} + repmat([0,Hy,(Nz-1)*Hz],nnodes,1);
    selem{s}= selem{1};
    [srobin{s},sid{s},sir{s},sib{s},sic{s},sbsend{s},sbnb{s},scsend{s},scnb{s}] ...
        = seekibc(bfnodes,bfelems,benodes,bvnodes,s,Nx,Ny,Nz,GammaD,GammaR);
% % % translation to (1,sy,Nz)
for sy= 3:Ny-1
    s1= 1+(sy-1)*Nx+(Nz-1)*Nx*Ny;
    scoord{s1}= scoord{s}+repmat([0,Hy*(sy-2),0],nnodes,1);
    selem{s1}= selem{s};
    srobin{s1}= srobin{s};
    sid{s1}= sid{s};
    sir{s1}= sir{s};
    sib{s1}= sib{s};  sic{s1}= sic{s};
    sbsend{s1}= sbsend{s};
    sbnb{s1}= sbnb{s}+(sy-2)*Nx;
    scsend{s1}= scsend{s};
    scnb{s1}= zeros(size(scnb{s},1),7);
    nb= find(scnb{s}(:));
    scnb{s1}(nb)= scnb{s}(nb)+(sy-2)*Nx;
end
end
%
% % the eighth edge (Nx,sy,Nz)
% % % canonical subdomain (Nx,2,Nz)
if Ny>2 && Nz>1 && Nx>1
    s= Nx+Nx+(Nz-1)*Nx*Ny;
    scoord{s}= scoord{1} + repmat([(Nx-1)*Hx,Hy,(Nz-1)*Hz],nnodes,1);
    selem{s}= selem{1};
    [srobin{s},sid{s},sir{s},sib{s},sic{s},sbsend{s},sbnb{s},scsend{s},scnb{s}] ...
        = seekibc(bfnodes,bfelems,benodes,bvnodes,s,Nx,Ny,Nz,GammaD,GammaR);
% % % translation to (Nx,sy,Nz)
for sy= 3:Ny-1
    s1= Nx+(sy-1)*Nx+(Nz-1)*Nx*Ny;
    scoord{s1}= scoord{s}+repmat([0,Hy*(sy-2),0],nnodes,1);
    selem{s1}= selem{s};
    srobin{s1}= srobin{s};
    sid{s1}= sid{s};
    sir{s1}= sir{s};
    sib{s1}= sib{s};  sic{s1}= sic{s};
    sbsend{s1}= sbsend{s};
    sbnb{s1}= sbnb{s}+(sy-2)*Nx;
    scsend{s1}= scsend{s};
    scnb{s1}= zeros(size(scnb{s},1),7);
    nb= find(scnb{s}(:));
    scnb{s1}(nb)= scnb{s}(nb)+(sy-2)*Nx;
end
end
%
% % the ninth edge (sx,1,1)
% % % canonical subdomain (2,1,1)
if Nx>2
    s= 2;
    scoord{s}= scoord{1} + repmat([Hx,0,0],nnodes,1);
    selem{s}= selem{1};
    [srobin{s},sid{s},sir{s},sib{s},sic{s},sbsend{s},sbnb{s},scsend{s},scnb{s}] ...
        = seekibc(bfnodes,bfelems,benodes,bvnodes,s,Nx,Ny,Nz,GammaD,GammaR);
% % % translation to (sx,1,1)
for sx= 3:Nx-1
    s1= sx;
    scoord{s1}= scoord{s}+repmat([Hx*(sx-2),0,0],nnodes,1);
    selem{s1}= selem{s};
    srobin{s1}= srobin{s};
    sid{s1}= sid{s};
    sir{s1}= sir{s};
    sib{s1}= sib{s};  sic{s1}= sic{s};
    sbsend{s1}= sbsend{s};
    sbnb{s1}= sbnb{s}+(sx-2);
    scsend{s1}= scsend{s};
    scnb{s1}= zeros(size(scnb{s},1),7);
    nb= find(scnb{s}(:));
    scnb{s1}(nb)= scnb{s}(nb)+(sx-2);
end
end
%
% % the tenth edge (sx,Ny,1)
% % % canonical subdomain (2,Ny,1)
if Nx>2 && Ny>1
    s= 2+(Ny-1)*Nx;
    scoord{s}= scoord{1} + repmat([Hx,(Ny-1)*Hy,0],nnodes,1);
    selem{s}= selem{1};
    [srobin{s},sid{s},sir{s},sib{s},sic{s},sbsend{s},sbnb{s},scsend{s},scnb{s}] ...
        = seekibc(bfnodes,bfelems,benodes,bvnodes,s,Nx,Ny,Nz,GammaD,GammaR);
% % % translation to (sx,Ny,1)
for sx= 3:Nx-1
    s1= sx+(Ny-1)*Nx;
    scoord{s1}= scoord{s}+repmat([Hx*(sx-2),0,0],nnodes,1);
    selem{s1}= selem{s};
    srobin{s1}= srobin{s};
    sid{s1}= sid{s};
    sir{s1}= sir{s};
    sib{s1}= sib{s};  sic{s1}= sic{s};
    sbsend{s1}= sbsend{s};
    sbnb{s1}= sbnb{s}+(sx-2);
    scsend{s1}= scsend{s};
    scnb{s1}= zeros(size(scnb{s},1),7);
    nb= find(scnb{s}(:));
    scnb{s1}(nb)= scnb{s}(nb)+(sx-2);
end
end
%
% % the eleventh edge (sx,1,Nz)
% % % canonical subdomain (2,1,Nz)
if Nx>2 && Nz>1
    s= 2+(Nz-1)*Nx*Ny;
    scoord{s}= scoord{1} + repmat([Hx,0,(Nz-1)*Hz],nnodes,1);
    selem{s}= selem{1};
    [srobin{s},sid{s},sir{s},sib{s},sic{s},sbsend{s},sbnb{s},scsend{s},scnb{s}] ...
        = seekibc(bfnodes,bfelems,benodes,bvnodes,s,Nx,Ny,Nz,GammaD,GammaR);
% % % translation to (sx,1,Nz)
for sx= 3:Nx-1
    s1= sx+(Nz-1)*Nx*Ny;
    scoord{s1}= scoord{s}+repmat([Hx*(sx-2),0,0],nnodes,1);
    selem{s1}= selem{s};
    srobin{s1}= srobin{s};
    sid{s1}= sid{s};
    sir{s1}= sir{s};
    sib{s1}= sib{s};  sic{s1}= sic{s};
    sbsend{s1}= sbsend{s};
    sbnb{s1}= sbnb{s}+(sx-2);
    scsend{s1}= scsend{s};
    scnb{s1}= zeros(size(scnb{s},1),7);
    nb= find(scnb{s}(:));
    scnb{s1}(nb)= scnb{s}(nb)+(sx-2);
end
end
%
% % the twelfth edge (sx,Ny,Nz)
% % % canonical subdomain (2,Ny,Nz)
if Nx>2 && Nz>1 && Ny>1
    s= 2+(Ny-1)*Nx+(Nz-1)*Nx*Ny;
    scoord{s}= scoord{1} + repmat([Hx,(Ny-1)*Hy,(Nz-1)*Hz],nnodes,1);
    selem{s}= selem{1};
    [srobin{s},sid{s},sir{s},sib{s},sic{s},sbsend{s},sbnb{s},scsend{s},scnb{s}] ...
        = seekibc(bfnodes,bfelems,benodes,bvnodes,s,Nx,Ny,Nz,GammaD,GammaR);
% % % translation to (sx,Ny,Nz)
for sx= 3:Nx-1
    s1= sx+(Ny-1)*Nx+(Nz-1)*Nx*Ny;
    scoord{s1}= scoord{s}+repmat([Hx*(sx-2),0,0],nnodes,1);
    selem{s1}= selem{s};
    srobin{s1}= srobin{s};
    sid{s1}= sid{s};
    sir{s1}= sir{s};
    sib{s1}= sib{s};  sic{s1}= sic{s};
    sbsend{s1}= sbsend{s};
    sbnb{s1}= sbnb{s}+(sx-2);
    scsend{s1}= scsend{s};
    scnb{s1}= zeros(size(scnb{s},1),7);
    nb= find(scnb{s}(:));
    scnb{s1}(nb)= scnb{s}(nb)+(sx-2);
end
end

% treat boundary subdomains on the 8 corners, translante scoord{1} and
% selem{1} then generate new mesh
%
% % subdomain at the 2nd corner: (Nx,1,1)
if Nx>1  
    s= Nx;
    scoord{s}= scoord{1} + repmat([(Nx-1)*Hx,0,0],nnodes,1);
    selem{s}= selem{1};
    [srobin{s},sid{s},sir{s},sib{s},sic{s},sbsend{s},sbnb{s},scsend{s},scnb{s}] ...
        = seekibc(bfnodes,bfelems,benodes,bvnodes,s,Nx,Ny,Nz,GammaD,GammaR);
end
%
% % subdomain at the 3rd corner: (Nx,Ny,1)
if Nx>1 && Ny>1 
    s= Nx+(Ny-1)*Nx;
    scoord{s}= scoord{1} + repmat([Hx*(Nx-1),Hy*(Ny-1),0],nnodes,1);
    selem{s}= selem{1};
    [srobin{s},sid{s},sir{s},sib{s},sic{s},sbsend{s},sbnb{s},scsend{s},scnb{s}] ...
        = seekibc(bfnodes,bfelems,benodes,bvnodes,s,Nx,Ny,Nz,GammaD,GammaR);
end
%
% % subdomain at the 4th corner: (1,Ny,1)
if Ny>1  
    s= 1+(Ny-1)*Nx;
    scoord{s}= scoord{1} + repmat([0,(Ny-1)*Hy,0],nnodes,1);
    selem{s}= selem{1};
    [srobin{s},sid{s},sir{s},sib{s},sic{s},sbsend{s},sbnb{s},scsend{s},scnb{s}] ...
        = seekibc(bfnodes,bfelems,benodes,bvnodes,s,Nx,Ny,Nz,GammaD,GammaR);
end
%
% % subdomain at the 5th corner: (1,1,Nz)
if Nz>1  
    s= 1+(Nz-1)*Nx*Ny;
    scoord{s}= scoord{1} + repmat([0,0,(Nz-1)*Hz],nnodes,1);
    selem{s}= selem{1};
    [srobin{s},sid{s},sir{s},sib{s},sic{s},sbsend{s},sbnb{s},scsend{s},scnb{s}] ...
        = seekibc(bfnodes,bfelems,benodes,bvnodes,s,Nx,Ny,Nz,GammaD,GammaR);
end
%
% % subdomain at the 6th corner: (Nx,1,Nz)
if Nz>1 && Nx>1  
    s= Nx+(Nz-1)*Nx*Ny;
    scoord{s}= scoord{1} + repmat([(Nx-1)*Hx,0,(Nz-1)*Hz],nnodes,1);
    selem{s}= selem{1};
    [srobin{s},sid{s},sir{s},sib{s},sic{s},sbsend{s},sbnb{s},scsend{s},scnb{s}] ...
        = seekibc(bfnodes,bfelems,benodes,bvnodes,s,Nx,Ny,Nz,GammaD,GammaR);
end
%
% % subdomain at the 7th corner: (Nx,Ny,Nz)
if Nz>1 && Ny>1 && Nx>1  
    s= Nx+(Ny-1)*Nx+(Nz-1)*Nx*Ny;
    scoord{s}= scoord{1}+repmat([(Nx-1)*Hx,(Ny-1)*Hy,(Nz-1)*Hz],nnodes,1);
    selem{s}= selem{1};
    [srobin{s},sid{s},sir{s},sib{s},sic{s},sbsend{s},sbnb{s},scsend{s},scnb{s}] ...
        = seekibc(bfnodes,bfelems,benodes,bvnodes,s,Nx,Ny,Nz,GammaD,GammaR);
end
%
% % subdomain at the 8th corner: (1,Ny,Nz)
if Nz>1 && Ny>1
    s= 1+(Ny-1)*Nx+(Nz-1)*Nx*Ny;
    scoord{s}= scoord{1} + repmat([0,(Ny-1)*Hy,(Nz-1)*Hz],nnodes,1);
    selem{s}= selem{1};
    [srobin{s},sid{s},sir{s},sib{s},sic{s},sbsend{s},sbnb{s},scsend{s},scnb{s}] ...
        = seekibc(bfnodes,bfelems,benodes,bvnodes,s,Nx,Ny,Nz,GammaD,GammaR);
end
%
% % the first subdomain is treated the last, 'cause other subdomains need
%   translation from its initial mesh
s= 1;
[srobin{s},sid{s},sir{s},sib{s},sic{s},sbsend{s},sbnb{s},scsend{s},scnb{s}] ...
        = seekibc(bfnodes,bfelems,benodes,bvnodes,s,Nx,Ny,Nz,GammaD,GammaR);

% end of this function                   
end