function [interb,interbsign]= getinterb(Nx,Ny,Nz,s,mode)
    [sx,sy]= ind2sub([Nx,Ny*Nz],s);         
    [sy,sz]= ind2sub([Ny,Nz],sy);
    outerb= [];
    if 1==sx
       outerb= [outerb, 1];
    end
    if Nx==sx
       outerb= [outerb, 2];
    end
    if 1==sy
       outerb= [outerb, 3];
    end
    if Ny==sy
       outerb= [outerb, 4];
    end
    if 1==sz
       outerb= [outerb, 5];
    end
    if Nz==sz
       outerb= [outerb, 6];
    end
    interb= setdiff(1:6,outerb);
    if mod(sx+sy+sz,2)==0
        interbsign= -1;
    else
        interbsign= 1;
    end
    if exist('mode','var') && 1==mode
        if ~isempty(outerb)
            interbsign= zeros(length(interb),1);
        else
            if mod(sx+sy+sz,2)==0
                interbsign= -1*ones(length(interb),1);
            else
                interbsign= ones(length(interb),1);
            end
            for ib= 1:length(interb)
                switch interb
                    case {1,2}
                        if 2==sx || Nx-1==sx
                            interbsign(ib)= 0;
                        end
                    case {3,4}
                        if 2==sy || Ny-1==sy
                            interbsign(ib)= 0;
                        end
                    case {5,6}
                        if 2==sz || Nz-1==sz
                            interbsign(ib)= 0;
                        end
                end
            end
        end
    end
end