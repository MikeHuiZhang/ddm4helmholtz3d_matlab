function [Rcp, Rwp, numplw]= ...
    qubeplw(coordinates,k2,GammaD,GammaR,nx,ny,nz,Nx,Ny,Nz,wl,cl,ntheta,nphi)
% sdiric is for 'cl' overlapping subdomains
% sdiriw is for 'wl-1' overlapping subdomains

% preparation 
[~,sdiric]= qubesubmesh(GammaD,GammaR,nx,ny,nz,Nx,Ny,Nz,cl);
Dc= qubepartunit(nx,ny,nz,Nx,Ny,Nz,cl,sdiric);
clear sdiric;
Rw= qubesubmesh(GammaD,GammaR,nx,ny,nz,Nx,Ny,Nz,wl-1);

% directions of plane waves in spherical coordinates
theta= linspace(0,pi/2,ntheta+2); theta= theta(2:ntheta+1);
phi= linspace(-pi,pi,nphi+1);  phi= phi(2:nphi+1); phi0= phi;
[theta,phi]= ndgrid(theta,phi); theta= theta(:)'; phi= phi(:)';
if ntheta>-1
    theta= [theta, -theta, zeros(1,nphi), pi/2, -pi/2];
    phi= [phi, pi+phi, phi0, 0, 0];  
    numplw= 2*ntheta*nphi+nphi+2;
else
    numplw= 0;
    theta= [];
    phi= [];
end

% construction of Rwp and Rcp
Ns= Nx*Ny*Nz; Rwp= cell(Ns,1); Rcp= cell(Ns,1);
if numplw~=0
    if isa(k2,'function_handle') || isa(k2,'inline')
       plw= exp(1i*(coordinates.*repmat(sqrt(k2(coordinates)),1,3))* ...
               [cos(theta).*cos(phi);cos(theta).*sin(phi);sin(theta)]);
%          plw= exp(1i*(coordinates*2*pi/3000)* ...
%                [cos(theta).*cos(phi);cos(theta).*sin(phi);sin(theta)]);
    elseif isa(k2,'float')
       plw= exp(1i*(coordinates*sqrt(k2))* ...
               [cos(theta).*cos(phi);cos(theta).*sin(phi);sin(theta)]);
    end
    for s= 1:Ns
       % for Rwp{s} 
       [~, jj, vv]= find(Rw{s});
       Dw= sparse(jj,1,vv,(nx+1)*(ny+1)*(nz+1),1,length(jj));
       Rw{s}= [];
       Rwp{s}= repmat(Dw,1,numplw).*plw;
       clear jj vv Dw;
       Rwp{s}= Rwp{s}.';
       % for Rcp{s}
       Rcp{s}= repmat(Dc{s},1,numplw).*plw;
       Rcp{s}= Rcp{s}.';
    end
else
    for s= 1:Ns
        Rwp{s}= sparse((nx+1)*(ny+1)*(nz+1),0);
        Rcp{s}= Rwp{s};
    end
end

end % -- end of this funciton