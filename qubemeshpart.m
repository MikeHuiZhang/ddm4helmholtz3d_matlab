function [selems,sdiri,srobinelems,sbdelems,Ro,Dw,Rcp,Rwp,numplw,Ri,Dwa, ...
    R0,elems0,diri0,robinelems0,ibdelems,ielems,idirichlet,irobinelems,Rz] = ...
    qubemeshpart(coordinates,GammaD,GammaR,nx,ny,nz,Nx,Ny,Nz,ol,wl,cl,ntheta,nphi,k2,zl)
% INPUT
% we assume both the nodes and elements are ordered in x, y, z
% GammaD    the dirichlet faces
% GammaR    the robin faces
% Nx,Ny,Nz  non-overlapping partition
% ol,wl,cl  we assume all the overlapping numer less than half of the 
%           widths of non-overlapping subdomains
% OUTPUT
% selems{s} points to indices of global elements in subdomain s
% sdiri{s}  points to indices of global nodes which are dirichlet
% srobinelems{s} points to indices of global robin bdry elem. in sub. s
% sbdelems{s} points to indices of global nodes for inner boundary elements
% Ro{s}  global to local mapping including inner boundary nodes
% Dw{s}  the partition of unity global vector corresponding to subdomain s
% Rcp{s} with rows being global vectors representing partition of unity
%        of plane waves to subdomain s
% Rwp{s} with rows being 0-1 cut-off of planes wave s to subdomain s
% Ri{s}  global to local interior (where weighting is one) mapping 
snx= nx/Nx;  sny= ny/Ny;  snz= nz/Nz; Ns= Nx*Ny*Nz; 
selems= cell(Ns,1); snodes= cell(Ns,1); sdiri= cell(Ns,1);
srobinelems= cell(Ns,1);  sbdelems= cell(Ns,1); Ro= cell(Ns,1); 
Dw= cell(Ns,1);  Dc= cell(Ns,1); Rcp= cell(Ns,1); Rwp= cell(Ns,1);
Dwa= cell(Ns,1);
for s= 1:Ns
   % for selems{s}
   [sx,sy]= ind2sub([Nx,Ny*Nz],s); % 3d position of this subdomain
   [sy,sz]= ind2sub([Ny,Nz],sy); 
   ix= ((sx-1)*snx-(sx>1)*ol+1):(sx*snx+(sx<Nx)*ol); % 3d position 
   iy= ((sy-1)*sny-(sy>1)*ol+1):(sy*sny+(sy<Ny)*ol); % of global elements
   iz= ((sz-1)*snz-(sz>1)*ol+1):(sz*snz+(sz<Nz)*ol);
   [IX,IY,IZ]= ndgrid(ix,iy,iz); 
   selems{s}= sub2ind([nx,ny,nz],IX(:),IY(:),IZ(:));
   nxe= length(ix); nye= length(iy); nze= length(iz);
   % for the intermediate result snodes{s}
   ix= ((sx-1)*snx-(sx>1)*ol+1):(sx*snx+(sx<Nx)*ol+1); % 3d position 
   iy= ((sy-1)*sny-(sy>1)*ol+1):(sy*sny+(sy<Ny)*ol+1); % of global nodes
   iz= ((sz-1)*snz-(sz>1)*ol+1):(sz*snz+(sz<Nz)*ol+1);
   [IX,IY,IZ]= ndgrid(ix,iy,iz); 
   snodes{s}= sub2ind([nx+1,ny+1,nz+1],IX(:),IY(:),IZ(:));
   % for sbdelems{s}, sdiri{s} and srobinelems{s}
   [bdnodes,bdelems]= qubebdmesh(nxe,nye,nze);
   num_sbdelems= 0; num_sdiri= 0; num_robinelems= 0;
   if sx>1
       num_sbdelems= num_sbdelems + nye*nze;
   elseif find(1==GammaD,1)
       num_sdiri= num_sdiri + (nye+1)*(nze+1);
   elseif find(1==GammaR,1)
       num_robinelems= num_robinelems + nye*nze;
   else
       disp('error input: face 1 is not in either GammaD or GammaR');
   end
   if sx<Nx
       num_sbdelems= num_sbdelems + nye*nze;
   elseif find(2==GammaD,1)
       num_sdiri= num_sdiri + (nye+1)*(nze+1);
   elseif find(2==GammaR,1)
       num_robinelems= num_robinelems + nye*nze;
   else
       disp('error input: face 2 is not in either GammaD or GammaR');
   end
   if sy>1
       num_sbdelems= num_sbdelems + nxe*nze;
   elseif find(3==GammaD,1)
       num_sdiri= num_sdiri + (nxe+1)*(nze+1);
   elseif find(3==GammaR,1)
       num_robinelems= num_robinelems + nxe*nze;
   else
       disp('error input: face 3 is not in either GammaD or GammaR');
   end
   if sy<Ny
       num_sbdelems= num_sbdelems + nxe*nze;
   elseif find(4==GammaD,1)
       num_sdiri= num_sdiri + (nxe+1)*(nze+1);
   elseif find(4==GammaR,1)
       num_robinelems= num_robinelems + nxe*nze;
   else
       disp('error input: face 4 is not in either GammaD or GammaR');    
   end
   if sz>1
       num_sbdelems= num_sbdelems + nxe*nye;
   elseif find(5==GammaD,1)
       num_sdiri= num_sdiri + (nxe+1)*(nye+1);
   elseif find(5==GammaR,1)
       num_robinelems= num_robinelems + nxe*nye;
   else
       disp('error input: face 5 is not in either GammaD or GammaR');    
   end
   if sz<Nz
       num_sbdelems= num_sbdelems + nxe*nye;
   elseif find(6==GammaD,1)
       num_sdiri= num_sdiri + (nxe+1)*(nye+1);
   elseif find(6==GammaR,1)
       num_robinelems= num_robinelems + nxe*nye;
   else
       disp('error input: face 6 is not in either GammaD or GammaR'); 
   end
   sbdelems{s}= zeros(num_sbdelems,4);
   sdiri{s}= zeros(num_sdiri,1);  
   srobinelems{s}= zeros(num_robinelems,1);
   num_sbdelems= 0; num_sdiri= 0; num_robinelems= 0; num_grobin= 0;
   if sx>1
       sbdelems{s}(num_sbdelems+1:num_sbdelems+nye*nze,:)= snodes{s}(bdelems{1});
       num_sbdelems= num_sbdelems + nye*nze;
   elseif find(1==GammaD,1)
       sdiri{s}(num_sdiri+1:num_sdiri+(nye+1)*(nze+1))= snodes{s}(bdnodes{1});
       num_sdiri= num_sdiri + (nye+1)*(nze+1);
   elseif find(1==GammaR,1)
       iy= ((sy-1)*sny-(sy>1)*ol+1):(sy*sny+(sy<Ny)*ol); % 2d position
       iz= ((sz-1)*snz-(sz>1)*ol+1):(sz*snz+(sz<Nz)*ol); % on face 1
       [IY,IZ]= ndgrid(iy,iz); 
       srobinelems{s}(num_robinelems+1:num_robinelems+nye*nze)= ...
           sub2ind([ny,nz],IY(:),IZ(:));
       num_robinelems= num_robinelems + nye*nze;
   end
   if find(1==GammaR,1)
      num_grobin= num_grobin + ny*nz;
   end
   if sx<Nx
       sbdelems{s}(num_sbdelems+1:num_sbdelems+nye*nze,:)= snodes{s}(bdelems{2});
       num_sbdelems= num_sbdelems + nye*nze;   
   elseif find(2==GammaD,1)
       sdiri{s}(num_sdiri+1:num_sdiri+(nye+1)*(nze+1))= snodes{s}(bdnodes{2});
       num_sdiri= num_sdiri + (nye+1)*(nze+1);
   elseif find(2==GammaR,1)
       iy= ((sy-1)*sny-(sy>1)*ol+1):(sy*sny+(sy<Ny)*ol); % 2d position
       iz= ((sz-1)*snz-(sz>1)*ol+1):(sz*snz+(sz<Nz)*ol); % on face 2
       [IY,IZ]= ndgrid(iy,iz); 
       srobinelems{s}(num_robinelems+1:num_robinelems+nye*nze)= ...
           sub2ind([ny,nz],IY(:),IZ(:)) + num_grobin;
       num_robinelems= num_robinelems + nye*nze;
   end
   if find(2==GammaR,1)
       num_grobin= num_grobin + ny*nz;
   end
   if sy>1
       sbdelems{s}(num_sbdelems+1:num_sbdelems+nxe*nze,:)= snodes{s}(bdelems{3});
       num_sbdelems= num_sbdelems + nxe*nze;   
   elseif find(3==GammaD,1)
       sdiri{s}(num_sdiri+1:num_sdiri+(nxe+1)*(nze+1))= snodes{s}(bdnodes{3});
       num_sdiri= num_sdiri + (nxe+1)*(nze+1);
   elseif find(3==GammaR,1)
       ix= ((sx-1)*snx-(sx>1)*ol+1):(sx*snx+(sx<Nx)*ol); % 2d position  
       iz= ((sz-1)*snz-(sz>1)*ol+1):(sz*snz+(sz<Nz)*ol); % on face 3
       [IX,IZ]= ndgrid(ix,iz); 
       srobinelems{s}(num_robinelems+1:num_robinelems+nxe*nze)= ...
           sub2ind([nx,nz],IX(:),IZ(:)) + num_grobin;
       num_robinelems= num_robinelems + nxe*nze;
   end
   if find(3==GammaR,1)
       num_grobin= num_grobin + nx*nz;
   end
   if sy<Ny
       sbdelems{s}(num_sbdelems+1:num_sbdelems+nxe*nze,:)= snodes{s}(bdelems{4});
       num_sbdelems= num_sbdelems + nxe*nze;   
   elseif find(4==GammaD,1)
       sdiri{s}(num_sdiri+1:num_sdiri+(nxe+1)*(nze+1))= snodes{s}(bdnodes{4});
       num_sdiri= num_sdiri + (nxe+1)*(nze+1);
   elseif find(4==GammaR,1)
       ix= ((sx-1)*snx-(sx>1)*ol+1):(sx*snx+(sx<Nx)*ol); % 2d position  
       iz= ((sz-1)*snz-(sz>1)*ol+1):(sz*snz+(sz<Nz)*ol); % on face 4
       [IX,IZ]= ndgrid(ix,iz); 
       srobinelems{s}(num_robinelems+1:num_robinelems+nxe*nze)= ...
           sub2ind([nx,nz],IX(:),IZ(:)) + num_grobin;
       num_robinelems= num_robinelems + nxe*nze;
   end
   if find(4==GammaR,1)
       num_grobin= num_grobin + nx*nz;
   end
   if sz>1
       sbdelems{s}(num_sbdelems+1:num_sbdelems+nye*nxe,:)= snodes{s}(bdelems{5});
       num_sbdelems= num_sbdelems + nxe*nye;   
   elseif find(5==GammaD,1)
       sdiri{s}(num_sdiri+1:num_sdiri+(nxe+1)*(nye+1))= snodes{s}(bdnodes{5});
       num_sdiri= num_sdiri + (nxe+1)*(nye+1);
   elseif find(5==GammaR,1)
       ix= ((sx-1)*snx-(sx>1)*ol+1):(sx*snx+(sx<Nx)*ol); % 2d position 
       iy= ((sy-1)*sny-(sy>1)*ol+1):(sy*sny+(sy<Ny)*ol); % on face 5
       [IX,IY]= ndgrid(ix,iy); 
       srobinelems{s}(num_robinelems+1:num_robinelems+nxe*nye)= ...
           sub2ind([nx,ny],IX(:),IY(:)) + num_grobin;
       num_robinelems= num_robinelems + nxe*nye;
   end
   if find(5==GammaR,1)
       num_grobin= num_grobin + nx*ny;
   end
   if sz<Nz
       sbdelems{s}(num_sbdelems+1:num_sbdelems+nye*nxe,:)= snodes{s}(bdelems{6});
   elseif find(6==GammaD,1)
       sdiri{s}(num_sdiri+1:num_sdiri+(nxe+1)*(nye+1))= snodes{s}(bdnodes{6});
   elseif find(6==GammaR,1)
       ix= ((sx-1)*snx-(sx>1)*ol+1):(sx*snx+(sx<Nx)*ol); % 2d position 
       iy= ((sy-1)*sny-(sy>1)*ol+1):(sy*sny+(sy<Ny)*ol); % on face 6
       [IX,IY]= ndgrid(ix,iy); 
       srobinelems{s}(num_robinelems+1:num_robinelems+nxe*nye)= ...
           sub2ind([nx,ny],IX(:),IY(:)) + num_grobin;
   end
   % for Ro{s}
   sdiri{s}= unique(sdiri{s});
   snodes{s}= setdiff(snodes{s},sdiri{s});
   Ro{s}= sparse(1:length(snodes{s}),snodes{s},1,length(snodes{s}), ...
       (nx+1)*(ny+1)*(nz+1),length(snodes{s}));
   % for Dw (and the intermediate result Dc)
   % - vectors supporting on subdomains
   % -- the nodes on the non-overlapping sub. have value 1
   ix= ((sx-1)*snx+1):(sx*snx+1); % 3d position 
   iy= ((sy-1)*sny+1):(sy*sny+1); % of global nodes
   iz= ((sz-1)*snz+1):(sz*snz+1);
   [IX,IY,IZ]= ndgrid(ix,iy,iz); 
   ii= sub2ind([nx+1,ny+1,nz+1],IX(:),IY(:),IZ(:));
   vi= ones(length(ii),1);
   % -- the nodes in the outside boundary layers
   %    we first give (3d position of bottom left corner)-1 for each layer
   %    and we generate the local 3d position for each layer
   %    then we add the two results to give the global 3d position
   numwl= 0; numcl= 0; layerfaces= [sx>1,sx<Nx,sy>1,sy<Ny,sz>1,sz<Nz];
   for layer= 1:max(wl,cl)-1
       numl= qubelayer(snx+(sx>1)*layer+(sx<Nx)*layer, ...
           sny+(sy>1)*layer+(sy<Ny)*layer,snz+(sz>1)*layer+(sz<Nz)*layer,layerfaces);
       if layer < wl
          numwl= numwl + numl;
       end
       if layer < cl
          numcl= numcl + numl;
       end
   end
   ib= zeros(numwl,1); vb= zeros(numwl,1); numwl= 0;
   ic= zeros(numcl,1); vc= zeros(numcl,1); numcl= 0;
   dispx= (sx-1)*snx; dispy= (sy-1)*sny; dispz= (sz-1)*snz;
   for layer= 1:max(wl,cl)-1
       dispx= dispx-(sx>1);  dispy= dispy-(sy>1);  dispz= dispz-(sz>1);
       [numl,IX,IY,IZ]= qubelayer(snx+(sx>1)*layer+(sx<Nx)*layer, ...
           sny+(sy>1)*layer+(sy<Ny)*layer,snz+(sz>1)*layer+(sz<Nz)*layer,layerfaces);
       if layer<wl
          ib(numwl+1:numwl+numl)= sub2ind([nx+1,ny+1,nz+1],IX(:)+dispx,IY(:)+dispy,IZ(:)+dispz);
          vb(numwl+1:numwl+numl)= 1-layer/wl;
          numwl= numwl+numl;
       end
       if layer<cl
          ic(numcl+1:numcl+numl)= sub2ind([nx+1,ny+1,nz+1],IX(:)+dispx,IY(:)+dispy,IZ(:)+dispz);
          vc(numcl+1:numcl+numl)= 1-layer/cl;
          numcl= numcl+numl;
       end
   end
   if ~isempty(sdiri{s})   % problem: sdiri{s} is for 'ol' not for 'wl'
      idiri= ismember(ib,sdiri{s});
      vb(idiri)= 0;
      idiri= ismember(ic,sdiri{s});
      vc(idiri)= 0;
   end
   Dw{s}= sparse([ii;ib],1,[vi;vb],(nx+1)*(ny+1)*(nz+1),1);
   Dc{s}= sparse([ii;ic],1,[vi;vc],(nx+1)*(ny+1)*(nz+1),1);
   if nargout>=11
      Dwa{s}= sparse([ii;ib],1,1,(nx+1)*(ny+1)*(nz+1),1);
   end
end
% finish Dw
Dtotal= sparse((nx+1)*(ny+1)*(nz+1),1);
for s= 1:Ns
      Dtotal= Dtotal+Dw{s};
end
for s= 1:Ns
     Dw{s}= Dw{s}./Dtotal;
end
% finish Dc
Dtotal= sparse((nx+1)*(ny+1)*(nz+1),1);
for s= 1:Ns
      Dtotal= Dtotal+Dc{s};
end
for s= 1:Ns
     Dc{s}= Dc{s}./Dtotal;
end
% for the intermediate result - global plane waves
% directions of plane waves in spherical coordinates
theta= linspace(0,pi/2,ntheta+2); theta= theta(2:ntheta+1);
phi= linspace(-pi,pi,nphi+1);  phi= phi(2:nphi+1); phi0= phi;
[theta,phi]= ndgrid(theta,phi); theta= theta(:)'; phi= phi(:)';
if ntheta>-1
    theta= [theta, -theta, zeros(1,nphi), pi/2, -pi/2];
    phi= [phi, pi+phi, phi0, 0, 0];  
    numplw= 2*ntheta*nphi+nphi+2;
else
    numplw= 0;
    theta= [];
    phi= [];
end
if numplw~=0
    if isa(k2,'function_handle') || isa(k2,'inline')
       plw= exp(1i*(coordinates.*repmat(sqrt(k2(coordinates)),1,3))* ...
               [cos(theta).*cos(phi);cos(theta).*sin(phi);sin(theta)]);
%          plw= exp(1i*(coordinates*2*pi/3000)* ...
%                [cos(theta).*cos(phi);cos(theta).*sin(phi);sin(theta)]);
    elseif isa(k2,'float')
       plw= exp(1i*(coordinates*sqrt(k2))* ...
               [cos(theta).*cos(phi);cos(theta).*sin(phi);sin(theta)]);
    end
    for s= 1:Ns
       % for Rwp{s} 
       [sx,sy]= ind2sub([Nx,Ny*Nz],s); % 3d position of this subdomain
       [sy,sz]= ind2sub([Ny,Nz],sy); 
       ix= ((sx-1)*snx-(sx>1)*(wl-1)+1):(sx*snx+(sx<Nx)*(wl-1)+1); % 3d position 
       iy= ((sy-1)*sny-(sy>1)*(wl-1)+1):(sy*sny+(sy<Ny)*(wl-1)+1); % of global nodes
       iz= ((sz-1)*snz-(sz>1)*(wl-1)+1):(sz*snz+(sz<Nz)*(wl-1)+1);
       [IX,IY,IZ]= ndgrid(ix,iy,iz); 
       snodes{s}= sub2ind([nx+1,ny+1,nz+1],IX(:),IY(:),IZ(:));
       snodes{s}= setdiff(snodes{s},sdiri{s});  % problem: sdiri{s} is for 'ol' not 'wl-1' 
       Rw= sparse(snodes{s},1,1,(nx+1)*(ny+1)*(nz+1),1);
       Rwp{s}= repmat(Rw,1,numplw).*plw;
       Rwp{s}= Rwp{s}.';
       % for Rcp{s}
       Rcp{s}= repmat(Dc{s},1,numplw).*plw;
       Rcp{s}= Rcp{s}.';
    end
else
    for s= 1:Ns
         Rwp{s}= sparse((nx+1)*(ny+1)*(nz+1),0);
         Rcp{s}= Rwp{s};
    end
end
if nargout < 10
    return;
end
% for Ri 
Ri= cell(Ns,1); inodes= cell(Ns,1);
for s= 1:Ns
    [sx,sy]= ind2sub([Nx,Ny*Nz],s); % 3d position of this subdomain
    [sy,sz]= ind2sub([Ny,Nz],sy);  
    ix= ((sx-1)*snx+(sx>1)*wl+1):(sx*snx-(sx<Nx)*wl+1); % 3d position 
    iy= ((sy-1)*sny+(sy>1)*wl+1):(sy*sny-(sy<Ny)*wl+1); % of global nodes
    iz= ((sz-1)*snz+(sz>1)*wl+1):(sz*snz-(sz<Nz)*wl+1);
    [IX,IY,IZ]= ndgrid(ix,iy,iz); 
    inodes{s}= sub2ind([nx+1,ny+1,nz+1],IX(:),IY(:),IZ(:));
    inodes{s}= setdiff(inodes{s},sdiri{s});  % problem: sdiri is for 'ol' not for 'wl'
    Ri{s}= sparse(1:length(inodes{s}),inodes{s},1,length(inodes{s}), ...
           (nx+1)*(ny+1)*(nz+1),length(inodes{s}));
end
% finish Dwa
if nargout<11
    return;
end
Dtotal= sparse((nx+1)*(ny+1)*(nz+1),1);
for s= 1:Ns
      Dtotal= Dtotal+Dwa{s};
end
for s= 1:Ns
     Dwa{s}= Dwa{s}./Dtotal;
end
%% for the subdomain 0
if nargout<12
    return;
end
% find elements in the subdomain 0
ix= zl+1:nx-zl;
iy= zl+1:ny-zl;
iz= zl+1:nz-zl;
iix= zeros(nx,1); iiy= zeros(ny,1); iiz= zeros(nz,1);
iix(ix)= mod(ix,snx)<zl+1 | mod(ix,snx)>snx-zl;
iiy(iy)= mod(iy,sny)<zl+1 | mod(iy,sny)>sny-zl;
iiz(iz)= mod(iz,snz)<zl+1 | mod(iz,snz)>snz-zl;
[IIX,~,~]= ndgrid(iix,zeros(ny,1),zeros(nz,1));
[~,IIY,~]= ndgrid(zeros(nx,1),iiy,zeros(nz,1));
[~,~,IIZ]= ndgrid(zeros(nx,1),zeros(ny,1),iiz);
II= IIX(:) | IIY(:) | IIZ(:);
elems0= 1:nx*ny*nz;
elems0= elems0(II);
% find all nodes in the subdomain 0, to construct R0 
ix= zl+2:nx-zl;
iy= zl+2:ny-zl;
iz= zl+2:nz-zl;
iix= zeros(nx+1,1); iiy= zeros(ny+1,1); iiz= zeros(nz+1,1);
iix(ix)= mod(ix-1,snx)<zl+1 | mod(ix-1,snx)>snx-zl-1;
iiy(iy)= mod(iy-1,sny)<zl+1 | mod(iy-1,sny)>sny-zl-1;
iiz(iz)= mod(iz-1,snz)<zl+1 | mod(iz-1,snz)>snz-zl-1;
[IIX,~,~]= ndgrid(iix,zeros(ny+1,1),zeros(nz+1,1));
[~,IIY,~]= ndgrid(zeros(nx+1,1),iiy,zeros(nz+1,1));
[~,~,IIZ]= ndgrid(zeros(nx+1,1),zeros(ny+1,1),iiz);
II= IIX(:) | IIY(:) | IIZ(:);
node0= 1:(nx+1)*(ny+1)*(nz+1);
node0= node0(II);
% find dirichlet nodes in the subdomain 0
numdiri0= 0;
if find(GammaD==1,1)
    numdiri0= numdiri0 + (2*zl+1)*((Ny-1)*(nz+1)+(Nz-1)*(ny+1))-(Ny-1)*(Nz-1)*(2*zl+1)^2;
end
if find(GammaD==2,1)
    numdiri0= numdiri0 + (2*zl+1)*((Ny-1)*(nz+1)+(Nz-1)*(ny+1))-(Ny-1)*(Nz-1)*(2*zl+1)^2;
end
if find(GammaD==3,1)
    numdiri0= numdiri0 + (2*zl+1)*((Nx-1)*(nz+1)+(Nz-1)*(nx+1))-(Nx-1)*(Nz-1)*(2*zl+1)^2;
end
if find(GammaD==4,1)
    numdiri0= numdiri0 + (2*zl+1)*((Nx-1)*(nz+1)+(Nz-1)*(nx+1))-(Nx-1)*(Nz-1)*(2*zl+1)^2;
end
if find(GammaD==5,1)
    numdiri0= numdiri0 + (2*zl+1)*((Nx-1)*(ny+1)+(Ny-1)*(nx+1))-(Nx-1)*(Ny-1)*(2*zl+1)^2;
end
if find(GammaD==6,1)
    numdiri0= numdiri0 + (2*zl+1)*((Nx-1)*(ny+1)+(Ny-1)*(nx+1))-(Nx-1)*(Ny-1)*(2*zl+1)^2;
end
diri0= zeros(numdiri0,1);  numdiri0 =0;
if find(GammaD==1,1)
    iy= zl+2:ny-zl;
    iiy= mod(iy-1,sny)<zl+1 | mod(iy-1,sny)>sny-zl-1;
    iy= iy(iiy);
    [IY,IZ]= ndgrid(iy,1:nz+1);
    II1= sub2ind([nx+1,ny+1,nz+1],1,IY(:),IZ(:));
    iz= zl+2:nz-zl;
    iiz= mod(iz-1,snz)<zl+1 | mod(iz-1,snz)>snz-zl-1;
    iz= iz(iiz);
    [IY,IZ]= ndgrid(1:ny+1,iz);
    II2= sub2ind([nx+1,ny+1,nz+1],1,IY(:),IZ(:));
    localnumdiri0= (2*zl+1)*((Ny-1)*(nz+1)+(Nz-1)*(ny+1))-(Ny-1)*(Nz-1)*(2*zl+1)^2;
    diri0(numdiri0+1:numdiri0+localnumdiri0)= unique([II1,II2]);
    numdiri0= numdiri0 + localnumdiri0;
end
if find(GammaD==2,1)
    iy= zl+2:ny-zl;
    iiy= mod(iy-1,sny)<zl+1 | mod(iy-1,sny)>sny-zl-1;
    iy= iy(iiy);
    [IY,IZ]= ndgrid(iy,1:nz+1);
    II1= sub2ind([nx+1,ny+1,nz+1],nx+1,IY(:),IZ(:));
    iz= zl+2:nz-zl;
    iiz= mod(iz-1,snz)<zl+1 | mod(iz-1,snz)>snz-zl-1;
    iz= iz(iiz);
    [IY,IZ]= ndgrid(1:ny+1,iz);
    II2= sub2ind([nx+1,ny+1,nz+1],nx+1,IY(:),IZ(:));
    localnumdiri0= (2*zl+1)*((Ny-1)*(nz+1)+(Nz-1)*(ny+1))-(Ny-1)*(Nz-1)*(2*zl+1)^2;
    diri0(numdiri0+1:numdiri0+localnumdiri0)= unique([II1,II2]);
    numdiri0= numdiri0 + localnumdiri0;
end
if find(GammaD==3,1)
    ix= zl+2:nx-zl;
    iix= mod(ix-1,snx)<zl+1 | mod(ix-1,snx)>snx-zl-1;
    ix= ix(iix);
    [IX,IZ]= ndgrid(ix,1:nz+1);
    II1= sub2ind([nx+1,ny+1,nz+1],IX(:),1,IZ(:));
    iz= zl+2:nz-zl;
    iiz= mod(iz-1,snz)<zl+1 | mod(iz-1,snz)>snz-zl-1;
    iz= iz(iiz);
    [IX,IZ]= ndgrid(1:nx+1,iz);
    II2= sub2ind([nx+1,ny+1,nz+1],IX(:),1,IZ(:));
    localnumdiri0= (2*zl+1)*((Nx-1)*(nz+1)+(Nz-1)*(nx+1))-(Nx-1)*(Nz-1)*(2*zl+1)^2;
    diri0(numdiri0+1:numdiri0+localnumdiri0)= unique([II1,II2]);
    numdiri0= numdiri0 + localnumdiri0;
end
if find(GammaD==4,1)
    ix= zl+2:nx-zl;
    iix= mod(ix-1,snx)<zl+1 | mod(ix-1,snx)>snx-zl-1;
    ix= ix(iix);
    [IX,IZ]= ndgrid(ix,1:nz+1);
    II1= sub2ind([nx+1,ny+1,nz+1],IX(:),ny+1,IZ(:));
    iz= zl+2:nz-zl;
    iiz= mod(iz-1,snz)<zl+1 | mod(iz-1,snz)>snz-zl-1;
    iz= iz(iiz);
    [IX,IZ]= ndgrid(1:nx+1,iz);
    II2= sub2ind([nx+1,ny+1,nz+1],IX(:),ny+1,IZ(:));
    localnumdiri0= (2*zl+1)*((Nx-1)*(nz+1)+(Nz-1)*(nx+1))-(Nx-1)*(Nz-1)*(2*zl+1)^2;
    diri0(numdiri0+1:numdiri0+localnumdiri0)= unique([II1,II2]);
    numdiri0= numdiri0 + localnumdiri0;
end
if find(GammaD==5,1)
    ix= zl+2:nx-zl;
    iix= mod(ix-1,snx)<zl+1 | mod(ix-1,snx)>snx-zl-1;
    ix= ix(iix);
    [IX,IY]= ndgrid(ix,1:ny+1);
    II1= sub2ind([nx+1,ny+1,nz+1],IX(:),IY(:),1);
    iy= zl+2:ny-zl;
    iiy= mod(iy-1,sny)<zl+1 | mod(iy-1,sny)>sny-zl-1;
    iy= iy(iiy);
    [IX,IY]= ndgrid(1:nx+1,iy);
    II2= sub2ind([nx+1,ny+1,nz+1],IX(:),IY(:),1);
    localnumdiri0= (2*zl+1)*((Nx-1)*(ny+1)+(Ny-1)*(nx+1))-(Nx-1)*(Ny-1)*(2*zl+1)^2;
    diri0(numdiri0+1:numdiri0+localnumdiri0)= unique([II1,II2]);
    numdiri0= numdiri0 + localnumdiri0;
end
if find(GammaD==6,1)
    ix= zl+2:nx-zl;
    iix= mod(ix-1,snx)<zl+1 | mod(ix-1,snx)>snx-zl-1;
    ix= ix(iix);
    [IX,IY]= ndgrid(ix,1:ny+1);
    II1= sub2ind([nx+1,ny+1,nz+1],IX(:),IY(:),nz+1);
    iy= zl+2:ny-zl;
    iiy= mod(iy-1,sny)<zl+1 | mod(iy-1,sny)>sny-zl-1;
    iy= iy(iiy);
    [IX,IY]= ndgrid(1:nx+1,iy);
    II2= sub2ind([nx+1,ny+1,nz+1],IX(:),IY(:),nz+1);
    localnumdiri0= (2*zl+1)*((Nx-1)*(ny+1)+(Ny-1)*(nx+1))-(Nx-1)*(Ny-1)*(2*zl+1)^2;
    diri0(numdiri0+1:numdiri0+localnumdiri0)= unique([II1,II2]);
end
% for R0
node0= setdiff(node0,diri0); nnode0= length(node0);
R0= sparse(1:nnode0,node0,1,nnode0,(nx+1)*(ny+1)*(nz+1),nnode0);
% find robinelems0 
numrobin0= 0;
if find(GammaR==1,1)
    numrobin0= numrobin0 + 2*zl*((Ny-1)*nz+(Nz-1)*ny)-(Ny-1)*(Nz-1)*4*zl^2;
end
if find(GammaR==2,1)
    numrobin0= numrobin0 + 2*zl*((Ny-1)*nz+(Nz-1)*ny)-(Ny-1)*(Nz-1)*4*zl^2;
end
if find(GammaR==3,1)
    numrobin0= numrobin0 + 2*zl*((Nx-1)*nz+(Nz-1)*nx)-(Nx-1)*(Nz-1)*4*zl^2;
end
if find(GammaR==4,1)
    numrobin0= numrobin0 + 2*zl*((Nx-1)*nz+(Nz-1)*nx)-(Nx-1)*(Nz-1)*4*zl^2;
end
if find(GammaR==5,1)
    numrobin0= numrobin0 + 2*zl*((Nx-1)*ny+(Ny-1)*nx)-(Nx-1)*(Ny-1)*4*zl^2;
end
if find(GammaR==6,1)
    numrobin0= numrobin0 + 2*zl*((Nx-1)*ny+(Ny-1)*nx)-(Nx-1)*(Ny-1)*4*zl^2;
end
robinelems0= zeros(numrobin0,1); numrobin0= 0; 
numrobin= 0;
if find(GammaR==1,1)
    iy= zl+1:ny-zl;
    iiy= mod(iy,sny)<zl+1 | mod(iy,sny)>sny-zl;
    iy= iy(iiy);
    [IY,IZ]= ndgrid(iy,1:nz);
    II1= sub2ind([ny,nz],IY(:),IZ(:));
    iz= zl+1:nz-zl;
    iiz= mod(iz,snz)<zl+1 | mod(iz,snz)>snz-zl;
    iz= iz(iiz);
    [IY,IZ]= ndgrid(1:ny,iz);
    II2= sub2ind([ny,nz],IY(:),IZ(:));
    localnumrobin0= 2*zl*((Ny-1)*nz+(Nz-1)*ny)-(Ny-1)*(Nz-1)*4*zl^2;
    robinelems0(numrobin0+1:numrobin0+localnumrobin0)= unique([II1,II2])+numrobin;
    numrobin0= numrobin0 + localnumrobin0;
    numrobin= numrobin + ny*nz; 
end
if find(GammaR==2,1)
    iy= zl+1:ny-zl;
    iiy= mod(iy,sny)<zl+1 | mod(iy,sny)>sny-zl;
    iy= iy(iiy);
    [IY,IZ]= ndgrid(iy,1:nz);
    II1= sub2ind([ny,nz],IY(:),IZ(:));
    iz= zl+1:nz-zl;
    iiz= mod(iz,snz)<zl+1 | mod(iz,snz)>snz-zl;
    iz= iz(iiz);
    [IY,IZ]= ndgrid(1:ny,iz);
    II2= sub2ind([ny,nz],IY(:),IZ(:));
    localnumrobin0= 2*zl*((Ny-1)*nz+(Nz-1)*ny)-(Ny-1)*(Nz-1)*4*zl^2;
    robinelems0(numrobin0+1:numrobin0+localnumrobin0)= unique([II1,II2])+numrobin;
    numrobin0= numrobin0 + localnumrobin0;
    numrobin= numrobin + ny*nz; 
end
if find(GammaR==3,1)
    ix= zl+1:nx-zl;
    iix= mod(ix,snx)<zl+1 | mod(ix,snx)>snx-zl;
    ix= ix(iix);
    [IX,IZ]= ndgrid(ix,1:nz);
    II1= sub2ind([nx,nz],IX(:),IZ(:));
    iz= zl+1:nz-zl;
    iiz= mod(iz,snz)<zl+1 | mod(iz,snz)>snz-zl;
    iz= iz(iiz);
    [IX,IZ]= ndgrid(1:nx,iz);
    II2= sub2ind([nx,nz],IX(:),IZ(:));
    localnumrobin0= 2*zl*((Nx-1)*nz+(Nz-1)*nx)-(Nx-1)*(Nz-1)*4*zl^2;
    robinelems0(numrobin0+1:numrobin0+localnumrobin0)= unique([II1,II2])+numrobin;
    numrobin0= numrobin0 + localnumrobin0;
    numrobin= numrobin + nx*nz; 
end
if find(GammaR==4,1)
    ix= zl+1:nx-zl;
    iix= mod(ix,snx)<zl+1 | mod(ix,snx)>snx-zl;
    ix= ix(iix);
    [IX,IZ]= ndgrid(ix,1:nz);
    II1= sub2ind([nx,nz],IX(:),IZ(:));
    iz= zl+1:nz-zl;
    iiz= mod(iz,snz)<zl+1 | mod(iz,snz)>snz-zl;
    iz= iz(iiz);
    [IX,IZ]= ndgrid(1:nx,iz);
    II2= sub2ind([nx,nz],IX(:),IZ(:));
    localnumrobin0= 2*zl*((Nx-1)*nz+(Nz-1)*nx)-(Nx-1)*(Nz-1)*4*zl^2;
    robinelems0(numrobin0+1:numrobin0+localnumrobin0)= unique([II1,II2])+numrobin;
    numrobin0= numrobin0 + localnumrobin0;
    numrobin= numrobin + nx*nz; 
end
if find(GammaR==5,1)
    ix= zl+1:nx-zl;
    iix= mod(ix,snx)<zl+1 | mod(ix,snx)>snx-zl;
    ix= ix(iix);
    [IX,IY]= ndgrid(ix,1:ny);
    II1= sub2ind([nx,ny],IX(:),IY(:));
    iy= zl+1:ny-zl;
    iiy= mod(iy,sny)<zl+1 | mod(iy,sny)>sny-zl;
    iy= iy(iiy);
    [IX,IY]= ndgrid(1:nx,iy);
    II2= sub2ind([nx,ny],IX(:),IY(:));
    localnumrobin0= 2*zl*((Nx-1)*ny+(Ny-1)*nx)-(Nx-1)*(Ny-1)*4*zl^2;
    robinelems0(numrobin0+1:numrobin0+localnumrobin0)= unique([II1,II2])+numrobin;
    numrobin0= numrobin0 + localnumrobin0;
    numrobin= numrobin + nx*ny; 
end
if find(GammaR==6,1)
    ix= zl+1:nx-zl;
    iix= mod(ix,snx)<zl+1 | mod(ix,snx)>snx-zl;
    ix= ix(iix);
    [IX,IY]= ndgrid(ix,1:ny);
    II1= sub2ind([nx,ny],IX(:),IY(:));
    iy= zl+1:ny-zl;
    iiy= mod(iy,sny)<zl+1 | mod(iy,sny)>sny-zl;
    iy= iy(iiy);
    [IX,IY]= ndgrid(1:nx,iy);
    II2= sub2ind([nx,ny],IX(:),IY(:));
    localnumrobin0= 2*zl*((Nx-1)*ny+(Ny-1)*nx)-(Nx-1)*(Ny-1)*4*zl^2;
    robinelems0(numrobin0+1:numrobin0+localnumrobin0)= unique([II1,II2])+numrobin; 
end
% for interior regions with no overlapping
ielems= cell(Ns,1); ibdelems= cell(Ns,1); idirichlet= cell(Ns,1);
irobinelems= cell(Ns,1); Rz= cell(Ns,1);
for s= 1:Ns
   % for ielems
   [sx,sy]= ind2sub([Nx,Ny*Nz],s); % 3d position of this subdomain
   [sy,sz]= ind2sub([Ny,Nz],sy); 
   ix= ((sx-1)*snx+(sx>1)*zl+1):(sx*snx-(sx<Nx)*zl); % 3d position 
   iy= ((sy-1)*sny+(sy>1)*zl+1):(sy*sny-(sy<Ny)*zl); % of global elements
   iz= ((sz-1)*snz+(sz>1)*zl+1):(sz*snz-(sz<Nz)*zl);
   [IX,IY,IZ]= ndgrid(ix,iy,iz); 
   ielems{s}= sub2ind([nx,ny,nz],IX(:),IY(:),IZ(:));
   nxe= length(ix); nye= length(iy); nze= length(iz);
   % for ibdelems, idirichlet, irobinelems
   [bdnodes,bdelems]= qubebdmesh(nxe,nye,nze);
   num_sbdelems= 0; num_sdiri= 0; num_robinelems= 0;
   if sx>1
       num_sbdelems= num_sbdelems + nye*nze;
   elseif find(1==GammaD,1)
       num_sdiri= num_sdiri + (nye+1)*(nze+1);
   elseif find(1==GammaR,1)
       num_robinelems= num_robinelems + nye*nze;
   else
       disp('error input: face 1 is not in either GammaD or GammaR');
   end
   if sx<Nx
       num_sbdelems= num_sbdelems + nye*nze;
   elseif find(2==GammaD,1)
       num_sdiri= num_sdiri + (nye+1)*(nze+1);
   elseif find(2==GammaR,1)
       num_robinelems= num_robinelems + nye*nze;
   else
       disp('error input: face 2 is not in either GammaD or GammaR');
   end
   if sy>1
       num_sbdelems= num_sbdelems + nxe*nze;
   elseif find(3==GammaD,1)
       num_sdiri= num_sdiri + (nxe+1)*(nze+1);
   elseif find(3==GammaR,1)
       num_robinelems= num_robinelems + nxe*nze;
   else
       disp('error input: face 3 is not in either GammaD or GammaR');
   end
   if sy<Ny
       num_sbdelems= num_sbdelems + nxe*nze;
   elseif find(4==GammaD,1)
       num_sdiri= num_sdiri + (nxe+1)*(nze+1);
   elseif find(4==GammaR,1)
       num_robinelems= num_robinelems + nxe*nze;
   else
       disp('error input: face 4 is not in either GammaD or GammaR');    
   end
   if sz>1
       num_sbdelems= num_sbdelems + nxe*nye;
   elseif find(5==GammaD,1)
       num_sdiri= num_sdiri + (nxe+1)*(nye+1);
   elseif find(5==GammaR,1)
       num_robinelems= num_robinelems + nxe*nye;
   else
       disp('error input: face 5 is not in either GammaD or GammaR');    
   end
   if sz<Nz
       num_sbdelems= num_sbdelems + nxe*nye;
   elseif find(6==GammaD,1)
       num_sdiri= num_sdiri + (nxe+1)*(nye+1);
   elseif find(6==GammaR,1)
       num_robinelems= num_robinelems + nxe*nye;
   else
       disp('error input: face 6 is not in either GammaD or GammaR'); 
   end
   ibdelems{s}= zeros(num_sbdelems,4);
   idirichlet{s}= zeros(num_sdiri,1);  
   irobinelems{s}= zeros(num_robinelems,1);
   num_sbdelems= 0; num_sdiri= 0; num_robinelems= 0; num_grobin= 0;
   ix= ((sx-1)*snx+(sx>1)*zl+1):(sx*snx-(sx<Nx)*zl+1); % 3d position 
   iy= ((sy-1)*sny+(sy>1)*zl+1):(sy*sny-(sy<Ny)*zl+1); % of global nodes
   iz= ((sz-1)*snz+(sz>1)*zl+1):(sz*snz-(sz<Nz)*zl+1);
   [IX,IY,IZ]= ndgrid(ix,iy,iz); 
   inodes{s}= sub2ind([nx+1,ny+1,nz+1],IX(:),IY(:),IZ(:));
   if sx>1
       ibdelems{s}(num_sbdelems+1:num_sbdelems+nye*nze,:)= inodes{s}(bdelems{1});
       num_sbdelems= num_sbdelems + nye*nze;
   elseif find(1==GammaD,1)
       idirichlet{s}(num_sdiri+1:num_sdiri+(nye+1)*(nze+1))= inodes{s}(bdnodes{1});
       num_sdiri= num_sdiri + (nye+1)*(nze+1);
   elseif find(1==GammaR,1)
       iy= ((sy-1)*sny+(sy>1)*zl+1):(sy*sny-(sy<Ny)*zl); % 2d position
       iz= ((sz-1)*snz+(sz>1)*zl+1):(sz*snz-(sz<Nz)*zl); % on face 1
       [IY,IZ]= ndgrid(iy,iz); 
       irobinelems{s}(num_robinelems+1:num_robinelems+nye*nze)= ...
           sub2ind([ny,nz],IY(:),IZ(:));
       num_robinelems= num_robinelems + nye*nze;
   end
   if find(1==GammaR,1)
      num_grobin= num_grobin + ny*nz;
   end
   if sx<Nx
       ibdelems{s}(num_sbdelems+1:num_sbdelems+nye*nze,:)= inodes{s}(bdelems{2});
       num_sbdelems= num_sbdelems + nye*nze;   
   elseif find(2==GammaD,1)
       idirichlet{s}(num_sdiri+1:num_sdiri+(nye+1)*(nze+1))= inodes{s}(bdnodes{2});
       num_sdiri= num_sdiri + (nye+1)*(nze+1);
   elseif find(2==GammaR,1)
       iy= ((sy-1)*sny+(sy>1)*zl+1):(sy*sny-(sy<Ny)*zl); % 2d position
       iz= ((sz-1)*snz+(sz>1)*zl+1):(sz*snz-(sz<Nz)*zl); % on face 2
       [IY,IZ]= ndgrid(iy,iz); 
       irobinelems{s}(num_robinelems+1:num_robinelems+nye*nze)= ...
           sub2ind([ny,nz],IY(:),IZ(:)) + num_grobin;
       num_robinelems= num_robinelems + nye*nze;
   end
   if find(2==GammaR,1)
       num_grobin= num_grobin + ny*nz;
   end
   if sy>1
       ibdelems{s}(num_sbdelems+1:num_sbdelems+nxe*nze,:)= inodes{s}(bdelems{3});
       num_sbdelems= num_sbdelems + nxe*nze;   
   elseif find(3==GammaD,1)
       idirichlet{s}(num_sdiri+1:num_sdiri+(nxe+1)*(nze+1))= inodes{s}(bdnodes{3});
       num_sdiri= num_sdiri + (nxe+1)*(nze+1);
   elseif find(3==GammaR,1)
       ix= ((sx-1)*snx+(sx>1)*zl+1):(sx*snx-(sx<Nx)*zl); % 2d position  
       iz= ((sz-1)*snz+(sz>1)*zl+1):(sz*snz-(sz<Nz)*zl); % on face 3
       [IX,IZ]= ndgrid(ix,iz); 
       irobinelems{s}(num_robinelems+1:num_robinelems+nxe*nze)= ...
           sub2ind([nx,nz],IX(:),IZ(:)) + num_grobin;
       num_robinelems= num_robinelems + nxe*nze;
   end
   if find(3==GammaR,1)
       num_grobin= num_grobin + nx*nz;
   end
   if sy<Ny
       ibdelems{s}(num_sbdelems+1:num_sbdelems+nxe*nze,:)= inodes{s}(bdelems{4});
       num_sbdelems= num_sbdelems + nxe*nze;   
   elseif find(4==GammaD,1)
       idirichlet{s}(num_sdiri+1:num_sdiri+(nxe+1)*(nze+1))= inodes{s}(bdnodes{4});
       num_sdiri= num_sdiri + (nxe+1)*(nze+1);
   elseif find(4==GammaR,1)
       ix= ((sx-1)*snx+(sx>1)*zl+1):(sx*snx-(sx<Nx)*zl); % 2d position  
       iz= ((sz-1)*snz+(sz>1)*zl+1):(sz*snz-(sz<Nz)*zl); % on face 4
       [IX,IZ]= ndgrid(ix,iz); 
       irobinelems{s}(num_robinelems+1:num_robinelems+nxe*nze)= ...
           sub2ind([nx,nz],IX(:),IZ(:)) + num_grobin;
       num_robinelems= num_robinelems + nxe*nze;
   end
   if find(4==GammaR,1)
       num_grobin= num_grobin + nx*nz;
   end
   if sz>1
       ibdelems{s}(num_sbdelems+1:num_sbdelems+nye*nxe,:)= inodes{s}(bdelems{5});
       num_sbdelems= num_sbdelems + nxe*nye;   
   elseif find(5==GammaD,1)
       idirichlet{s}(num_sdiri+1:num_sdiri+(nxe+1)*(nye+1))= inodes{s}(bdnodes{5});
       num_sdiri= num_sdiri + (nxe+1)*(nye+1);
   elseif find(5==GammaR,1)
       ix= ((sx-1)*snx+(sx>1)*zl+1):(sx*snx-(sx<Nx)*zl); % 2d position 
       iy= ((sy-1)*sny+(sy>1)*zl+1):(sy*sny-(sy<Ny)*zl); % on face 5
       [IX,IY]= ndgrid(ix,iy); 
       irobinelems{s}(num_robinelems+1:num_robinelems+nxe*nye)= ...
           sub2ind([nx,ny],IX(:),IY(:)) + num_grobin;
       num_robinelems= num_robinelems + nxe*nye;
   end
   if find(5==GammaR,1)
       num_grobin= num_grobin + nx*ny;
   end
   if sz<Nz
       ibdelems{s}(num_sbdelems+1:num_sbdelems+nye*nxe,:)= inodes{s}(bdelems{6});
   elseif find(6==GammaD,1)
       idirichlet{s}(num_sdiri+1:num_sdiri+(nxe+1)*(nye+1))= inodes{s}(bdnodes{6});
   elseif find(6==GammaR,1)
       ix= ((sx-1)*snx+(sx>1)*zl+1):(sx*snx-(sx<Nx)*zl); % 2d position 
       iy= ((sy-1)*sny+(sy>1)*zl+1):(sy*sny-(sy<Ny)*zl); % on face 6
       [IX,IY]= ndgrid(ix,iy); 
       irobinelems{s}(num_robinelems+1:num_robinelems+nxe*nye)= ...
           sub2ind([nx,ny],IX(:),IY(:)) + num_grobin;
   end
   % for Rz
   inodes{s}= setdiff(inodes{s},idirichlet{s}); nnodes= length(inodes{s});
   Rz{s}= sparse(1:nnodes,inodes{s},1,nnodes,(nx+1)*(ny+1)*(nz+1),nnodes);
end

end