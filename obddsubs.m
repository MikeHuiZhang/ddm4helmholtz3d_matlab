function [u,flag,relres,iter,resvec]= ...
    obddsubs(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR, ...
           Nx,Ny,Nz,ol,nthetav,nphiv,neumann,wl)
%obddsubs: the substructured form of obdd.m
% no partition of unit, the communication relation is decomposed to
% faces, edges and vertices

%% partition of the global mesh into overlapping submeshes
tsetup= tic;
fprintf('\npartition\n');
tpart= tic;
[coordinates,elements,~,robin]= qubemesh(xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR);
[Ro,sdiri,~,sbdelems,srobinelems,selems,sbdpos]= qubesubmesh(GammaD,GammaR,nx,ny,nz,Nx,Ny,Nz,ol,1);
[sfnodes,sendf,sfsend,ngf,senodes,sende,sesend,nge,...
    svnodes,sendv,svsend,ngv, sfelems,seelems,svelems]= ...
    qubecommedge(GammaD,nx,ny,nz,Nx,Ny,Nz,ol);
disp('size of interface variables'); disp(ngf+nge+ngv);
Ns= Nx*Ny*Nz; nn= (nx+1)*(ny+1)*(nz+1);
hx= (xr-xl)/nx; hy= (yr-yl)/ny; hz= (zr-zl)/nz;
NumCommEdges= 0;  ngb= ngf + nge + ngv;
% partition of unity for recovering u
if ~exist('wl','var')
    wl= ol+1;
end
[~,sdiriw]= qubesubmesh(GammaD,GammaR,nx,ny,nz,Nx,Ny,Nz,wl);
Dw= qubepartunit(nx,ny,nz,Nx,Ny,Nz,wl,sdiriw,1);
clear sdiriw;
toc(tpart);

%% calculate elements stiffness and loads
fprintf('\nelements stiffness and loads\n');
tag= tic;
getAM(); getgpw(); % for use by stima.m and rhs.m
getM2d(); getgpw2d(); % for use by stima2d and rhs2d
global unused_src numsrc; % for use by rhs.m
[I,J]= ndgrid(1:8,1:8); I= I(:); J= J(:);
[Ir,Jr]= ndgrid(1:4,1:4); Ir= Ir(:); Jr= Jr(:);
ne= size(elements,1);
ii= zeros(64,ne); jj= zeros(64,ne); Aa= zeros(64,ne);
for j= 1:ne
    Ae= stima(coordinates(elements(j,:),:),k2);
    ii(:,j)= elements(j,I); % assemble vectors
    jj(:,j)= elements(j,J); % for speed
    Aa(:,j)= Ae(:);
end
iib= []; ba= [];
if ~(isa(f,'float') && 0==f && isempty(pointsource))
   iib= zeros(8,ne); ba= zeros(8,ne);
   for j = 1:ne
      n4e= elements(j,:);
      iib(:,j)= n4e;
      ba(:,j)= rhs(coordinates(n4e,:),f,pointsource);
   end
end
% treat outer Robin b.c.
nr= size(robin,1); 
ir= []; jr= []; Ar= [];
if ~(isa(a,'float') && 0==a)
   ir= zeros(16,nr); jr= zeros(16,nr); Ar= zeros(16,nr);
   for j = 1:nr
       ir(:,j)= robin(j,Ir);
       jr(:,j)= robin(j,Jr); 
       Ae= stima2d(coordinates(robin(j,:),:),a);
       Ar(:,j)= Ae(:);
   end
end
irb= []; br= [];
if ~(isa(uR,'float') && 0==uR)
   irb= zeros(4,nr); br= zeros(4,nr);
   for j = 1:nr
      n4e= robin(j,:);
      irb(:,j)= n4e;
      br(:,j)= rhs2d(coordinates(n4e,:),uR);
   end
end
clear robin;
unused_src= []; numsrc= [];
toc(tag);

%% assemble subdomains matrices and rhs
fprintf('\nassembly subdomains\n');
tas= tic;
A= cell(Ns,1);  b= cell(Ns,1); rhsl= zeros(ngf+nge+ngv,1); 
Aget= cell(Ns,1); Agive= cell(Ns,1);
for s= 1:Ns
    Agive{s}= sparse(ngf+nge+ngv,nn);
end
for s= 1:Ns
    % treat internal Robin b.c.
    if ~exist('neumann','var') || neumann==0      
        if isa(k2,'function_handle') || isa(k2,'inline')
           reg= @(x)(-sqrt(k2(x))*1i);
        elseif isa(k2,'float')
            reg= -sqrt(k2)*1i;
        end
        nb= size(sbdelems{s},1);
        ib= zeros(16,nb); jb= zeros(16,nb); Ab= zeros(16,nb);
        for j = 1:nb
            ib(:,j)= sbdelems{s}(j,Ir);
            jb(:,j)= sbdelems{s}(j,Jr);
            switch sbdpos{s}(j)
                case 1
                    trans= [hx/2 0 0; hx/2 0 0; hx/2 0 0; hx/2 0 0];
                case 2
                    trans= -[hx/2 0 0; hx/2 0 0; hx/2 0 0; hx/2 0 0];
                case 3
                    trans= [0 hy/2 0; 0 hy/2 0; 0 hy/2 0; 0 hy/2 0];
                case 4
                    trans= -[0 hy/2 0; 0 hy/2 0; 0 hy/2 0; 0 hy/2 0];
                case 5
                    trans= [0 0 hz/2; 0 0 hz/2; 0 0 hz/2; 0 0 hz/2];
                case 6
                    trans= -[0 0 hz/2; 0 0 hz/2; 0 0 hz/2; 0 0 hz/2];
            end
            Ae= stima2d(coordinates(sbdelems{s}(j,:),:)+trans,reg); 
            Ab(:,j)= Ae(:);
        end
    else
        ib= []; jb= []; Ab= [];
    end
    sbdelems{s}= [];
    % solid elements
    iis= ii(:,selems{s}); iis= iis(:);
    jjs= jj(:,selems{s}); jjs= jjs(:);
    Aas= Aa(:,selems{s}); Aas= Aas(:);
    iibs= []; bas= [];
    if ~(isa(f,'float') && 0==f && isempty(pointsource))
        iibs= iib(:,selems{s});
        bas= ba(:,selems{s});
    end
    selems{s}= [];
    % outer Robin
    if ~isempty(ir)
        irs= ir(:,srobinelems{s}); irs= irs(:);
        jrs= jr(:,srobinelems{s}); jrs= jrs(:);
        Ars= Ar(:,srobinelems{s}); Ars= Ars(:);
    else
        irs= [];
        jrs= [];
        Ars= [];
    end
    irbs= []; brs= [];
    if ~(isa(uR,'float') && 0==uR)
       irbs= irb(:,srobinelems{s}); 
       brs= br(:,srobinelems{s});
    end
    srobinelems{s}= [];
    % setup a global matrix and a rhs for the subdomain
    Ag= sparse([iis;irs;ib(:)],[jjs;jrs;jb(:)],[Aas;Ars;Ab(:)],nn,nn);
    clear iis jjs Aas irs jrs Ars ib jb Ab;
    bg= sparse([iibs(:);irbs(:)],1,[bas(:);brs(:)],nn,1);
    clear iibs bas irbs brs;
    % treatment of local Dirichlet
    if ~isempty(sdiri{s})
        if isa(uD,'float')
            bg= setsparse(bg,sdiri{s},ones(length(sdiri{s}),1),uD);
        elseif isa(uD,'inline') || isa(uD,'function_handle')
            bg= setsparse(bg,sdiri{s},ones(length(sdiri{s}),1),uD(coordinates(sdiri{s},:)));
        end
        [iid, jjd]= find(Ag);
        idxofzero = ismember(iid,sdiri{s}); 
        Ag= setsparse(Ag,iid(idxofzero),jjd(idxofzero),0);
        Ag= setsparse(Ag,sdiri{s},sdiri{s},1);
        clear iid jjd idxofzero;
    end 
    % restrict to the subdomain
    A{s}= Ro{s}*Ag*Ro{s}.';
    b{s}= Ro{s}*bg;
    % restrict to communication edges
    Aget{s}= sparse(size(Ro{s},1),ngf+nge+ngv);
    [sx,sy]= ind2sub([Nx,Ny*Nz],s); % 3d position of this subdomain
    [sy,sz]= ind2sub([Ny,Nz],sy);
    % - faces
    if 1==s
        sdisp= 0;
    else
        sdisp= sendf(s-1);
    end
    for m= 1:6
        if 1==m
            idx= 1:sfsend(1,s);
        else
            idx= sfsend(m-1,s)+1:sfsend(m,s);
        end
        Rf= sparse(1:length(idx),sfnodes{s}(idx),1,length(idx),nn);
        Ef= sparse(idx+sdisp,1:length(idx),1,ngb,length(idx));
        if ~isempty(Rf)
            NumCommEdges= NumCommEdges + 1;
            Aget{s}= Aget{s} + Ro{s}*(Rf.'*( Ef.' ));
            rhsl(idx+sdisp)= rhsl(idx+sdisp) - Rf*bg;
        end
        idx= double(unique(elements(sfelems{s,m},:)));
        Rfelems= sparse(1:length(idx),idx,1,length(idx),nn);
        if 1==m
           % - - left face
           if sx > 1
               s2= s-1;
           end
        elseif 2==m
           % - -right face
           if sx < Nx
               s2= s+1;
           end
        elseif 3==m
           % - - front face
           if sy > 1
               s2= s-Nx;
           end
        elseif 4==m
           % - - back face
           if sy < Ny
               s2= s+Nx;
           end
        elseif 5==m
            % - - bottom face
            if sz > 1
                s2= s-Nx*Ny;
            end
        elseif 6==m
            % - - top face
            if sz < Nz
                s2= s+Nx*Ny;
            end
        end
        if ~isempty(Ef)
            Agive{s2}= Agive{s2} + Ef*Rf*Ag*(Rfelems.')*Rfelems;
        end
        clear Rf Ef Rfelems;
    end
    % edges
    if 1==s
        sdisp= ngf;
    else
        sdisp= ngf + sende(s-1);
    end
    for m= 1:12
        if 1==m
            idx= 1:sesend(1,s);
        else
            idx= sesend(m-1,s)+1:sesend(m,s);
        end
        Re= sparse(1:length(idx),senodes{s}(idx),1,length(idx),nn);
        Ee= sparse(idx+sdisp,1:length(idx),1,ngb,length(idx));
        if ~isempty(Re)
            Aget{s}= Aget{s} + Ro{s}*(Re.'*( Ee.' ));
            NumCommEdges= NumCommEdges + 1;
            rhsl(idx+sdisp)= rhsl(idx+sdisp) - Re*bg;
        end
        idx= double(unique( elements(seelems{s,m},:) )); % unique resort
        Reelems= sparse(1:length(idx),idx,1,length(idx),nn);
        if 1==m
           % - - - - 1
           if sx>1 && sy>1
               s2= s-Nx-1;
           elseif 1==sx && sy>1
               s2= s-Nx;
           elseif sx>1 && sy==1
               s2= s-1;
           end
        elseif 2==m
           % - - - - 2
           if sx<Nx && sy>1
               s2= s-Nx+1;
           elseif sx==Nx && sy>1
               s2= s-Nx;
           elseif sx<Nx && sy==1
               s2= s+1;
           end
        elseif 3==m
           % - - - - 3
           if sx>1 && sy<Ny
               s2= s+Nx-1;
           elseif sx==1 && sy<Ny
               s2= s+Nx;
           elseif sx>1 && sy==Ny
               s2= s-1;
           end
        elseif 4==m
           % - - - - 4
           if sx<Nx && sy<Ny
               s2= s+Nx+1;
           elseif sx==Nx && sy<Ny  
               s2= s+Nx;
           elseif sx<Nx && sy==Ny
               s2= s+1;
           end
        elseif 5==m
           % - - - - 5
           if sx>1 && sz>1
               s2= s-Nx*Ny-1;
           elseif sx==1 && sz>1
               s2= s-Nx*Ny;
           elseif sx>1 && sz==1
               s2= s-1;
           end
        elseif 6==m
            % - - - - 6
            if sx<Nx && sz>1
               s2= s-Nx*Ny+1;
            elseif sx==Nx && sz>1
               s2= s-Nx*Ny;
            elseif sx<Nx && sz==1
               s2= s+1;
            end
        elseif 7==m
           % - - - - 7
           if sx>1 && sz<Nz
               s2= s+Nx*Ny-1;
           elseif sx==1 && sz<Nz
               s2= s+Nx*Ny;
           elseif sx>1 && sz==Nz
               s2= s-1;
           end
        elseif 8==m
           % - - - - 8
           if sx<Nx && sz<Nz
               s2= s+Nx*Ny+1;
           elseif sx==Nx && sz<Nz
               s2= s+Nx*Ny;
           elseif sx<Nx && sz==Nz
               s2= s+1;
           end
        elseif 9==m
           % - - - - 9
           if sy>1 && sz>1
               s2= s - Nx - Nx*Ny;
           elseif sy==1 && sz>1
               s2= s - Nx*Ny;
           elseif sy>1 && sz==1
               s2= s - Nx;
           end
        elseif 10==m
           % - - - - 10
           if sy<Ny && sz>1
               s2= s + Nx - Nx*Ny;
           elseif sy==Ny && sz>1
               s2= s - Nx*Ny;
           elseif sy<Ny && sz==1
               s2= s + Nx;
           end
        elseif 11==m
           % - - - - 11
           if sy>1 && sz<Nz
               s2= s - Nx + Nx*Ny;
           elseif sy==1 && sz<Nz
               s2= s + Nx*Ny;
           elseif sy>1 && sz==Nz
               s2= s - Nx;
           end
        elseif 12==m
           % - - - - 12
           if sy<Ny && sz<Nz
              s2= s + Nx + Nx*Ny;
           elseif sy==Ny && sz<Nz
              s2= s + Nx*Ny;
           elseif sy<Ny && sz==Nz
              s2= s + Nx;
           end
        end
        if ~isempty(Ee)
            Agive{s2}= Agive{s2} + Ee*Re*Ag*(Reelems.')*Reelems;
        end
        clear Re Ee Reelems;
    end
     % vertices
    if 1==s
        sdisp= ngf + nge;
    else
        sdisp= ngf + nge + sendv(s-1);
    end
    for m= 1:8
        if 1==m
            idx= 1:svsend(1,s);
        else
            idx= svsend(m-1,s)+1:svsend(m,s);
        end
        Rv= sparse(1:length(idx),svnodes{s}(idx),1,length(idx),nn);
        Ev= sparse(idx+sdisp,1:length(idx),1,ngb,length(idx));
        if ~isempty(Rv)
            Aget{s}= Aget{s} + Ro{s}*(Rv.'*( Ev.' ));
            NumCommEdges= NumCommEdges + 1;
            rhsl(idx+sdisp)= rhsl(idx+sdisp) - Rv*bg;
        end
        idx= double(unique( elements(svelems{s,m},:) ));
        Rvelems= sparse(1:length(idx),idx,1,length(idx),nn);
        if 1==m
            % - - - - 1
            if sx>1 && sy>1 && sz>1
                s2= s-1-Nx-Nx*Ny;
            elseif sx==1 && sy>1 && sz>1
                s2= s-Nx-Nx*Ny;
            elseif sx==1 && sy==1 && sz>1
                s2= s-Nx*Ny;
            elseif sx==1 && sy>1 && sz==1
                s2= s-Nx;
            elseif sx>1 && sy==1 && sz>1
                s2= s-1-Nx*Ny;
            elseif sx>1 && sy==1 && sz==1
                s2= s-1;
            elseif sx>1 && sy>1 && sz==1
                s2= s-1-Nx;
            end
        elseif 2==m
            % - - - - 2
            if sx<Nx && sy>1 && sz>1
                s2= s + 1 - Nx - Nx*Ny;
            elseif sx==Nx && sy>1 && sz>1
                s2= s - Nx - Nx*Ny;
            elseif sx==Nx && sy==1 && sz>1
                s2= s - Nx*Ny;
            elseif sx==Nx && sy>1 && sz==1
                s2= s - Nx;
            elseif sx<Nx && sy==1 && sz>1
                s2= s + 1 - Nx*Ny;
            elseif sx<Nx && sy==1 && sz==1
                s2= s + 1;
            elseif sx<Nx && sy>1 && sz==1
                s2= s + 1 - Nx;
            end
        elseif 3==m
            % - - - - 3
            if sx<Nx && sy<Ny && sz>1
                s2= s + 1 + Nx - Nx*Ny;
            elseif sx==Nx && sy<Ny && sz>1
                s2= s + Nx - Nx*Ny;
            elseif sx==Nx && sy==Ny && sz>1
                s2= s - Nx*Ny;
            elseif sx==Nx && sy<Ny && sz==1
                s2= s + Nx;
            elseif sx<Nx && sy==Ny && sz>1
                s2= s + 1 - Nx*Ny;
            elseif sx<Nx && sy==Ny && sz==1
                s2= s + 1;
            elseif sx<Nx && sy<Ny && sz==1
                s2= s + 1 + Nx;
            end
        elseif 4==m
            % - - - - 4
            if sx>1 && sy<Ny && sz>1
                s2= s -1 + Nx - Nx*Ny;
            elseif sx==1 && sy<Ny && sz>1
                s2= s + Nx - Nx*Ny;
            elseif sx==1 && sy==Ny && sz>1
                s2= s - Nx*Ny;
            elseif sx==1 && sy<Ny && sz==1
                s2= s + Nx;
            elseif sx>1 && sy==Ny && sz>1
                s2= s -1 - Nx*Ny;
            elseif sx>1 && sy==Ny && sz==1
                s2= s - 1;
            elseif sx>1 && sy<Ny && sz==1
                s2= s - 1 + Nx ;
            end
        elseif 5==m
            % - - - - 5
            if sx>1 && sy>1 && sz<Nz
                s2= s - 1 - Nx + Nx*Ny;
            elseif sx==1 && sy>1 && sz<Nz
                s2= s - Nx + Nx*Ny;
            elseif sx==1 && sy==1 && sz<Nz
                s2= s + Nx*Ny;
            elseif sx==1 && sy>1 && sz==Nz
                s2= s - Nx;
            elseif sx>1 && sy==1 && sz<Nz
                s2= s - 1 + Nx*Ny;
            elseif sx>1 && sy==1 && sz==Nz
                s2= s - 1;
            elseif sx>1 && sy>1 && sz==Nz
                s2= s - 1 - Nx;
            end
        elseif 6==m
            if sx<Nx && sy>1 && sz<Nz
               s2= s + 1 - Nx + Nx*Ny;
            elseif sx==Nx && sy>1 && sz<Nz
                s2= s - Nx + Nx*Ny;
            elseif sx==Nx && sy==1 && sz<Nz
                s2= s + Nx*Ny;
            elseif sx==Nx && sy>1 && sz==Nz
                s2= s - Nx;
            elseif sx<Nx && sy==1 && sz<Nz
                s2= s + 1 + Nx*Ny;
            elseif sx<Nx && sy==1 && sz==Nz
                s2= s + 1;
            elseif sx<Nx && sy>1 && sz==Nz
                s2= s + 1 - Nx;
            end
        elseif 7==m
            % - - - - 7
            if sx<Nx && sy<Ny && sz<Nz
                s2= s + 1 + Nx + Nx*Ny;
            elseif sx==Nx && sy<Ny && sz<Nz
                s2= s + Nx + Nx*Ny;
            elseif sx==Nx && sy==Ny && sz<Nz
                s2= s + Nx*Ny;
            elseif sx==Nx && sy<Ny && sz==Nz
                s2= s + Nx;
            elseif sx<Nx && sy==Ny && sz<Nz
                s2= s + 1 + Nx*Ny;
            elseif sx<Nx && sy==Ny && sz==Nz
                s2= s + 1;
            elseif sx<Nx && sy<Ny && sz==Nz
                s2= s + 1 + Nx;
            end
        elseif 8==m
            % - - - - 8
            if sx>1 && sy<Ny && sz<Nz
                s2= s - 1 + Nx + Nx*Ny;
            elseif sx==1 && sy<Ny && sz<Nz
                s2= s + Nx + Nx*Ny;
            elseif sx==1 && sy==Ny && sz<Nz
                s2= s + Nx*Ny;
            elseif sx==1 && sy<Ny && sz==Nz
                s2= s + Nx;
            elseif sx>1 && sy==Ny && sz<Nz
                s2= s - 1 + Nx*Ny;
            elseif sx>1 && sy==Ny && sz==Nz
                s2= s - 1;
            elseif sx>1 && sy<Ny && sz==Nz
                s2= s - 1 + Nx;
            end
        end
        if ~isempty(Ev)
            Agive{s2}= Agive{s2} + Ev*Rv*Ag*(Rvelems.')*Rvelems;
        end
        clear Rv Ev Rvelems;
    end
    clear Ag; 
    clear bg;
    clear idx sdisp;
end
clear elements selems sdiri srobinelems sbdelems sfelems seelems svelems;
clear ii jj Aa ir jr Ar iib ba irb br;
toc(tas);

%% LU of subdomain matrices
fprintf('\nsubdomains lu\n');
tsub= tic;
L= cell(Ns,1); U= cell(Ns,1); P= cell(Ns,1); Q= cell(Ns,1);
for s= 1:Ns
   [L{s},U{s},P{s},Q{s}]= lu(A{s});
   A{s}= [];
end
clear A;
toc(tsub);
disp('setup mostly finished');
toc(tsetup);

%% assemble coarse problem
for nnt= 1:length(nthetav)
    ntheta= nthetav(nnt); nphi= nphiv(nnt);
    fprintf('\n/ / / / / / / / / / / / / / / / plane waves %d  %d / / / / / / /\n',ntheta,nphi);
tcoarse= tic;    
% directions of plane waves in spherical coordinates
theta= linspace(0,pi/2,ntheta+2); theta= theta(2:ntheta+1);
phi= linspace(-pi,pi,nphi+1);  phi= phi(2:nphi+1); phi0= phi;
[theta,phi]= ndgrid(theta,phi); theta= theta(:)'; phi= phi(:)';
if ntheta>-1
    theta= [theta, -theta, zeros(1,nphi), pi/2, -pi/2];
    phi= [phi, pi+phi, phi0, 0, 0];  
    numplw= 2*ntheta*nphi+nphi+2;
else
    numplw= 0;
end
Qmu= sparse(ngf+nge+ngv,NumCommEdges*numplw); 
nmu= 0;
tolQmu= 0.1;             % threshold to filter Qmu

% set up Qmu
for s= 1:Ns
    % faces
    if 1==s
        sdisp= 0;
    else
        sdisp= sendf(s-1);
    end
    for m= 1:6
        if 1==m
            idx= 1:sfsend(1,s);
        else
            idx= sfsend(m-1,s)+1:sfsend(m,s);
        end
        if numplw~=0 && ~isempty(idx)
            coords= coordinates(sfnodes{s}(idx),:);
            if isa(k2,'function_handle') || isa(k2,'inline')
                   qbss= exp(1i*(coords.*repmat(sqrt(k2(coords)),1,3))* ...
                       [cos(theta).*cos(phi);cos(theta).*sin(phi);sin(theta)]);
            elseif isa(k2,'float')
                   qbss= exp(1i*sqrt(k2)*coords* ...
                       [cos(theta).*cos(phi);cos(theta).*sin(phi);sin(theta)]); 
            end
            [qqbss,rqbss,~]= qr(qbss); 
            if size(rqbss,1)>1
                rjj= diag(rqbss); 
                qbss= qqbss(:,abs(rjj)>tolQmu); 
            else
                qbss= qqbss(1);
            end
            Qmu(sdisp+idx,nmu+1:nmu+size(qbss,2))= qbss;
            nmu= nmu + size(qbss,2);
            clear qbss qqbss rqbss rjj idx;
        end
    end
    % edges
    if 1==s
        sdisp= ngf;
    else
        sdisp= ngf + sende(s-1);
    end
    for m= 1:12
        if 1==m
            idx= 1:sesend(1,s);
        else
            idx= sesend(m-1,s)+1:sesend(m,s);
        end
        if numplw~=0 && ~isempty(idx)
            coords= coordinates(senodes{s}(idx),:);
            if isa(k2,'function_handle') || isa(k2,'inline')
                   qbss= exp(1i*(coords.*repmat(sqrt(k2(coords)),1,3))* ...
                       [cos(theta).*cos(phi);cos(theta).*sin(phi);sin(theta)]);
            elseif isa(k2,'float')
                   qbss= exp(1i*sqrt(k2)*coords* ...
                       [cos(theta).*cos(phi);cos(theta).*sin(phi);sin(theta)]); 
            end
            [qqbss,rqbss,~]= qr(qbss);  
            if size(rqbss,1)>1
                rjj= diag(rqbss);   
                qbss= qqbss(:,abs(rjj)>tolQmu); 
            else
                qbss= qqbss(1);
            end
            Qmu(sdisp+idx,nmu+1:nmu+size(qbss,2))= qbss;
            nmu= nmu + size(qbss,2);
            clear qbss qqbss rqbss rjj idx;
        end
    end
    % vertices
    if 1==s
        sdisp= ngf + nge;
    else
        sdisp= ngf + nge + sendv(s-1);
    end
    if numplw~=0
        for m= 1:8
            if 1==m
                idx= sdisp + ( 1:svsend(1,s) );
            else
                idx= sdisp + ( svsend(m-1,s)+1:svsend(m,s) );
            end
            if ~isempty(idx)
                Qmu(idx,nmu+1)= 1;
                nmu= nmu + 1;
            end
        end
    end
end
clear theta phi coords;
if nnt==length(nthetav)
   clear coordinates sfnodes senodes svnodes sendf sende sendv sfsend sesend svsend;
end
Qmu= Qmu(:,1:nmu);
fprintf('\nsize of coarse problem is %d\n',nmu);

% coarse matrix and lu
if numplw~=0
    Fcc= Qmu'*Fsub(Qmu);
    [Lcc,Ucc,Pcc,Qcc]= lu(Fcc);
end
clear Fcc;
toc(tcoarse);

%% initsolve
if 1==nnt
    fprintf('\ninit subsolve');
    tinit= tic;
    rhsl= rhsl + T(sparse(ngf+nge+ngv,1),b);
    rhsl0= rhsl;
    toc(tinit);
end
if numplw~=0
    fprintf('\ninit coarse solve');
    tinit= tic;
    rhsmu= Qmu'*rhsl0;
    mu= Qcc * (Ucc\(Lcc\( Pcc*rhsmu )));
    clear rhsmu;
    rhsl= Qmu*mu;
    rhsl= T(rhsl) - rhsl + rhsl0;
    toc(tinit);
end

%% GMRES iteration
fprintf('\ngmres with obddsubs\n');
tg= tic;
tol= 1e-6;  restart= [];  maxit= 200; 
[lambda,flag,relres,iter,resvec] = gmres(@F,rhsl,restart,tol,maxit,[],[],sparse(size(rhsl,1),1)); 
toc(tg);
if exist('neumann','var') && 1==neumann
    fname= sprintf('o%dl%dn.mat',ol,numplw);
else
    fname= sprintf('o%dl%dr.mat',ol,numplw);
end   
save(fname,'flag','relres','iter','resvec');
disp('iter='); disp(iter);
disp('relres='); disp(relres);

disp('\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \');
end

%% recover u
if numplw~=0
    mu= mu - Qcc * (Ucc\(Lcc\( Pcc*( Qmu'*Fsub(lambda) ) )));
    lambda= lambda + Qmu*mu;
    clear mu;
end
u= zeros(nn,1);
for s= 1:Ns
    us= SolveSub(s,lambda,b);
    u= u + Dw{s}.*(Ro{s}.'*us);
end

%% subfunctions
    function y= F(x)
        % with coarse solve 
        y= x - T(x);
        if numplw~=0
            z= Qmu'*y;
            z= Qcc * (Ucc\(Lcc\( Pcc*z )));
            z= Qmu*z;
            y= y - z + T(z);
        end
    end
    function y= Fsub(x)
        % without coarse solve
        y= x - T(x);
    end

    function usub= SolveSub(sub,x,f)
        % subsolve using x as b.c., i.e. rhs nonzero at the interface,
        % and f{sub} as load
        % prepare rhs
        if 2==nargin
            bsub= sparse(size(Ro{sub},1),size(x,2));
        elseif 3==nargin
            bsub= f{sub};
        else
            disp('SolveSub: nargin must be 2 or 3');
        end
        if nnz(x)~=0
            bsub= bsub + Aget{sub} * x;
        end
        % solve
        usub= Q{sub} * (U{sub}\(L{sub}\( P{sub}*bsub )));
    end

    function y= T(x,f)
       % x consists of lambda defined on faces, edges and vertices
       % f{s} is the load for the subdomain s
       % y is new lambda communicated from the subsolutions with x 
       y= sparse(size(x,1),size(x,2));
       for sub= 1:Ns
           % subsolve
           if nargin==1
               usub= SolveSub(sub,x);
           elseif nargin==2
               usub= SolveSub(sub,x,f);
           else
               disp('T: nargin must be 1 or 2');
           end
           % calculate y from subdomain solutions, give neighbors their 
           %  interface values
           if nnz(usub)==0
               continue;
           end
           usub= sparse(Ro{sub}.'*usub);
           y= y + Agive{sub} * usub;
       end
    end

end  % -- end of obddsubs.m