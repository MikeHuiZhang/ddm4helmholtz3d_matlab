function re= wavenumber(coord,datafile,nz,ny,nx,Lz,Ly,Lx,omega)
%This function reads(when called for the first time) the piecewise constant
% velocity from the input 'datafile' and gives the wavenumber= omega/velocity
% at the point specified by the input 'coord'
persistent data_array;
if isempty(data_array)
    fid= fopen(datafile);
    data_array= fread(fid,inf,'float64');
    fclose(fid);
end
hz= Lz/nz; hy= Ly/ny; hx= Lx/nx;
iz= floor(coord(:,3)/hz)+1;
iy= floor(coord(:,2)/hy)+1;
ix= floor(coord(:,1)/hx)+1;
iz(iz>nz)= nz;
iy(iy>ny)= ny;
ix(ix>nx)= nx;
ii= (ix-1)*ny*nz + (iy-1)*nz + iz;
re= omega./data_array(ii);
end    