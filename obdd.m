function [u,flag,relres,iter,resvec,specF]= ...
    obdd(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR, ...
           Nx,Ny,Nz,ol,wlv,clv,nthetav,nphiv,neumann)
% INPUT
% coordinates, elements: we assume they are generated from qubemesh
% Nx, Ny, Nz: nonoverlapping partition to subdomains
% ol:  number of outside layers for overlapping extension
% wl:  number of outside layers for weighting
% cl:  number of outside layers for coarse functions
% Remark 
% we assume ol, wl and cl are not so large that overlapping
% subdomains communicate with at most 26  neighbors.

%% partition of the global mesh into overlapping submeshes
tsetup= tic;
fprintf('\npartition\n');
tpart= tic;
[coordinates,elements,dirichlet,robin]= qubemesh(xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR);
% [selems,sdiri,srobinelems,sbdelems,Ro,Dw,Rcp,Rwp,numplw] = ...
%         qubemeshpart(coordinates,GammaD,GammaR,nx,ny,nz,Nx,Ny,Nz,...
%                    ol,wl,cl,ntheta,nphi,k2);
[Ro,~,~,sbdelems,srobinelems,selems]= qubesubmesh(GammaD,GammaR,nx,ny,nz,Nx,Ny,Nz,ol);
toc(tpart);

%% assemble global matrix and rhs
fprintf('\nassembly global\n');
tag= tic;
Ns= Nx*Ny*Nz; nn= (nx+1)*(ny+1)*(nz+1);
A= cell(Ns,1); 
getAM(); getgpw(); % for use by stima.m and rhs.m
getM2d(); getgpw2d(); % for use by stima2d and rhs2d
global unused_src numsrc; % for use by rhs.m
[I,J]= ndgrid(1:8,1:8); I= I(:); J= J(:);
[Ir,Jr]= ndgrid(1:4,1:4); Ir= Ir(:); Jr= Jr(:);
ne= size(elements,1);
ii= zeros(64,ne); jj= zeros(64,ne); Aa= zeros(64,ne);
for j= 1:ne
    Ae= stima(coordinates(elements(j,:),:),k2);
    ii(:,j)= elements(j,I); % assemble vectors
    jj(:,j)= elements(j,J); % for speed
    Aa(:,j)= Ae(:);
end
iib= []; ba= [];
if ~(isa(f,'float') && 0==f && isempty(pointsource))
   iib= zeros(8,ne); ba= zeros(8,ne);
   for j = 1:ne
      n4e= elements(j,:);
      iib(:,j)= n4e;
      ba(:,j)= rhs(coordinates(n4e,:),f,pointsource);
   end
end
clear elements;
% treat outer Robin b.c.
nr= size(robin,1); 
ir= []; jr= []; Ar= [];
if ~(isa(a,'float') && 0==a)
   ir= zeros(16,nr); jr= zeros(16,nr); Ar= zeros(16,nr);
   for j = 1:nr
       ir(:,j)= robin(j,Ir);
       jr(:,j)= robin(j,Jr); 
       Ae= stima2d(coordinates(robin(j,:),:),a);
       Ar(:,j)= Ae(:);
   end
end
irb= []; br= [];
if ~(isa(uR,'float') && 0==uR)
   irb= zeros(4,nr); br= zeros(4,nr);
   for j = 1:nr
      n4e= robin(j,:);
      irb(:,j)= n4e;
      br(:,j)= rhs2d(coordinates(n4e,:),uR);
   end
end
clear robin;
% treat Dirichlet boundary lastly, to avoid Robin change
bb= sparse([iib(:);irb(:)],1,[ba(:);br(:)],nn,1);
clear iib irb ba br;
ud= sparse(nn,1);
if isa(uD,'float')
   ud(dirichlet)= uD*ones(size(dirichlet,1),1);
elseif isa(uD,'inline') || isa(uD,'function_handle')
   ud(dirichlet)= uD(coordinates(dirichlet,:));
end
AA= sparse([ii(:);ir(:)],[jj(:);jr(:)],[Aa(:);Ar(:)],nn,nn);
bb= bb - AA*ud;
bb(dirichlet)= 0;
[iiA, jjA]= find(AA);
idxofzero = ismember(iiA,double(dirichlet)); 
AA= setsparse(AA,iiA(idxofzero),jjA(idxofzero),0);
AA= setsparse(AA,double(dirichlet),double(dirichlet),1);
clear iiA jjA idxofzero dirichlet;
toc(tag);

%% assemble subdomains matrices 
fprintf('\nassembly subdomains\n');
tas= tic;
for s= 1:Ns
    % treat internal Robin b.c.
    if ~exist('neumann','var') || neumann==0      
        if isa(k2,'function_handle') || isa(k2,'inline')
           reg= @(x)(-sqrt(k2(x))*1i);
        elseif isa(k2,'float')
            reg= -sqrt(k2)*1i;
        end
        nb= size(sbdelems{s},1);
        ib= zeros(16,nb); jb= zeros(16,nb); Ab= zeros(16,nb);
        for j = 1:nb
            ib(:,j)= sbdelems{s}(j,Ir);
            jb(:,j)= sbdelems{s}(j,Jr);
            Ae= stima2d(coordinates(sbdelems{s}(j,:),:),reg); 
            Ab(:,j)= Ae(:);
        end
    end
    sbdelems{s}= [];
    % outer Robin
    iis= ii(:,selems{s}); iis= iis(:);
    jjs= jj(:,selems{s}); jjs= jjs(:);
    Aas= Aa(:,selems{s}); Aas= Aas(:);
    selems{s}= [];
    if ~isempty(ir)
        irs= ir(:,srobinelems{s}); irs= irs(:);
        jrs= jr(:,srobinelems{s}); jrs= jrs(:);
        Ars= Ar(:,srobinelems{s}); Ars= Ars(:);
        srobinelems{s}= [];
    else
        irs= [];
        jrs= [];
        Ars= [];
    end
    % setup and restrict matrix to subdomain
    Ag= sparse([iis;irs;ib(:)],[jjs;jrs;jb(:)],[Aas;Ars;Ab(:)],nn,nn);
    A{s}= Ro{s}*Ag*Ro{s}.';
    clear Ag;
end
unused_src= []; numsrc= [];
clear selems sdiri srobinelems sbdelems;
clear ii jj Aa ir jr Ar ib jb Ab;
clear iis jjs Aas irs jrs Ars iibs bas irbs brs;
toc(tas);

%% LU of subdomain matrices
fprintf('\nsubdomains lu\n');
tsub= tic;
L= cell(Ns,1); U= cell(Ns,1); P= cell(Ns,1); Q= cell(Ns,1);
for s= 1:Ns
   [L{s},U{s},P{s},Q{s}]= lu(A{s});
   A{s}= [];
end
clear A;
toc(tsub);
disp('setup mostly finished');
toc(tsetup);
%% assemble coarse problem
for nnt= 1:length(nthetav)
    ntheta= nthetav(nnt); nphi= nphiv(nnt);
    fprintf('\n/ / / / / / / / / / / / / / / / plane waves %d  %d / / / / / / /\n',ntheta,nphi);
for nwl= 1:length(wlv)
    wl= wlv(nwl);
    fprintf('\n= = = = = = = = wl %d = = = = = = = = =\n',wl);
    tw= tic;
    [~,sdiriw]= qubesubmesh(GammaD,GammaR,nx,ny,nz,Nx,Ny,Nz,wl);
    Dw= qubepartunit(nx,ny,nz,Nx,Ny,Nz,wl,sdiriw);
    clear sdiriw;
    toc(tw)
for ncl= 1:(length(clv)*(ntheta>-1) + (ntheta<=-1));
    cl= clv(ncl);
    fprintf('\n----------cl %d----------\n',cl);
    tc= tic;
    
[Rcp, Rwp, numplw]= qubeplw(coordinates,k2,GammaD,GammaR,nx,ny,nz,Nx,Ny,Nz,wl,cl,ntheta,nphi);
if nnt==length(nthetav) && nwl==length(wlv) && ncl==length(clv)
    clear coordinates;
end
% by assumption, every wl extended subdomain overlap with at most 26 cl 
% extended subdomains
if numplw~=0
    iis= cell(Ns,Ns); jjs= cell(Ns,Ns); vvs= cell(Ns,Ns); nzzAc= 0;
    for s= 1:Ns
       [sx,sy]= ind2sub([Nx,Ny*Nz],s); % 3d position of this subdomain
       [sy,sz]= ind2sub([Ny,Nz],sy);
       dsx= -(sx>1)*1:(sx<Nx)*1; dsy= -(sy>1)*1:(sy<Ny)*1; 
       dsz= -(sz>1)*1:(sz<Nz)*1;
       [dsx,dsy,dsz]= ndgrid(dsx,dsy,dsz);
       for j= 1:length(dsx(:))
           s2= sub2ind([Nx,Ny,Nz],sx+dsx(j),sy+dsy(j),sz+dsz(j));        
           Acs= Rwp{s}*AA*Rcp{s2}';
           [iis{s,s2},jjs{s,s2},vvs{s,s2}]= find(Acs);
           clear Acs;
           nzzAc= nzzAc + length(iis{s,s2});
       end
    end
    ii= zeros(nzzAc,1); jj= zeros(nzzAc,1); vv= zeros(nzzAc,1); 
    nzzAc= 0; 
    for s= 1:Ns
       [sx,sy]= ind2sub([Nx,Ny*Nz],s); % 3d position of this subdomain
       [sy,sz]= ind2sub([Ny,Nz],sy);
       dsx= -(sx>1)*1:(sx<Nx)*1; dsy= -(sy>1)*1:(sy<Ny)*1;
       dsz= -(sz>1)*1:(sz<Nz)*1;
       [dsx,dsy,dsz]= ndgrid(dsx,dsy,dsz);
       for j= 1:length(dsx(:))
           s2= sub2ind([Nx,Ny,Nz],sx+dsx(j),sy+dsy(j),sz+dsz(j));
           idx= nzzAc+1:nzzAc+length(iis{s,s2});
           ii(idx)= iis{s,s2}+(s-1)*numplw;
           jj(idx)= jjs{s,s2}+(s2-1)*numplw;
           vv(idx)= vvs{s,s2};
           nzzAc= nzzAc + length(iis{s,s2});
       end    
    end
    Ac= sparse(ii,jj,vv);
    if size(Ac,1)~=size(Ac,2)
        disp('coarse matrix, numrow~=numcol');
    end
    if size(Ac,1)~=numplw*Ns
        disp('coarse matrix, numrow~=numplw*Ns');
    end
    clear ii jj vv iis jjs vvs dsx dsy dsz idx;
end
%% LU factorization of and coarse problems
if numplw~=0
   [Lc,Uc,Pc,Qc]= lu(Ac);
   clear Ac;
end
toc(tc);
%% preconditioned system rhs: additive coarse and subdomain solves
fprintf('\ninitsolve for additive coarse\n');
tinit= tic;
if numplw~=0  % coarse solve
    g= P0(bb);
else
    g= sparse(size(bb,1),1);
end
% subdomain solve
for s= 1:Ns
   g= g + Dw{s}.*(Ro{s}.'*(Q{s} * (U{s}\(L{s}\(P{s}*(Ro{s}*bb))))));
end
toc(tinit);

%% GMRES iteration for additive coarse
fprintf('\ngmres for additive coarse\n');
tg= tic;
tol= 1e-6;  restart= [];  maxit= 200; 
[u,flag,relres,iter,resvec] = gmres(@T,g,restart,tol,maxit,[],[],sparse(size(bb,1),1)); 
u= u + ud;
toc(tg);
fname= sprintf('o%dl%dw%dc%da.mat',ol,numplw,wl,cl);
save(fname,'flag','relres','iter','resvec');
disp('iter='); disp(iter);
disp('relres='); disp(relres);
% test direct solve
% flag= 0; relres= []; iter= 0; resvec=[]; 
% u= AA\bb;
%% computing spectra
specF= [];

%% preconditioned system rhs: multiplicative coarse and subdomain solves
fprintf('\ninitsolve for multiplicative coarse\n');
tinit= tic;
if numplw~=0  % coarse solve
    u0= P0(bb);
else
    u0= sparse(size(bb,1),1);
end
% subdomain solve
res0= bb - AA*u0;
g= sparse(size(bb,1),1);
for s= 1:Ns
   g= g + Dw{s}.*(Ro{s}.'*(Q{s} * (U{s}\(L{s}\(P{s}*(Ro{s}*res0))))));
end
clear res0;
AAg= AA*g;
if numplw~=0
   g= g - P0(AAg);
end
toc(tinit);

%% GMRES iteration for multiplicative coarse
fprintf('\ngmres for multiplicative coarse\n');
tg= tic;
[u,flag,relres,iter,resvec] = gmres(@Tm,g,restart,tol,maxit,[],[],sparse(size(bb,1),1)); 
u= u + u0 + ud;
toc(tg);
fname= sprintf('o%dl%dw%dc%dm.mat',ol,nnt,wl,cl);
save(fname,'flag','relres','iter','resvec');
disp('iter='); disp(iter);
disp('relres='); disp(relres);

disp('----------------------------');
end
disp('=============================================');
end
disp('\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \');
end
%% coarse solve operator P0 (doesnot contain AA)
    function x= P0(y)
        x= sparse(size(y,1),1);
        % yc:= Rwp*y
        yc= zeros(Ns*numplw,1);
        for sub= 1:Ns
            yc((sub-1)*numplw+1:sub*numplw)= Rwp{sub}*y;
        end
        % yc:= Ac\yc
        yc= Qc * (Uc\(Lc\(Pc*yc)));
        % x:= Rcp'*xc = P0 y
        for sub= 1:Ns
            x= x + sparse(Rcp{sub}'*yc((sub-1)*numplw+1:sub*numplw));
        end
        clear yc;
    end

%% preconditioned system operator with additive coarse
%  T:= T0 + T1 + ...
    function y= T(x)
        y= sparse(size(x,1),1);
        AAx= AA*x;
        if numplw~=0  % coarse solve
           y= y + P0(AAx);
        end
        % subdomain solve
        for ms= 1:Ns
           y= y + Dw{ms}.*(Ro{ms}.'*(Q{ms} * (U{ms}\(L{ms}\(P{ms}*(Ro{ms}*AAx))))));
        end
        clear AAx;
    end
%% preconditioned system operator with multiplicative coarse
%  T:= (I-T0)(T1 + ...)
    function pxx= Tm(xx)
        pxx= sparse(size(xx,1),1);
        AAxx= AA*xx;
        % subdomain solve
        for ms= 1:Ns
           pxx= pxx + Dw{ms}.*(Ro{ms}.'*(Q{ms} * (U{ms}\(L{ms}\(P{ms}*(Ro{ms}*AAxx))))));
        end
        clear AAxx;
        % coarse correction
        if numplw~=0  
           AApxx= AA*pxx; 
           pxx= pxx - P0(AApxx);
           clear AApxx;
        end
    end
end % -- end of the function obdd
