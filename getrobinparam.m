function [reg1, reg2, totalreg]= getrobinparam(k2,kmin2,kmax2,CL,interbsign)
% variable wavenumber
   if isa(k2,'function_handle') || isa(k2,'inline')
       % zero order
       if nargout==1
          % non-overlapping O0/T0
          if nargin==3
              % (a) O0
              reg1= @(x) (1-1i)*sqrt(sqrt((2*sqrt(k2(x)*kmin2)-kmin2).*(kmax2-k2(x)))/2).*(kmin2<k2(x))...
                  +((kmin2-k2(x)).*(kmax2-k2(x))).^(1/4).*(kmin2>=k2(x));
              % (b) T0
               reg1= @(x) -1i*sqrt(k2(x));
          % overlapping OO0
          elseif nargin==4
              % (a) kh= Cw, H fixed
              %           Cw= pi/5/sqrt(2); % effective h: from 2d to 3d
              %           Cp= 2^(-1/4)*kmin2^(1/8)*Cw^(3/4);
              %           reg1= @(x) Cp*pi^(-3/4)/kmax2^(-3/2)*(kmin2<k2(x))...
              %               +((kmin2-k2(x)).*(kmax2-k2(x))).^(1/4).*(kmin2>=k2(x));
              % (b) k,H fixed, scale with h, use also asymptotic
              %     for kmin2>k2
              h_eff= pi/sqrt(kmax2);
              reg1= @(x)(1-1i)*(2*sqrt(k2(x)*kmin2)-kmin2).^(1/3)* ...
                  (CL*pi/sqrt(kmax2))^(-1/3)/2.*(kmin2<k2(x))...
                  +(0.5*(kmin2-k2(x))/h_eff/CL).^(1/3).*(kmin2>=k2(x));
              % (c) analytical formula for k2-kmin2 is relatively large than
              %     1/L, c.f. Maple code olphelm.mw
              %           h_eff= pi/sqrt(kmax2);
              %           reg1= @(x)(1-1i)*((2*sqrt(k2(x)*kmin2)-kmin2).*...
              %               (k2(x)-kmin2)).^(1/4).*(kmin2<k2(x))...
              %               +(0.5*(kmin2-k2(x))/h_eff).^(1/3).*(kmin2>=k2(x));
              % (d) T0
              reg1= @(x) -1i*sqrt(k2(x));
          end
       % second order 
       elseif nargout==2
         % non-overlapping O2/T2  
         if nargin==3  
           % (a) Gander 2002
               a= @(x)(-1i*(k2(x)-kmin2).^(1/4).*(2*sqrt(k2(x)*kmin2)-kmin2).^(1/4));
               % formerly, by mistake, I write the second term for b as
               % kmax2-2*sqrt(kmax2*k2), but it also works, not much
               % difference in iteration numbers
               b= @(x)((kmax2-k2(x)).^(1/4).*(2*sqrt(kmin2*k2(x))+kmin2).^(1/4));
               reg1= @(x)((a(x).*b(x)-k2(x))./(a(x)+b(x)).*(kmin2<k2(x)) + ...
                   + (kmax2*sqrt(kmin2-k2(x))-kmin2*sqrt(kmax2-k2(x)))/sqrt(2*(kmax2-kmin2)) ...
                   ./(sqrt(kmax2-k2(x))-sqrt(kmin2-k2(x))).^(1/4) ...
                   ./((kmax2-k2(x)).*sqrt(kmin2-k2(x))-(kmin2-k2(x)).*sqrt(kmax2-k2(x))).^(1/4).*(kmin2>=k2(x)));
               reg2= @(x)(1./(a(x)+b(x)).*(kmin2<k2(x)) + (kmin2>=k2(x)).* ...
                   (sqrt(kmax2-k2(x))-sqrt(kmin2-k2(x))).^(3/4)/sqrt(2*(kmax2-kmin2)) ...
                   ./((kmax2-k2(x)).*sqrt(kmin2-k2(x))-(kmin2-k2(x)).*sqrt(kmax2-k2(x))).^(1/4));

             % (b) transformed from non-overlapping two-side parameters in 
             %     Gander 2007, h asymptotics
%              km= @(x)(sqrt(k2(x))-sqrt(kmin2));
%              Cw= @(x)(k2(x)-km(x).^2);
%              a= @(x)(1-1i)*(kmax2^(3/8)*Cw(x).^(1/8));
%              b= @(x)(1-1i)*(kmax2^(1/8)*Cw(x).^(3/8)/2);
%              reg1= @(x) (kmin2<k2(x)).*(a(x).*b(x)-k2(x))./(a(x)+b(x))...
%                    + (kmin2>=k2(x)).*(kmax2*sqrt(kmin2-k2(x))- ...
%                    kmin2*sqrt(kmax2-k2(x)))/sqrt(2*(kmax2-kmin2)) ...
%                    ./(sqrt(kmax2-k2(x))-sqrt(kmin2-k2(x))).^(1/4) ...
%                    ./((kmax2-k2(x)).*sqrt(kmin2-k2(x))-(kmin2-k2(x)).*sqrt(kmax2-k2(x))).^(1/4);
%              reg2= @(x) (kmin2<k2(x))./(a(x)+b(x)) + (kmin2>=k2(x)).* ...
%                    (sqrt(kmax2-k2(x))-sqrt(kmin2-k2(x))).^(3/4)/sqrt(2*(kmax2-kmin2)) ...
%                    ./((kmax2-k2(x)).*sqrt(kmin2-k2(x))-(kmin2-k2(x)).*sqrt(kmax2-k2(x))).^(1/4);
             % (c) Taylor second order
              reg1= @(x) -sqrt(k2(x))*1i;
              reg2= @(x) 1i/2./sqrt(k2(x));

           % overlapping OO2/OT2
           elseif nargin==4 % asymptotics for overlapping 
               % (a) similar to Gander's paper on non-overlapping for Helmholtz
               % use the non-overlapping zero-order result for the spdproblem
%                a= @(x)(-1i*(k2(x)-kmin2).^(1/4).*(2*sqrt(k2(x)*kmin2)-kmin2).^(1/4));
%                % b: use the overlapping zero-order result for the spd problem
%                h_eff= pi/sqrt(kmax2);
%                b= @(x)(0.5*(kmin2-k2(x))/h_eff).^(1/3);
%                % for kmin2<k2, compose from a, b; for kmin2>k2 use spd result directly
%                reg1= @(x)(a(x).*b(x)-k2(x))./(a(x)+b(x)).*(kmin2<k2(x))...
%                    + ((kmin2-k2(x)).^2/8/h_eff/CL).^(1/5).*(kmin2>=k2(x));
%                reg2= @(x) 1./(a(x)+b(x)).*(kmin2<k2(x)) + ...
%                    ((CL*h_eff)^3./(kmin2-k2(x))/2).^(1/5).*(kmin2>=k2(x));
               % (b) second order from two-side overlapping, see Gander's maple
               % codes (printed), scale with h, we tried all four paramters
               % p1,q1,p2,q2, but it does not work; 
               % note that to transform from two-side to second order
               % change CL to CL/2 
%                CL= CL/2;
%                h_eff= pi/sqrt(kmax2);
%                xp2= @(x) 2*sqrt(k2(x))*sqrt(kmin2)+kmin2;
%                xm2= @(x) 2*sqrt(k2(x))*sqrt(kmin2)-kmin2;
%                a= @(x) 0.5/CL^0.6*2^0.6./(xp2(x)+xm2(x)).^0.4.* ...
%                    xp2(x).^0.2.*xm2(x).^0.4*h_eff^(-0.6) - 1i*h_eff^(-0.2);
%                b= @(x) 0.5/CL^0.2*2^0.2./(xp2(x)+xm2(x)).^0.8.*(xp2(x).^0.4 ...
%                    .*xm2(x).^0.8-1i*xp2(x).^0.9.*xm2(x).^0.3)*h_eff^(-0.2);
%                reg1= @(x)(a(x).*b(x)-k2(x))./(a(x)+b(x)).*(kmin2<k2(x))...
%                    + ((kmin2-k2(x)).^2/8/h_eff/CL).^(1/5).*(kmin2>=k2(x));
%                reg2= @(x) 1./(a(x)+b(x)).*(kmin2<k2(x)) + ...
%                    ((CL*h_eff)^3./(kmin2-k2(x))/2).^(1/5).*(kmin2>=k2(x));
             % (c) similar to (b), use q1=p1, q2=p2, see Martin's Maple
             % codes (printed)
             % note that to transform from two-side to second order
             % change CL to CL/2 
             h_eff= pi/sqrt(kmax2);
             xm2= @(x) 2*sqrt(k2(x))*sqrt(kmin2)-kmin2;
             % or use xp2, but still name it as xm2
             % xm2= @(x) 2*sqrt(k2(x))*sqrt(kmin2)+kmin2;
             CL= CL/2;
             a= @(x) (1-1i)*1/4/CL^(3/5)*2^(4/5)*(xm2(x)).^(1/5)*h_eff^(-3/5);
             b= @(x) (1-1i)*1/4/CL^(1/5)*2^(3/5)*(xm2(x)).^(2/5)*h_eff^(-1/5);
             reg1= @(x) (kmin2<k2(x)).*(a(x).*b(x)-k2(x))./(a(x)+b(x))...
                   + (kmin2>=k2(x)).*((kmin2-k2(x)).^2/8/h_eff/CL).^(1/5);
             reg2= @(x) (kmin2<k2(x))./(a(x)+b(x)) + ...
                   (kmin2>=k2(x)).*((CL*h_eff)^3./(kmin2-k2(x))/2).^(1/5);
            % (d) Taylor second order
             reg1= @(x) -sqrt(k2(x))*1i;
             reg2= @(x) 1i/2./sqrt(k2(x));
         end
       % two-sided
       elseif nargout==3
            km= @(x)(sqrt(k2(x))-sqrt(kmin2));
            Cw= @(x)(k2(x)-km(x).^2);
            regp= @(x)(kmax2^(3/8)*Cw(x).^(1/8));
            regm= @(x)(kmax2^(1/8)*Cw(x).^(3/8)/2);
            if interbsign==1
                reg1= @(x)((1-1i)*regp(x));
            else
                reg1= @(x)((1-1i)*regm(x));
            end
            totalreg= @(x)(1-1i)*(regp(x)+regm(x));
            reg2= [];
       end
       
% constant wavenumber
    elseif isa(k2,'float')
        
        % zero order 
          if nargout==1
              
              % non-overlapping O0
              if nargin==3 
                % (a) Gander 2002
                if kmin2<k2
                    km= sqrt(k2)-sqrt(kmin2);
                    reg1= (1-1i)*sqrt(sqrt((k2-km^2)*(kmax2-k2))/2);
                else
                    reg1= ...%((kmin2+2*sqrt(k2*kmin2))*(kmax2-k2))^(1/4);
                        ((kmin2-k2)*(kmax2-k2))^(1/4);
                end
                % (b) Taylor 0
                 reg1= -1i*sqrt(k2);

              % overlapping O0
              elseif nargin==4
                 % (b) k,H fixed, scale with h, use also asymptotic
                 %     for kmin2>k2
                 h_eff= pi/sqrt(kmax2);
                 reg1= (1-1i)*(2*sqrt(k2*kmin2)-kmin2)^(1/3)* ...
                     (CL*pi/sqrt(kmax2))^(-1/3)/2.*(kmin2<k2)...
                     +(0.5*(kmin2-k2)/h_eff/CL).^(1/3).*(kmin2>=k2);
              end
         % second order
          elseif nargout==2
              % non-overlapping O2
              if nargin==3  
                  % (a) from Gander 2002
                  if kmin2<k2
                      a= -1i*(k2-kmin2)^(1/4)*(2*sqrt(k2*kmin2)-kmin2)^(1/4);
                      b= (kmax2-k2)^(1/4)*(kmax2-2*sqrt(kmax2*k2))^(1/4);
                      reg1= (a*b-k2)/(a+b);
                      reg2= 1/(a+b);
                  else
                      reg1= kmax2*sqrt(kmin2-k2)-kmin2*sqrt(kmax2-k2);
                      reg1= reg1/sqrt(2*(kmax2-kmin2));
                      reg1= reg1/(sqrt(kmax2-k2)-sqrt(kmin2-k2))^(1/4);
                      reg1= reg1/((kmax2-k2)*sqrt(kmin2-k2)-(kmin2-k2)*sqrt(kmax2-k2))^(1/4);
                      reg2= (sqrt(kmax2-k2)-sqrt(kmin2-k2))^(3/4);
                      reg2= reg2/sqrt(2*(kmax2-kmin2));
                      reg2= reg2/((kmax2-k2)*sqrt(kmin2-k2)-(kmin2-k2)*sqrt(kmax2-k2))^(1/4);
                  end
                  % (b) transformed from non-overlap two-side param. 
                  %     in Gander 2007, q1=p1, q2=p2
%                   km= sqrt(k2)-sqrt(kmin2);
%                   Cw= k2-km^2;
%                   a= (1-1i)*(kmax2^(3/8)*Cw^(1/8));
%                   b= (1-1i)*(kmax2^(1/8)*Cw^(3/8)/2);
%                   reg1= (kmin2<k2/4)*(a*b-k2)/(a+b)...
%                       + (kmin2>=k2/4)*(kmax2*sqrt(kmin2-k2)-...
%                       kmin2*sqrt(kmax2-k2))/sqrt(2*(kmax2-kmin2)) ...
%                       ./(sqrt(kmax2-k2)-sqrt(kmin2-k2)).^(1/4) ...
%                       ./((kmax2-k2).*sqrt(kmin2-k2)-(kmin2-k2).*sqrt(kmax2-k2)).^(1/4);
%                   reg2= (kmin2<k2/4)/(a+b) + (kmin2>=k2/4)* ...
%                       (sqrt(kmax2-k2)-sqrt(kmin2-k2)).^(3/4)/sqrt(2*(kmax2-kmin2)) ...
%                       ./((kmax2-k2).*sqrt(kmin2-k2)-(kmin2-k2).*sqrt(kmax2-k2)).^(1/4);
                  % (c) Taylor second order
                   reg1= -sqrt(k2)*1i;
                   reg2= 1i/2./sqrt(k2);
             % overlapping OO2
             elseif nargin==4 
                  % (c) h asymptotics, transformed from two-side parameters
                  % see Gander's Maple codes (printed), q1=p1, q2=p2
                  h_eff= pi/sqrt(kmax2);
                  CL= CL/2;
                  if kmin2<k2
                       xm2= 2*sqrt(k2)*sqrt(kmin2)-kmin2;
                       % or use xp2, but still name it as xm2
                       % xm2= 2*sqrt(k2)*sqrt(kmin2)+kmin2;
                       a= (1-1i)*1/4/CL^(3/5)*2^(4/5)*xm2.^(1/5)*h_eff^(-3/5);
                       b= (1-1i)*1/4/CL^(1/5)*2^(3/5)*xm2.^(2/5)*h_eff^(-1/5);
                       reg1= (a.*b-k2)./(a+b);
                       reg2= 1./(a+b);
                  else % from Gander 2006 on SPD problem
                      reg1= ((kmin2-k2).^2/8/h_eff/CL).^(1/5);
                      reg2= ((CL*h_eff)^3./(kmin2-k2)/2).^(1/5);
                  end
                   % (d) Taylor second order
                   reg1= -sqrt(k2)*1i;
                   reg2= 1i/2./sqrt(k2);
              end
          % two-sided 2d
          elseif nargout==3
              km= sqrt(k2)-sqrt(kmin2);
              Cw= k2-km^2;
              regp= kmax2^(3/8)*Cw^(1/8);
              regm= kmax2^(1/8)*Cw^(3/8)/2;
              if interbsign==1
                  reg1= (1-1i)*regp;
              else
                  reg1= (1-1i)*regm;
              end
              totalreg= (1-1i)*(regp + regm);
              reg2= [];
          end
          % FETI-2LM
%         reg= sqrt(k2)*1i;  
%         totalreg= 2*reg; 
        % test
%         reg= (1-1i)*1e-6;
%         totalreg= 2*reg;
   end
end