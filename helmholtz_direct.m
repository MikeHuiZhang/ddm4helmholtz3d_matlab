function helmholtz_direct()

clc;
clear all;
% The Helmholtz problem is
%    -\lap u - k^2 u = f,   in \Omega
%                 u = uD,   on \Gamma_D
%       du/dn + a u = uR,   on \Gamma_R
% where a can be zero for Neumann boundary.


%diary(num2str(clock));
fid1= fopen('iter_direct.txt','w');
fprintf(fid1,' O0  O2  OO0  OO2\n');
fid2= fopen('cputime_direct.txt','w');
fprintf(fid2,' O0  O2   OO0  OO2  \n');
fid3= fopen('reg_direct.txt','w');
fprintf(fid3,'O0     O2                  OO0    OO2\n');

% equation: example 1 constant wavenumber
k= 4*pi; % wavenumber
k2= k^2;  
a= -1i*k; % absorbing coefficient 
f= 0; 
pointsource= [0.3,0.3,0.03,1]; % source
uD= 0; % Dirichlet boundary value
uR= 0; % absorbing boundary value

% geometry: continuous description of the qube domain and boundary
xl= 0; xr= 1;
yl= 0; yr= 1;
zl= 0; zr= 1;

GammaD= [3,4,5,6]; % 1 left, 2 right, 3 front, 4 back, 5 bottom, 6 top 
GammaR= [1,2];

% discretization parameter
for nx= 10:10:60 
    ny= nx; nz= nx;
% hx= (xr-xl)/nx; hy= (yr-yl)/ny; hz= (zr-zl)/nz;

% ddm solution
% partition to Nx X Ny X Nz subdomains, fetidp require Nx > freq*Lx/c/sqrt(2)/pi    
Nx= 2; Ny= 1; Nz= 1; 
ntheta= -1;  nphi= 0;  % number of plane waves is 2*ntheta*nphi+nphi+2, or 0 when ntheta=-1
randinit= 0; % wheter use random initial guess
olp= 1; % overlap

% search for good parameters for O0
p= 1:5:1;
iterv= zeros(length(p),1); tv= zeros(length(p),1);
for idx= 1:length(p)
    tdd= tic;
    [~,~,~,iter]= ...
        nos(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,GammaD,GammaR,nx,ny,nz,...
        Nx,Ny,Nz,ntheta,nphi,1,2,0,p(idx)*(1-1i),randinit);
    telapsed= toc(tdd);
    iterv(idx)= iter(2);
    tv(idx)= telapsed;
end
[~,idx]= min(iterv);
fprintf(fid1,'%3d  ',iterv(idx));
fprintf(fid2,'%3.1f  ',tv(idx));
fprintf(fid3,'%3.1f', p(idx));

% search for good parameters for O2
alpha= 1:5:1; beta= 1:5:1;
iterv= zeros(length(alpha)*length(beta),1);
tv= zeros(length(alpha)*length(beta),1);
idx= 0;
for ia= 1:length(alpha)
    for ib= 1:length(beta)
        idx= idx + 1;
        reg= [(-alpha(ia)*1i*beta(ib)-k2)/(beta(ib)-alpha(ia)*1i),1/(beta(ib)-alpha(ia)*1i)]; 
                   % special form of Robin
        tdd= tic;
        [~,~,~,iter]= ...
            nos(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,GammaD,GammaR,nx,ny,nz,...
            Nx,Ny,Nz,ntheta,nphi,1,2,2,reg,randinit);
        telapsed= toc(tdd);
        iterv(idx)= iter(2);
        tv(idx)= telapsed;
    end
end
[~,idx]= min(iterv);
fprintf(fid1,'%3d  ',iterv(idx));
fprintf(fid2,'%3.1f  ',tv(idx));
[ib,ia]= ind2sub([length(beta),length(alpha)], idx);
fprintf(fid3,'(%3.1f, %3.1f)', alpha(ia), beta(ib));

% OO0
p= 1:5:1;
iterv= zeros(length(p),1); tv= zeros(length(p),1);
for idx= 1:length(p)
    tdd= tic;
    [~,~,~,iter]= ...
        oossubs(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR, ...
        Nx,Ny,Nz,olp,ntheta,nphi,0,p(idx)*(1-1i),randinit);
    telapsed= toc(tdd);
    iterv(idx)= iter(2);
    tv(idx)= telapsed;
end
[~,idx]= min(iterv);
fprintf(fid1,'%3d  ',iterv(idx));
fprintf(fid2,'%3.1f  ',tv(idx));
fprintf(fid3,'%3.1f', p(idx));

% OO2
alpha= 1:5:100; beta= 1:5:100;
iterv= zeros(length(alpha)*length(beta),1);
tv= zeros(length(alpha)*length(beta),1);
idx= 0;
for ia= 1:length(alpha)
    for ib= 1:length(beta)
        idx= idx + 1;
        reg= [(-alpha(ia)*1i*beta(ib)-k2)/(beta(ib)-alpha(ia)*1i),1/(beta(ib)-alpha(ia)*1i)];
        % special form of Robin
        tdd= tic;
        [~,~,~,iter]= ...
            oossubs(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR, ...
            Nx,Ny,Nz,olp,ntheta,nphi,2,reg,randinit);
        telapsed= toc(tdd);
        iterv(idx)= iter(2);
        tv(idx)= telapsed;
    end
end
[~,idx]= min(iterv);
fprintf(fid1,'%3d  ',iterv(idx));
fprintf(fid2,'%3.1f  ',tv(idx));
[ib,ia]= ind2sub([length(beta),length(alpha)], idx);
fprintf(fid3,'(%3.1f, %3.1f)', alpha(ia), beta(ib));
[alpha,beta]= meshgrid(alpha,beta);
contour(alpha,beta,reshape(iterv,length(beta),length(alpha)));

fprintf(fid1,'\n');
fprintf(fid2,'\n');
fprintf(fid3,'\n');
end

fclose('all');
%diary off;

end