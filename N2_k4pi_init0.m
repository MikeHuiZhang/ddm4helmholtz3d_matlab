% domain: unit cube, 
% source: [0.3,0.3,0.03,1]
% initial guess: zero
% k= 4*pi;
% Nx= 2; Ny= 1; Nz=1;
h= 1/(10:10:60);
%% iteration numbers
% non-overlapping
iter_FL= [12 14 16 16 16 0];
iter_FD= [1 1 1 1 1 1];
iter_FH= [13 15 16 17 17 0];
iter_O0= [22 27 30 32 34 0];
iter_O0d= [21 23 20 19 19 0];
iter_O2= [16 21 24 25 27 0];
iter_O2d1= [11 14 13 13 0 0];
iter_O2d2= [11 12 12 12 0 0];
iter_T0= [37 44 40 35 0 0];
iter_T2= [27 25 25 26 0 0];
% with olp= 2*h
iter_OD= [15 21 24 27 0 0];
iter_OO0= [13 13 13 13 0 0];
iter_OO0d= [13 13 13 13 0 0];
iter_OO2= [11 11 10 10 0 0];
iter_OO2d1= [9 9 9 9 0 0];
iter_OO2d2= [10 10 10 10 0 0];
iter_OT0= [10 15 17 18 0 0];
iter_OT2= [10 16 18 20 0 0];
% with fixed overlap
iter_OfD= [];
iter_OfO0= [];
iter_OfO0d= [];
iter_OfO2= [];
iter_OfO2d1= [];
iter_OfO2d2= [];
iter_OfT0= [];
iter_OfT2= [];

%% cputime
% non-overlapping
cpu_FL= [];
cpu_FD= [];
cpu_FH= [];
cpu_O0= [];
cpu_O0d= [];
cpu_O2= [];
cpu_O2d1= [];
cpu_O2d2= [];
cpu_T0= [];
cpu_T2= [];
% with olp= 2*h
cpu_OD= [];
cpu_OO0= [];
cpu_OO0d= [];
cpu_OO2= [];
cpu_OO2d1= [];
cpu_OO2d2= [];
cpu_OT0= [];
cpu_OT2= [];
% with fixed overlap
cpu_OfD= [];
cpu_OfO0= [];
cpu_OfO0d= [];
cpu_OfO2= [];
cpu_OfO2d1= [];
cpu_OfO2d2= [];
cpu_OfT0= [];
cpu_OfT2= [];

%% regularization parameter

reg_O0= [];
reg_O0d= [];
reg_O2= [];
reg_O2d1= [];
reg_O2d2= [];
reg_OO0= [];
reg_OO0d= [];
reg_OO2= [];
reg_OO2d1= [];
reg_OO2d2= [];

