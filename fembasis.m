function re= fembasis(gp)
% INPUT gp: N by 3 coordinates of N points on the [-1,1]^3
% OUTPUT re: N by 8 values of the eight tri-linear nodal basis functions
x= gp(:,1); y= gp(:,2); z= gp(:,3);
re= [(1-x).*(1-y).*(1-z),(1+x).*(1-y).*(1-z),(1+x).*(1+y).*(1-z),(1-x).*(1+y).*(1-z), ...
    (1-x).*(1-y).*(1+z),(1+x).*(1-y).*(1+z),(1+x).*(1+y).*(1+z),(1-x).*(1+y).*(1+z)]/8;
end