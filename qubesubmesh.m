function [Ro,sdiri,sbdnodes,sbdelems,srobinelems,selems,sbdpos]= ...
    qubesubmesh(GammaD,GammaR,nx,ny,nz,Nx,Ny,Nz,ol,withdiri)
% 
snx= nx/Nx;  sny= ny/Ny;  snz= nz/Nz; Ns= Nx*Ny*Nz; 
Ro= cell(Ns,1); 
snodes= cell(Ns,1);  snodes2= cell(Ns,1);
sdiri= cell(Ns,1);
sbdelems= cell(Ns,1); sbdnodes= cell(Ns,1); sbdpos= cell(Ns,1);
srobinelems= cell(Ns,1);  selems= cell(Ns,1); 
for s= 1:Ns
   [sx,sy]= ind2sub([Nx,Ny*Nz],s); % 3d position of this subdomain
   [sy,sz]= ind2sub([Ny,Nz],sy); 
   ix= ((sx-1)*snx-(sx>1)*ol+1):(sx*snx+(sx<Nx)*ol+1); % 3d position 
   iy= ((sy-1)*sny-(sy>1)*ol+1):(sy*sny+(sy<Ny)*ol+1); % of global nodes
   iz= ((sz-1)*snz-(sz>1)*ol+1):(sz*snz+(sz<Nz)*ol+1);
   nxe= length(ix)-1; nye= length(iy)-1; nze= length(iz)-1;
   [IX,IY,IZ]= ndgrid(ix,iy,iz);  
   clear ix iy iz; 
   snodes{s}= sub2ind([nx+1,ny+1,nz+1],IX(:),IY(:),IZ(:));
   clear IX IY IZ;
   num_sdiri= 0; 
   if sx==1 
       if find(1==GammaD,1)
          num_sdiri= num_sdiri + (nye+1)*(nze+1);
       end
   end
   if sx==Nx 
       if find(2==GammaD,1)
          num_sdiri= num_sdiri + (nye+1)*(nze+1);
       end
   end
   if sy==1 
       if find(3==GammaD,1)
          num_sdiri= num_sdiri + (nxe+1)*(nze+1);
       end
   end
   if sy==Ny 
       if find(4==GammaD,1)
          num_sdiri= num_sdiri + (nxe+1)*(nze+1);  
       end
   end
   if sz==1 
       if find(5==GammaD,1)
          num_sdiri= num_sdiri + (nxe+1)*(nye+1); 
       end
   end
   if sz==Nz 
       if find(6==GammaD,1)
          num_sdiri= num_sdiri + (nxe+1)*(nye+1);
       end
   end
   sdiri{s}= zeros(num_sdiri,1);  num_sdiri= 0; 
   bdnodes= qubebdmesh(nxe,nye,nze);
   if sx==1 
       if find(1==GammaD,1)
          sdiri{s}(num_sdiri+1:num_sdiri+(nye+1)*(nze+1))= snodes{s}(bdnodes{1});
          num_sdiri= num_sdiri + (nye+1)*(nze+1);
       end
   end
   if sx==Nx 
       if find(2==GammaD,1)
          sdiri{s}(num_sdiri+1:num_sdiri+(nye+1)*(nze+1))= snodes{s}(bdnodes{2});
          num_sdiri= num_sdiri + (nye+1)*(nze+1);
       end
   end
   if sy==1 
       if find(3==GammaD,1)
          sdiri{s}(num_sdiri+1:num_sdiri+(nxe+1)*(nze+1))= snodes{s}(bdnodes{3});
          num_sdiri= num_sdiri + (nxe+1)*(nze+1);
       end
   end
   if sy==Ny 
       if find(4==GammaD,1)
          sdiri{s}(num_sdiri+1:num_sdiri+(nxe+1)*(nze+1))= snodes{s}(bdnodes{4});
          num_sdiri= num_sdiri + (nxe+1)*(nze+1);
       end
   end
   if sz==1 
       if find(5==GammaD,1)
          sdiri{s}(num_sdiri+1:num_sdiri+(nxe+1)*(nye+1))= snodes{s}(bdnodes{5});
          num_sdiri= num_sdiri + (nxe+1)*(nye+1);
       end
   end
   if sz==Nz 
       if find(6==GammaD,1)
          sdiri{s}(num_sdiri+1:num_sdiri+(nxe+1)*(nye+1))= snodes{s}(bdnodes{6});
       end
   end
   sdiri{s}= unique(sdiri{s}); 
   if ~exist('withdiri','var') || withdiri==0
       snodes2{s}= setdiff(snodes{s},sdiri{s});
       Ro{s}= sparse(1:length(snodes2{s}),snodes2{s},1,length(snodes2{s}), ...
          (nx+1)*(ny+1)*(nz+1),length(snodes2{s}));
   else
       Ro{s}= sparse(1:length(snodes{s}),snodes{s},1,length(snodes{s}), ...
          (nx+1)*(ny+1)*(nz+1),length(snodes{s}));
   end
end

if nargout >= 3

for s= 1:Ns
    [sx,sy]= ind2sub([Nx,Ny*Nz],s); % 3d position of this subdomain
    [sy,sz]= ind2sub([Ny,Nz],sy); 
    nxe= snx + (sx>1)*ol + (sx<Nx)*ol;
    nye= sny + (sy>1)*ol + (sy<Ny)*ol;
    nze= snz + (sz>1)*ol + (sz<Nz)*ol;
    num_bdnodes= 0; num_sbdelems= 0; num_robinelems= 0; 
    if sx>1
       num_bdnodes= num_bdnodes + (nye+1)*(nze+1);
       num_sbdelems= num_sbdelems + nye*nze;
    elseif find(1==GammaR,1)
       num_robinelems= num_robinelems + nye*nze;
    end
    if sx<Nx
       num_bdnodes= num_bdnodes + (nye+1)*(nze+1);
       num_sbdelems= num_sbdelems + nye*nze;
    elseif find(2==GammaR,1)
       num_robinelems= num_robinelems + nye*nze;
    end
    if sy>1
       num_bdnodes= num_bdnodes + (nxe+1)*(nze+1);
       num_sbdelems= num_sbdelems + nxe*nze;
    elseif find(3==GammaR,1)
       num_robinelems= num_robinelems + nxe*nze;
    end
    if sy<Ny
       num_bdnodes= num_bdnodes + (nxe+1)*(nze+1);
       num_sbdelems= num_sbdelems + nxe*nze;
    elseif find(4==GammaR,1)
       num_robinelems= num_robinelems + nxe*nze;  
    end
    if sz>1
       num_bdnodes= num_bdnodes + (nxe+1)*(nye+1);
       num_sbdelems= num_sbdelems + nxe*nye;
    elseif find(5==GammaR,1)
       num_robinelems= num_robinelems + nxe*nye;    
    end
    if sz<Nz
       num_bdnodes= num_bdnodes + (nxe+1)*(nye+1);
       num_sbdelems= num_sbdelems + nxe*nye;
    elseif find(6==GammaR,1)
       num_robinelems= num_robinelems + nxe*nye;
    end
    sbdnodes{s}= zeros(num_bdnodes,1);
    sbdelems{s}= zeros(num_sbdelems,4);  sbdpos{s}= zeros(num_sbdelems,4);
    srobinelems{s}= zeros(num_robinelems,1);
    num_bdnodes= 0; num_sbdelems= 0; num_robinelems= 0; num_grobin= 0;
    [bdnodes,bdelems]= qubebdmesh(nxe,nye,nze);
    if sx>1
       sbdnodes{s}(num_bdnodes+1:num_bdnodes+(nye+1)*(nze+1))= snodes{s}(bdnodes{1});
       num_bdnodes= num_bdnodes + (nye+1)*(nze+1);
       sbdelems{s}(num_sbdelems+1:num_sbdelems+nye*nze,:)= snodes{s}(bdelems{1});
       sbdpos{s}(num_sbdelems+1:num_sbdelems+nye*nze)= ones(nye*nze,1); 
       num_sbdelems= num_sbdelems + nye*nze;
    elseif find(1==GammaR,1)
       iy= ((sy-1)*sny-(sy>1)*ol+1):(sy*sny+(sy<Ny)*ol); % 2d position
       iz= ((sz-1)*snz-(sz>1)*ol+1):(sz*snz+(sz<Nz)*ol); % on face 1
       [IY,IZ]= ndgrid(iy,iz); 
       srobinelems{s}(num_robinelems+1:num_robinelems+nye*nze)= ...
           sub2ind([ny,nz],IY(:),IZ(:));
       num_robinelems= num_robinelems + nye*nze;
    end
    if find(1==GammaR,1)
       num_grobin= num_grobin + ny*nz;
    end
    if sx<Nx
       sbdnodes{s}(num_bdnodes+1:num_bdnodes+(nye+1)*(nze+1))= snodes{s}(bdnodes{2});
       num_bdnodes= num_bdnodes + (nye+1)*(nze+1);
       sbdelems{s}(num_sbdelems+1:num_sbdelems+nye*nze,:)= snodes{s}(bdelems{2});
       sbdpos{s}(num_sbdelems+1:num_sbdelems+nye*nze)= 2*ones(nye*nze,1);
       num_sbdelems= num_sbdelems + nye*nze;   
    elseif find(2==GammaR,1)
       iy= ((sy-1)*sny-(sy>1)*ol+1):(sy*sny+(sy<Ny)*ol); % 2d position
       iz= ((sz-1)*snz-(sz>1)*ol+1):(sz*snz+(sz<Nz)*ol); % on face 2
       [IY,IZ]= ndgrid(iy,iz); 
       srobinelems{s}(num_robinelems+1:num_robinelems+nye*nze)= ...
           sub2ind([ny,nz],IY(:),IZ(:)) + num_grobin;
       num_robinelems= num_robinelems + nye*nze;
    end
    if find(2==GammaR,1)
       num_grobin= num_grobin + ny*nz;
    end
    if sy>1
       sbdnodes{s}(num_bdnodes+1:num_bdnodes+(nxe+1)*(nze+1))= snodes{s}(bdnodes{3});
       num_bdnodes= num_bdnodes + (nxe+1)*(nze+1);
       sbdelems{s}(num_sbdelems+1:num_sbdelems+nxe*nze,:)= snodes{s}(bdelems{3});
       sbdpos{s}(num_sbdelems+1:num_sbdelems+nxe*nze)= 3*ones(nxe*nze,1);
       num_sbdelems= num_sbdelems + nxe*nze;   
    elseif find(3==GammaR,1)
       ix= ((sx-1)*snx-(sx>1)*ol+1):(sx*snx+(sx<Nx)*ol); % 2d position  
       iz= ((sz-1)*snz-(sz>1)*ol+1):(sz*snz+(sz<Nz)*ol); % on face 3
       [IX,IZ]= ndgrid(ix,iz); 
       srobinelems{s}(num_robinelems+1:num_robinelems+nxe*nze)= ...
           sub2ind([nx,nz],IX(:),IZ(:)) + num_grobin;
       num_robinelems= num_robinelems + nxe*nze;
    end
    if find(3==GammaR,1)
       num_grobin= num_grobin + nx*nz;
    end
    if sy<Ny
       sbdnodes{s}(num_bdnodes+1:num_bdnodes+(nxe+1)*(nze+1))= snodes{s}(bdnodes{4});
       num_bdnodes= num_bdnodes + (nxe+1)*(nze+1);
       sbdelems{s}(num_sbdelems+1:num_sbdelems+nxe*nze,:)= snodes{s}(bdelems{4});
       sbdpos{s}(num_sbdelems+1:num_sbdelems+nxe*nze)= 4*ones(nxe*nze,1);
       num_sbdelems= num_sbdelems + nxe*nze;   
    elseif find(4==GammaR,1)
       ix= ((sx-1)*snx-(sx>1)*ol+1):(sx*snx+(sx<Nx)*ol); % 2d position  
       iz= ((sz-1)*snz-(sz>1)*ol+1):(sz*snz+(sz<Nz)*ol); % on face 4
       [IX,IZ]= ndgrid(ix,iz); 
       srobinelems{s}(num_robinelems+1:num_robinelems+nxe*nze)= ...
           sub2ind([nx,nz],IX(:),IZ(:)) + num_grobin;
       num_robinelems= num_robinelems + nxe*nze;
    end
    if find(4==GammaR,1)
       num_grobin= num_grobin + nx*nz;
    end
    if sz>1
       sbdnodes{s}(num_bdnodes+1:num_bdnodes+(nxe+1)*(nye+1))= snodes{s}(bdnodes{5});
       num_bdnodes= num_bdnodes + (nxe+1)*(nye+1);
       sbdelems{s}(num_sbdelems+1:num_sbdelems+nye*nxe,:)= snodes{s}(bdelems{5});
       sbdpos{s}(num_sbdelems+1:num_sbdelems+nxe*nye)= 5*ones(nxe*nye,1);
       num_sbdelems= num_sbdelems + nxe*nye;   
    elseif find(5==GammaR,1)
       ix= ((sx-1)*snx-(sx>1)*ol+1):(sx*snx+(sx<Nx)*ol); % 2d position 
       iy= ((sy-1)*sny-(sy>1)*ol+1):(sy*sny+(sy<Ny)*ol); % on face 5
       [IX,IY]= ndgrid(ix,iy); 
       srobinelems{s}(num_robinelems+1:num_robinelems+nxe*nye)= ...
           sub2ind([nx,ny],IX(:),IY(:)) + num_grobin;
       num_robinelems= num_robinelems + nxe*nye;
    end
    if find(5==GammaR,1)
       num_grobin= num_grobin + nx*ny;
    end
    if sz<Nz
       sbdnodes{s}(num_bdnodes+1:num_bdnodes+(nxe+1)*(nye+1))= snodes{s}(bdnodes{6});
       sbdelems{s}(num_sbdelems+1:num_sbdelems+nye*nxe,:)= snodes{s}(bdelems{6});
       sbdpos{s}(num_sbdelems+1:num_sbdelems+nxe*nye)= 6*ones(nxe*nye,1);
    elseif find(6==GammaR,1)
       ix= ((sx-1)*snx-(sx>1)*ol+1):(sx*snx+(sx<Nx)*ol); % 2d position 
       iy= ((sy-1)*sny-(sy>1)*ol+1):(sy*sny+(sy<Ny)*ol); % on face 6
       [IX,IY]= ndgrid(ix,iy); 
       srobinelems{s}(num_robinelems+1:num_robinelems+nxe*nye)= ...
           sub2ind([nx,ny],IX(:),IY(:)) + num_grobin;
    end
    clear ix iy iz IX IY IZ;
    if nargout >= 6
        ix= ((sx-1)*snx-(sx>1)*ol+1):(sx*snx+(sx<Nx)*ol); % 3d position 
        iy= ((sy-1)*sny-(sy>1)*ol+1):(sy*sny+(sy<Ny)*ol); % of global elements
        iz= ((sz-1)*snz-(sz>1)*ol+1):(sz*snz+(sz<Nz)*ol);
        [IX,IY,IZ]= ndgrid(ix,iy,iz); 
        selems{s}= sub2ind([nx,ny,nz],IX(:),IY(:),IZ(:));
    end
    clear ix iy iz IX IY IZ;
end

end