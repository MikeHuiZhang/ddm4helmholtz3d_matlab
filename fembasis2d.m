function  re= fembasis2d(gp)
% INPUT gp: N by 2 coordinates of N points on the [-1,1]^2
% OUTPUT re: N by 4 values of the four bi-linear nodal basis functions
x= gp(:,1); y= gp(:,2);
re= [(1-x).*(1-y),(1+x).*(1-y),(1+x).*(1+y),(1-x).*(1+y)]/4;
end