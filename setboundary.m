function [diri,robin]= setboundary(GammaD,GammaR,bdnodes,bdelems)
%This function set up dirichlet nodes and robin boundary
% elements for a domain given face numbers 1~6 in GammaD, GammaR,
% and given all boundary nodes and boundary elements. The output diri nodes
% are on faces of GammaD. The output robin boundary elements are on faces
% of GammaR. 
%
ndiri= 0;
for j=1:length(GammaD)
    ndiri= ndiri+ size(bdnodes{GammaD(j)},1);
end
diri= zeros(ndiri,1,'uint32');
ndiri= 0;
for j=1:length(GammaD)
    diri(ndiri+1:ndiri+size(bdnodes{GammaD(j)},1))= bdnodes{GammaD(j)};
    ndiri= ndiri+size(bdnodes{GammaD(j)},1);
end
diri= unique(diri);
nrobin= 0;
for j=1:length(GammaR)
    nrobin= nrobin+ size(bdelems{GammaR(j)},1);
end
robin= zeros(nrobin,4,'uint32');
nrobin= 0;
for j=1:length(GammaR)
    robin(nrobin+1:nrobin+size(bdelems{GammaR(j)},1),:)= bdelems{GammaR(j)};
    nrobin= nrobin+size(bdelems{GammaR(j)},1);
end