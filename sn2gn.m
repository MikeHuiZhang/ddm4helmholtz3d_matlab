function gnodes= sn2gn(snodes,nx,ny,nz,Nx,Ny,Nz,s)

% 3d position of this subdomain
[sx,sy]= ind2sub([Nx,Ny*Nz],s);         
[sy,sz]= ind2sub([Ny,Nz],sy);

% 3d position of nodes in this subdomain
snx= nx/Nx;  sny= ny/Ny;  snz= nz/Nz;
[ixs,iys]= ind2sub([snx+1,(sny+1)*(snz+1)],snodes);  
[iys,izs]= ind2sub([sny+1,snz+1],iys);

% 3d position of nodes in the whole domain
ix= ixs + (sx-1)*snx;
iy= iys + (sy-1)*sny;
iz= izs + (sz-1)*snz;

% 1d position of nodes in the whole domain
gnodes= ix + (iy-1)*(nx+1) + (iz-1)*(nx+1)*(ny+1);