function helmholtz_local()

% The Helmholtz problem is
%    -\lap u - k^2 u = f,   in \Omega
%                 u = uD,   on \Gamma_D
%       du/dn + a u = uR,   on \Gamma_R
% where a can be zero for Neumann boundary.


clear all; % important to clear 'global' variables
% equation: example 1 constant wavenumber
% k= 1; % wavenumber
% k2= k^2;  
% a= -1i*k; % absorbing coefficient 
% f= 0; 
% pointsource= [0.3,0.3,0.3,1]; % source
% % pointsource= [];
% uD= 1; % Dirichlet boundary value
% uR= 0; % absorbing boundary value
% exmaple 2 variable wavenumber
Lz= 4200; Ly= 13520; Lx= 13520;
% wnz= 210;  wny= 676; wnx= 676; 
% wnz= 42; wny= 169; wnx= 169;
% wnz= 14; wny= 52; wnx= 52;
% datafile= sprintf('data/seg%d_%d_%d',wnz,wny,wnx);
% freq= 1;
% omega= 2*pi*freq;
% k= @(x)(wavenumber(x,datafile,wnz,wny,wnx,Lz,Ly,Lx,omega));
% k2= @(x)((k(x)).^2);
% a= @(x)(-1i*k(x));
f= 0;  pointsource= [6000,6760,10,1];
uD= 0;
uR= 0;
% example 3
% Lz= 4200; Ly= 13520; Lx= 13520;
% freq= 0.5; 
% omega= 2*pi*freq;
% k= omega/2296.2;
% k2= k^2;
% a= -1i*k;
% f= 0;  pointsource= [6000,6760,10,1];
% uD= 0;
% uR= 0;
% manufactured example 1
% uexact= @(x)(cos(2*pi*x(:,1)).*cos(2*pi*x(:,2)).*cos(2*pi*x(:,3)));
% % uexact= @(x)(exp(-2*x(:,1)).*cos(2*pi*x(:,2)).*cos(2*pi*x(:,3)));
% % k= @(x)(x(:,1).^4+1); %(4*ones(size(x(:,1)))); %
% k= @(x)(x(:,2).^(1/2)+x(:,1).^(1/4)+x(:,3).^3+3);
% k2= @(x)(k(x).^2); 
% a= @(x)(-1i*k(x));
% f= @(x)((12*pi^2-k2(x)).*uexact(x));
% % f= @(x)((8*pi^2-4-k2(x)).*uexact(x)); % change this along with uexact
% pointsource= [];
% uD= uexact;
% uR= @(x)(a(x).*uexact(x)); % valid for du/dn=0
% manufactured example 2
% uexact= @(x)(cos(2*pi*x(:,1)).*cos(2*pi*x(:,2)).*cos(2*pi*x(:,3)));
% k= 4; 
% k2= k^2;
% a= -1i*k;
% f= @(x)((12*pi^2-k2).*uexact(x)); % change this along with uexact
% pointsource= [];
% uD= uexact;
% uR= @(x)(a*uexact(x)); % valid for du/dn=0
% manufactured example 3
% uexact= @(x)(exp(-x(:,1))+1);
% k= 4; 
% k2= k^2;
% a= 1;
% f= @(x)(-exp(-x(:,1))-k2.*uexact(x)); % change this along with uexact
% pointsource= [];
% uD= uexact;
% uR= 1; % valid for with robin only on 2 (right face)
% manufactured example 4
% k= 4;
% k2= k^2;
% pointsource= [0.3,0.3,0.3,1]; 
% dist2src= @(x)(sqrt((x(:,1)-0.3).^2+(x(:,2)-0.3).^2+(x(:,3)-0.3).^2));
% uexact= @(x)(cos(k*dist2src(x))./dist2src(x)/4/pi);
% a= 0;
% f= 0;
% uD= uexact;
% uR= 0;  % invalid: we should use no Robin condition

% geometry: continuous description of the qube domain and boundary
if exist('Lx','var')
    xl= 0;  xr= Lx;
    yl= 0;  yr= Ly;
    zl= 0;  zr= Lz;
else
    xl= 0; xr= 1;
    yl= 0; yr= 1;
    zl= 0; zr= 1;
end
GammaD= []; % 1 left, 2 right, 3 front, 4 back, 5 bottom, 6 top 
GammaR= [1,2,3,4,5,6];
% GammaD= @(x,y,z)(x*y*z); % Dirichlet boundary is GammaD = 0
% GammaR= @(x,y,z)((x-1)*(y-1)*(z-1)); % Robin boundary is GammaR = 0

% discretization parameter
% nx= 20; ny= 20; nz= 20;
nx= 48; ny= 48; nz= 16;
% hx= (xr-xl)/nx; hy= (yr-yl)/ny; hz= (zr-zl)/nz;

% fem direct solution on the whole mesh 
% [coordinates,elements,dirichlet,robin]= qubemesh(xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR);
% disp('fem');
% tic
% % udirect= femdirect(k2,f,pointsource,uD,a,uR,coordinates,elements,dirichlet,robin);
% udirect= femdirect(@ksquare,f,pointsource,uD,@arobin,uR,coordinates,elements,dirichlet,robin);
% % clear coordinates elements dirichlet robin;
% toc
femsol= load('fem_m8_fhalf_h10.mat');
udirect= femsol.udirect;
clear femsol;
% global gp gw; 
% if (exist('uexact','var'))
%     errfem2cont= uexact(coordinates) - udirect;
%     maxerrfem2cont= max(abs(errfem2cont)); % compare to uexact
%     disp('maxerrfem2cont='); disp(maxerrfem2cont); 
% %     pseudol2errfem2cont= sqrt((Mass*errfem2cont).'*errfem2cont);
% %     disp('mass matrix norm of errfem2cont is');
% %     disp(pseudol2errfem2cont);
%      errfem2contL2= 0;
%      for j = 1:size(elements,1)
%          vertices= coordinates(elements(j,:),:);
%          DPhi= [vertices(2,:)-vertices(1,:);vertices(4,:)-vertices(1,:);vertices(5,:)-vertices(1,:)]'/2;
%          detDPhi= det(DPhi);
%          ufem= fembasis(gp)*udirect(elements(j,:));
%          DPhigp= (1+gp)*DPhi'+repmat(vertices(1,:),size(gp,1),1);
%          errfem2contL2= errfem2contL2 + gw'*abs(uexact(DPhigp)-ufem).^2*detDPhi;
%      end
%      errfem2contL2= sqrt(errfem2contL2);
%      disp('errfem2contL2');
%      disp(errfem2contL2);
% %     errfem2cont= reshape(errfem2cont,nx+1,ny+1,nz+1);
% %     [X,Y,Z]= meshgrid(xl:hx:xr,yl:hy:yr,zl:hz:zr);
% %     slice(X,Y,Z,errfem2cont, 0.5, 0.5, 0.5);
% %     xlabel('x'); ylabel('y'); zlabel('z');
% %     colorbar; 
% %     figure;
% %     [X2,Y2]= ndgrid(xl:hx:xr,yl:hy:yr);
% %     for iz= 1:nz+1
% %         surf(X2,Y2,errfem2cont(:,:,iz)); xlabel('x'); ylabel('y');
% %         title(['z=',num2str((iz-1)*hz)]);
% %         pause;
% %     end
% end

% ddm solution
Nxv= [6];  Nyv= [6]; Nzv= [2];
for numrun= 1:length(Nxv)
    
% partition to Nx X Ny X Nz subdomains, fetidp require Nx > freq*Lx/c/sqrt(2)/pi
Nx= Nxv(numrun); Ny= Nyv(numrun); Nz= Nzv(numrun);  
freq= 0.5;
disp('spd is satisfied for c>');
disp(2*freq/sqrt((Nx/(xr-xl))^2 + (Ny/(yr-yl))^2 + (Nz/(zr-zl))^2));
disp('for c= 1500, H should be smaller than');
disp(sqrt(3)*pi/2/pi/freq*1500);
disp('for c= 1500, h should be smaller than');
disp(pi/5/(2*pi*freq)*1500);
ntheta= [-1 0];  nphi= [0 4];  % number of plane waves is 2*ntheta*nphi+nphi+2, or 0 when ntheta=-1
tdd= tic;
% [udd,flag,relres,iter,resvec]= fetidp(@ksquare,f,pointsource,uD,@arobin,uR,xl,xr,yl,yr,zl,zr,...
%     GammaD,GammaR,nx,ny,nz,Nx,Ny,Nz,ntheta,nphi,1,2);
% [udd,flag,relres,iter,resvec]= nos(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,...
%     GammaD,GammaR,nx,ny,nz,Nx,Ny,Nz,ntheta,nphi,1,2);
ol= 1; 
% wl= 2; cl= 2; zerol= ol+1;
% [udd,flag,relres,iter,resvec,specF]= obdd(@ksquare,f,pointsource,uD,@arobin,uR, ...
%     xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR,Nx,Ny,Nz,ol,wl,cl,ntheta,nphi);
% [udd,flag,relres,iter,resvec,specF]= obdd(k2,f,pointsource,uD,a,uR, ...
%     xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR,Nx,Ny,Nz,ol,wl,cl,ntheta,nphi);
% [udd,flag,relres,iter,resvec]= obddsubs(k2,f,pointsource,uD,a,uR, ...
%     xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR,Nx,Ny,Nz,ol,ntheta,nphi);
[udd,flag,relres,iter,resvec]= obddsubs(@ksquare,f,pointsource,uD,@arobin,uR, ...
    xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR,Nx,Ny,Nz,ol,ntheta,nphi);
postprocess();
[udd,flag,relres,iter,resvec]= oossubs(@ksquare,f,pointsource,uD,@arobin,uR, ...
    xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR,Nx,Ny,Nz,ol,ntheta,nphi,2);
postprocess();
[udd,flag,relres,iter,resvec]= schwarzsubs(@ksquare,f,pointsource,uD,@arobin,uR, ...
    xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR,Nx,Ny,Nz,ol,ntheta,nphi);
postprocess();
[udd,flag,relres,iter,resvec]= oossubs(@ksquare,f,pointsource,uD,@arobin,uR, ...
    xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR,Nx,Ny,Nz,ol,ntheta,nphi,0);
postprocess();
[udd,flag,relres,iter,resvec]= obddsubs(@ksquare,f,pointsource,uD,@arobin,uR, ...
    xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR,Nx,Ny,Nz,ol,ntheta,nphi,1);
postprocess();
disp('total dd time');
toc(tdd);

end

    function postprocess()
        if ~isempty(resvec)
            figure;
            semilogy(resvec);
            xlabel('iteration');
            ylabel('residual');
            disp('relres='); disp(relres);
        end
        if ~isempty(udd)
            disp('iter='); disp(iter);
            switch flag
                case 1
                    disp('gmres reached maximum iteration number but did not converge');
                case 2
                    disp('preconditioner M is ill-conditioned');
                case 3
                    disp('gmres stagnated (two consective iterates were the same)');
            end
        end
        if (exist('udirect','var') && ~isempty(udd))
            errdd2fem= abs(udd - udirect);
            maxerrdd2fem= max(errdd2fem);
            disp('maxerrdd2fem='); disp(maxerrdd2fem);
            %     [X,Y,Z]= meshgrid(xl:hx:xr,yl:hy:yr,zl:hz:zr);
            %     errdd2fem= reshape(errdd2fem,nx+1,ny+1,nz+1);
            %     slice(X,Y,Z,errdd2fem, 0.5, 0.5, 0.5);
            %     xlabel('x'); ylabel('y'); zlabel('z');
            %     colorbar;
            %     figure;
            %     [X2,Y2]= ndgrid(xl:hx:xr,yl:hy:yr);
            %     surf(X2,Y2,errdd2fem(:,:,nz/2+1)); xlabel('x'); ylabel('y');
        end
        if exist('uexact','var') && exist('coordinates','var')
            errfem2cont= uexact(coordinates) - udd;
            maxerrfem2cont= max(abs(errfem2cont)); % compare to uexact
            disp('maxerrudd2cont='); disp(maxerrfem2cont);
        end
        if (exist('specF','var')) && ~isempty(specF)
            figure;
            plot(specF,'o','MarkerSize',6);
        end
    end

end  % -- end of helmholtz