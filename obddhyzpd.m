function [u,flag,relres,iter,resvec,specF]= ...
    obddhyzpd(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR, ...
           Nx,Ny,Nz,ol,wl,cl,ntheta,nphi,zerol)
% INPUT
% coordinates, elements: we assume they are generated from qubemesh
% Nx, Ny, Nz: nonoverlapping partition to subdomains
% ol:  number of outside layers for overlapping extension
% wl:  number of outside layers for weighting
% cl:  number of outside layers for coarse functions
% Remark 
% we assume ol, wl and cl are not so large that overlapping
% subdomains communicate with at most 26  neighbors.
%% partition of the global mesh into overlapping submeshes 
[coordinates,elements,dirichlet,robin]= qubemesh(xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR);
[selems,sdiri,srobinelems,sbdelems,Ro,Dw,~,~,~,Ri,~, ...
    R0,elems0,diri0,robinelems0,ibdelems] = ...
    qubemeshpart(coordinates,GammaD,GammaR,nx,ny,nz,Nx,Ny,Nz,...
                 ol,wl,cl,ntheta,nphi,k2,zerol);
%% assemble global matrix and rhs 
Ns= Nx*Ny*Nz; nn= (nx+1)*(ny+1)*(nz+1);
A= cell(Ns,1); 
getAM(); getgpw(); % for use by stima.m and rhs.m
getM2d(); getgpw2d(); % for use by stima2d and rhs2d
global unused_src numsrc; % for use by rhs.m
[I,J]= ndgrid(1:8,1:8); I= I(:); J= J(:);
[Ir,Jr]= ndgrid(1:4,1:4); Ir= Ir(:); Jr= Jr(:);
ne= size(elements,1);
ii= zeros(64,ne); jj= zeros(64,ne); Aa= zeros(64,ne);
for j= 1:ne
    Ae= stima(coordinates(elements(j,:),:),k2);
    ii(:,j)= elements(j,I); % assemble vectors
    jj(:,j)= elements(j,J); % for speed
    Aa(:,j)= Ae(:);
end
iib= []; ba= [];
if ~(isa(f,'float') && 0==f && isempty(pointsource))
   iib= zeros(8,ne); ba= zeros(8,ne);
   for j = 1:ne
      n4e= elements(j,:);
      iib(:,j)= n4e;
      ba(:,j)= rhs(coordinates(n4e,:),f,pointsource);
   end
end
unused_src= []; numsrc= [];
% treat outer Robin b.c.
nr= size(robin,1); 
ir= []; jr= []; Ar= [];
if ~(isa(a,'float') && 0==a)
   ir= zeros(16,nr); jr= zeros(16,nr); Ar= zeros(16,nr);
   for j = 1:nr
       ir(:,j)= robin(j,Ir);
       jr(:,j)= robin(j,Jr); 
       Ae= stima2d(coordinates(robin(j,:),:),a);
       Ar(:,j)= Ae(:);
   end
end
irb= []; br= [];
if ~(isa(uR,'float') && 0==uR)
   irb= zeros(4,nr); br= zeros(4,nr);
   for j = 1:nr
      n4e= robin(j,:);
      irb(:,j)= n4e;
      br(:,j)= rhs2d(coordinates(n4e,:),uR);
   end
end
% treat Dirichlet boundary lastly, to avoid Robin change
if isa(uD,'float')
   bd= uD*ones(size(dirichlet,1),1);
elseif isa(uD,'inline') || isa(uD,'function_handle')
   bd = uD(coordinates(dirichlet,:));
end
bb= sparse([iib(:);irb(:);double(dirichlet)],1,[ba(:);br(:);bd],nn,1);
AA= sparse([ii(:);ir(:)],[jj(:);jr(:)],[Aa(:);Ar(:)],nn,nn);
[iiA, jjA]= find(AA);
idxofzero = ismember(iiA,double(dirichlet)); 
AA= setsparse(AA,iiA(idxofzero),jjA(idxofzero),0);
AA= setsparse(AA,double(dirichlet),double(dirichlet),1);
clear iiA jjA iib ba irb br bd idxofzero;
%% assemble subdomains matrices 
for s= 1:Ns
    % treat internal Robin b.c.
    if isa(k2,'function_handle') || isa(k2,'inline')
       reg= @(x)(-sqrt(k2(x))*1i);
    elseif isa(k2,'float')
        reg= -sqrt(k2)*1i;
    end
    nb= size(sbdelems{s},1);
    ib= zeros(16,nb); jb= zeros(16,nb); Ab= zeros(16,nb);
    for j = 1:nb
        ib(:,j)= sbdelems{s}(j,Ir);
        jb(:,j)= sbdelems{s}(j,Jr);
        Ae= stima2d(coordinates(sbdelems{s}(j,:),:),reg); 
        Ab(:,j)= Ae(:);
    end
    % treat Dirichlet boundary lastly, to avoid Robin change
    iis= ii(:,selems{s}); iis= iis(:);
    jjs= jj(:,selems{s}); jjs= jjs(:);
    Aas= Aa(:,selems{s}); Aas= Aas(:);
    if ~isempty(ir)
        irs= ir(:,srobinelems{s}); irs= irs(:);
        jrs= jr(:,srobinelems{s}); jrs= jrs(:);
        Ars= Ar(:,srobinelems{s}); Ars= Ars(:);
    else
        irs= [];
        jrs= [];
        Ars= [];
    end
    Ag= sparse([iis;irs;ib(:)],[jjs;jrs;jb(:)],[Aas;Ars;Ab(:)],nn,nn);
    [iis, jjs]= find(Ag);
    idxofzero = ismember(iis,sdiri{s}); 
    Ag= setsparse(Ag,iis(idxofzero),jjs(idxofzero),0);
    Ag= setsparse(Ag,sdiri{s},sdiri{s},1);
    % restrict matrix to subdomain
    A{s}= Ro{s}*Ag*Ro{s}.';
end
%% assemble the zero-th subdomain
% treat internal Robin b.c.
if isa(k2,'function_handle') || isa(k2,'inline')
   reg= @(x)(-sqrt(k2(x))*1i);
elseif isa(k2,'float')
    reg= -sqrt(k2)*1i;
end
nb= 0;
for s= 1:Ns
    nb= nb + size(ibdelems{s},1);
end
ib= zeros(16,nb); jb= zeros(16,nb); Ab= zeros(16,nb); nb= 0;
for s= 1:Ns
    for j = 1:size(ibdelems{s},1)
        ib(:,nb+j)= ibdelems{s}(j,Ir);
        jb(:,nb+j)= ibdelems{s}(j,Jr);
        Ae= stima2d(coordinates(ibdelems{s}(j,:),:),reg); 
        Ab(:,nb+j)= Ae(:);
    end
    nb= nb + size(ibdelems{s},1);
end
% treat Dirichlet boundary lastly, to avoid Robin change
iis= ii(:,elems0); iis= iis(:);
jjs= jj(:,elems0); jjs= jjs(:);
Aas= Aa(:,elems0); Aas= Aas(:);
if ~isempty(ir)
    irs= ir(:,robinelems0); irs= irs(:);
    jrs= jr(:,robinelems0); jrs= jrs(:);
    Ars= Ar(:,robinelems0); Ars= Ars(:);
else
    irs= [];
    jrs= [];
    Ars= [];
end
Ag= sparse([iis;irs;ib(:)],[jjs;jrs;jb(:)],[Aas;Ars;Ab(:)],nn,nn);
[iis, jjs]= find(Ag);
idxofzero = ismember(iis,diri0); 
Ag= setsparse(Ag,iis(idxofzero),jjs(idxofzero),0);
Ag= setsparse(Ag,diri0,diri0,1);
% restrict matrix to subdomain
A0= R0*Ag*R0.';
clear Ag;
clear ii jj Aa ir jr Ar ib jb Ab iib ba irb br;
clear iis jjs Aas irs jrs Ars iibs bas irbs brs;
Ai= cell(Ns,1);
for s= 1:Ns
    Ai{s}= Ri{s}*AA*Ri{s}';
end
%% LU factorization of subdomain and coarse problems
L= cell(Ns,1); U= cell(Ns,1); P= cell(Ns,1); Q= cell(Ns,1);
for s= 1:Ns
   [L{s},U{s},P{s},Q{s}]= lu(A{s});
   A{s}= [];
end
clear A;
[Lzero,Uzero,Pzero,Qzero]= lu(A0);
clear A0;
Li= cell(Ns,1); Ui= cell(Ns,1); Pi= cell(Ns,1); Qi= cell(Ns,1);
for s= 1:Ns
    [Li{s},Ui{s},Pi{s},Qi{s}]= lu(Ai{s});
    Ai{s}= [];
end
clear Ai;
%% preconditioned system operator 
%  T:= (I-T0)(T1 + ... +T_Ns)(I-T0), the latter (I-T0) can be omitted
    function pxx= T(xx)
        % subdomain solve
        AAxx= AA*xx; pxx= sparse(size(xx,1),1);
        for ms= 1:Ns
           pxx= pxx + Dw{ms}.*(Ro{ms}.'*(Q{ms} * (U{ms}\(L{ms}\(P{ms}*(Ro{ms}*AAxx))))));
        end
        % interior correction
        AAxx= AAxx-AA*pxx;
        for ms= 1:Ns
           pxx= pxx + Ri{ms}'*(Qi{ms} * (Ui{ms}\(Li{ms}\(Pi{ms}*(Ri{ms}*AAxx))))); 
        end
        % subdomain zero correction
        pxx= pxx - R0.'*(Qzero * (Uzero\(Lzero\(Pzero*(R0*(AA*pxx)))))); 
    end

%% preconditioned system rhs: initial coarse and subdomain solves
% subdomain zero correction
u0= R0.'*(Qzero * (Uzero\(Lzero\(Pzero*(R0*bb)))));
% subdomain solve
res0= bb - AA*u0;
g= sparse(size(bb,1),1);
for s= 1:Ns
   g= g + Dw{s}.*(Ro{s}.'*(Q{s} * (U{s}\(L{s}\(P{s}*(Ro{s}*res0))))));
end
% interior correction
gold= res0 - AA*g;
for s= 1:Ns
   g= g + Ri{s}'*(Qi{s} * (Ui{s}\(Li{s}\(Pi{s}*(Ri{s}*gold)))));  
end
% subdomain zero correction
g= g - R0.'*(Qzero * (Uzero\(Lzero\(Pzero*(R0*(AA*g))))));
%% GMRES iteration
tol= 1e-6;  restart= [];  maxit= 50; 
[u,flag,relres,iter,resvec] = gmres(@T,g,restart,tol,maxit,[],[],sparse(size(bb,1),1)); 
u= u + u0;
% test direct solve
% flag= 0; relres= []; iter= 0; resvec=[]; 
% u= AA\bb;
%% computing spectra
specF= [];
end
