function [gbs2sbs,sbs2gbs,ngc,gcsend,scs2gcs,ngb,gbsend,gbsma] ...
    = gbc(scoord,sic,sbsend,sbnb,scsend,scnb,Ns,gtol)
%    gbsend(:)  end positions of boundary segments in global inner boundary
%     gbsma(:)  the master subdomain of this global boundary segment
%   gbs2sbs{s}(:)  segment number from global boundary to s subdomain
% sbs2gbs{s}()  numbering of boundary segments from local to global
%    gcsend(:)  end positions of cross segements in global cross
% scs2gcs{s}()  numbering of cross segments from local to global
% we assume 
% (i)  'b' segment is intersection of a unique pair of subdomains
% (ii) for each segment all subdomans have nodes in the same order and
%      one node belongs to one and only one segment

% initialization
ngb= 0; ngc= 0; % current positin in gb and gc
gbsend= []; gcsend= [];
nbs= 0; ncs= 0; % current number of segments in gb and gc
sbs2gbs= cell(Ns,1); scs2gcs= cell(Ns,1);
gbs2sbs= cell(Ns,1);

% go through all subdomains and their segments, add to gb and gc
% if among all subdomains sharing this segment the current subdomain has
% the smallest index 
for s= 1:Ns
   if length(sbnb{s})~=length(sbsend{s})
      display('gbc: sbnb isnot the same long as sbsend');
      return;
   end
   if size(scnb{s},1)~=length(scsend{s})
      display('gbc: row size of scnb isnot the same as scsend');
      return;
   end
   % b: interior of boundary faces
   for jj= 1:length(sbsend{s})
       snb= sbnb{s}(jj);
       if snb>s
           if 1==jj
              ngb= ngb + sbsend{s}(1);
           else
              ngb= ngb + sbsend{s}(jj) - sbsend{s}(jj-1);
           end
           gbsend= [gbsend, ngb];
           nbs= nbs + 1;
           gbsma(nbs)= s;
           gbs2sbs{s}(nbs)= jj;
           sbs2gbs{s}(jj)= nbs;
           for kk= 1:length(sbsend{snb})
              % we assume intersection of s and snb is one segment
              if sbnb{snb}(kk)==s  
                 sbs2gbs{snb}(kk)= nbs;
                 gbs2sbs{snb}(nbs)= kk;
                 break;
              end
           end
       end
   end
   % c: cross-points 
   for jj= 1:length(scsend{s}) % go through every segment of subdomain s 
       [row,col,snb]= find(scnb{s}(jj,:));
       if snb>s  % s is the minimum
           if 1==jj
               sstart= 1; % start position of this segment in 'sic{s}'
           else
               sstart= scsend{s}(jj-1)+1;
           end
           % check only the 1st node in this segement, we assume for each
           % segment all subdomans have nodes in the same order and
           % one node belongs to one and only one segment
           coords= scoord{s}(sic{s}(sstart),:); 
           ngc= ngc+ scsend{s}(jj)-sstart+1;
           gcsend= [gcsend, ngc];
           ncs= ncs + 1;
           scs2gcs{s}(jj)= ncs;
           for ll= 1:length(snb)
               s2= snb(ll);
               for kk= 1:length(scsend{s2})
                   if 1==kk
                        sstart2= 1;
                   else
                        sstart2= scsend{s2}(kk-1)+1;
                   end
                   coords2= scoord{s2}(sic{s2}(sstart2),:);
                   if  (coords2-coords < gtol) & (coords2-coords > -gtol)
                      scs2gcs{s2}(kk)= ncs;
                      break;
                   end                      
               end
           end         
       end
   end
end

% end of this function
end     