function helmholtz_simple()

clc;
clear all;
% The Helmholtz problem is
%    -\lap u - k^2 u = f,   in \Omega
%                 u = uD,   on \Gamma_D
%       du/dn + a u = uR,   on \Gamma_R
% where a can be zero for Neumann boundary.


% constant wavenumber
% $$$ k2= (4*pi)^2; 
% $$$ ar= -1i*sqrt(k2);
% $$$ Lx= 1;   Ly= 1;   Lz= 1;
% $$$ pointsource= [0.5,0.5,0.1,1];
% $$$ nx= 20; ny= nx; nz= ny;
% $$$ uD= 0; % Dirichlet boundary value
% $$$ uR= 0; % absorbing boundary value
% SEG-SALT wavenumber
k2= @ksquare; ar= @arobin;
Lx= 13520; Ly= 13520; Lz= 4200;
pointsource= [6760,6760,10,1];
% rand on unit square
% $$$ k2= @ksquare2; ar= @arobin2;
% $$$ Lx= 1; Ly= 1; Lz= 1;
% $$$ pointsource= [.33, .33, .33, 1]

nx= 48; ny= 48; nz= 16;

% data
f= 0;  
uD= 0;
uR= 0;

% geometry: continuous description of the qube domain and boundary
xl= 0;  xr= Lx;
yl= 0;  yr= Ly;
zl= 0;  zr= Lz;
GammaD= []; % 1 left, 2 right, 3 front, 4 back, 5 bottom, 6 top 
GammaR= [1,2,3,4,5,6];

% discretization parameter
hx= (xr-xl)/nx; hy= (yr-yl)/ny; hz= (zr-zl)/nz;

% fem direct solution on the whole mesh 
[coordinates,elements,dirichlet,robin]= qubemesh(xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR);
disp('fem');
tic
[udirect,A,b]= femdirect(k2,f,pointsource,uD,ar,uR,coordinates,elements,dirichlet,robin);
toc
uu= reshape(udirect,nx+1,ny+1,nz+1);
uu= permute(uu,[3,2,1]);
save('~/Codes/Helmholtz/3D/u.mat','uu');
save('~/Codes/Helmholtz/3D/A.mat','A');
save('~/Codes/Helmholtz/3D/b.mat','b');

% ddm solution
% partition to Nx X Ny X Nz subdomains, fetidp require Nx > freq*Lx/c/sqrt(2)/pi    
% $$$ Nx= 3;  Ny= 3;  Nz= 1;
% $$$ % diary(num2str(clock));   
% $$$ ntheta= 0;  nphi= 4;  % number of plane waves is 2*ntheta*nphi+nphi+2, or 0 when ntheta=-1
% $$$ randinit= 0; maxit= 200;
% $$$ tdd= tic;
% $$$ [udd,flag,relres,iter,resvec]= ...
% $$$     fetidp(k2,f,pointsource,uD,ar,uR,xl,xr,yl,yr,zl,zr,GammaD,GammaR,nx,ny,...
% $$$            nz,Nx,Ny,Nz,ntheta,nphi,1,1,randinit,maxit);
% $$$ [udd,flag,relres,iter,resvec]= ...
% $$$     nos(k2,f,pointsource,uD,ar,uR,xl,xr,yl,yr,zl,zr,GammaD,GammaR,nx,ny,nz,...
% $$$         Nx,Ny,Nz,ntheta,nphi,3,2,0,[],randinit,maxit);
% $$$ postprocess();

% $$$ [udd,flag,relres,iter,resvec,reg]= ...
% $$$     oossubs(k2,f,pointsource,uD,ar,uR,xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR, ...
% $$$            Nx,Ny,Nz,1,ntheta,nphi,2,[],randinit,[],3);
% $$$ postprocess();
% $$$ toc(tdd);
% $$$ uddres= real(A*udd-b);
% $$$ disp(['maximum residual = ',num2str(max(uddres))]);
% $$$ uddres= reshape(uddres,nx+1,ny+1,nz+1);
% $$$ figure
% $$$ [X2,Y2]= ndgrid(xl:hx:xr,yl:hy:yr);
% $$$ surf(X2,Y2,uddres(:,:,nz/2)); xlabel('x'); ylabel('y');
% $$$ axis tight
% set(gca,'NextPlot','replacechildren');
% Preallocate the struct array for the struct returned by getframe
% resmov(10) = struct('cdata',[],'colormap',[]);
% Record the movie
% for j = 1:10 
%     [udd,flag,relres,iter,resvec]= ...
%     fetidp(k2,f,pointsource,uD,ar,uR,xl,xr,yl,yr,zl,zr,GammaD,GammaR,nx,ny,...
%            nz,Nx,Ny,Nz,ntheta,nphi,1,2,randinit,maxit);
% %     [udd,flag,relres,iter,resvec]= ...
% %     nos(k2,f,pointsource,uD,ar,uR,xl,xr,yl,yr,zl,zr,GammaD,GammaR,nx,ny,nz,...
% %         Nx,Ny,Nz,ntheta,nphi,1,2,0,[],randinit,j);
%     uddres= real(A*udd-b);
%     uddres= reshape(uddres,nx+1,ny+1,nz+1);
%     surf(X2,Y2,uddres(:,:,nz/2)); xlabel('x'); ylabel('y');
%     resmov(j) = getframe;
% end
% save('resfetil.mat','resmov');
%diary off;

    function postprocess()
        if exist('resvec','var') && ~isempty(resvec)
            figure;
            semilogy(resvec);
            xlabel('iteration');
            ylabel('residual');
            disp('relres='); disp(relres);
        end
        if exist('udd','var') && ~isempty(udd)
            disp('iter='); disp(iter);
            switch flag
                case 1
                    disp('gmres reached maximum iteration number but did not converge');
                case 2
                    disp('preconditioner M is ill-conditioned');
                case 3
                    disp('gmres stagnated (two consective iterates were the same)');
            end
        end
        if (exist('udirect','var') && exist('udd','var') && ~isempty(udd))
            errdd2fem= abs(udd - udirect);
            maxerrdd2fem= max(errdd2fem);
            disp('maxerrdd2fem='); disp(maxerrdd2fem);
            if exist('A','var') && exist('b','var')
                uddres= real(A*udd-b);
                uddres= reshape(uddres,nx+1,ny+1,nz+1);
%                 [X,Y,Z]= meshgrid(xl:hx:xr,yl:hy:yr,zl:hz:zr);
%                 slice(X,Y,Z,uddres, 0.5, 0.5, 0.5);
%                 xlabel('x'); ylabel('y'); zlabel('z');
%                 colorbar;
%                 figure;
% $$$                 [X2,Y2]= ndgrid(xl:hx:xr,yl:hy:yr);
% $$$                 surf(X2,Y2,uddres(:,:,nz/2+1)); xlabel('x'); ylabel('y');
            end
        end
        if exist('uexact','var') && exist('udirect','var') && exist('coordinates','var')
            errfem2cont= uexact(coordinates) - udirect;
            maxerrfem2cont= max(abs(errfem2cont)); % compare to uexact
            disp('maxerrfem2cont='); disp(maxerrfem2cont);
        end
        if (exist('specF','var')) && ~isempty(specF)
            figure;
            plot(specF,'o','MarkerSize',6);
        end       
    end  % -- end of function postprocess
end
