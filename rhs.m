function re= rhs(vertices,f,p)
% INPUT vertices: coordinates of 8 nodes on a brick elment
%       f: nonzero float or functions
%       p: [coordinates of point sources, amptitude] 
% OUTPUT re: 8 by 1 vector, values of (phi_j,f) for eight nodal basis
%            functions.
% DPhi [1+xi;1+et;1+ze] + [x1;y1;z1] maps the canonical brick node to 'vertices'
global gp gw;
global unused_src numsrc;
if isempty(unused_src)
    unused_src= 1:size(p,1);
end
if isempty(numsrc)
    numsrc= size(p,1);
end
re = zeros(8,1);
% repeat for stima.m
DPhi= [vertices(2,:)-vertices(1,:);vertices(4,:)-vertices(1,:);vertices(5,:)-vertices(1,:)]'/2;
detDPhi= det(DPhi);
if isa(f,'float') && 0~=f
    re= re + detDPhi*f*ones(8,1);
elseif isa(f,'inline') || isa(f,'function_handle')
    % values of the eight basis functions at gauss quadrature points
    phi= fembasis(gp);
    % gauss quadrature points on the element of 'vertices'
    DPhigp= (1+gp)*DPhi'+ vertices(ones(size(gp,1),1),:);
    re= re + detDPhi*phi'*(gw.*f(DPhigp));
end
if nargin==3 && numsrc
    cp= (p(unused_src,1:3) - vertices(ones(length(unused_src),1),:))/(DPhi') - 1;
    ii= (abs(cp(:,1))<=1 & abs(cp(:,2))<=1 & abs(cp(:,3))<=1);
    if nnz(ii)
       re= re + (sum(p(unused_src(ii),4*ones(1,8)).*fembasis(cp(ii,:)),1))';
       numsrc= numsrc - sum(ii);
       unused_src= setdiff(unused_src,unused_src(ii));
       disp('got a delta in the current element with vetrices coordinates');
       disp(vertices);
       disp('contrib to rhs is'); disp(re);
    end
end
    
end