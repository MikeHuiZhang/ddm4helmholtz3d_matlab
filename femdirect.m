function [u,A,b]= femdirect(k2,f,pointsource,uD,a,uR,coordinates,elements,dirichlet,robin)
fprintf('\nsetup\n');
tsetup= tic;
% initialization of matrix and rhs, avoid matrix memory changes too much
n= size(elements,1);
ii= zeros(64,n); jj= zeros(64,n); % faster than zeros(n,64)
Aa= zeros(64,n);
[I,J]= ndgrid(1:8,1:8); I= I(:); J= J(:);
b = zeros(size(coordinates,1),1);

% assembly of matrix and rhs without respect to boundary conditions
getAM(); getgpw(); % for use by stima.m and rhs.m
for j = 1:n
   Ae= stima(coordinates(elements(j,:),:),k2);
   ii(:,j)= elements(j,I); % assemble vectors
   jj(:,j)= elements(j,J); % for speed
   Aa(:,j)= Ae(:);
end
global unused_src numsrc;
if ~(isa(f,'float') && 0==f && isempty(pointsource))
   for j = 1:size(elements,1)
      b(elements(j,:)) = b(elements(j,:)) + rhs(coordinates(elements(j,:),:),f,pointsource);
   end
end
unused_src= []; numsrc= [];


% treatment of Robin or Neumann boundary
getM2d(); getgpw2d(); % for use by stima2d and rhs2d
ir= []; jr= []; Ar= [];
if ~(isa(a,'float') && 0==a)
   nr= size(robin,1);
   ir= zeros(16,nr); jr= zeros(16,nr); Ar= zeros(16,nr);
   [Ir,Jr]= ndgrid(1:4,1:4); Ir= Ir(:); Jr= Jr(:);
   for j = 1:nr
       ir(:,j)= robin(j,Ir);
       jr(:,j)= robin(j,Jr); 
       Ae= stima2d(coordinates(robin(j,:),:),a);
       Ar(:,j)= Ae(:);
   end
end
A= sparse([ii(:);ir(:)],[jj(:);jr(:)],[Aa(:);Ar(:)]);
clear ii ir jj jr Aa Ar Ae I J Ir Jr; 
if ~(isa(uR,'float') && 0==uR)
   for j = 1:size(robin,1)
      b(robin(j,:)) = b(robin(j,:)) + rhs2d(coordinates(robin(j,:),:),uR);
   end
end

% treatment of Dirichlet boundary lastly, to avoid Robin change
if isa(uD,'float')
   b(dirichlet)= uD*ones(size(dirichlet,1),1);
elseif isa(uD,'inline') || isa(uD,'function_handle')
   b(dirichlet) = uD(coordinates(dirichlet,:));
end
% takes 3.8 s, transpose for speed
% A= A.';
% for j= 1:size(dirichlet,1) 
%     A(:,dirichlet(j))= 0;  % faster than A(:,dirichlet)= 0 or sparse()
%     A(dirichlet(j),dirichlet(j))= 1;
% end
% % A(:,dirichlet)= sparse(size(A,1),size(dirichlet,1));
% % A(dirichlet,dirichlet)= speye(size(dirichlet,1));
% A= A.';
% use 'sparseset' of Bruno Luong, super fast
[ii, jj]=find(A);
idxofzero = ismember(ii,dirichlet); 
A= setsparse(A,ii(idxofzero),jj(idxofzero),0);
A= setsparse(A,double(dirichlet),double(dirichlet),1);
toc(tsetup);

% solution
fprintf('\nsolve\n');
tsolve= tic;
u= A\b; 
% -- another way, much worse
% x= zeros(size(coordinates,1));
% x(dirichlet)= b(dirichlet);
% freenodes= setdiff(1:size(coordinates,1),dirichlet);
% b= b-A*x;
% b(freenodes)= A(freenodes,freenodes)\b(freenodes);
toc(tsolve);
end