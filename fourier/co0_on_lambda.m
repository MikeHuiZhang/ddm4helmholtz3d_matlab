clear all;
w= 10;
H= 1;   % subdomain size along decomposed x-direction
L= 0.1*H; % overlap 
Hy= H;  % subdomain size along y-direction
kmin= pi/Hy;
J= 100; % number of subdomains along x-direction
k= [];
k= [k kmin:kmin:floor(w/kmin)*kmin];
k= [k floor(w/kmin)*kmin+kmin:kmin:10*w]; 
k= k.';
r= sqrt(k.^2-w^2);
z= cos(2*pi*(1:J)/J);
figure();
for pp= 1:10
    for qq= 0:0
        p= (1+1i)*pp;
        % p= pp + qq*k.^2;
        d= exp(-r*(H+L)).*(p-r).^2 - exp(r*(H+L)).*(p+r).^2;
        a= (p-r).^2.*exp(-r*L) - (p+r).*exp(r*L);
        b= (p.^2-r.^2).*(exp(-r*H)-exp(r*H));
        a= a./d;
        b= b./d;
        delta= a.^2*z.^2 + repmat(-a.^2 + b.^2,1,size(z,2));
        rho= [a*z+sqrt(delta), a*z-sqrt(delta)];
        subplot(2,1,1), plot(0.0001*1i+1-rho(:),'.');
        strleg= 'spectra points for system of Robin wihtout coarse';
        legend(strleg);
        if imag(p)==0
           title(['p=',num2str(pp),'+',num2str(qq),'k^2']);
        else
           title(['p=',num2str(p)]);
        end
        subplot(2,1,2), plot(k,max(abs(rho),[],2));
        legend('convergence rate versus frequency in y');
        xlabel('k_y');
        ylabel('rho');
        pause();
    end
end
