clear all;
w= 10;
L= 0.1;
k= 0:0.1:w-0.1; 
% k= 5:0.1:8;
k= [k w+0.1:0.1:w+40]; 
r= sqrt(k.^2-w^2);
rho= exp(2*r*L);
rho= rho./(rho-1);
plot(conj(rho),'.');
strleg= 'non-overlapping';

% second form by change signs of the preconditioner matrix both second row 
% and second column
rho2= exp(2*r*L);
rho2= [(3*rho2-1)./(exp(r*L)-exp(-r*L)).^2, ...
       (-rho2-1)./(exp(r*L)-exp(-r*L)).^2];
hold on;
%plot(conj(rho2),'g+','MarkerSize',3)];
%strleg= [strleg, 'second form of non-overlapping'];

% third form by using preconditioner from omega0 only
rho3= [exp(-2*r*L)+exp(-r*L)-exp(r*L)-1, exp(-2*r*L)-exp(-r*L)+exp(r*L)-1];
rho3= rho3./repmat((exp(r*L)-exp(-r*L)).^2,1,2);
hold on;
%plot(conj(rho3),'g+');
%strleg= [strleg, 'third form of non-overlapping'];

% overlapping
rho_olp= exp(-2*r*L);
rho_olp= rho_olp./(1-rho_olp);
% olp= plot(conj(rho_olp),'ro');
% strleg= [strleg, 'overlapping'];

% overlapping 2: the middle subdomain is used only for preconditioner
rho_olp2= exp(r*L)+3*exp(-r*L)+3+exp(-2*r*L);
rho_olp2= rho_olp2./(exp(r*L)-exp(-r*L));
plot(rho_olp2, 'r+');
strleg= [strleg, 'overlapping 2'];

legend(strleg);

