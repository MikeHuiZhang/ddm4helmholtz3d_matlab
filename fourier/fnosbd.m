% for all Dirichlet,  x\in [-L,L] decomposed to [-L,0] and [0,L]
k= 9;   
p= 18.5213*(1-1i); 
L= 1/2; basefreq= pi; maxl= 40;
l= zeros((maxl+1)*(maxl+2)/2,1); countl= 0;
for m= 0:maxl
    l(countl+1:countl+maxl+1-m)= (m:maxl).^2+m^2;
    countl= countl + maxl+1-m;
end       
xi2= l*basefreq^2; 
r1= -1i*sqrt(k^2-xi2(xi2<k^2)); % k is higher
r2= sqrt(xi2(xi2>k^2)-k^2); % k is smaller
r= [r1;r2];
specnos= (exp(r*L).*(r-p)+exp(-r*L).*(r+p))./(exp(r*L).*(r+p)+exp(-r*L).*(r-p));
% form 1
specnos1= [1 + specnos; -1 + specnos];
figure;
plot(specnos1,'o');
% form 2
specnos2= [1 + specnos; 1 - specnos];
figure;
plot(specnos2,'o');
