function o0helm3d(w,h,H,pq,dk)
% draw convergence rate for a given p-iq for helmholtz in 3d
% also draw spectral picture

p= pq(1);
q= pq(2);
L= 1; % length of x-direction, needed if consider Robin cond. along x 
basefreq= pi/H;
kmin= sqrt(2)*basefreq;
kmax= sqrt(2)*pi/h;
kl= kmin:dk:w;
kh= w:dk:kmax;
xl= sqrt(w^2-kl.^2);
xh= sqrt(kh.^2-w^2);
figure;
if ~isempty(xl)
    subplot(1,2,1), hold on, plot(kl,gl(xl));
end
plot(kh,gh(xh),'g');
plot(w*ones(11,1),linspace(0,1,11),'--');
plot((w-kmin)*ones(11,1),linspace(0,1,11),'-.');
plot((w+kmin)*ones(11,1),linspace(0,1,11),'-.');

% discrete freq.
n= floor(kmax/basefreq);
[m,n]= ndgrid(1:n,1:n);
k= sqrt(m.^2+n.^2)*basefreq;
k= k(:); k= k(k<=kmax);
kl= k(k<w); kh= k(k>=w);
xl= sqrt(w^2-kl.^2);
xh= sqrt(kh.^2-w^2);
if ~isempty(xl)
    plot(kl,gl(xl),'b*');
end
plot(kh,gh(xh),'r*');

% legend('cont. low','cont.high','w','dis. low','dis. high');

% spectral picture
r1= -1i*sqrt(w^2-kl.^2); % k<w
r2= sqrt(kh.^2-w^2); % k>w
sp1= rho(r1);
sp1= [1 + sp1; 1 - sp1];
sp2= rho(r2);
sp2= [1 + sp2; 1 - sp2];
subplot(1,2,2), hold on, plot(sp1,'o');
plot(sp2+1e-10*1i,'r.');

    function re= gl(x)
        re= ((x-q).^2+p^2)./((x+q).^2+p^2);
    end
        
    function re= gh(x)
        re= ((x-p).^2+q^2)./((x+p).^2+q^2);
    end

    function re= rho(r)
       re= (r-p+1i*q)./(r+p-1i*q); 
    end

    
  
end