
k= 20;   % wavenumber
L= 1; % subdomain size
h= 1/10;% mesh size
kmin= sqrt(2)*pi/L;
kmax= sqrt(2)*pi/h;
kneg= k-sqrt(2)*pi/L;
kpos= k+sqrt(2)*pi/L;

cond= 1;
if 2*k^2<=kneg^2+kpos^2
    disp('assumption 1 is satisfied');
else
    disp('assumption 1 is not satisfied');
    cond= 0;
end
if 2*k^2>kmin^2+kpos^2
    disp('assumption 2 is satisfied');
else
    disp('assumption 2 is not satisfied');
    cond= 0;
end
if 2*k^2<kmin^2+kmax^2
    disp('assumption 3 is satisfied');
else
    disp('assumption 3 is not satisfied');
    cond= 0;
end

delta= 0; % size of the overlapping region

if cond
    if delta==0
        a= 1i*(k^2-kmin^2)^(1/4)*(k^2-kneg^2)^(1/4);
        b= (kmax^2-k^2)^(1/4)*(kpos^2-k^2)^(1/4);
        p= (a*b-k^2)/(a+b); q= 1/(a+b);
        xi1= linspace(kmin,kneg,100); % k is higher
        r1=  1i*sqrt(k^2-xi1.^2);
        c1= abs((r1-a)./(r1+a)).*abs((r1-b)./(r1+b));
        xi2= linspace(kpos,kmax,100); % k is smaller
        r2= sqrt(xi2.^2-k^2);
        c2= abs((r1-a)./(r1+a)).*abs((r1-b)./(r1+b));
        plot(xi1,c1,xi2,c2,'r');
        title(['p=',num2str(p),',   q=',num2str(q),' given by opt. for non-overlapping']);
        pause;
    else
        plot(xi1,c1,xi2,c2,'r');
        title(['p=',num2str(p),',   q=',num2str(q),' given by opt. for non-overlapping']);
        pause;
    end
    
elseif kmin>k
    if delta==0
        p= kmax^2*sqrt(kmin^2-k^2)-kmin^2*sqrt(kmax^2-k^2);
        p= p/sqrt(2*(kmax^2-kmin^2));
        p= p/(sqrt(kmax^2-k^2)-sqrt(kmin^2-k^2))^(1/4);
        p= p/((kmax^2-k^2)*sqrt(kmin^2-k^2)-(kmin^2-k^2)*sqrt(kmax^2-k^2))^(1/4);
        q= (sqrt(kmax^2-k^2)-sqrt(kmin^2-k^2))^(3/4);
        q= q/sqrt(2*(kmax^2-kmin^2));
        q= q/((kmax^2-k^2)*sqrt(kmin^2-k^2)-(kmin^2-k^2)*sqrt(kmax^2-k^2))^(1/4);
    else
        r= sqrt(kmin^2-k^2);
        rp= @(p)(sqrt((2*p+delta*p^2)/delta));
        frate= @(p)((r-p)^2/(r+p)^2*exp(-2*r*delta)-(rp(p)-p)^2/(rp(p)+p)^2*exp(-2*rp(p)*delta));
        frate(1) 
        frate(100)
        p= fzero(frate,[1,100]);
    end
    xi2= linspace(kmin,kmax,100);
    r2= sqrt(xi2.^2-k^2);
    c2= abs((r2-p-q*xi2.^2)./(r2+p+q*xi2.^2).*exp(-r2*delta));
    plot(xi2,c2,'r');
    title(['p=',num2str(p),',   q=',num2str(q),' given by opt. for non-overlapping']);
    pause;
end   

% [p,q]= meshgrid(0:2:30,0:2:30); p= p(:); q= q(:);
% p= 0:100; q= zeros(size(p));
% for m= 1:length(p)
%     xi1= kmin:0.1:kneg; % k is higher
%     r1=  1i*sqrt(k^2-xi1.^2);
%     xi2= kpos:0.1:kmax; % k is smaller
%     r2= sqrt(xi2.^2-k^2);
%     c1= abs((r1-p(m)-1i*q(m))./(r1+p(m)+1i*q(m)).*exp(-r1*delta));
%     c2= abs((r2-p(m)-1i*q(m))./(r2+p(m)+1i*q(m)).*exp(-r2*delta));
%     plot(xi1,c1,xi2,c2,'r');
%     title(['p=',num2str(p(m)),',   q=',num2str(q(m))]);
%     pause;
% end