clear all;
close all;
% this script uses oo0helm.m to compute optimized parameters;
% the aim is to find out how these parameters scale with mesh
% size h and wavenumber w
% 

h= 2.^(-12:-1:-16);
Cw= pi/5;
nh= size(h,2);
pq= zeros(nh,4);
g= zeros(size(h)); xmax= zeros(size(h));
for ih= 1:nh
    w= 10;%Cw/h(ih); % wavenumber
    % lambda= 2*pi/w; % wavelength
    % Hy can not be integer times 1/2 of lambda
    % Hy= 10.6*lambda; % 16*h(ih); % size of the subdomain along y-direction
    Hy= 1; % large enough
    pq0= [1 1 1 1];  
    L= 2*h(ih);
    dk= 0.01;
    [pqopt,gopt,xhmax]= ...
        oo2helm(w,h(ih),L,[],pq0,Hy,dk);
    pq(ih,:)= pqopt;
    g(ih)= gopt;  % convergence rate
    xmax(ih)= xhmax; % maximum point for high frequency 
end

figure;

subplot(1,3,1), loglog(h,pq(:,1),'+-'), ylabel('p1'), xlabel('h');
ph= (log(pq(nh,1))-log(pq(nh-1,1)))/(log(h(nh))-log(h(nh-1)));
title(['p1~h order',num2str(ph)]);
hold on;
loglog(h,4*h.^ph);
fprintf('log2(p1h(i+1)/p1h(i)):\n');
disp(log2(pq(2:nh,1)./pq(1:nh-1,1)));

subplot(1,3,2), loglog(h,pq(:,2),'+-'), ylabel('q1'), xlabel('h');
qh= (log(pq(nh,2))-log(pq(nh-1,2)))/(log(h(nh))-log(h(nh-1)));
title(['q~h order',num2str(qh)]);
hold on;
loglog(h,4*h.^qh);
fprintf('log2(q2h(i+1)/q2h(i)):\n');
disp(log2(pq(2:nh,2)./pq(1:nh-1,2)));



subplot(1,3,3), loglog(h,xmax,'+-'), ylabel('xmax'), xlabel('h');
xmaxh= (log(xmax(nh))-log(xmax(nh-1)))/(log(h(nh))-log(h(nh-1)));
title(['xmax~h order',num2str(xmaxh)]);
hold on;
loglog(h,4*h.^xmaxh);
fprintf('log2(xmaxh(i+1)/xmaxh(i)):\n');
disp(log2(xmax(2:nh)./xmax(1:nh-1)));

figure;


subplot(1,2,1), loglog(h,pq(:,3),'+-'), ylabel('p2'), xlabel('h');
ph= (log(pq(nh,3))-log(pq(nh-1,3)))/(log(h(nh))-log(h(nh-1)));
title(['p2~h order',num2str(ph)]);
hold on;
loglog(h,4*h.^ph);
fprintf('log2(p2h(i+1)/p2h(i)):\n');
disp(log2(pq(2:nh,3)./pq(1:nh-1,3)));

subplot(1,2,2), loglog(h,pq(:,4),'+-'), ylabel('q1'), xlabel('h');
qh= (log(pq(nh,4))-log(pq(nh-1,4)))/(log(h(nh))-log(h(nh-1)));
title(['q2~h order',num2str(qh)]);
hold on;
loglog(h,4*h.^qh);
fprintf('log2(q2h(i+1)/q2h(i)):\n');
disp(log2(pq(2:nh,4)./pq(1:nh-1,4)));

