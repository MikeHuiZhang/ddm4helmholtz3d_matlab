function [pqopt,gopt,maxk]= o0helm(w, h, H, dk, pqinit)
%This function uses fminsearch to find min_(p,q) max_k of the 
% one-parameter (p-1i*q) converence rate for each two iterations for 
% the Helmholtz equation, in two subdomains, without overlap
% [popt, qopt]= o0helm(w, h, H, dk, pqinit)
% InPUT
%       w: the wavenumber from the Helmholtz equation, default 10
%       h: the mesh size, scalar for 2d, two scalars for 3d 
%       H: the subdomain size, scalar for 2d, two scalars for 3d
%       pqinit: initial guess for opt. parameters
%               scalar for q1=p1, two scalars for q2=p2
% OUTPUT
%       pqopt:  optimized parameters
%       gopt:   optimized convergence rate

if length(h)==1
    kmax= pi/h;
else
    kmax= pi*norm(1./h,2);
end
if length(H)==1
    kmin= pi/H;
else
    kmin= min(pi./H);
end

kl= kmin:dk:w-kmin;  % kl is non-empty only if w>2*kmin
kh= w+kmin:dk:kmax;  % kmax is non-empty only if kmax - kmin > w
xl= sqrt(w^2-kl.^2);
xh= sqrt(kh.^2-w^2);
figure;

[pqopt, gopt]= fminsearch(@rho,pqinit);
% options = psoptimset('PollMethod','MADSPositiveBasis2N', ...
%    'SearchMethod',@MADSPositiveBasis2N);
% [pqopt, gopt]= patternsearch(@rho,[p0,q0],[],[],[],[],[],[],[],options);
% [pqopt, gopt]= patternsearch(@rho,[p0,q0]); 

    function re= rho(pq)
        p= pq(1);
        if size(pq,2)==2
            q= pq(2);
        else
            q= p;
        end
        glmax= 0; klmax= [];
        if  ~isempty(xl)
            if gl(xl(1))<gl(xl(size(xl,2)))
                klmax= w-kmin;
                glmax= gl(xl(size(xl,2)));
            else
                klmax= kmin;
                glmax= gl(xl(1));
            end    
            subplot(1,2,1), plot(kl,gl(xl));
            xlabel('k');
            title(['klmax=',num2str(klmax),',   glmax=', num2str(glmax)]);
        end
        
        % [xhmax, ghmax]= ...
        %    fminsearch(@(x)(-gh(x)), kp);
        % ghmax= -ghmax;
        [ghmax,ikhmax]= max(gh(xh));
        khmax= kh(ikhmax);
        subplot(1,2,2), plot(kh,gh(xh));
        xlabel('k'); 
        title(['khmax=',num2str(khmax),',   ghmax=', num2str(ghmax)]);
        
        % pause(0.1);
        maxk= [klmax,khmax];
        re= max(glmax,ghmax);
        
        function re= gl(x)
            re= ((x-q).^2+p^2)./((x+q).^2+p^2);
        end
        
        function re= gh(x)
            re= ((x-p).^2+q^2)./((x+p).^2+q^2);
        end
    
    end
    
end