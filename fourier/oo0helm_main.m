clear all;
close all;
% this script uses oo0helm.m to compute optimized parameters;
% the aim is to find out how these parameters scale with mesh
% size h and wavenumber w
% the parameter is p-iq

h= 2.^(-4:-1:-12);
Cw= pi/5;
nh= size(h,2);
p= zeros(size(h)); q= p; 
g= zeros(size(h)); xmax= zeros(size(h));
for ih= 1:nh
    w= Cw/h(ih); % wavenumber
    % lambda= 2*pi/w; % wavelength
    % Hy can not be integer times 1/2 of lambda
    % Hy= 10.6*lambda; % 16*h(ih); % size of the subdomain along y-direction
    Hy= 1; % large enough
    p0= 1; % 1/10/h(ih); 
    q0= 1;
    L= 2*h(ih);
    dk= 0.01;
    [pqopt,gopt,xhmax]= ...
        oo0helm(w,h(ih),L,[],p0,q0,Hy,dk);
    p(ih)= pqopt(1);
    if size(pqopt,2)==2
       q(ih)= pqopt(2);
    end
    g(ih)= gopt;  % convergence rate
    xmax(ih)= xhmax; % maximum point for high frequency 
end

figure;

subplot(1,3,1), loglog(h,p,'+-'), ylabel('p'), xlabel('h');
ph= (log(p(nh))-log(p(nh-1)))/(log(h(nh))-log(h(nh-1)));
title(['p~h order',num2str(ph)]);
hold on;
loglog(h,4*h.^ph);
fprintf('log2(ph(i+1)/ph(i)):\n');
disp(log2(p(2:nh)./p(1:nh-1)));

if q
    subplot(1,3,2), loglog(h,q,'+-'), ylabel('q'), xlabel('h');
    qh= (log(q(nh))-log(q(nh-1)))/(log(h(nh))-log(h(nh-1)));
    title(['q~h order',num2str(qh)]);
    hold on;
    loglog(h,4*h.^qh);
    fprintf('log2(qh(i+1)/qh(i)):\n');
    disp(log2(q(2:nh)./q(1:nh-1)));
end


subplot(1,3,3), loglog(h,xmax,'+-'), ylabel('xmax'), xlabel('h');
xmaxh= (log(xmax(nh))-log(xmax(nh-1)))/(log(h(nh))-log(h(nh-1)));
title(['xmax~h order',num2str(xmaxh)]);
hold on;
loglog(h,4*h.^xmaxh);
fprintf('log2(xmaxh(i+1)/xmaxh(i)):\n');
disp(log2(xmax(2:nh)./xmax(1:nh-1)));