clear all;
clf;
w= 0;                        % wavenumber
k= [1:0.1:w-1,0:0.1:w+10]; % frequency along y direction 
z= -1:0.1:1;                 % frequency of coefficient vector along x
[k,z]= ndgrid(k,z);
r= sqrt(k.^2-w.^2);          % characteristic root for solution
H= 1;                        % subdomain size along x direction decomposed
L= 0.1;                      % overlap

for pp= 0.1
   p= pp;                 % Robin parameter
   % entries of the iteration matrix
   d0= (p+r).^2.*exp(r*(H+L))-(p-r).^2.*exp(-r*(H+L));
   a1= (p+r).^2.*exp(r*L)./d0;
   a2= -(p-r).^2.*exp(-r*L)./d0;
   b1= (p.^2-r.^2).*exp(r*H)./d0;
   b2= -(p.^2-r.^2).*exp(-r*H)./d0;
   
   % square of 2-norm of the iteration matrix
   lambda= (abs(a2)).^2+(abs(a1)).^2+(abs(b1)).^2+(abs(b2)).^2 ...
       +2*z.*real(a2.*conj(a1)+b2.*conj(b1));
   lambda= lambda + sqrt(lambda.^2-abs(a2.^2+a1.^2-b1.^2-b2.^2).^2);
   lambda= real(lambda);
   surf(k,z,lambda);
   xlabel('k');  ylabel('z');  title(['p=',num2str(p)]);
   pause(1)
   axis tight
end