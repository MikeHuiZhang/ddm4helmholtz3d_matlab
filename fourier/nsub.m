clear all;
w= 10;
H= 1;  % subdomain size along decomposed x-direction
Hy= H; % subdomain size along y-direction
J= 100; % number of subdomains along x-direction
kmin= pi/Hy;
k= [];
k= [k kmin:kmin:floor(w/kmin)*kmin];
k= [k floor(w/kmin)*kmin+kmin:kmin:w*100]; 
k= k.';
r= sqrt(k.^2-w^2);
z= cos(2*pi*(1:J)/J);
rho= repmat(coth(r*H).^2,1,size(z,2)) - 4*(exp(r*H)-exp(-r*H)).^(-2)*(z.^2);
figure();
plot(0.0001*1i+rho(:),'.');
title('feti for N subdomains of size H');
legend(['w=',num2str(w),',  H=',num2str(H),',  Hy=',num2str(Hy)]);

