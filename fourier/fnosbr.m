% Robin on left and right faces, and Dirichlet on all the other faces 
% x\in [-L,L] decomposed to [-L,0] and [0,L]
w= 4*pi;   
p= 20*(1-1i); %18.5213*(1-1i); 
L= 1/2; 
H= 1;
h= 1/20;
basefreq= pi/H; kmin= sqrt(2)*basefreq;
kmax= sqrt(2)*pi/h;
n= floor(kmax/basefreq);
[m,n]= ndgrid(1:n,1:n);
k= sqrt(m.^2+n.^2)*basefreq;
k= k(:); k= k(k<kmax);
kl= k(k<w); kh= k(k>=w);
r1= -1i*sqrt(w^2-kl.^2); % k<w
r2= sqrt(kh.^2-w^2); % k>w
r= [r1;r2];
specnos= (exp(r*L).*(r-p).*(r-1i*w)+exp(-r*L).*(r+p).*(-r-1i*w))./ ...
    (exp(r*L).*(r+p).*(r-1i*w)+exp(-r*L).*(r-p).*(-r-1i*w));
% form 1
% specnos1= [1 + specnos; -1 + specnos];
% figure;
% plot(specnos1,'o');
% form 2
specnos2= [1 + specnos; 1 - specnos];
figure;
plot(specnos2,'o');
