k= 9;   p= 18; %18.5213*(1+1i);
xi= 0:0.001:k; % k is higher
r1= []; %1i*sqrt(k^2-xi.^2);
xi= k:0.001:100; % k is smaller
r2= sqrt(xi.^2-k^2);
r= [r1 r2];
specnos= (r-p)./(r+p);
% form 1
specnos1= [1 + specnos, -1 + specnos];
figure;
plot(specnos1,'o');
% form 2
specnos2= [1 + specnos, 1 - specnos];
figure;
plot(specnos2,'o');
