function o0helm3dbr_manysub(w,h,Hy,N,pq,dk)
% draw convergence rate for a given p-iq for helmholtz in 3d
% also draw spectral picture

p= pq(1);
q= pq(2);
H= 1/N; % the subdomain size along x-direction
ni= N-1; % number of interface points
e= ones(ni,1);
basefreq= pi/Hy;
kmin= sqrt(2)*basefreq;
kmax= sqrt(2)*pi/h;
kl= kmin:dk:(w-dk);
kh= (w+dk):dk:kmax;
xl= sqrt(w^2-kl.^2);
xh= sqrt(kh.^2-w^2);
figure; hold on; 
if ~isempty(xl)
    plot(kl,glbr(xl));
end
plot(kh,ghbr(xh),'g');
plot(w*ones(11,1),linspace(0,2,11),'--');

% discrete freq.
n= floor(kmax/basefreq);
[m,n]= ndgrid(1:n,1:n);
k= sqrt(m.^2+n.^2)*basefreq;
k= k(:); k= k(k<kmax);
kl= k(k<w); kh= k(k>w);
xl= sqrt(w^2-kl.^2);
xh= sqrt(kh.^2-w^2);
if ~isempty(xl)
    plot(kl,glbr(xl),'b*');
end
plot(kh,ghbr(xh),'r*');

% legend('cont. low','cont.high','w','dis. low','dis. high');

% spectral picture
r1= -1i*sqrt(w^2-kl.^2); % k<w
r2= sqrt(kh.^2-w^2); % k>w
sp1= rhobr(r1);
sp1= 1 - sp1(:);
sp2= rhobr(r2);
sp2= 1-sp2(:);
figure, hold on, plot(sp1,'o');
plot(sp2,'r.');
    
    function re= glbr(x)
        r= -1i*x;
        re= max(abs(rhobr(r)),[],1);
    end

    function re= ghbr(x)
        r= x;
        re= max(abs(rhobr(r)),[],1);      
    end
  
    function re= rhobr(r)
       re= zeros(ni*2,length(r));
       for ir= 1:length(r)
         c= ((-r(ir)^2+1i*w*r(ir)+r(ir)*(p-1i*q)-1i*w*(p-1i*q))...
             *exp(r(ir)*H)+(r(ir)^2+1i*w*r(ir)+r(ir)*(p-1i*q)+1i*w*(p-1i*q))...
             *exp(-r(ir)*H))/((r(ir)^2+r(ir)*(p-1i*q)-1i*w*r(ir)...
             -1i*w*(p-1i*q))*exp(r(ir)*H)+(-r(ir)^2+r(ir)*(p-1i*q)...
             -1i*w*r(ir)+1i*w*(p-1i*q))*exp(-r(ir)*H));
         a= ((r(ir)^2-(p-1i*q)^2)*exp(r(ir)*H)+(-r(ir)^2+(p-1i*q)^2)*exp(-r(ir)*H))...
             /((-r(ir)^2-2*r(ir)*(p-1i*q)-(p-1i*q)^2)*exp(r(ir)*H)+(r(ir)^2-2*r(ir)...
             *(p-1i*q)+(p-1i*q)^2)*exp(-r(ir)*H));
         b= -4*r(ir)*(p-1i*q)/((-r(ir)^2-2*r(ir)*(p-1i*q)-(p-1i*q)^2)...
             *exp(r(ir)*H)+(r(ir)^2-2*r(ir)*(p-1i*q)+(p-1i*q)^2)*exp(-r(ir)*H));
         F11= spdiags(b*e,1,ni,ni);
         F12= spdiags(a*e,0,ni,ni);        
         F= [F11, F12;
             F12, F11.'];
         F(ni,2*ni)= c;
         F(ni+1,1)= c;
         re(:,ir)= eig(full(F));
       end
    end

end