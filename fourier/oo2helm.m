function [pqopt,gopt,xmax]= oo2helm(w, h, L, kmin, pq0, Hy, dk)
% pq0, pqopt: p1-iq1, p2-iq2, [p1,q1,p2,q2]

if nargin<1 || isempty(w)
    w= 10;
end
if nargin<2 || isempty(h)
    h= 0.01;
end
if nargin<3 || isempty(L)
    L= h;
end
if nargin<4 || isempty(kmin)
    kmin= 0.1;
end
if kmin==0
    disp('kmin must be larger than zero');
end
if nargin<5
    pq0= [1 1 1 1];
end
kmax= pi/h;
if nargin>=6 && ~isempty(Hy)
    kmin= pi/Hy;  % assuming Dirichlet along y-direction
    km= w-kmin;%floor(w/kmin)*kmin;
    kp= w+kmin;%(floor(w/kmin)+1)*kmin;
else
    km= w-kmin;  % the low frequency is in [kmin, km]
    kp= w+kmin;  % the high frequency is in [kp, kmax]
end

if nargin<8 || isempty(dk)
    dk= kmin;
end

kl= kmin:dk:km;
kh= kp:dk:kmax;
xl= sqrt(w^2-kl.^2);
xh= sqrt(kh.^2-w^2);
figure;

% [pqopt, gopt]= fminsearch(@rho,pq0);
% options = psoptimset('PollMethod','MADSPositiveBasis2N', ...
%    'SearchMethod',@MADSPositiveBasis2N);
% [pqopt, gopt]= patternsearch(@rho,pq0,[],[],[],[],[],[],[],options);
[pqopt, gopt]= patternsearch(@rho,pq0); 

    function re= rho(pq)
        p1= pq(1); q1= pq(2);
        p2= pq(3); q2= pq(4);
        glmax= 0;
        if  ~isempty(xl)
            glmax= max(gl(xl(1)),gl(xl(size(xl,2))));
            subplot(1,2,1), plot(kl,gl(xl));
            xlabel('k');
            title(['w=',num2str(w),',   glmax=', num2str(glmax)]);
        end
        
        % [xhmax, ghmax]= ...
        %    fminsearch(@(x)(-gh(x)), kp);
        % ghmax= -ghmax;
        [ghmax,ixhmax]= max(gh(xh));
        xhmax= xh(ixhmax);
        if xhmax > max(xh)
            xhmax= max(xh);
            ghmax= gh(xhmax);
        end
        subplot(1,2,2), plot(kh,gh(xh));
        xlabel('k'); 
        title(['w=',num2str(w),',   xhmax=',num2str(xhmax), ...
               ',   ghmax=', num2str(ghmax)]);
        
        % pause(0.1);
        xmax= xhmax;
        re= max(glmax,ghmax);
        
        function re= gl(x)
            re= ((x-q1).^2+p1^2)./((x+q1).^2+p1^2) ...
                .*((x-q2).^2+p2^2)./((x+q2).^2+p2^2);
        end
        
        function re= gh(x)
            re= ((x-p1).^2+q1^2).*exp(-4*L*x)./((x+p1).^2+q1^2) ...
                .*((x-p2).^2+q2^2)./((x+p2).^2+q2^2);
        end
    
    end
    
end