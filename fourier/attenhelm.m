% Helmholtz with attenuation
% - u_xx - u_yy - (1 - a i) w^2 u = f
a= 0.0001;
w= 6;
v= w^2*a;
L= 0.1;
k= [0.1:0.1:w-0.001, w+0.1:0.1:w+100];
x= (1/2)*sqrt(2*sqrt(k.^4-2*k.^2*w^2+w^4+w^4*a^2)+2*k.^2-2*w^2);
r= x-1i*x/v;

g= @(pq) max( exp(-2*L*x).*(x.^2*(1+v^2)-2*pq(2)*x*v+pq(2)^2*v^2- ...
    2*pq(1)*x*v^2+pq(1)^2*v^2)./...
    (x.^2*(1+v^2)+2*pq(2)*x*v+pq(2)^2*v^2+2*pq(1)*x*v^2+pq(1)^2*v^2) );

[pq,gopt]= fminsearch(g,[6,6]);
disp(['v=',num2str(v)]);
disp(pq);
disp(['p/q=',num2str(pq(1)/pq(2))]);
disp(gopt);

