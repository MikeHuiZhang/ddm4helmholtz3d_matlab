% we check diffrent cases for the non-overlapping o0 paramters
% Gander 2002 assumes kmin<(sqrt(3)-1)/2*w about 0.366*w, and in
% which case effecting local maximums are at w- and kmax
% we verify the statement and the corresponding formula with Matlab 
% fminsearch. We also look for the effecting local maximums in other cases
% Anyway, kmin < w is assumed; otherwise we use the SPD opt. param.
function o0helm_main()

kmin= 10;  H= pi/kmin;
for w= kmin+1:4*kmin
    disp(['kmin/w',num2str(kmin/w)]);
    h= pi/5/w;
    kmax= pi/h;
    wm= w-kmin;  wp= w+kmin;
    pf= sqrt(sqrt(w^2-wm^2)*sqrt(kmax^2-w^2)/2); % formula from Gander 2002
    dk= 0.1; 
    kl= kmin:dk:wm;  kh= wp:dk:kmax; % frequency excluding (w-,w+)
    gf= max([gl(sqrt(w^2-kl.^2),pf,pf),gh(sqrt(kh.^2-w^2),pf,pf)]);
    disp(['forumla gives ',num2str(pf),'   g=',num2str(gf)]);
    pqinit= 1;
    [p,g]= o0helm(w, h, H, dk, pqinit);
    disp(['fminsearch gives p=',num2str(p),'   g=',num2str(g)]);
    pause();
    close all;
end


    function re= gl(x,p,q)
        re= ((x-q).^2+p^2)./((x+q).^2+p^2);
    end

    function re= gh(x,p,q)
        re= ((x-p).^2+q^2)./((x+p).^2+q^2);
    end
end