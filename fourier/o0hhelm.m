function [pqopt,gopt,maxk]= o0hhelm(w1, w2, h, H, dk, pqinit)
%This function uses fminsearch to find min_(p1,q1,p2,q2) max_k of the 
% two parameters (p1-1i*q1) and (p2-1i*q2) converence rate for each four 
% iterations for the Helmholtz equation, in two subdomains, without overlap
% [popt, qopt, maxk]= o0hhelm(w1, w2, h, H, dk, pqinit)
% InPUT
%       w1, w2: the wavenumbers in two subdomains, respectively
%       h: for 2d problem, h is a scalar, taken as mesh size in y-direction 
%          for 3d problems, h consists of mesh sizes in y- and z- directions
%       H: similar to h, but as subdomain size
%       dk: samples of k, we can take very small dk to have approimate 
%           continuous k
%       pqinit: initial guess for opt. parameters
%               if it consists of 4 scalars then all four parameters
%               if it consists of 2 scalars then q1=p1, q2=p2
%               if it consists of 1 scalar then p2=q2=q1=p1
% OUTPUT
%       pqopt:  optimized parameters
%       gopt:   optimized convergence rate
%       maxk:   where convergence rate attains local maximums

wmin= min(w1,w2);
wmax= max(w1,w2);
if length(h)==1
    kmax= pi/h;
else
    kmax= pi*norm(1./h,2);
end
if length(H)==1
    kmin= pi/H;
else
    kmin= min(pi./H);
end
kl= kmin:dk:wmin;
klh= wmin:dk:wmax;
kh= wmax:dk:kmax;

xl1= sqrt(wmin^2-kl.^2);
xl2= sqrt(wmax^2-kl.^2);
xlh1= sqrt(klh.^2-wmin^2);
xlh2= sqrt(wmax^2-klh.^2);
xh1= sqrt(kh.^2-wmin^2);
xh2= sqrt(kh.^2-wmax^2);
figure;



[pqopt, gopt]= fminsearch(@rho,pqinit);
% options = psoptimset('PollMethod','MADSPositiveBasis2N', ...
%    'SearchMethod',@MADSPositiveBasis2N);
% [pqopt, gopt]= patternsearch(@rho,[p0,q0],[],[],[],[],[],[],[],options);
% [pqopt, gopt]= patternsearch(@rho,[p0,q0]); 

    function re= rho(pq)
        if length(pq)==4
            p1= pq(1); 
            q1= pq(2);
            p2= pq(3);
            q2= pq(4);
        elseif length(pq)==2
            p1= pq(1); q1= p1;
            p2= pq(2); q2= p2;
        elseif length(pq)==1
            p1= pq(1); q1= p1;
            p2= p1; q2= p2;
        end
        
        glmax= 0; klmax= [];
        if  ~isempty(xl1)
            [glmax,iklmax]= max(gl(xl1,xl2));
            klmax= kl(iklmax);
            subplot(1,3,1), plot(kl,gl(xl1,xl2));
            xlabel('k');
            title(['klmax=', num2str(klmax),',   glmax=', num2str(glmax)]);
        end
        
        glhmax= 0; klhmax=[];
        if ~isempty(xlh1)
            [glhmax,iklhmax]= max(glh(xlh1,xlh2));
            klhmax= klh(iklhmax);
            subplot(1,3,2), plot(klh,glh(xlh1,xlh2));
            xlabel('k');
            title(['klhmax=',num2str(klhmax),', glhmax=', num2str(glhmax)]);
        end
           
        [ghmax,ikhmax]= max(gh(xh1,xh2));
        khmax= kh(ikhmax);
        subplot(1,3,3), plot(kh,gh(xh1,xh2));
        xlabel('k'); 
        title(['khmax=',num2str(khmax),',   ghmax=', num2str(ghmax)]);
        
        % pause(0.1);
        maxk= [klmax,klhmax,khmax];
        re= max([glmax,glhmax,ghmax]);
        
        function y= gl(x1,x2)
            y= ((x2-q1).^2+p1^2).*((x1-q2).^2+p2^2)./(((x1+q1).^2+p1^2).* ...
                ((x2+q2).^2+p2^2));
        end
        
        function y= glh(x1,x2)
            y= ((x2-q1).^2+p1^2).*((x1-p2).^2+q2^2)./(((x1+p1).^2+q1^2).* ...
                ((x2+q2).^2+p2^2));
        end
        
        function y= gh(x1,x2)
            y= ((x2-p1).^2+q1^2).*((x1-p2).^2+q2^2)./(((x1+p1).^2+q1^2).* ...
                ((x2+p2).^2+q2^2));
        end
    
    end
    
end   % --- end of o0hhelm.m