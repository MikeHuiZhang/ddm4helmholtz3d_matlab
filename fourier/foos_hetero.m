clear all;
k= 17;   % wavenumber in the left subdomain  
k2= 1;   % wavenumber in the right subdomain
L= 1/4;   % subdomain size
h= 1/20;  % mesh size
kmin= sqrt(2)*pi/L;
kmax= sqrt(2)*pi/h;
kneg= k-sqrt(2)*pi/L;
kpos= k+sqrt(2)*pi/L;
kneg2= k2-sqrt(2)*pi/L;
kpos2= k2+sqrt(2)*pi/L;

cond= 1;    % condition to use opt. results in Gander 2002 for Helmholtz
if 2*k^2<=kneg^2+kpos^2
    disp('assumption 1 is satisfied');
else
    disp('assumption 1 is not satisfied');
    cond= 0;
end
if 2*k^2>kmin^2+kpos^2
    disp('assumption 2 is satisfied');
else
    disp('assumption 2 is not satisfied');
    cond= 0;
end
if 2*k^2<kmin^2+kmax^2
    disp('assumption 3 is satisfied');
else
    disp('assumption 3 is not satisfied');
    cond= 0;
end

delta= 0; % size of the overlapping region

% analytical results
if cond   % use opt. results in Gander 2002 for Helmholtz
    p= sqrt(sqrt((k^2-kneg^2)*(kmax^2-k^2))/2);
    q= p;
    xi1= linspace(kmin,kneg,100); % k is higher
    r1=  1i*sqrt(k^2-xi1.^2);
    xi2= linspace(kpos,kmax,100); % k is smaller
    r2= sqrt(xi2.^2-k^2);
    c1= abs((r1-p-1i*q)./(r1+p+1i*q).*exp(-r1*delta));
    c2= abs((r2-p-1i*q)./(r2+p+1i*q).*exp(-r2*delta));
    plot(xi1,c1,xi2,c2,'r');
    title(['p=',num2str(p),',   q=',num2str(q),' given by opt. for non-overlapping']);
    pause;
elseif kmin>k     % spd case, we use opt. results from Gander 2006
    if delta==0
        p= ((kmin^2-k^2)*(kmax^2-k^2))^(1/4);
    else
        r= sqrt(kmin^2-k^2);
        rp= @(p)(sqrt((2*p+delta*p^2)/delta));
        frate= @(p)((r-p)^2/(r+p)^2*exp(-2*r*delta)-(rp(p)-p)^2/(rp(p)+p)^2*exp(-2*rp(p)*delta));
        frate(1) 
        frate(100)
        p= fzero(frate,[1,100]);
    end
    q= 0;
    xi= kmin:0.1:kmax;
    r1= sqrt(xi.^2-k^2);
    r2= sqrt(xi.^2-k2^2);
    c1= abs((r1-p-1i*q)./(r2+p+1i*q).*exp(-r1*delta));
    c2= abs((r2-p-1i*q)./(r1+p+1i*q).*exp(-r2*delta));
    % plot(xi,c1,xi,c2,'r');
    plot(xi,c1.*c2,'r');
    title(['p=',num2str(p),',   q=',num2str(q),' given by opt. for non-overlapping']);
    pause;
end   

% direct search
% [p,q]= meshgrid(0:2:30,0:2:30); p= p(:); q= q(:);
p= 0:100;    % real part
q= zeros(size(p)); % imaginary part
for m= 1:length(p) 
    xi= kmin:0.1:kmax;
    r1= sqrt(xi.^2-k^2);
    r2= sqrt(xi.^2-k2^2);
    c1= abs((r1-p(m)-1i*q(m))./(r2+p(m)+1i*q(m)).*exp(-r1*delta));
    c2= abs((r2-p(m)-1i*q(m))./(r1+p(m)+1i*q(m)).*exp(-r2*delta));
    % plot(xi,c1,xi,c2,'r');
    plot(xi,c1.*c2,'r');
    title(['p=',num2str(p(m)),',   q=',num2str(q(m))]);
    pause;
end