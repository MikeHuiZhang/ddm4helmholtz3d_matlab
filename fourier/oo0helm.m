function [pqopt,gopt,xmax]= oo0helm(w, h, L, kmin, p0, q0, Hy, dk)
%This function uses fminsearch to find min_(p,q) max_k of the 
% one-parameter (p-1i*q) converence rate for each two iterations for 
% the Helmholtz equation, in two subdomains
% [popt, qopt]= o0_helm(h, L, w, kmin, p0, q0)
% InPUT
%       w: the wavenumber from the Helmholtz equation, default 10
%       h: the mesh size for y-direction which gives kmax=pi/h in y,
%          default 0.01 
%       L: the size of overlap, default to be h
%       kmin: the minimum frequency in y, default to be 0.1
%       p0, q0: initial guess for opt. parameters, default ones
% OUTPUT
%       popt, qopt: optimized parameters
%       gopt:   optimized convergence rate
if nargin<1 || isempty(w)
    w= 10;
end
if nargin<2 || isempty(h)
    h= 0.01;
end
if nargin<3 || isempty(L)
    L= h;
end
if nargin<4 || isempty(kmin)
    kmin= 0.1;
end
if kmin==0
    disp('kmin must be larger than zero');
end
if nargin<5
    p0= 1;
    q0= 1;
end
kmax= pi/h;
if nargin>=7 && ~isempty(Hy)
    kmin= pi/Hy;  % assuming Dirichlet along y-direction
    km= w-kmin;%floor(w/kmin)*kmin;
    kp= w+kmin;%(floor(w/kmin)+1)*kmin;
else
    km= w-kmin;  % the low frequency is in [kmin, km]
    kp= w+kmin;  % the high frequency is in [kp, kmax]
end

if nargin<8 || isempty(dk)
    dk= kmin;
end

kl= kmin:dk:km;
kh= kp:dk:kmax;
xl= sqrt(w^2-kl.^2);
xh= sqrt(kh.^2-w^2);
figure;

[pqopt, gopt]= fminsearch(@rho,[p0,q0]);
% options = psoptimset('PollMethod','MADSPositiveBasis2N', ...
%    'SearchMethod',@MADSPositiveBasis2N);
% [pqopt, gopt]= patternsearch(@rho,[p0,q0],[],[],[],[],[],[],[],options);
% [pqopt, gopt]= patternsearch(@rho,[p0,q0]); 

    function re= rho(pq)
        p= pq(1);
        if size(pq,2)==2
            q= pq(2);
        else
            q=p;
        end
        glmax= 0;
        if  ~isempty(xl)
            glmax= max(gl(xl(1)),gl(xl(size(xl,2))));
            subplot(1,2,1), plot(kl,gl(xl));
            xlabel('k');
            title(['w=',num2str(w),',   glmax=', num2str(glmax)]);
        end
        
        % [xhmax, ghmax]= ...
        %    fminsearch(@(x)(-gh(x)), kp);
        % ghmax= -ghmax;
        [ghmax,ixhmax]= max(gh(xh));
        xhmax= xh(ixhmax);
        if xhmax > max(xh)
            xhmax= max(xh);
            ghmax= gh(xhmax);
        end
        subplot(1,2,2), plot(kh,gh(xh));
        xlabel('k'); 
        title(['w=',num2str(w),',   xhmax=',num2str(xhmax), ...
               ',   ghmax=', num2str(ghmax)]);
        
        % pause(0.1);
        xmax= xhmax;
        re= max(glmax,ghmax);
        
        function re= gl(x)
            re= ((x-q).^2+p^2)./((x+q).^2+p^2);
        end
        
        function re= gh(x)
            re= ((x-p).^2+q^2).*exp(-2*L*x)./((x+p).^2+q^2);
        end
    
    end
    
end