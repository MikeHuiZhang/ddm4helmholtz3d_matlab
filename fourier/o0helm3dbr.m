function o0helm3dbr(w,h,H,pq,dk)
% draw convergence rate for a given p-iq for helmholtz in 3d
% also draw spectral picture

p= pq(1);
q= pq(2);
L= 1/2; % the whole domain is (-L,L)
basefreq= pi/H;
kmin= sqrt(2)*basefreq;
kmax= sqrt(2)*pi/h;
kl= kmin:dk:w;
kh= w:dk:kmax;
xl= sqrt(w^2-kl.^2);
xh= sqrt(kh.^2-w^2);
figure;
if ~isempty(xl)
    subplot(1,2,1), hold on, plot(kl,glbr(xl));
end
plot(kh,ghbr(xh),'g');
plot(w*ones(11,1),linspace(0,1,11),'--');

% discrete freq.
n= floor(kmax/basefreq);
[m,n]= ndgrid(1:n,1:n);
k= sqrt(m.^2+n.^2)*basefreq;
k= k(:); k= k(k<kmax);
kl= k(k<w); kh= k(k>=w);
xl= sqrt(w^2-kl.^2);
xh= sqrt(kh.^2-w^2);
if ~isempty(xl)
    plot(kl,glbr(xl),'b*');
end
plot(kh,ghbr(xh),'r*');

% legend('cont. low','cont.high','w','dis. low','dis. high');

% spectral picture
r1= -1i*sqrt(w^2-kl.^2); % k<w
r2= sqrt(kh.^2-w^2); % k>w
sp1= rhobr(r1);
sp1= [1 + sp1; 1 - sp1];
sp2= rhobr(r2);
sp2= [1 + sp2; 1 - sp2];
subplot(1,2,2), hold on, plot(sp1,'o');
plot(sp2,'r.');
    
    function re= glbr(x)
        r= -1i*x;
        re= abs(rhobr(r));
    end

    function re= ghbr(x)
        r= x;
        re= abs(rhobr(r));      
    end
  
    function re= rhobr(r)
       
         re= ((-r.^2+1i*w*r+r*(p-1i*q)-1i*w*(p-1i*q)).*exp(r*L)+ ...
             (r.^2+1i*w*r+r*(p-1i*q)+1i*w*(p-1i*q)).*exp(-r*L))./...
             ((r.^2+r*(p-1i*q)-1i*w*r-1i*w*(p-1i*q)).*exp(r*L)+ ...
             (-r.^2+r*(p-1i*q)-1i*w*r+1i*w*(p-1i*q)).*exp(-r*L));
        
    end

end