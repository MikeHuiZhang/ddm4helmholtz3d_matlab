function [u,flag,relres,iter,resvec,reg,specF]= nos(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr, ...
    zl,zr,GammaD,GammaR,nx,ny,nz,Nx,Ny,Nz,nthetav,nphiv,solvetask,form,order,reg,randinit,...
    maxit,symmetry)
% use optimized Schwarz method to solve the Helmholtz equation
% We assume the set of 'b' nodes are not empty, i.e. Nx is a proper factor
% of nx and similarly for Ny, Nz, because we iterate vector on 'b' nodes.

tsetup= tic;
%% partition of geometry
% subdomain meshes
fprintf('\npartition \n');
tpart= tic;
Ns= Nx*Ny*Nz;
[scoord,selem,srobinelem,sii,sdiri,srobin,sb,sc,sbsend,sbnb,scsend,scnb,...
    ~,~,~,~,~,bdelems] ...
    = qubepartmesh(xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR,Nx,Ny,Nz);

% global interface 'b' and corner 'c' nodes
gtol= [(xr-xl)/nx/10,(yr-yl)/ny/10,(zr-zl)/nz/10]; % geometry tolerance 
[gbs2sbs,sbs2gbs,ngc,gcsend,scs2gcs,ngb,gbsend,gbsma]= gbc(scoord,sc,sbsend,sbnb,scsend,scnb,Ns,gtol);
% product boundary mesh
pbsend= zeros(Ns,1);
npb= 0;
for s= 1:Ns
    npb= npb + size(sb{s},1);
    pbsend(s)= npb;
end
toc(tpart);
telapsed= toc(tsetup);
%% assemble subdomain matrices and rhs 
tsetup= tic;
fprintf('\nassembly \n');
tassemble= tic;
b= cell(Ns,1); A= cell(Ns,1); Sb= cell(Ns,1); 
getAM(); getgpw();    % for use by stima.m and rhs.m
getM2d(); getgpw2d(); % for use by stima2d and rhs2d
[I,J]= ndgrid(1:8,1:8); I= I(:); J= J(:);
[IS,JS]= ndgrid(1:4,1:4); IS= IS(:); JS= JS(:);
global unused_src numsrc;
if ~exist('reg','var') || isempty(reg) 
   pisgiven= 0; % robin parameter p is not given
else
    pisgiven= 1;
    if exist('order','var') && order==2
        reg1= reg(1); reg2= reg(2);
    end
end
for s= 1:Ns
    % first assembly without respect to boundary condition
    ne= size(selem{s},1);
    ii= zeros(64,ne); jj= zeros(64,ne); 
    Aa= zeros(64,ne);
    b{s}= zeros(size(scoord{s},1),1);
    for j= 1:ne
       Ae= stima(scoord{s}(selem{s}(j,:),:),k2);
       ii(:,j)= selem{s}(j,I); % assemble vectors
       jj(:,j)= selem{s}(j,J); % for speed
       Aa(:,j)= Ae(:);
    end
    A{s}= sparse(ii,jj,Aa);
    if ~(isa(f,'float') && 0==f && isempty(pointsource))
       for j = 1:ne
           b{s}(selem{s}(j,:)) = b{s}(selem{s}(j,:)) + ...
               rhs(scoord{s}(selem{s}(j,:),:),f,pointsource);
       end
    end
    selem{s}= [];

    % treatment of Robin or Neumann boundary
    if ~(isa(a,'float') && 0==a)
       for j = 1:size(srobinelem{s},1)
           n4e= srobinelem{s}(j,:);
           A{s}(n4e,n4e)= A{s}(n4e,n4e) + stima2d(scoord{s}(n4e,:),a);
       end
    end
    if ~(isa(uR,'float') && 0==uR)
       for j = 1:size(srobinelem{s},1)
           n4e= srobinelem{s}(j,:);
           b{s}(n4e) = b{s}(n4e) + rhs2d(scoord{s}(n4e,:),uR);
       end
    end
    srobinelem{s}= [];
    
    % treatment of regularization term: Robin transmission boundary
    [interb, interbsign]= getinterb(Nx,Ny,Nz,s); 
    %disp(['interbsign=',num2str(interbsign)]);
    nbe= 0;
    for ij= 1:length(interb)
        nbe= nbe + size(bdelems{interb(ij)},1);
    end
    iiSA= zeros(16,nbe); jjSA= zeros(16,nbe); 
    SAba= zeros(16,nbe);
    iiS= zeros(16,nbe); jjS= zeros(16,nbe); 
    Sba= zeros(16,nbe);
    nbe= 0; 
    for ij= 1:length(interb)
        switch interb(ij)
            case {1,2}
                h1= (yr-yl)/ny; h2= (zr-zl)/nz; h3= (xr-xl)/nx;
                L1= (yr-yl)/Ny; L2= (zr-zl)/Nz; 
            case {3,4}
                h1= (xr-xl)/nx; h2= (zr-zl)/nz; h3= (yr-yl)/ny;
                L1= (xr-xl)/Nx; L2= (zr-zl)/Nz;
            case {5,6}
                h1= (xr-xl)/nx; h2= (yr-yl)/ny; h3= (zr-zl)/nz;
                L1= (xr-xl)/Nx; L2= (yr-yl)/Ny;
        end
        switch interb(ij)
            case {1,2}
                trans= [h3/2 0 0; h3/2 0 0; h3/2 0 0; h3/2 0 0];
            case {3,4}
                trans= [0 h3/2 0; 0 h3/2 0; 0 h3/2 0; 0 h3/2 0];
            case {5,6}
                trans= [0 0 h3/2; 0 0 h3/2; 0 0 h3/2; 0 0 h3/2];
        end
        kmax2= (pi/h1)^2+(pi/h2)^2;
        kmin2= (pi/L1)^2+(pi/L2)^2;
        if ~pisgiven 
            if ~exist('order','var') || order==0
               reg= getrobinparam(k2,kmin2,kmax2);
            elseif order==1 % two-side parameters
               [reg, ~, totalreg]= getrobinparam(k2,kmin2,kmax2,[],interbsign);
            else
                [reg1,reg2]= getrobinparam(k2,kmin2,kmax2);
                reg= [reg1,reg2];
            end
        end
        for j = 1:size(bdelems{interb(ij)},1)
           n4e= bdelems{interb(ij)}(j,:);
           if ~exist('order','var') || order==0 || order==1
               SAe= stima2d(scoord{s}(n4e,:)+trans,reg);
               if 1==j
                   if isa(reg,'float')
                      disp(['reg=',num2str(reg)]); 
                   end
               end
           else
               SAe= stima2d(scoord{s}(n4e,:)+trans,reg1,reg2);
               if 1==j
                   if isa(reg1,'float')
                      disp(['reg1=',num2str(reg1)]); 
                   end
                   if isa(reg2,'float')
                      disp(['reg2=',num2str(reg2)]); 
                   end
               end
           end
           iiSA(:,j+nbe)= bdelems{interb(ij)}(j,IS);
           jjSA(:,j+nbe)= bdelems{interb(ij)}(j,JS); 
           SAba(:,j+nbe)= SAe(:);
           if exist('order','var') && order==1
              Se= stima2d(scoord{s}(n4e,:)+trans,totalreg);
           else
              Se= 2*SAe; 
           end
           iiS(:,j+nbe)= bdelems{interb(ij)}(j,IS);
           jjS(:,j+nbe)= bdelems{interb(ij)}(j,JS); 
           Sba(:,j+nbe)= Se(:);
        end
        nbe= nbe + size(bdelems{interb(ij)},1);
    end
    
    SA= sparse(iiSA,jjSA,SAba,size(A{s},1),size(A{s},2));
    A{s}(sb{s},sb{s})= A{s}(sb{s},sb{s}) + SA(sb{s},sb{s});
    clear iiSA jjSA SAba;
    
    Sb{s}= sparse(iiS,jjS,Sba,size(A{s},1),size(A{s},2));
    Sb{s}= Sb{s}(sb{s},sb{s});
    clear iiS jjS Sba;
        
    % treatment of Dirichlet lastly, otherwise Robin changes Dirichlet
    if isa(uD,'float')
       b{s}(sdiri{s})= uD*ones(size(sdiri{s},1),1);
    elseif isa(uD,'inline') || isa(uD,'function_handle')
       b{s}(sdiri{s}) = uD(scoord{s}(sdiri{s},:));
    end
    [ii, jj]= find(A{s});
    idxofzero = ismember(ii,sdiri{s}); 
    A{s}= setsparse(A{s},ii(idxofzero),jj(idxofzero),0);
    A{s}= setsparse(A{s},double(sdiri{s}),double(sdiri{s}),1);
end
unused_src= []; numsrc= []; 
clear selem srobinelem bdelems ii jj Aa I J IS JS SA idxofzero bdelems;
toc(tassemble);
telapsed= telapsed + toc(tsetup);
%% set up plane wave basis on product interface 
for nnt= 1:length(nthetav)
    ntheta= nthetav(nnt);
    nphi= nphiv(nnt);
    
fprintf('\n--------------- planewaves %d   %d --------------\n',ntheta,nphi);
tQmu= tic;
% % directions of plane waves in spherical coordinates
theta= linspace(0,pi/2,ntheta+2); theta= theta(2:ntheta+1);
phi= linspace(-pi,pi,nphi+1);  phi= phi(2:nphi+1); phi0= phi;
[theta,phi]= ndgrid(theta,phi); theta= theta(:)'; phi= phi(:)';
if ntheta>-1
    theta= [theta, -theta, zeros(1,nphi), pi/2, -pi/2];
    phi= [phi, pi+phi, phi0, 0, 0];  
    numplw= 2*ntheta*nphi+nphi+2;
else
    numplw= 0;
end
% % global Qmug
Qmug= sparse(ngb,length(gbsend)*numplw);
nmug= 0;  gmuend= zeros(length(gbsend),1);
tolQmug= 0.1;             % threhold to filter Qmug
if numplw~=0
    for j= 1:length(gbsend)
        sma= gbsma(j);            % master subdomain of this segment
        sbs= gbs2sbs{sma}(j);          % index of this segment in the master
        if 1==sbs
           coords= scoord{sma}(sb{sma}(1:sbsend{sma}(1)),:);
        else
           coords= scoord{sma}(sb{sma}(sbsend{sma}(sbs-1)+1: ...
                                      sbsend{sma}(sbs)),:);
        end
        if 1==j
            ii= (1:gbsend(j))';
        else
            ii= (gbsend(j-1)+1:gbsend(j))';
        end
        if isa(k2,'function_handle') || isa(k2,'inline')
           qbss= exp(1i*(coords.*repmat(sqrt(k2(coords)),1,3))* ...
               [cos(theta).*cos(phi);cos(theta).*sin(phi);sin(theta)]);
        elseif isa(k2,'float')
           qbss= exp(1i*sqrt(k2)*coords* ...
               [cos(theta).*cos(phi);cos(theta).*sin(phi);sin(theta)]); 
        end
    %     qbss= [qbss,ones(size(qbss,1),1)]; % averages
        [qqbss,rqbss,~]= qr(qbss);
        rjj= diag(rqbss);
        qbss= qqbss(:,abs(rjj)>tolQmug); 
        Qmug(ii,nmug+1:nmug+size(qbss,2))= qbss;
        nmug= nmug + size(qbss,2);
        gmuend(j)= nmug;
    end
    Qmug= Qmug(:,1:nmug);
end
clear theta phi coords ii qbss qqbss rqbss rjj;
% % Qmu{s} with columns being traces of plane waves on 'b' segments
Qmu= cell(Ns,1);
nmu= zeros(Ns,1); smusend= cell(Ns,1); nmuend= zeros(Ns,1);
for s= 1:Ns
    Qmu{s}= sparse(size(sb{s},1),length(sbsend{s})*numplw);
    if numplw~=0
        for j= 1:length(sbsend{s})
            if 1==j
                ii= (1:sbsend{s}(1))';
            else
                ii= (sbsend{s}(j-1)+1:sbsend{s}(j))';
            end
            jg= sbs2gbs{s}(j);
            if 1==jg
                iig= (1:gbsend(jg))';
                ncol= gmuend(jg);
                gdisp= 0;
            else
                iig= (gbsend(jg-1)+1:gbsend(jg))';
                ncol= gmuend(jg)-gmuend(jg-1);
                gdisp= gmuend(jg-1);
            end
            Qmu{s}(ii,nmu(s)+1:nmu(s)+ncol)= Qmug(iig,gdisp+1:gdisp+ncol);
            nmu(s)= nmu(s) + ncol;
            smusend{s}(j)= nmu(s);
        end
        if 1==s
            nmuend(s)= nmu(s);
        else
            nmuend(s)= nmuend(s-1)+nmu(s);
        end
        Qmu{s}= Qmu{s}(:,1:nmu(s));
    end
end
sumnmu= sum(nmu);
clear Qmug iig ii gdisp ncol gmuend nmug;
if nnt==length(nthetav)
   clear scoord; 
end
toc(tQmu);
fprintf('\nsize of coarse problem is %d\n',ngc+sumnmu);

%% LU for subdomain problems
% do lu decomposition firstly to have as large as possible usable memory
if 1==nnt
tsetup= tic;    
fprintf('\nLU4F\n');
tsub= tic;
si= cell(Ns,1);              % indices of 'i' nodes ([sii;sdiri;srobin])
nsi= zeros(Ns,1);            % number of 'i' nodes
sr= cell(Ns,1);              % indices of 'r' nodes ('i' and 'b' nodes)
Lr= cell(Ns,1);              % LU decomposition of Arr
Ur= cell(Ns,1);              % Arr is the part of matrix A for 'r' nodes 
Pr= cell(Ns,1);              % Pr Arr Qr == Lr Qr
Qr= cell(Ns,1);  
for s= 1:Ns
    si{s}= [sii;sdiri{s};srobin{s}]; 
    nsi(s)= size(si{s},1);
    sr{s}= [si{s};sb{s}]; 
    si{s}= [];
    [Lr{s},Ur{s},Pr{s},Qr{s}]= lu(A{s}(sr{s},sr{s}));
    sr{s}= [];
end
toc(tsub);
disp('nnz of Lr{1}, Ur{1}, Pr{1}, Qr{1}');
disp([nnz(Lr{1}),nnz(Ur{1}),nnz(Pr{1}),nnz(Qr{1})]);
telapsed= telapsed + toc(tsetup);
end
%% setup coarse matrices on corners
if 1==nnt
tsetup= tic;    
fprintf('\ncoarse corners\n');
tcc= tic;
Rc= cell(Ns,1);              % restriction of a vector on global 'c' to a 
                             % vector on local 'c'  
Acr= cell(Ns,1);             % partitioned matrices of unassembled A
Arc= cell(Ns,1);
Acc= cell(Ns,1);
for s= 1:Ns
    if ~isempty(sc{s})
       nsc= size(sc{s},1);
       sc2gc= zeros(nsc,1); 
       for j= 1:length(scsend{s})
           if 1==j
               sstart= 1;
           else
               sstart= scsend{s}(j-1)+1;
           end
           igcs= scs2gcs{s}(j);
           gsend= gcsend(igcs);
           if 1==igcs
               gstart= 1;
           else
               gstart= gcsend(igcs-1)+1;
           end
           sc2gc(sstart:scsend{s}(j))= gstart:gsend;
       end
       Rc{s}= sparse(1:nsc,sc2gc(1:nsc),1,nsc,ngc,nsc);
    end
    scs2gcs{s}= [];
    % partition matrices for construct Fcc
    sr{s}= [sii;sdiri{s};srobin{s};sb{s}]; 
    Acr{s}= A{s}(sc{s},sr{s});
    Arc{s}= A{s}(sr{s},sc{s});
    Acc{s}= A{s}(sc{s},sc{s});
    sr{s}= []; A{s}= []; 
end
clear gcsend scs2gcs sc2gc A;
toc(tcc);
telapsed= telapsed + toc(tsetup);
end
%% assemble and lu of the coarse matrix with plane waves and corners
fprintf('\ncoarse\n');
tc= tic;
% contributions of subdomains
iFcs1= cell(Ns,1);               % block (1,1)
jFcs1= cell(Ns,1);
vFcs1= cell(Ns,1);
iFcs2= cell(Ns,1);               % block (1,2)
jFcs2= cell(Ns,1);
vFcs2= cell(Ns,1);
iFcs3= cell(Ns,1);               % block (2,1) 
jFcs3= cell(Ns,1);
vFcs3= cell(Ns,1);
iFcs4d= cell(Ns,1);              % diagonal of block (2,2) 
jFcs4d= cell(Ns,1);
vFcs4d= cell(Ns,1);
iFcs4do= cell(Ns,1);             %  
jFcs4do= cell(Ns,1);
vFcs4do= cell(Ns,1);
iFcs4o= cell(Ns,1);              % off diagonal of block (2,2)
jFcs4o= cell(Ns,1);
vFcs4o= cell(Ns,1);
nzzFs= 0;
npb= 0; npmu=0;
for s= 1:Ns
    % matrix of the coarse problem
    if ngc~=0
       F1= Qr{s}*(Ur{s}\(Lr{s}\(Pr{s}*Arc{s})));
       [iFcs1{s},jFcs1{s},vFcs1{s}]= find(Rc{s}'*(Acc{s}-Acr{s}*F1)*Rc{s});
    end
    if ngc~=0 && nmu(s)~=0
       if exist('solvetask','var') && 2==solvetask
           [iFcs3{s},jFcs3{s},vFcs3{s}]= find(-[sparse(nmu(s),nsi(s)), ...
                                              Qmu{s}.'*Sb{s}]*F1*Rc{s});
       else
           [iFcs3{s},jFcs3{s},vFcs3{s}]= find(-[sparse(nmu(s),nsi(s)), ...
                                              Qmu{s}'*Sb{s}]*F1*Rc{s});
       end
       clear F1;
       if exist('form','var') && 2==form  % assume Qmu^{st} = Qmu^{ts}
           % permute rows
           iFcs3old= iFcs3{s};
           for j= 1:length(sbsend{s})
               if 1==j
                   itemp= (iFcs3old<=smusend{s}(j));
                   sstart= 1;
               else
                   itemp= (iFcs3old<=smusend{s}(j) & iFcs3old>smusend{s}(j-1));
                   sstart= smusend{s}(j-1)+1;
               end         
               s2= sbnb{s}(j);
               j2= gbs2sbs{s2}(sbs2gbs{s}(j));
               if 1==j2
                   sstart2= 1;
               else
                   sstart2= smusend{s2}(j2-1)+1;
               end
               if s2>1
                   sstart2= sstart2 + nmuend(s2-1);
               end
               iFcs3{s}(itemp)= iFcs3old(itemp) + sstart2 - sstart;
               % == iFcsold+nmuend(s-1) + sstart2-(sstart+nmuend(s-1))
           end
       elseif s>1
           iFcs3{s}= iFcs3{s} + nmuend(s-1);
       end
    end
    if nmu(s)~=0
       % version 1 do
       if ~exist('symmetry','var') || 0==symmetry
           F2= Qr{s}*(Ur{s}\(Lr{s}\(Pr{s}*[sparse(nsi(s),nmu(s));Qmu{s}])));
       % version 2, complex symmtric
       else
           F2= Qr{s}*(Ur{s}\(Lr{s}\(Pr{s}*([sparse(nsi(s),nmu(s));Sb{s}*Qmu{s}]))));
       end
       % assume Qmu^{st} = Qmu^{ts}, othewise we should use Qmuopp{s}.' instead of Qmu{s}.'
       if exist('solvetask','var') && 2==solvetask  
           [iFcs4d{s},jFcs4d{s},vFcs4d{s}]= find(Qmu{s}.'*Qmu{s});
           [iFcs4do{s},jFcs4do{s},vFcs4do{s}]= find(-[sparse(nmu(s),nsi(s)), ...
                                                 Qmu{s}.'*Sb{s}]*F2);
       else
           [iFcs4d{s},jFcs4d{s},vFcs4d{s}]= find(Qmu{s}'*Qmu{s});
           [iFcs4do{s},jFcs4do{s},vFcs4do{s}]= find(-[sparse(nmu(s),nsi(s)), ...
                                                 Qmu{s}'*Sb{s}]*F2);
       end
       if exist('form','var') && 2==form  
           % permute rows
           iFcs4doold= iFcs4do{s};
           for j= 1:length(sbsend{s})
               if 1==j
                   itemp= (iFcs4doold<=smusend{s}(j));
                   sstart= 1;
               else
                   itemp= (iFcs4doold<=smusend{s}(j) & iFcs4doold>smusend{s}(j-1));
                   sstart= smusend{s}(j-1)+1;
               end         
               s2= sbnb{s}(j);
               j2= gbs2sbs{s2}(sbs2gbs{s}(j));
               if 1==j2
                   sstart2= 1;
               else
                   sstart2= smusend{s2}(j2-1)+1;
               end
               if s2>1
                   sstart2= sstart2 + nmuend(s2-1);
               end
               iFcs4do{s}(itemp)= iFcs4doold(itemp) + sstart2 - sstart;
           end
       elseif s>1
           iFcs4do{s}= iFcs4do{s} + nmuend(s-1);
       end
    end
    if ngc~=0 && nmu(s)~=0
       [iFcs2{s},jFcs2{s},vFcs2{s}]= find(-Rc{s}'*Acr{s}*F2);
    end
    clear F2;
    if nmu(s)~=0
       nzzFcs4o= 0;
       for j= 1:length(sbsend{s})
           if 1==j
               nmus= smusend{s}(j);
           else
               nmus= smusend{s}(j)-smusend{s}(j-1);
           end
           nzzFcs4o= nzzFcs4o+nmus*nmus; 
       end
       iFcs4o{s}= zeros(nzzFcs4o,1);
       jFcs4o{s}= zeros(nzzFcs4o,1);
       nzzFcs4o= 0;
       for j= 1:length(sbsend{s})
           s2= sbnb{s}(j);
           j2= gbs2sbs{s2}(sbs2gbs{s}(j));
           if 1==j
               sstart= 1;
               sbst= 1;
           else
               sstart= smusend{s}(j-1)+1;
               sbst= sbsend{s}(j-1)+1;
           end
           if 1==j2
               sstart2= 1;
               sbst2= 1;
           else
               sstart2= smusend{s2}(j2-1)+1;
               sbst2= sbsend{s2}(j2-1)+1;
           end   
           nmus2= (smusend{s}(j)-sstart+1)^2;
           if 1==s
               itemp= sstart:smusend{s}(j);
           else
               itemp= nmuend(s-1)+(sstart:smusend{s}(j));
           end
           if 1==s2
               jtemp= sstart2:smusend{s2}(j2);
           else
               jtemp= nmuend(s2-1)+(sstart2:smusend{s2}(j2));
           end
           [iFcs4o{s}(nzzFcs4o+1:nzzFcs4o+nmus2),jFcs4o{s}(nzzFcs4o+1: ...
               nzzFcs4o+nmus2)]= ndgrid(itemp,jtemp);
           if exist('solvetask','var') && 2==solvetask
               vFcs4o{s}(nzzFcs4o+1:nzzFcs4o+nmus2)= Qmu{s}(sbst:sbsend{s}(j),...
                    sstart:smusend{s}(j)).'*Qmu{s2}(sbst2:sbsend{s2}(j2),sstart2:smusend{s2}(j2));
           else
               vFcs4o{s}(nzzFcs4o+1:nzzFcs4o+nmus2)= Qmu{s}(sbst:sbsend{s}(j),...
                    sstart:smusend{s}(j))'*Qmu{s2}(sbst2:sbsend{s2}(j2),sstart2:smusend{s2}(j2));
           end
           nzzFcs4o= nzzFcs4o+nmus2;
       end
    end
    nzzFs= nzzFs + length(iFcs1{s}) + length(iFcs2{s}) ...
           + length(iFcs3{s}) + length(iFcs4d{s}) + length(iFcs4do{s}) ...
           + length(iFcs4o{s});
    % move the pointers to product 'b' nodes and to product mu
    npb= npb + size(sb{s},1);
    npmu= npmu + nmu(s);
end
clear iFcs4doold iFcs3old itemp jtemp smusend;
% assemble and LU decomposition of the coarse problems
iFcc= zeros(nzzFs,1); jFcc= zeros(nzzFs,1); vFcc= zeros(nzzFs,1);
nzzFs= 0; npmu= 0;
for s= 1:Ns
    iFcc(nzzFs+1:nzzFs+length(iFcs1{s}))= iFcs1{s};
    jFcc(nzzFs+1:nzzFs+length(iFcs1{s}))= jFcs1{s};
    vFcc(nzzFs+1:nzzFs+length(iFcs1{s}))= vFcs1{s};
    nzzFs= nzzFs + length(iFcs1{s});
    iFcc(nzzFs+1:nzzFs+length(iFcs2{s}))= iFcs2{s};
    jFcc(nzzFs+1:nzzFs+length(iFcs2{s}))= jFcs2{s}+ngc+npmu;
    vFcc(nzzFs+1:nzzFs+length(iFcs2{s}))= vFcs2{s};
    nzzFs= nzzFs + length(iFcs2{s});
    iFcc(nzzFs+1:nzzFs+length(iFcs3{s}))= iFcs3{s}+ngc;
    jFcc(nzzFs+1:nzzFs+length(iFcs3{s}))= jFcs3{s};
    vFcc(nzzFs+1:nzzFs+length(iFcs3{s}))= vFcs3{s};
    nzzFs= nzzFs + length(iFcs3{s});
    iFcc(nzzFs+1:nzzFs+length(iFcs4d{s}))= iFcs4d{s}+ngc+npmu;
    jFcc(nzzFs+1:nzzFs+length(iFcs4d{s}))= jFcs4d{s}+ngc+npmu;
    vFcc(nzzFs+1:nzzFs+length(iFcs4d{s}))= vFcs4d{s};
    nzzFs= nzzFs + length(iFcs4d{s});
    iFcc(nzzFs+1:nzzFs+length(iFcs4do{s}))= iFcs4do{s}+ngc;
    jFcc(nzzFs+1:nzzFs+length(iFcs4do{s}))= jFcs4do{s}+ngc+npmu;
    vFcc(nzzFs+1:nzzFs+length(iFcs4do{s}))= vFcs4do{s};
    nzzFs= nzzFs + length(iFcs4do{s});
    iFcc(nzzFs+1:nzzFs+length(iFcs4o{s}))= iFcs4o{s}+ngc;
    jFcc(nzzFs+1:nzzFs+length(iFcs4o{s}))= jFcs4o{s}+ngc;
    vFcc(nzzFs+1:nzzFs+length(iFcs4o{s}))= vFcs4o{s};
    nzzFs= nzzFs + length(iFcs4o{s});
    % move the pointer to product mu
    npmu= npmu + nmu(s);
end
clear iFcs1 jFcs1 vFcs1 iFcs2 jFcs2 vFcs2 iFcs3 jFcs3 vFcs3;
clear iFcs4d jFcs4d vFcs4d iFcs4do jFcs4do vFcs4do iFcs4o jFcs4o vFcs4o;
Fcc= sparse(iFcc,jFcc,vFcc);
clear iFcc jFcc vFcc;
if ~isempty(Fcc)
   [Lcc,Ucc,Pcc,Qcc] = lu(Fcc);
end
clear Fcc;
toc(tc);

%% initial subdomain solve 
%right hand side (rhsc;rhsmu;rhsl) for (uc;mu;lambda) after eliminating 
% ur=(ui,ub) will be used to eliminate [uc;mu] (add terms to rhsl) and 
% back solve uc from lambda
if 1==nnt
tsetup= tic;
fprintf('\ninit subsolve\n');
tinitsub= tic;
ur= cell(Ns,1);              % vectors on 'r' nodes
rhsc= zeros(ngc,1);          % rhs for global 'c' d.o.f.s in uc             
rhsl0= zeros(pbsend(Ns),1);          % rhs for two-field multiplier lambda 
npb= 0; npmu= 0;
for s= 1:Ns
    sr{s}= [sii;sdiri{s};srobin{s};sb{s}]; 
    ur{s}= Qr{s}*(Ur{s}\(Lr{s}\(Pr{s}*b{s}(sr{s})))); 
    sr{s}= [];
    if ~isempty(sc{s})
       rhsc= rhsc + Rc{s}'*( b{s}(sc{s}) - Acr{s}*ur{s} );
    end
    nsb= size(sb{s},1);
    if ~isempty(sb{s})
        rhsl0(npb+1:npb+nsb)= -Sb{s}*ur{s}(nsi(s)+1:nsi(s)+nsb);  % sign -
    end
    ur{s}= [];
    npb= npb + nsb;
    npmu= npmu + nmu(s);
end
% form 2: permutate right hand side for lambda
if exist('form','var') && 2==form
   rhsl0= LtL(rhsl0) - rhsl0; 
end
toc(tinitsub);
telapsed= telapsed + toc(tsetup);
fprintf('\n');
disp('init setup');
disp(telapsed);
end
%% initial coarse solve
fprintf('\ninit coarse solve\n');
tinitcoarse= tic;
% right hand side for mu, coefficients of planes waves
rhsmu= zeros(sumnmu,1);
npb= 0; npmu= 0;
for s= 1:Ns
    nsb= size(sb{s},1);
    if nmu(s)~=0 && nsb~=0
        if exist('solvetask','var') && 2==solvetask
            rhsmu(npmu+1:npmu+nmu(s))= Qmu{s}.'*rhsl0(npb+1:npb+nsb);
        else
            rhsmu(npmu+1:npmu+nmu(s))= Qmu{s}'*rhsl0(npb+1:npb+nsb);
        end
    end
    npb= npb + nsb;
    npmu= npmu + nmu(s);
end
% right hand side for the unpreconditioned system F*lambda = rhsl, 
% obtained by eliminating uc
rhsl= rhsl0;
if (ngc+sumnmu)~=0
   uc= Qcc * (Ucc\(Lcc\(Pcc*[rhsc;rhsmu])));
   rhsltemp= Fcb(uc);
   uc= [];
   if exist('form','var') && 2==form
       rhsl= rhsl + LtL(rhsltemp) - rhsltemp;
   else
       rhsl= rhsl + rhsltemp;
   end
end
% clear variables to be no use
clear rhsltemp;
toc(tinitcoarse);

%% Spectra plot
if nargout==7 
   specF= eigs(@F,pbsend(Ns),pbsend(Ns));
end
%% GMRES iteration
if exist('solvetask','var') && 0==solvetask
    u= [];
    flag= [];
    relres= [];
    iter= [];
    resvec= [];
    return;
end

if ~exist('solvetask','var') || 1==solvetask
    tol= 1e-6;  restart= [];  
    if ~exist('maxit','var')
        maxit= 200; 
    end
    % intitial guess: zeros 
    fprintf('\n gmres for nos \n');
    titer= tic;
    if ~exist('randinit','var') || 0==randinit
      [lambda,flag,relres,iter,resvec] = ... 
         gmres(@F,rhsl,restart,tol,maxit,[],[],zeros(pbsend(Ns),1)); 
    else
      [lambda,flag,relres,iter,resvec] = ... 
         gmres(@F,rhsl,restart,tol,maxit,[],[],rand(pbsend(Ns),1));  
    end
    toc(titer);
%     if exist('order','var')
%        filename= sprintf('c%dnos%d.mat',nnt,order);
%     else
%        filename= sprintf('c%dnos0.mat',nnt); 
%     end
%     save(filename,'flag','relres','iter','resvec');
    disp('relres='); disp(relres);
    disp('iter='); disp(iter);
end

disp('----------------------------------------------------------------');
if nnt~=length(nthetav)
   clear Lcc Ucc Pcc Qcc;
end

end
%% CSQMR iteration
% if exist('solvetask','var') && 2==solvetask
%     tol= 1e-6;  maxit= 200; 
%     [lambda,resvec] = csqmr(@F,rhsl,tol,maxit,zeros(pbsend(Ns),1));
%     flag= []; iter= length(resvec); relres= resvec(iter); 
% end
%% Richardson iteration on formulation 2
if exist('solvetask','var') && 3==solvetask
    tol= 1e-6; 
    if ~exist('randinit','var') || 0==randinit  
       lambda= zeros(pbsend(Ns),1);
    else
       lambda= rand(pbsend(Ns),1);
    end
    if ~exist('maxit','var')
        maxit= 200; 
    end
    relax= 1; flag= 0; 
    normrhsl= norm(rhsl);
    resvec= zeros(maxit,1);
    for iter= 1:maxit
        res= rhsl-F(lambda);
        relres= norm(res)/normrhsl;
        resvec(iter)= relres;
        if relres < tol
            break;
        end
        lambda= lambda + relax*res;
    end
    resvec= resvec(1:iter);
end
%% Solve out u
% tic
% first, uc from the (lambda,uc) equation with (ui,ub) eliminated
if (ngc+sumnmu)~=0
   uc= zeros(ngc+sumnmu,1); % reset to zeros
   npb= 0; 
   for s= 1:Ns
      nsb= size(sb{s},1);
      % version 1 
      if ~exist('symmetry','var') || 0==symmetry 
          ur{s}= Qr{s}*(Ur{s}\(Lr{s}\(Pr{s}*[sparse(nsi(s),1);lambda(npb+1:npb+nsb)])));
      else
      % version 2
          ur{s}= Qr{s}*(Ur{s}\(Lr{s}\(Pr{s}*[sparse(nsi(s),1); ...
              Sb{s}*lambda(npb+1:npb+nsb)])));
      end
      if ngc~=0
         uc(1:ngc)= uc(1:ngc) + Rc{s}'*(Acr{s}*ur{s});
         Acr{s}= [];
      end
      npb= npb + nsb;
   end
   clear Acr;
   rhsltemp= zeros(pbsend(Ns),1);
   npb= 0;  
   for s= 1:Ns
       nsb= size(sb{s},1);
       rhsltemp(npb+1:npb+nsb)= Sb{s}*ur{s}(nsi(s)+1:nsi(s)+nsb);
       npb= npb + nsb;
   end
   if exist('form','var') && 2==form
      rhsltemp= LtL(rhsltemp)-rhsltemp; 
   end
   rhsltemp= rhsltemp - LtL(lambda);
   npb= 0; npmu= 0; 
   for s= 1:Ns
      nsb= size(sb{s},1);
      if nmu(s)~=0
         if exist('solvetask','var') && 2==solvetask
            uc(ngc+npmu+1:ngc+npmu+nmu(s))= Qmu{s}.'*rhsltemp(npb+1:npb+nsb);
         else
            uc(ngc+npmu+1:ngc+npmu+nmu(s))= Qmu{s}'*rhsltemp(npb+1:npb+nsb);  
         end
      end
      npb= npb + nsb;
      npmu= npmu + nmu(s);
   end
   if ngc~=0
       rhsc= rhsc + uc(1:ngc);           % sign +
   end
   if sumnmu~=0
       rhsmu= rhsmu + uc(ngc+1:ngc+sumnmu);
   end
   uc= Qcc * (Ucc\(Lcc\(Pcc*[rhsc;rhsmu])));
end
clear rhsc rhsmu;

% second, ur= (ui,ub) from the (ui,ub,lambda,uc) equation (substitution of
% lambda and uc)
npb= 0;  npmu= 0; 
for s= 1:Ns
   nsb= size(sb{s},1);
   sr{s}= [sii;sdiri{s};srobin{s};sb{s}];
   if ~isempty(sc{s})
      b{s}(sr{s})= b{s}(sr{s}) - Arc{s}*(Rc{s}*uc(1:ngc));
      Arc{s}= [];
   end
   if nmu(s)~=0
       % version 1
       if ~exist('symmetry','var') || 0==symmetry
          b{s}(sb{s})= b{s}(sb{s}) - Qmu{s}*uc(ngc+npmu+1:ngc+npmu+nmu(s));
       % version 2
       else
          b{s}(sb{s})= b{s}(sb{s})-Sb{s}*Qmu{s}*uc(ngc+npmu+1:ngc+npmu+nmu(s));
       end
   end
   if ~exist('symmetry','var') || 0==symmetry
       b{s}(sb{s})= b{s}(sb{s}) - lambda(npb+1:npb+nsb); % sign -
   else
       b{s}(sb{s})= b{s}(sb{s}) - Sb{s}*lambda(npb+1:npb+nsb); 
       Sb{s}= [];
   end
   ur{s}= Qr{s}*(Ur{s}\(Lr{s}\(Pr{s}*(b{s}(sr{s})))));
   sr{s}= []; Lr{s}= []; Ur{s}= []; Pr{s}= []; Qr{s}= [];
   npb= npb + nsb;
   npmu= npmu + nmu(s);
end
clear Sb sr Lr Ur Pr Qr Arc;

% third, local uc substracted from global uc
ucs= cell(Ns,1);
if ngc~=0
   for s= 1:Ns
      if ~isempty(sc{s}) 
         ucs{s}= Rc{s}*uc(1:ngc);
      end
   end
end
clear Rc;

% finally, assemble global solution u
u= zeros((nx+1)*(ny+1)*(nz+1),1);
snx= nx/Nx; sny= ny/Ny; snz= nz/Nz;
for s= 1:Ns
   s2g= sn2gn(1:(snx+1)*(sny+1)*(snz+1),nx,ny,nz,Nx,Ny,Nz,s);
   si{s}= [sii;sdiri{s};srobin{s}];
   u(s2g(si{s}))= ur{s}(1:nsi(s));
   si{s}= []; sdiri{s}= []; srobin{s}= [];
   nsr= size(ur{s},1);
   u(s2g(sb{s}))= u(s2g(sb{s})) + 0.5*ur{s}((nsi(s)+1):nsr);
   ur{s}= [];
   if ~isempty(sc{s})
       for jj= 1:length(scsend{s}) % go through every corner segment
           [~,~,snb]= find(scnb{s}(jj,:));
           if snb>s  % s is the minimum
               if  1==jj
                   ics= 1:scsend{s}(jj);
               else
                   ics= scsend{s}(jj-1)+1:scsend{s}(jj);
               end
               u(s2g(sc{s}(ics)))= ucs{s}(ics);
           end
       end
   end
end
% disp('recover u');
% toc
%% matrix function Lb{m}'*Lb*x

%% matrix function L^T_b L_b
function xnew= LtL(x)
   xnew= zeros(size(x));
   npb= 0;
   for m= 1:Ns
      nsb= size(sb{m},1);
      % compute Lb{m}'*Lb*x
      for n= 1:length(sbsend{m})
         s2= sbnb{m}(n);
         n2= gbs2sbs{s2}(sbs2gbs{m}(n));
         if 1==n
             sbst= 1;
         else
             sbst= sbsend{m}(n-1)+1;
         end
         if 1==n2
             sbst2= 1;
         else
             sbst2= sbsend{s2}(n2-1)+1;
         end   
         itemp= npb+(sbst:sbsend{m}(n));
         if 1==s2
             itemp2= sbst2:sbsend{s2}(n2);
         else
             itemp2= pbsend(s2-1)+(sbst2:sbsend{s2}(n2));
         end
         xnew(itemp)= x(itemp)+x(itemp2);
      end
      npb= npb + nsb;
   end
end

%% matrix function Fcb
function yb= Fcb(xc)
    yb= zeros(pbsend(Ns),1);
    % [0,LtL*Qmu] 
    npb= 0; npmu= 0;
    for m = 1:Ns
        nsb= size(sb{m},1);
        if nmu(m)~=0
             yb(npb+1:npb+nsb)= yb(npb+1:npb+nsb) - Qmu{m} ...
                 *xc((ngc+npmu+1:ngc+npmu+nmu(m)));
        end
        npb= npb + nsb;
        npmu= npmu + nmu(m);
    end
    yb= LtL(yb);
    % Sr*inv(Zrr)[Zrc*Rc, Ir*Qmu]
    npb= 0; npmu= 0;
    for m= 1:Ns
         nsb= size(sb{m},1);
         % attention: make sure matrix*vector done first
         if ~isempty(sc{m})
             % version 1
             if ~exist('symmetry','var') || 0==symmetry
                 yb(npb+1:npb+nsb)= yb(npb+1:npb+nsb) + [sparse(nsb,nsi(m)), ...
                     Sb{m}]*(Qr{m}*(Ur{m}\(Lr{m}\(Pr{m}*(Arc{m}*(Rc{m}*xc(1:ngc)) + ... 
                     [sparse(nsi(m),1);Qmu{m}*xc((ngc+npmu+1:ngc+npmu+nmu(m)))])))));
             else
             % version 2
                 yb(npb+1:npb+nsb)= yb(npb+1:npb+nsb) + [sparse(nsb,nsi(m)); ...
                 Sb{m}]*(Qr{m}*(Ur{m}\(Lr{m}\(Pr{m}*(Arc{m}*(Rc{m}*xc(1:ngc)) + ... 
                 [sparse(nsi(m),1);Sb{m}*Qmu{m}*xc((ngc+npmu+1:ngc+npmu+nmu(m)))])))));
             end
         end
         % move the pointers to product 'b' nodes and to product mu
         npb= npb + nsb;
         npmu= npmu + nmu(m);
    end
end

%% matrix function F of the interface problem F*lambda = rhsl for 
%  the two-field multiplier lambda, robin to robin
function y = F(x) % y= F*x
   y= zeros(size(x));  
   % - Sr*inv(Zrr)*Ir
   npb= 0; npmu= 0;
   for m= 1:Ns
      nsb= size(sb{m},1);
      % version 1
      if ~exist('symmetry','var') || 0==symmetry
          ur{m}= Qr{m}*(Ur{m}\(Lr{m}\(Pr{m}*[zeros(nsi(m),1);x(npb+1:npb+nsb)])));
          % faster than [sparse(nsi(m, ...
      else
      % version 2
          ur{m}= Qr{m}*(Ur{m}\(Lr{m}\(Pr{m}*[zeros(nsi(m),1);Sb{m}*x(npb+1:npb+nsb)])));
      end
      y(npb+1:npb+nsb)= y(npb+1:npb+nsb)-Sb{m}*ur{m}(nsi(m)+1:nsi(m)+nsb);
      npb= npb + nsb;
      npmu= npmu + nmu(m);
   end
   if exist('form','var') && 2==form
      y= LtL(y)-y; 
   end
   % + LtL*x
   y= y + LtL(x);
   % prepare rhs for coarse problem
   uc= zeros(ngc+sumnmu,1);
   npb= 0; npmu= 0;
   for m= 1:Ns
      nsb= size(sb{m},1);
      if ngc~=0
         uc(1:ngc)= uc(1:ngc) + Rc{m}'*(Acr{m}*ur{m});
      end
      ur{m}= [];
      if nmu(m)~=0
          if exist('solvetask','var') && 2==solvetask
              uc(ngc+npmu+1:ngc+npmu+nmu(m))= -Qmu{m}.'*y(npb+1:npb+nsb);
          else
              uc(ngc+npmu+1:ngc+npmu+nmu(m))= -Qmu{m}'*y(npb+1:npb+nsb);
          end
      end
      npb= npb + nsb;
      npmu= npmu + nmu(m);
   end
   % coarse solve and correction
   if (ngc+sumnmu)~=0
      uc= Qcc * (Ucc\(Lcc\(Pcc*uc)));
      ytemp= Fcb(uc);
      uc= [];
      if ~exist('form','var') || 2~=form
          y= y - ytemp;
      else
          y= y - LtL(ytemp) + ytemp;
      end
   end
end
end    % end of fetidp function