function getM2d()
% for use by stima2d.m
% [-1,1]^2 element mass matrix corresponding to (u,v)
global M2d;
M2d= [  1/9,  1/18, 1/36, 1/18
        1/18,  1/9, 1/18, 1/36
        1/36, 1/18,  1/9, 1/18
        1/18, 1/36, 1/18,  1/9]*4;
    