 function postprocess()
        if exist('resvec','var') && ~isempty(resvec)
            figure;
            semilogy(resvec);
            xlabel('iteration');
            ylabel('residual');
            disp('relres='); disp(relres);
        end
        if exist('udd','var') && ~isempty(udd)
            disp('iter='); disp(iter);
            switch flag
                case 1
                    disp('gmres reached maximum iteration number but did not converge');
                case 2
                    disp('preconditioner M is ill-conditioned');
                case 3
                    disp('gmres stagnated (two consective iterates were the same)');
            end
        end
        if (exist('udirect','var') && exist('udd','var') && ~isempty(udd))
            errdd2fem= abs(udd - udirect);
            maxerrdd2fem= max(errdd2fem);
            disp('maxerrdd2fem='); disp(maxerrdd2fem);
            %     [X,Y,Z]= meshgrid(xl:hx:xr,yl:hy:yr,zl:hz:zr);
            %     errdd2fem= reshape(errdd2fem,nx+1,ny+1,nz+1);
            %     slice(X,Y,Z,errdd2fem, 0.5, 0.5, 0.5);
            %     xlabel('x'); ylabel('y'); zlabel('z');
            %     colorbar;
            %     figure;
            %     [X2,Y2]= ndgrid(xl:hx:xr,yl:hy:yr);
            %     surf(X2,Y2,errdd2fem(:,:,nz/2+1)); xlabel('x'); ylabel('y');
        end
        if exist('uexact','var') && exist('udirect','var') && exist('coordinates','var')
            errfem2cont= uexact(coordinates) - udirect;
            maxerrfem2cont= max(abs(errfem2cont)); % compare to uexact
            disp('maxerrfem2cont='); disp(maxerrfem2cont);
        end
        if (exist('specF','var')) && ~isempty(specF)
            figure;
            plot(specF,'o','MarkerSize',6);
        end       
    end  % -- end of function postprocess