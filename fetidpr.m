function [u,flag,relres,iter,resvec,specF]= fetidpr(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr, ...
    zl,zr,GammaD,GammaR,nx,ny,nz,Nx,Ny,Nz,nthetav,nphiv,solvetask,withprecond,randinit)
% use fetidp method to solve the Helmholtz equation
% We assume the set of 'b' nodes are not empty, i.e. Nx is a proper factor
% of nx and similarly for Ny, Nz, because we iterate vector on 'b' nodes.
% Different from the fetidp.m for advection problem, here lambda is 
% minus lambda there because a minus is putted before Br' there. So F are
% the same but rhsl are different by a minus.
% ---- modified from fetidp.m to include regularization term on the
% interface, the signs for two-sides of interface is also changed to
% checkboard pattern, and the preconditioner need action of inverse Mbb
% ---- difference from fetidpr1.m, for A{s} the integration is taken on the 
% complete faces including corners
% ---- difference from fetidpr2.m, Mbb is deleted.
% ---- difference from fetidpr3.m, A{s} regularization matrix is lumped

tinitsetup= tic;
%% partition of geometry
% subdomain meshes
fprintf('\npartition \n');
tpart= tic;
Ns= Nx*Ny*Nz;
[scoord,selem,srobinelem,sii,sdiri,srobin,sb,sc,sbsend,sbnb,scsend,scnb,bdintelems] ...
    = qubepartmesh(xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR,Nx,Ny,Nz);

% global interface 'b' and corner 'c' nodes
gtol= [(xr-xl)/nx/10,(yr-yl)/ny/10,(zr-zl)/nz/10]; % geometry tolerance 
[gbs2sbs,sbs2gbs,ngc,gcsend,scs2gcs,ngb,gbsend,gbsma]= gbc(scoord,sc,...
    sbsend,sbnb,scsend,scnb,Ns,gtol);
toc(tpart);

%% assemble subdomain matrices and rhs 
fprintf('\nassembly \n');
tassemble= tic;
b= cell(Ns,1); A= cell(Ns,1);
getAM(); getgpw(); % for use by stima.m and rhs.m
getM2d(); getgpw2d(); % for use by stima2d and rhs2d
[I,J]= ndgrid(1:8,1:8); I= I(:); J= J(:);
global unused_src numsrc;
for s= 1:Ns
    % first assembly without respect to boundary condition
    ne= size(selem{s},1);
    ii= zeros(64,ne); jj= zeros(64,ne); 
    Aa= zeros(64,ne);
    b{s}= zeros(size(scoord{s},1),1);
    for j= 1:ne
       Ae= stima(scoord{s}(selem{s}(j,:),:),k2);
       ii(:,j)= selem{s}(j,I); % assemble vectors
       jj(:,j)= selem{s}(j,J); % for speed
       Aa(:,j)= Ae(:);
    end
    A{s}= sparse(ii,jj,Aa);
    if ~(isa(f,'float') && 0==f && isempty(pointsource))
       for j = 1:ne
           b{s}(selem{s}(j,:)) = b{s}(selem{s}(j,:)) + ...
               rhs(scoord{s}(selem{s}(j,:),:),f,pointsource);
       end
    end
    selem{s}= [];

    % treatment of Robin or Neumann boundary
    if ~(isa(a,'float') && 0==a)
       for j = 1:size(srobinelem{s},1)
           n4e= srobinelem{s}(j,:);
           A{s}(n4e,n4e)= A{s}(n4e,n4e) + stima2d(scoord{s}(n4e,:),a);
       end
    end
    if ~(isa(uR,'float') && 0==uR)
       for j = 1:size(srobinelem{s},1)
           n4e= srobinelem{s}(j,:);
           b{s}(n4e) = b{s}(n4e) + rhs2d(scoord{s}(n4e,:),uR);
       end
    end
    srobinelem{s}= [];
    
    % treatment of regularization term
    [interb,signr]= getinterb(Nx,Ny,Nz,s,1);
    for ij= 1:length(interb)
        if isa(k2,'function_handle') || isa(k2,'inline')
            reg= @(x)(signr(ij)*sqrt(k2(x))*1i);
        elseif isa(k2,'float')
            reg= signr(ij)*sqrt(k2)*1i;
        end
        for j = 1:size(bdintelems{interb(ij)},1)
           n4e= bdintelems{interb(ij)}(j,:);
           A{s}(n4e,n4e)= A{s}(n4e,n4e) + diag(sum(stima2d(scoord{s}(n4e,:),reg))); 
        end
    end
    
    % treatment of Dirichlet lastly, otherwise Robin changes Dirichlet
    if isa(uD,'float')
       b{s}(sdiri{s})= uD*ones(size(sdiri{s},1),1);
    elseif isa(uD,'inline') || isa(uD,'function_handle')
       b{s}(sdiri{s}) = uD(scoord{s}(sdiri{s},:));
    end
    if ~isempty(sdiri{s})
        [ii, jj]=find(A{s});
        idxofzero = ismember(ii,sdiri{s}); 
        A{s}= setsparse(A{s},ii(idxofzero),jj(idxofzero),0);
        A{s}= setsparse(A{s},double(sdiri{s}),double(sdiri{s}),1);
    end
end
unused_src= []; numsrc= [];
clear selem srobinelem ii jj Aa Ae I J n4e idxofzero;
toc(tassemble);
telaps= toc(tinitsetup);
%% setup plane waves basis on global 'b' segments
for nnt= 1:length(nthetav)
    ntheta= nthetav(nnt);
    nphi= nphiv(nnt);
    
fprintf('\n--------------- planewaves %d   %d --------------\n',ntheta,nphi);
tQmu= tic;
% directions of plane waves
theta= linspace(0,pi/2,ntheta+2); theta= theta(2:ntheta+1);
phi= linspace(-pi,pi,nphi+1);  phi= phi(2:nphi+1); phi0= phi;
[theta,phi]= ndgrid(theta,phi); theta= theta(:)'; phi= phi(:)';
if ntheta>-1
    theta= [theta, -theta, zeros(1,nphi), pi/2, -pi/2];
    phi= [phi, pi+phi, phi0, 0, 0];  
    numplw= 2*ntheta*nphi+nphi+2;
else
    numplw= 0;
end
% set up Qmu
Qmu= sparse(ngb,length(gbsend)*numplw);
nmu= 0;
tolQmu= 0.1;             % threhold to filter Qmu
if numplw~=0
    for j= 1:length(gbsend)
        sma= gbsma(j);            % master subdomain of this segment
        sbs= gbs2sbs{sma}(j);          % index of this segment in the master
        if 1==sbs
           coords= scoord{sma}(sb{sma}(1:sbsend{sma}(1)),:);
        else
           coords= scoord{sma}(sb{sma}(sbsend{sma}(sbs-1)+1: ...
                                      sbsend{sma}(sbs)),:);
        end
        if 1==j
            ii= (1:gbsend(j))';
        else
            ii= (gbsend(j-1)+1:gbsend(j))';
        end
        if isa(k2,'function_handle') || isa(k2,'inline')
           qbss= exp(1i*(coords.*repmat(sqrt(k2(coords)),1,3))* ...
               [cos(theta).*cos(phi);cos(theta).*sin(phi);sin(theta)]);
        elseif isa(k2,'float')
           qbss= exp(1i*sqrt(k2)*coords* ...
               [cos(theta).*cos(phi);cos(theta).*sin(phi);sin(theta)]); 
        end
    %     qbss= [qbss,ones(size(qbss,1),1)]; % averages
        [qqbss,rqbss,~]= qr(qbss);
        rjj= diag(rqbss);
        qbss= qqbss(:,abs(rjj)>tolQmu); 
        Qmu(ii,nmu+1:nmu+size(qbss,2))= qbss;
        nmu= nmu + size(qbss,2);
    end
    Qmu= Qmu(:,1:nmu);
end
clear theta phi coords ii qbss qqbss rqbss rjj;
if nnt==length(nthetav)
   clear scoord gbs2sbs gbsma; 
end
toc(tQmu);
fprintf('\nsize of coarse problem is %d\n',ngc+nmu);

%% LU for subdomain problems
% do lu decomposition firstly to have as large as possible usable memory
if 1==nnt
tinitsetup= tic;    
fprintf('\nLU4F\n');
tsub= tic;
sr= cell(Ns,1);              % indices of 'r' nodes ('i' and 'b' nodes)
Lr= cell(Ns,1);              % LU decomposition of Arr
Ur= cell(Ns,1);              % Arr is the part of matrix A for 'r' nodes 
Pr= cell(Ns,1);              % Pr Arr Qr == Lr Qr
Qr= cell(Ns,1);  
for s= 1:Ns
    sr{s}= [sii;sdiri{s};srobin{s};sb{s}]; 
    [Lr{s},Ur{s},Pr{s},Qr{s}]= lu(A{s}(sr{s},sr{s}));
    sr{s}= [];
end
toc(tsub);
telaps= telaps + toc(tinitsetup);
disp('nnz of Lr{1}, Ur{1}, Pr{1}, Qr{1}');
disp([nnz(Lr{1}),nnz(Ur{1}),nnz(Pr{1}),nnz(Qr{1})]);

end
%% LU of Aii for preconditioner PD
if 1==nnt
tinitsetup= tic;    
fprintf('\nLU4PD\n'); 
tpd= tic;
si= cell(Ns,1);              % indices of 'i' nodes ([sii;sdiri;srobin])
nsi= zeros(Ns,1);            % number of 'i' nodes
Li= cell(Ns,1);              % LU of Aii 
Ui= cell(Ns,1); 
Pi= cell(Ns,1);
Qi= cell(Ns,1);
for s= 1:Ns
    si{s}= [sii;sdiri{s};srobin{s}]; 
    nsi(s)= size(si{s},1);
    if ~exist('withprecond','var') || 1==withprecond
        [Li{s},Ui{s},Pi{s},Qi{s}]= lu(A{s}(si{s},si{s}));
    end
    si{s}= [];
end
toc(tpd);
telaps= telaps + toc(tinitsetup);
end
%% setup restriction matices
if 1==nnt
tinitsetup= tic;    
fprintf('\nrestriction\n');
tB= tic;
Bb= cell(Ns,1);              % signed extension of local 'b' to global 'b'                         
Br= cell(Ns,1);              % signed extension of local 'r' to global 'b'
for s= 1:Ns
    if ~isempty(sb{s})
        nsb= size(sb{s},1);
        sb2gb= zeros(nsb,1);
        sbsign= zeros(nsb,1);
        for j= 1:length(sbsend{s})
           if 1==j
               sstart= 1;
           else
               sstart= sbsend{s}(j-1)+1;
           end
           igbs= sbs2gbs{s}(j);
           gsend= gbsend(igbs);
           if 1==igbs
               gstart= 1;
           else
               gstart= gbsend(igbs-1)+1;
           end
           sb2gb(sstart:sbsend{s}(j))= gstart:gsend;
           sbsign(sstart:sbsend{s}(j))= (sbnb{s}(j)>s) - (sbnb{s}(j)<s);
        end
        Bb{s}= sparse(sb2gb(1:nsb),1:nsb,sbsign,ngb,nsb,nsb);
        Br{s}= sparse(sb2gb(1:nsb),nsi(s)+(1:nsb),sbsign,ngb,nsb+nsi(s),nsb);
    end
    sbnb{s}= [];  sbs2gbs{s}= [];
end
clear sb2gb sbnb sbs2gbs;
toc(tB);
telaps= telaps + toc(tinitsetup);
end
if length(nthetav)==nnt
    clear gbsend sbsend;
end
%% setup of coarse matrix on corners
if 1==nnt
tinitsetup= tic;
fprintf('\ncoarse corners\n');
tcc= tic;
Acr= cell(Ns,1);             % partitioned matrices of A      
Arc= cell(Ns,1); 
Acc= cell(Ns,1);
Rc= cell(Ns,1);              % restriction of a vector on global 'c' to a 
                             % vector on local 'c'   
for s= 1:Ns
    % set up Rc{s}
    if size(sc{s})~=0
       nsc= size(sc{s},1);
       sc2gc= zeros(nsc,1); 
       for j= 1:length(scsend{s})
           if 1==j
               sstart= 1;
           else
               sstart= scsend{s}(j-1)+1;
           end
           igcs= scs2gcs{s}(j);
           gsend= gcsend(igcs);
           if 1==igcs
               gstart= 1;
           else
               gstart= gcsend(igcs-1)+1;
           end
           sc2gc(sstart:scsend{s}(j))= gstart:gsend;
       end
       Rc{s}= sparse(1:nsc,sc2gc(1:nsc),1,nsc,ngc,nsc);
    else
        Rc{s}= zeros(sc{s},ngc);
    end
    scs2gcs{s}= [];
    % partition matrices for construct Fcc
    sr{s}= [sii;sdiri{s};srobin{s};sb{s}]; 
    Acr{s}= A{s}(sc{s},sr{s});
    Arc{s}= A{s}(sr{s},sc{s});
    Acc{s}= A{s}(sc{s},sc{s});
    sr{s}= [];
end
clear gcsend scs2gcs sc2gc;
toc(tcc);
telaps= telaps + toc(tinitsetup);
end
%% assemble and lu of the coarse matrix with plane waves and corners
fprintf('\ncoarse\n');
tc= tic;
iFcs= cell(Ns,1); jFcs= cell(Ns,1); vFcs= cell(Ns,1);
nzzFs= 0;
for s= 1:Ns
    % matrix of the coarse problem
    if ngc~=0
        F1= Qr{s}*(Ur{s}\(Lr{s}\(Pr{s}*Arc{s})));
    else
        F1= sparse(size(Lr{s},1),0);
    end
    if nmu~=0
       F2= Qr{s}*(Ur{s}\(Lr{s}\(Pr{s}*(Br{s}'*Qmu))));
    else
        F2= sparse(size(Lr{s},1),0);
    end
    [iFcs{s},jFcs{s},vFcs{s}]= find( ...
          [Rc{s}'*(Acc{s}-Acr{s}*F1)*Rc{s},  -Rc{s}'*Acr{s}*F2;
           -Qmu'*Br{s}*F1*Rc{s}, -Qmu'*Br{s}*F2]);
    clear F1 F2;
    nzzFs= nzzFs + length(iFcs{s});
end
iFcc= zeros(nzzFs,1); jFcc= zeros(nzzFs,1); vFcc= zeros(nzzFs,1);
nzzFs= 0;
for s= 1:Ns
    iFcc(nzzFs+1:nzzFs+length(iFcs{s}))= iFcs{s};
    jFcc(nzzFs+1:nzzFs+length(iFcs{s}))= jFcs{s};
    vFcc(nzzFs+1:nzzFs+length(iFcs{s}))= vFcs{s};
    nzzFs= nzzFs + length(iFcs{s});
    iFcs{s}= [];  jFcs{s}= []; vFcs{s}= [];
end
clear iFcs jFcs vFcs;
Fcc= sparse(iFcc,jFcc,vFcc);
clear iFcc jFcc vFcc;
if ~isempty(Fcc)
   [Lcc,Ucc,Pcc,Qcc] = lu(Fcc);
end
clear Fcc;
toc(tc);

%% initial subdomain solve 
% r.h.s.(rhsc;rhsmu;rhsl) for (uc;mu;lambda) after eliminating 
% ur=(ui,ub), to be used to eliminate [uc;mu] (add terms to rhsl) and 
% back solve uc from lambda
if   1==nnt
tinitsetup= tic;    
fprintf('\ninit subsolve\n');
tinitsub= tic;
rhsc= zeros(ngc,1);           % rhs for global 'c' d.o.f. uc             
rhsl0= zeros(ngb,1);           % rhs for the multiplier lambda on 'b' nodes
ur= cell(Ns,1);               % vectors on 'r' nodes
for s= 1:Ns
    sr{s}= [sii;sdiri{s};srobin{s};sb{s}]; 
    ur{s}= Qr{s}*(Ur{s}\(Lr{s}\(Pr{s}*b{s}(sr{s})))); 
    sr{s}= [];
    if ~isempty(sc{s})
       rhsc= rhsc + Rc{s}'*( b{s}(sc{s}) - Acr{s}*ur{s} );
    end
    rhsl0= rhsl0 + Br{s} * ur{s};  % sign +
    ur{s}= [];
end
toc(tinitsub);
telaps= telaps + toc(tinitsetup);
end
%% initial coarse solve
% right hand side for mu, coefficients of planes waves
fprintf('\ninit coarse solve\n');
tinitcoarse= tic;
rhsmu= zeros(nmu,1);
if nmu~=0
    rhsmu= -Qmu'*rhsl0;
end
% right hand side rhsl for the unpreconditioned system F*lambda = rhsl, 
% obtained by eliminating uc; set up weighting vector
if (ngc+nmu)~=0
   uc= Qcc * (Ucc\(Lcc\(Pcc*[rhsc;rhsmu])));
end
rhsl= rhsl0;
for s= 1:Ns
     % attention: make sure matrix*vector done first
     if ~isempty(sc{s})
         rhsl= rhsl - Br{s}*(Qr{s}*(Ur{s}\(Lr{s} ...  % sign -
             \(Pr{s}*(Arc{s}*(Rc{s}*uc(1:ngc)) + ... 
             Br{s}'*(Qmu*uc((ngc+1:ngc+nmu))))))));  
     end
end
uc= [];
toc(tinitcoarse);

%% calculating weights
if 1==nnt
tinitsetup= tic;    
fprintf('\nweights\n');
tw= tic;
Abb= cell(Ns,1); 
totalstiff= zeros(ngb,1);     % use to compute wts
wts= cell(Ns,1);              % subdomain weights, use for precond. PD & PL
for s= 1:Ns
    % total stiffness and local stiffness, use for preconditioner and
    % assembly of global solution
    Abb{s}= A{s}(sb{s},sb{s});
    wts{s}= diag(Abb{s});
    totalstiff = totalstiff + (Bb{s}.*Bb{s})*wts{s};
end
for s=1:Ns
    wts{s}= 1- wts{s}./((Bb{s}.*Bb{s})'*totalstiff); % 1- ..., modified 22-09-2011
end
clear totalstiff;
toc(tw);
telaps= telaps + toc(tinitsetup);
end
%% partition matrices for use of preconditioner PD and PL
if 1==nnt
tinitsetup= tic;    
fprintf('\npreconditioner\n');
tpre= tic;
Aib= cell(Ns,1);             
Abi= cell(Ns,1); 
for s= 1:Ns
    si{s}= [sii;sdiri{s};srobin{s}];  
    Aib{s}= A{s}(si{s},sb{s});
    Abi{s}= A{s}(sb{s},si{s});
    si{s}= [];
    A{s}= []; 
end
clear A;
toc(tpre);
telaps= telaps + toc(tinitsetup);
disp('init setup without plw');
disp(telaps);
end
%% plot the spectra
if nargout==6
    if exist('withprecond','var') && ~withprecond
        specF= eigs(@F,ngb,ngb);
    elseif exist('withprecond','var') && 2==withprecond
        specF= eigs(@(x)(PL(F(x))),ngb,ngb);
    else
        specF= eigs(@(x)(PD(F(x))),ngb,ngb);
    end
end
%% GMRES iteration
if exist('solvetask','var') && 0==solvetask
    u= [];
    flag= [];
    relres= [];
    iter= [];
    resvec= [];
    return;
end

if ~exist('solvetask','var') || 1==solvetask
    tol= 1e-6;  restart= [];  maxit= 200;  
    % zeros initial guess
    if exist('withprecond','var')
      if 0==withprecond
       fprintf('\n gmres with no preconditioner\n');
       titer= tic;
       if ~exist('randinit','var') || 0==randinit
           [lambda,flag,relres,iter,resvec] = gmres(@F,rhsl,restart,tol,maxit,[],[],zeros(ngb,1));
       else
           [lambda,flag,relres,iter,resvec] = gmres(@F,rhsl,restart,tol,maxit,[],[],rand(ngb,1));
       end
       toc(titer);
       % filename= sprintf('c%dn.mat',nnt);
       % save(filename,'flag','relres','iter','resvec');
       disp('relres='); disp(relres);
       disp('iter='); disp(iter);
      elseif 2==withprecond
       fprintf('\n gmres with lumped preconditioner\n');
       titer= tic;
       if ~exist('randinit','var') || 0==randinit
           [lambda,flag,relres,iter,resvec] = gmres(@F,rhsl,restart,tol,maxit,@PL,...
                                             [],zeros(ngb,1)); 
       else
           [lambda,flag,relres,iter,resvec] = gmres(@F,rhsl,restart,tol,maxit,@PL,...
                                             [],rand(ngb,1)); 
       end
       toc(titer);
       % filename= sprintf('c%dl.mat',nnt);
       % save(filename,'flag','relres','iter','resvec');
       disp('relres='); disp(relres);
       disp('iter='); disp(iter);
      else
       fprintf('\n gmres with Dirichlet preconditioner\n');
       titer= tic;
       if ~exist('randinit','var') || 0==randinit
          [lambda,flag,relres,iter,resvec] = gmres(@F,rhsl,restart,tol,maxit,@PD,...
                                             [],zeros(ngb,1));
       else
           [lambda,flag,relres,iter,resvec] = gmres(@F,rhsl,restart,tol,maxit,@PD,...
                                             [],rand(ngb,1));
       end
       toc(titer);
       %  filename= sprintf('c%dd.mat',nnt);
       %  save(filename,'flag','relres','iter','resvec');
       disp('relres='); disp(relres);
       disp('iter='); disp(iter);
      end
    else
       fprintf('\n gmres with no preconditioner\n');
       titer= tic;
       if ~exist('randinit','var') || 0==randinit
           [~,flag,relres,iter,resvec] = gmres(@F,rhsl,restart,tol,maxit,[],[],zeros(ngb,1)); 
       else
           [~,flag,relres,iter,resvec] = gmres(@F,rhsl,restart,tol,maxit,[],[],rand(ngb,1));
       end
       toc(titer);
       %  filename= sprintf('c%dnr.mat',nnt);
       %  save(filename,'flag','relres','iter','resvec');
       disp('relres='); disp(relres);
       disp('iter='); disp(iter);
       fprintf('\n gmres with lumped preconditioner\n');
       titer= tic;
       if ~exist('randinit','var') || 0==randinit
           [~,flag,relres,iter,resvec] = gmres(@F,rhsl,restart,tol,maxit,@PL,...
                                             [],zeros(ngb,1)); 
       else
           [~,flag,relres,iter,resvec] = gmres(@F,rhsl,restart,tol,maxit,@PL,...
                                             [],rand(ngb,1));
       end
       toc(titer);
       %  filename= sprintf('c%dlr.mat',nnt);
       %  save(filename,'flag','relres','iter','resvec');
       disp('relres='); disp(relres);
       disp('iter='); disp(iter);
       fprintf('\n gmres with Dirichlet preconditioner\n');
       titer= tic;
       if ~exist('randinit','var') || 0==randinit
           [lambda,flag,relres,iter,resvec] = gmres(@F,rhsl,restart,tol,maxit,@PD,...
                                             [],zeros(ngb,1));
       else
           [lambda,flag,relres,iter,resvec] = gmres(@F,rhsl,restart,tol,maxit,@PD,...
                                             [],rand(ngb,1));
       end
       toc(titer);
       % filename= sprintf('c%ddr.mat',nnt);
       % save(filename,'flag','relres','iter','resvec');
       disp('relres='); disp(relres);
       disp('iter='); disp(iter);
    end
end

disp('----------------------------------------------------------------');
if nnt~=length(nthetav)
   clear  Lcc Ucc Pcc Qcc;
end
   
end
%% CSQMR iteration
% if exist('solvetask','var') && 2==solvetask
%     tol= 1e-6;  maxit= 100; 
%     if exist('withprecond','var') && ~withprecond
%        [lambda,resvec] = csqmr(@F,rhsl,tol,maxit,zeros(ngb,1));
%     else
%        [lambda,resvec] = csqmr(@(x)(PD(F(x))),rhsl,tol,maxit,zeros(ngb,1)); 
%     end
%     flag= []; iter= length(resvec); relres= resvec(iter); 
% end
%% Richardson iterion
% if exist('solvetask','var') && 3==solvetask
%     tol= 1e-6; maxit= 100; lambda= zeros(ngb,1);
%     relax= 1; flag= []; normrhsl= norm(rhsl);
%     resvec= zeros(maxit,1);
%     for iter= 1:maxit
%         res= rhsl-F(lambda);
%         relres= norm(res)/normrhsl;
%         resvec(iter)= relres;
%         if relres < tol
%             break;
%         end
%         lambda= lambda + relax*PD(res);
%     end
%     resvec= resvec(1:iter);
% end
%% Solve out u
fprintf('\nrecover\n');
tback= tic;
% first uc from the (lambda,uc) equation with (ui,ub) eliminated
clear Bb Li Pi Qi Aib Abi Abb rhsl0 rhsl;
if (ngc+nmu)~=0
   uc= zeros(ngc+nmu,1); % reset to zeros
   for s= 1:Ns 
       ur{s}= Qr{s}*(Ur{s}\(Lr{s}\(Pr{s}*(Br{s}'* lambda))));
       if ~isempty(Rc{s})
          uc= uc + [Rc{s}'*(Acr{s}*ur{s});Qmu'*(Br{s}*ur{s})];
       end
       Acr{s}= [];
   end
   rhsc= rhsc + uc(1:ngc);           % sign +
   rhsmu= rhsmu + uc(ngc+1:ngc+nmu);
   uc= Qcc * (Ucc\(Lcc\(Pcc*[rhsc;rhsmu])));
end
clear Acr rhsc rhsmu;

% second ur= (ui,ub) from the (ui,ub,lambda,uc) equation (substitution of
% lambda and uc)
for s= 1:Ns
   sr{s}= [sii;sdiri{s};srobin{s};sb{s}];
   if ~isempty(sc{s})
      b{s}(sr{s})= b{s}(sr{s}) - Arc{s}*(Rc{s}*uc(1:ngc)) - ...
          Br{s}'*(Qmu*uc(ngc+1:ngc+nmu));
   end
   Arc{s}= [];
   b{s}(sr{s})= b{s}(sr{s}) - Br{s}'*lambda; % sign -
   ur{s}= Qr{s}*(Ur{s}\(Lr{s}\(Pr{s}*(b{s}(sr{s})))));
   sr{s}= []; Lr{s}= []; Ur{s}= []; Pr{s}= []; Qr{s}= []; Br{s}= [];
end
clear sr Lr Ur Pr Qr Arc Br;

% third local uc substracted from global uc
ucs= cell(Ns,1);
if ngc~=0
   for s= 1:Ns
      if ~isempty(sc{s}) 
         ucs{s}= Rc{s}*uc(1:ngc);
      end
      Rc{s}= [];
   end
end
clear Rc;

% finally, assemble global solution u
u= zeros((nx+1)*(ny+1)*(nz+1),1);
snx= nx/Nx; sny= ny/Ny; snz= nz/Nz;
for s= 1:Ns
   s2g= sn2gn(1:(snx+1)*(sny+1)*(snz+1),nx,ny,nz,Nx,Ny,Nz,s);
   si{s}= [sii;sdiri{s};srobin{s}];
   u(s2g(si{s}))= ur{s}(1:nsi(s));
   si{s}= []; sdiri{s}= []; srobin{s}= [];
   nsr= size(ur{s},1);
   u(s2g(sb{s}))= u(s2g(sb{s})) + wts{s}.*ur{s}((nsi(s)+1):nsr);
   ur{s}= [];  wts{s}= [];
   if ~isempty(sc{s})
       for jj= 1:length(scsend{s}) % go through every corner segment
           [~,~,snb]= find(scnb{s}(jj,:));
           if snb>s  % s is the minimum
               if  1==jj
                   ics= 1:scsend{s}(jj);
               else
                   ics= scsend{s}(jj-1)+1:scsend{s}(jj);
               end
               u(s2g(sc{s}(ics)))= ucs{s}(ics);
           end
       end
       ucs{s}= [];
   end
end
clear ucs;
toc(tback);
fprintf('\n');

%% matrix function F of the interface problem F*lambda = rhsl for 
%  the multiplier lambda   
function y= F(x) % y= F*x
   y= zeros(ngb,1);
   uc= zeros(ngc+nmu,1);
   for m= 1:Ns
      ur{m}= Qr{m}*(Ur{m}\(Lr{m}\(Pr{m}*(Br{m}'*x))));
      y= y + Bb{m} * ur{m}( nsi(m)+1 : size(ur{m},1) );
      if (ngc+nmu)~=0
         uc= uc + [Rc{m}'*(Acr{m}*ur{m});Qmu'*(Br{m}*ur{m})];
      end
      ur{m}= [];
   end
   if (ngc+nmu)~=0
      uc= Qcc * (Ucc\(Lcc\(Pcc*uc)));
      for m= 1:Ns
          if ~isempty(Rc{m})
              ur{m}= Qr{m}*(Ur{m}\(Lr{m}\(Pr{m}*(Arc{m}*(Rc{m}*uc(1:ngc)) ...
                  + Br{m}'*(Qmu*uc(ngc+1:ngc+nmu))))));
              y= y + Bb{m} * ur{m}( nsi(m)+1 : size(ur{m},1) );
              ur{m}= [];
          end
      end
   end
   uc= [];
end

%% matrix function PD for the preconditioned system 
%  PD*F*lambda = PD*rhsl
function y= PD(x) % y= PD*x
   y= zeros(ngb,1); 
   for m= 1:Ns
      y= y+ Bb{m}*( wts{m}.*( Abb{m}*( wts{m}.*( Bb{m}'*x ))));
      y= y- Bb{m}*( wts{m}.*( Abi{m}*( Qi{m}*( Ui{m}\ ...
          ( Li{m}\( Pi{m}*( Aib{m}*( wts{m}.*( Bb{m}'*x)))))))));    
   end      
end

%% matrix function PL the lumped preconditioner 
    function y= PL(x)
        y= zeros(ngb,1); 
        for m= 1:Ns
           y= y+ Bb{m}*( wts{m}.*( Abb{m}*( wts{m}.*( Bb{m}'*x ))));
        end
    end       


end    % end of fetidp function