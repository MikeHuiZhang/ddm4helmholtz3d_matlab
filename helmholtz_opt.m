function helmholtz_opt()

clc;
clear all;
% The Helmholtz problem is
%    -\lap u - k^2 u = f,   in \Omega
%                 u = uD,   on \Gamma_D
%       du/dn + a u = uR,   on \Gamma_R
% where a can be zero for Neumann boundary.


%diary(num2str(clock));
fid1= fopen('iter_opt.txt','w');
fprintf(fid1,' O0  O2  OO0  OO2\n');
fid2= fopen('cputime_opt.txt','w');
fprintf(fid2,' O0  O2   OO0  OO2  \n');
fid3= fopen('reg_opt.txt','w');
fprintf(fid3,'O0     O2                  OO0    OO2\n');

% equation: example 1 constant wavenumber
k= pi; % wavenumber
k2= k^2;  
a= -1i*k; % absorbing coefficient 
f= 0; 
pointsource= [0.3,0.3,0.03,1]; % source
uD= 0; % Dirichlet boundary value
uR= 0; % absorbing boundary value

% geometry: continuous description of the qube domain and boundary
xl= 0; xr= 1;
yl= 0; yr= 1;
zl= 0; zr= 1;

GammaD= [3,4,5,6]; % 1 left, 2 right, 3 front, 4 back, 5 bottom, 6 top 
GammaR= [1,2];

% discretization parameter
for nx= 10:10:10 
    ny= nx; nz= nx;
% hx= (xr-xl)/nx; hy= (yr-yl)/ny; hz= (zr-zl)/nz;

% ddm solution
% partition to Nx X Ny X Nz subdomains, fetidp require Nx > freq*Lx/c/sqrt(2)/pi    
Nx= 2; Ny= 1; Nz= 1; 
ntheta= -1;  nphi= 0;  % number of plane waves is 2*ntheta*nphi+nphi+2, or 0 when ntheta=-1
randinit= 0; % wheter use random initial guess
olp= 1; % overlap

% fminsearch for good parameters for O0
[p,iter]= patternsearch(@nos0,10);
fprintf(fid1,'%3d  ',iter);
fprintf(fid2,'%3.1f  ',telapsed);
fprintf(fid3,'%3.1f', p);

% fminsearch for good parameters for O2
[p,iter]= patternsearch(@nos2,[1,1,1,1]);
fprintf(fid1,'%3d  ',iter);
fprintf(fid2,'%3.1f  ',telapsed);
fprintf(fid3,'(%3.1f, %3.1f, %3.1f, %3.1f)', p(1), p(2),p(3), p(4));

% OO0
tdd= tic;
[udd,flag,relres,iter,resvec]= ...
    oossubs(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR, ...
           Nx,Ny,Nz,olp,ntheta,nphi,0,[],randinit);
telapsed= toc(tdd);
fprintf(fid1,'%3d  ',iter(2));
fprintf(fid2,'%3.1f  ',telapsed);

% OO2
tdd= tic;
[udd,flag,relres,iter,resvec]= ...
    oossubs(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR, ...
           Nx,Ny,Nz,olp,ntheta,nphi,2);
telapsed= toc(tdd);
fprintf(fid1,'%3d  ',iter(2));
fprintf(fid2,'%3.1f  ',telapsed);

fprintf(fid1,'\n');
fprintf(fid2,'\n');
fprintf(fid3,'\n');
end

fclose('all');
%diary off;

    function iter= nos0(p)
        tdd= tic;
        [udd,flag,relres,iter,resvec]= ...
            nos(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,GammaD,GammaR,nx,ny,nz,...
            Nx,Ny,Nz,ntheta,nphi,1,2,0,p*(1-1i),randinit);
        iter= iter(2);
        telapsed= toc(tdd);
    end

    function iter= nos2(p)
        tdd= tic;
        [udd,flag,relres,iter,resvec]= ...
            nos(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,GammaD,GammaR,nx,ny,nz,...
            Nx,Ny,Nz,ntheta,nphi,1,2,2,[p(1)-1i*p(2),p(3)-1i*p(4)],randinit);
        iter= iter(2);
        telapsed= toc(tdd);
    end

end