

close all; clear all;



% some random test data

n = 1000;

A = sprandsym(n,7/n);

A = A + 1i*sprandsym(A) + 5*speye(n);

b = randn(n,1) + 1i*randn(n,1);

tol = 1e-8;

maxit = 100;

x0 = randn(n,1); 



% run complex symm. QMR and GMRES

[~,nres1] = csqmr(A,b,tol,maxit,x0);

[~,~,~,~,nres2] = gmres(A,b,[],tol,maxit,[],[],x0);



% plot residuals

semilogy(nres1);

hold on

semilogy(nres2,'r');

legend('QMR','GMRES'); 

xlabel('iteration'); 

ylabel('rel. residual norm');



% a Helmholtz example



n=30;

k=20;

G=numgrid('S',n);

h=1/(n-1);

A=delsq(G)/h^2-k^2*speye((n-2)*(n-2));

b=zeros(size(A,1),1);

b(1)=1;

u=A\b;

U=G;

U(G>0)=full(u(G(G>0)));

mesh(U);



tol = 1e-8;

maxit = 500;

x0 = zeros(size(b)); 



% run complex symm. QMR and GMRES

[x1,nres1] = csqmr(A,b,tol,maxit,x0);

[x2,~,~,~,nres2] = gmres(A,b,[],tol,maxit,[],[],x0);



% plot residuals

semilogy(nres1);

hold on

semilogy(nres2,'r');

legend('QMR','GMRES'); 

xlabel('iteration'); 

ylabel('rel. residual norm');

