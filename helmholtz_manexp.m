% manufactured example 1
% uexact= @(x)(cos(2*pi*x(:,1)).*cos(2*pi*x(:,2)).*cos(2*pi*x(:,3)));
% % uexact= @(x)(exp(-2*x(:,1)).*cos(2*pi*x(:,2)).*cos(2*pi*x(:,3)));
% % k= @(x)(x(:,1).^4+1); %(4*ones(size(x(:,1)))); %
% k= @(x)(x(:,2).^(1/2)+x(:,1).^(1/4)+x(:,3).^3+3);
% k2= @(x)(k(x).^2); 
% a= @(x)(-1i*k(x));
% f= @(x)((12*pi^2-k2(x)).*uexact(x));
% % f= @(x)((8*pi^2-4-k2(x)).*uexact(x)); % change this along with uexact
% pointsource= [];
% uD= uexact;
% uR= @(x)(a(x).*uexact(x)); % valid for du/dn=0
% manufactured example 2
% uexact= @(x)(cos(2*pi*x(:,1)).*cos(2*pi*x(:,2)).*cos(2*pi*x(:,3)));
% uexact= @(x)(cos(2*pi*x(:,1)).*cos(2*pi*x(:,2)).^2.*cos(2*pi*x(:,3)));
% k= 3*pi; 
% k2= k^2;
% a= -1i*k;
% % f= @(x)((12*pi^2-k2).*uexact(x)); % change this along with uexact
% f= @(x) -8*cos(2*pi*x(:,1))*pi^2.*cos(2*pi*x(:,3)).*(-2*cos(2*pi*x(:,2)).^2 ...
%        + sin(2*pi*x(:,2)).^2) - k^2*uexact(x);
% pointsource= [];
% uD= uexact;
% uR= @(x)(a*uexact(x)); % valid for du/dn=0
% manufactured example 3
% uexact= @(x)(exp(-x(:,1))+1);
% k= 4; 
% k2= k^2;
% a= 1;
% f= @(x)(-exp(-x(:,1))-k2.*uexact(x)); % change this along with uexact
% pointsource= [];
% uD= uexact;
% uR= 1; % valid for with robin only on 2 (right face)
% manufactured example 4
% k= 4;
% k2= k^2;
% pointsource= [0.3,0.3,0.3,1]; 
% dist2src= @(x)(sqrt((x(:,1)-0.3).^2+(x(:,2)-0.3).^2+(x(:,3)-0.3).^2));
% uexact= @(x)(cos(k*dist2src(x))./dist2src(x)/4/pi);
% a= 0;
% f= 0;
% uD= uexact;
% uR= 0;  % invalid: we should use no Robin condition