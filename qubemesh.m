function [coordinates,elements,dirichlet,robin]= qubemesh(xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR)
% generating qube mesh
% INPUT 
% [xl,xr]X[yl,yr]X[zl,zr]: the qube domain
%              nx, ny, nz: partitions along x,y,z
%          GammaD, GammaR: Dirichlet and Robin, 1 left, 2 right, 3 front, 
%                          4 back, 5 bottom, 6 top  
% OUTPUT
% coordinates: N by 3 matrix for 1~N nodes, N= (nx+1)*(ny+1)*(nz+1),ordered
%              in such that x varies first, then y, z varies the slowest, 
%              from the lower bounds to upper bounds 
%    elements: M by 8 matrix for 1~M elements, M= nx*ny*nz, the order is 
%              similar to coordinates each row is numbers of eight nodes
%              data type is 'uint32', allow 1624^3 grid cells
%   dirichlet: column vector with numbers of Dirichlet nodes
%       robin: matrix with 4 columns, each row is numbers of four nodes
%              corresponding to an rectangle on boundary faces
hx= (xr-xl)/nx; hy= (yr-yl)/ny; hz= (zr-zl)/nz;
jz= kron((1:(nz+1))',ones((nx+1)*(ny+1),1));
jy= repmat(kron((1:(ny+1))',ones(nx+1,1)),nz+1,1);
jx= repmat((1:(nx+1))',(ny+1)*(nz+1),1);
coordinates(1:(nx+1)*(ny+1)*(nz+1),:)= [xl+hx*(jx-1), yl+hy*(jy-1), zl+hz*(jz-1)];
elements= repmat((1:nx)',ny*nz,1)+repmat(kron((0:ny-1)'*(nx+1),ones(nx,1)),nz,1) ...
    + kron((0:nz-1)'*(nx+1)*(ny+1),ones(nx*ny,1));
elements= [elements, elements+1, elements+1+nx+1,elements+nx+1];
elements= uint32([elements, elements+(nx+1)*(ny+1)]); % allow 1624^3 grid cells
if 9==nargin && 2==nargout
    return;
end
% global indices of boundary nodes
leftbd= 1+repmat((0:ny)*(nx+1),1,nz+1)+kron((0:nz)*(nx+1)*(ny+1),ones(1,ny+1));
rightbd= nx+1+repmat((0:ny)*(nx+1),1,nz+1)+kron((0:nz)*(nx+1)*(ny+1),ones(1,ny+1));
frontbd= repmat(1:nx+1,1,nz+1)+kron((0:nz)*(nx+1)*(ny+1),ones(1,nx+1));
backbd= repmat(1:nx+1,1,nz+1)+ny*(nx+1)+kron((0:nz)*(nx+1)*(ny+1),ones(1,nx+1));
bottombd= repmat(1:nx+1,1,ny+1)+kron((0:ny)*(nx+1),ones(1,nx+1));
topbd= repmat(1:nx+1,1,ny+1)+kron((0:ny)*(nx+1),ones(1,nx+1))+nz*(nx+1)*(ny+1);
% boundary elements using local numbering on this face 
leftbdelem=  repmat((1:ny)',nz,1)+kron((0:(nz-1))'*(ny+1),ones(ny,1));
leftbdelem= [leftbdelem, leftbdelem+1, leftbdelem+1+ny+1,leftbdelem+ny+1];
rightbdelem= leftbdelem;
frontbdelem= repmat((1:nx)',nz,1)+kron((0:(nz-1))'*(nx+1),ones(nx,1));
frontbdelem= [frontbdelem, frontbdelem+1, frontbdelem+1+nx+1,frontbdelem+nx+1];
backbdelem= frontbdelem;
bottombdelem= repmat((1:nx)',ny,1)+kron((0:(ny-1))'*(nx+1),ones(nx,1));
bottombdelem= [bottombdelem, bottombdelem+1, bottombdelem+1+nx+1,bottombdelem+nx+1];
topbdelem= bottombdelem;
% Dirichlet boundary nodes and Robin boundary elements with global indices
% of nodes
ndirichlet= 0;
for j= 1:length(GammaD)
    switch GammaD(j)
        case {1,2}
            ndirichlet= ndirichlet + (ny+1)*(nz+1);
        case {3,4}
            ndirichlet= ndirichlet + (nz+1)*(nx+1);
        case {5,6}
            ndirichlet= ndirichlet + (nx+1)*(ny+1);
        otherwise
            disp('wrong number for qube boundary faces,must be 1~6');
    end
end
dirichlet= zeros(ndirichlet,1,'uint32');
currentpos= 1;
for j= 1:length(GammaD)
    switch GammaD(j)
        case 1
            dirichlet(currentpos:currentpos+length(leftbd)-1)= leftbd';
            currentpos= currentpos+length(leftbd);
        case 2
            dirichlet(currentpos:currentpos+length(rightbd)-1)= rightbd';
            currentpos= currentpos+length(rightbd);
        case 3
            dirichlet(currentpos:currentpos+length(frontbd)-1)= frontbd';
            currentpos= currentpos+length(frontbd);
        case 4
            dirichlet(currentpos:currentpos+length(backbd)-1)= backbd';
            currentpos= currentpos+length(backbd);
        case 5
            dirichlet(currentpos:currentpos+length(bottombd)-1)= bottombd';
            currentpos= currentpos+length(bottombd);
        case 6
            dirichlet(currentpos:currentpos+length(topbd)-1)= topbd';
            currentpos= currentpos+length(topbd);
        otherwise
            disp('wrong number for qube boundary faces,must be 1~6');
    end
end
dirichlet= unique(dirichlet);
nrobin= 0;
for j= 1:length(GammaR)
    switch GammaR(j)
        case {1,2}
            nrobin= nrobin + ny*nz;
        case {3,4}
            nrobin= nrobin + nz*nx;
        case {5,6}
            nrobin= nrobin + nx*ny;
        otherwise
            disp('wrong number for qube boundary faces,must be 1~6');
    end
end
robin= zeros(nrobin,4,'uint32');
currentpos= 1;
for j= 1:length(GammaR)
    switch GammaR(j)
        case 1
            robin(currentpos:currentpos+size(leftbdelem,1)-1,:)= leftbd(leftbdelem);
            currentpos= currentpos+size(leftbdelem,1);
        case 2
            robin(currentpos:currentpos+size(rightbdelem,1)-1,:)= rightbd(rightbdelem);
            currentpos= currentpos+size(rightbdelem,1);
        case 3
            robin(currentpos:currentpos+size(frontbdelem,1)-1,:)= frontbd(frontbdelem);
            currentpos= currentpos+size(frontbdelem,1);
        case 4
            robin(currentpos:currentpos+size(backbdelem,1)-1,:)= backbd(backbdelem);
            currentpos= currentpos+size(backbdelem,1);
        case 5
            robin(currentpos:currentpos+size(bottombdelem,1)-1,:)= bottombd(bottombdelem);
            currentpos= currentpos+size(bottombdelem,1);
        case 6
            robin(currentpos:currentpos+size(topbdelem,1)-1,:)= topbd(topbdelem);
            currentpos= currentpos+size(topbdelem,1);
        otherwise
            disp('wrong number for qube boundary faces,must be 1~6');
    end
end
end