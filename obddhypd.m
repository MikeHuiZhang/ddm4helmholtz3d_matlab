function [u,flag,relres,iter,resvec,specF]= ...
    obddhypd(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR, ...
           Nx,Ny,Nz,ol,wl,cl,ntheta,nphi)
% INPUT
% coordinates, elements: we assume they are generated from qubemesh
% Nx, Ny, Nz: nonoverlapping partition to subdomains
% ol:  number of outside layers for overlapping extension
% wl:  number of outside layers for weighting
% cl:  number of outside layers for coarse functions
% Remark 
% we assume ol, wl and cl are not so large that overlapping
% subdomains communicate with at most 26  neighbors.
%% partition of the global mesh into overlapping submeshes 
[coordinates,elements,dirichlet,robin]= qubemesh(xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR);
[selems,sdiri,srobinelems,sbdelems,Ro,Dw,Rcp,Rwp,numplw,Ri] = ...
    qubemeshpart(coordinates,GammaD,GammaR,nx,ny,nz,Nx,Ny,Nz,...
                 ol,wl,cl,ntheta,nphi,k2);
%% assemble global matrix and rhs 
Ns= Nx*Ny*Nz; nn= (nx+1)*(ny+1)*(nz+1);
A= cell(Ns,1); 
getAM(); getgpw(); % for use by stima.m and rhs.m
getM2d(); getgpw2d(); % for use by stima2d and rhs2d
global unused_src numsrc; % for use by rhs.m
[I,J]= ndgrid(1:8,1:8); I= I(:); J= J(:);
[Ir,Jr]= ndgrid(1:4,1:4); Ir= Ir(:); Jr= Jr(:);
ne= size(elements,1);
ii= zeros(64,ne); jj= zeros(64,ne); Aa= zeros(64,ne);
for j= 1:ne
    Ae= stima(coordinates(elements(j,:),:),k2);
    ii(:,j)= elements(j,I); % assemble vectors
    jj(:,j)= elements(j,J); % for speed
    Aa(:,j)= Ae(:);
end
iib= []; ba= [];
if ~(isa(f,'float') && 0==f && isempty(pointsource))
   iib= zeros(8,ne); ba= zeros(8,ne);
   for j = 1:ne
      n4e= elements(j,:);
      iib(:,j)= n4e;
      ba(:,j)= rhs(coordinates(n4e,:),f,pointsource);
   end
end
% treat outer Robin b.c.
nr= size(robin,1); 
ir= []; jr= []; Ar= [];
if ~(isa(a,'float') && 0==a)
   ir= zeros(16,nr); jr= zeros(16,nr); Ar= zeros(16,nr);
   for j = 1:nr
       ir(:,j)= robin(j,Ir);
       jr(:,j)= robin(j,Jr); 
       Ae= stima2d(coordinates(robin(j,:),:),a);
       Ar(:,j)= Ae(:);
   end
end
irb= []; br= [];
if ~(isa(uR,'float') && 0==uR)
   irb= zeros(4,nr); br= zeros(4,nr);
   for j = 1:nr
      n4e= robin(j,:);
      irb(:,j)= n4e;
      br(:,j)= rhs2d(coordinates(n4e,:),uR);
   end
end
% treat Dirichlet boundary lastly, to avoid Robin change
if isa(uD,'float')
   bd= uD*ones(size(dirichlet,1),1);
elseif isa(uD,'inline') || isa(uD,'function_handle')
   bd = uD(coordinates(dirichlet,:));
end
bb= sparse([iib(:);irb(:);double(dirichlet)],1,[ba(:);br(:);bd],nn,1);
AA= sparse([ii(:);ir(:)],[jj(:);jr(:)],[Aa(:);Ar(:)],nn,nn);
[iiA, jjA]= find(AA);
idxofzero = ismember(iiA,double(dirichlet)); 
AA= setsparse(AA,iiA(idxofzero),jjA(idxofzero),0);
AA= setsparse(AA,double(dirichlet),double(dirichlet),1);
clear iiA jjA iib ba irb br bd idxofzero;
%% assemble subdomains matrices 
for s= 1:Ns
    % treat internal Robin b.c.
    if isa(k2,'function_handle') || isa(k2,'inline')
       reg= @(x)(-sqrt(k2(x))*1i);
    elseif isa(k2,'float')
        reg= -sqrt(k2)*1i;
    end
    nb= size(sbdelems{s},1);
    ib= zeros(16,nb); jb= zeros(16,nb); Ab= zeros(16,nb);
    for j = 1:nb
        ib(:,j)= sbdelems{s}(j,Ir);
        jb(:,j)= sbdelems{s}(j,Jr);
        Ae= stima2d(coordinates(sbdelems{s}(j,:),:),reg); 
        Ab(:,j)= Ae(:);
    end
    % treat Dirichlet boundary lastly, to avoid Robin change
    iis= ii(:,selems{s}); iis= iis(:);
    jjs= jj(:,selems{s}); jjs= jjs(:);
    Aas= Aa(:,selems{s}); Aas= Aas(:);
    if ~isempty(ir)
        irs= ir(:,srobinelems{s}); irs= irs(:);
        jrs= jr(:,srobinelems{s}); jrs= jrs(:);
        Ars= Ar(:,srobinelems{s}); Ars= Ars(:);
    else
        irs= [];
        jrs= [];
        Ars= [];
    end
    Ag= sparse([iis;irs;ib(:)],[jjs;jrs;jb(:)],[Aas;Ars;Ab(:)],nn,nn);
    [iis, jjs]= find(Ag);
    idxofzero = ismember(iis,sdiri{s}); 
    Ag= setsparse(Ag,iis(idxofzero),jjs(idxofzero),0);
    Ag= setsparse(Ag,sdiri{s},sdiri{s},1);
    % restrict matrix to subdomain
    A{s}= Ro{s}*Ag*Ro{s}.';
end
unused_src= []; numsrc= [];
clear Ag;
clear ii jj Aa ir jr Ar ib jb Ab iib ba irb br;
clear iis jjs Aas irs jrs Ars iibs bas irbs brs;
Ai= cell(Ns,1);
for s= 1:Ns
    Ai{s}= Ri{s}*AA*Ri{s}';
end
%% assemble coarse problem
% by assumption, every wl extended subdomain overlap with at most 26 cl 
% extended subdomains
if numplw~=0
    iis= cell(Ns,Ns); jjs= cell(Ns,Ns); vvs= cell(Ns,Ns); nzzAc= 0;
    for s= 1:Ns
       [sx,sy]= ind2sub([Nx,Ny*Nz],s); % 3d position of this subdomain
       [sy,sz]= ind2sub([Ny,Nz],sy);
       dsx= -(sx>1)*1:(sx<Nx)*1; dsy= -(sy>1)*1:(sy<Ny)*1; 
       dsz= -(sz>1)*1:(sz<Nz)*1;
       [dsx,dsy,dsz]= ndgrid(dsx,dsy,dsz);
       for j= 1:length(dsx(:))
           s2= sub2ind([Nx,Ny,Nz],sx+dsx(j),sy+dsy(j),sz+dsz(j));        
           Acs= Rwp{s}*AA*Rcp{s2}';
           [iis{s,s2},jjs{s,s2},vvs{s,s2}]= find(Acs);
           nzzAc= nzzAc + length(iis{s,s2});
       end
    end
    ii= zeros(nzzAc,1); jj= zeros(nzzAc,1); vv= zeros(nzzAc,1); 
    nzzAc= 0;
    for s= 1:Ns
       [sx,sy]= ind2sub([Nx,Ny*Nz],s); % 3d position of this subdomain
       [sy,sz]= ind2sub([Ny,Nz],sy);
       dsx= -(sx>1)*1:(sx<Nx)*1; dsy= -(sy>1)*1:(sy<Ny)*1;
       dsz= -(sz>1)*1:(sz<Nz)*1;
       [dsx,dsy,dsz]= ndgrid(dsx,dsy,dsz);
       for j= 1:length(dsx(:))
           s2= sub2ind([Nx,Ny,Nz],sx+dsx(j),sy+dsy(j),sz+dsz(j));
           idx= nzzAc+1:nzzAc+length(iis{s,s2});
           ii(idx)= iis{s,s2}+(s-1)*numplw;
           jj(idx)= jjs{s,s2}+(s2-1)*numplw;
           vv(idx)= vvs{s,s2};
           nzzAc= nzzAc + length(iis{s,s2});
       end    
    end
    Ac= sparse(ii,jj,vv);
    clear ii jj vv iis jjs vvs dsx dsy dsz idx;
end
%% LU factorization of subdomain and coarse problems
L= cell(Ns,1); U= cell(Ns,1); P= cell(Ns,1); Q= cell(Ns,1);
for s= 1:Ns
   [L{s},U{s},P{s},Q{s}]= lu(A{s});
   A{s}= [];
end
clear A;
if numplw~=0
   [Lc,Uc,Pc,Qc]= lu(Ac);
   clear Ac;
end
Li= cell(Ns,1); Ui= cell(Ns,1); Pi= cell(Ns,1); Qi= cell(Ns,1);
for s= 1:Ns
    [Li{s},Ui{s},Pi{s},Qi{s}]= lu(Ai{s});
    Ai{s}= [];
end
clear Ai;
%% coarse solve operator P0 (doesnot contain AA)
    function x= P0(y)
        x= sparse(size(y,1),1);
        % yc:= Rwp*y
        yc= zeros(Ns*numplw,1);
        for sub= 1:Ns
            yc((sub-1)*numplw+1:sub*numplw)= Rwp{sub}*y;
        end
        % xc:= Ac\yc
        xc= Qc * (Uc\(Lc\(Pc*yc)));
        % x:= Rcp'*xc = P0 y
        for sub= 1:Ns
            x= x + Rcp{sub}'*xc((sub-1)*numplw+1:sub*numplw);
        end
    end

%% preconditioned system operator 
%  T:= T0 + T1 + ...
    function pxx= T(xx)
        pxx= sparse(size(xx,1),1);
        AAxx= AA*xx;
        % subdomain solve
        for ms= 1:Ns
           pxx= pxx + Dw{ms}.*(Ro{ms}.'*(Q{ms} * (U{ms}\(L{ms}\(P{ms}*(Ro{ms}*AAxx))))));
        end
        % interior correction
        AAxx= AAxx-AA*pxx;
        for ms= 1:Ns
           pxx= pxx + Ri{ms}'*(Qi{ms} * (Ui{ms}\(Li{ms}\(Pi{ms}*(Ri{ms}*AAxx))))); 
        end
        % coarse correction
        if numplw~=0  
           AApxx= AA*pxx; 
           pxx= pxx - P0(AApxx);
        end
    end

%% preconditioned system rhs: initial coarse and subdomain solves
if numplw~=0  % initial coarse solve 
    u0= P0(bb);
else
    u0= sparse(size(bb,1),1);
end
% subdomain correction
res0= bb - AA*u0;
g= sparse(size(bb,1),1);
for s= 1:Ns
   g= g + Dw{s}.*(Ro{s}.'*(Q{s} * (U{s}\(L{s}\(P{s}*(Ro{s}*res0))))));
end
% interior correction
gold= res0 - AA*g;
for s= 1:Ns
   g= g + Ri{s}'*(Qi{s} * (Ui{s}\(Li{s}\(Pi{s}*(Ri{s}*gold)))));  
end
% coarse correction
AAg= AA*g;
if numplw~=0
   g= g - P0(AAg);
end
%% GMRES iteration
tol= 1e-6;  restart= [];  maxit= 50; 
[u,flag,relres,iter,resvec] = gmres(@T,g,restart,tol,maxit,[],[],sparse(size(bb,1),1)); 
u= u + u0;
% test direct solve
% flag= 0; relres= []; iter= 0; resvec=[]; 
% u= AA\bb;
%% computing spectra
specF= [];
end
