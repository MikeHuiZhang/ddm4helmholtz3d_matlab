function helmholtz()

clc;
clear all;
% The Helmholtz problem is
%    -\lap u - k^2 u = f,   in \Omega
%                 u = uD,   on \Gamma_D
%       du/dn + a u = uR,   on \Gamma_R
% where a can be zero for Neumann boundary.


%diary(num2str(clock));
fid1= fopen('iter.txt','w');
fprintf(fid1,' FL   FD   FH   O0   O0d   O2   O2d1   O2d2   T0   T2   OD   OO0   OO0d   OO2   OO2d1   OO2d2   OT0   OT2\n');
fid2= fopen('cputime.txt','w');
fprintf(fid2,'    FL      FD      FH      O0     O0d      O2    O2d1    O2d2      T0      T2      OD     OO0    OO0d     OO2   OO2d1   OO2d2     OT0     OT2\n');
fid3= fopen('reg.txt','w');
fprintf(fid3,'       O0              O0d                               O2  ');
fprintf(fid3,'                           O2d1                             O2d2  ');
fprintf(fid3,'            OO0             OO0d                              OO2  ');
fprintf(fid3,'                          OO2d1                           OO2d2\n');

% equation: example 1 constant wavenumber
k= 4*pi; % wavenumber
k2= k^2;  
a= -1i*k; % absorbing coefficient 
f= 0; 
pointsource= [0.3,0.3,0.03,1]; % source
uD= 0; % Dirichlet boundary value
uR= 0; % absorbing boundary value

% geometry: continuous description of the qube domain and boundary
xl= 0; xr= 1;
yl= 0; yr= 1;
zl= 0; zr= 1;

GammaD= [3,4,5,6]; % 1 left, 2 right, 3 front, 4 back, 5 bottom, 6 top 
GammaR= [1,2];

% discretization parameter
for nx= 10:10:60 
    ny= nx; nz= nx;
    
% ddm solution
% partition to Nx X Ny X Nz subdomains, fetidp require Nx > freq*Lx/c/sqrt(2)/pi    
Nx= 2; Ny= 1; Nz= 1; 
if norm(pi./[((xr-xl)/Nx),(yr-yl)/Ny,(zr-zl)/Nz])>sqrt(k2)
    ispd= 1;
else
    ispd= 0;
end
ntheta= -1;  nphi= 0;  % number of plane waves is 2*ntheta*nphi+nphi+2, or 0 when ntheta=-1
randinit= 1; % wheter use random initial guess
olp= 1; % overlap

% it is very strange that the first run of ddm always give larger cputime
% so we just run a ddm first but discard its cputime
tic;
fetidp(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,GammaD,GammaR,nx,ny,...
           nz,Nx,Ny,Nz,ntheta,nphi,1,2,randinit);
toc;

% FL
tdd= tic; 
[~,~,~,iter,~]= ...
    fetidp(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,GammaD,GammaR,nx,ny,...
           nz,Nx,Ny,Nz,ntheta,nphi,1,2,randinit);
telapsed= toc(tdd);
fprintf(fid1,'%3d  ',iter(2));
fprintf(fid2,'%6.1f  ',telapsed);

% FD
tdd= tic; 
[~,~,~,iter,~]= ...
    fetidp(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,GammaD,GammaR,nx,ny,...
           nz,Nx,Ny,Nz,ntheta,nphi,1,1,randinit);
telapsed= toc(tdd);
fprintf(fid1,'%3d  ',iter(2));
fprintf(fid2,'%6.1f  ',telapsed);

% FH: use no preconditioner for FETI-H
tdd= tic; 
[~,~,~,iter,~]= ...
    fetidpr(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,GammaD,GammaR,...
            nx,ny,nz,Nx,Ny,Nz,ntheta,nphi,1,0,randinit);       
telapsed= toc(tdd);
fprintf(fid1,'%3d  ',iter(2));
fprintf(fid2,'%6.1f  ',telapsed);

% O0
tdd= tic; 
[~,~,~,iter,~,reg]= ...
    nos(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,GammaD,GammaR,nx,ny,nz,...
        Nx,Ny,Nz,ntheta,nphi,1,2,0,[],randinit);
telapsed= toc(tdd);
fprintf(fid1,'%3d   ',iter(2));
fprintf(fid2,'%6.1f  ',telapsed);
fprintf(fid3,'(%6.2g,%6.2g)  ', real(reg),imag(reg));

% search for good parameters for O0
p= 1:5:100; 
iterv= zeros(length(p),1); tv= zeros(length(p),1);
for idx= 1:length(p)
    if ispd
        reg= p(idx);
    else
        reg= p(idx)*(1-1i); % search only parameters in the special form
    end
    tdd= tic;
    [~,~,~,iter]= ...
        nos(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,GammaD,GammaR,nx,ny,nz,...
        Nx,Ny,Nz,ntheta,nphi,1,2,0,reg,randinit);
    telapsed= toc(tdd);
    iterv(idx)= iter(2);
    tv(idx)= telapsed;
end
[~,idx]= min(iterv);
fprintf(fid1,'%3d  ',iterv(idx));
fprintf(fid2,'%6.1f  ',tv(idx));
fprintf(fid3,'(%6.2g,%6.2g)  ', real(reg),imag(reg));

% O2
tdd= tic;
[~,~,~,iter,~,reg]= ...
    nos(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,GammaD,GammaR,nx,ny,nz,...
        Nx,Ny,Nz,ntheta,nphi,1,2,2,[],randinit);
telapsed= toc(tdd);
fprintf(fid1,'%3d    ',iter(2));
fprintf(fid2,'%6.1f  ',telapsed);
fprintf(fid3,'(%6.2g,%6.2g),(%6.2g,%6.2g)  ', real(reg(1)),imag(reg(1)), real(reg(2)),imag(reg(2)));

% search for good parameters for O2, 1
alpha= 1:5:100; beta= 1:5:100;
iterv= zeros(length(alpha)*length(beta),1);
tv= zeros(length(alpha)*length(beta),1);
idx= 0;
for ia= 1:length(alpha)
    for ib= 1:length(beta)
        idx= idx + 1;
        reg= [(-alpha(ia)*1i*beta(ib)-k2)/(beta(ib)-alpha(ia)*1i),1/(beta(ib)-alpha(ia)*1i)]; 
                   % special form 1 of Robin
        tdd= tic;
        [~,~,~,iter]= ...
            nos(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,GammaD,GammaR,nx,ny,nz,...
            Nx,Ny,Nz,ntheta,nphi,1,2,2,reg,randinit);
        telapsed= toc(tdd);
        iterv(idx)= iter(2);
        tv(idx)= telapsed;
    end
end
[~,idx]= min(iterv);
fprintf(fid1,'%3d    ',iterv(idx));
fprintf(fid2,'%6.1f  ',tv(idx));
[ib,ia]= ind2sub([length(beta),length(alpha)], idx);
reg= [(-alpha(ia)*1i*beta(ib)-k2)/(beta(ib)-alpha(ia)*1i),1/(beta(ib)-alpha(ia)*1i)];
fprintf(fid3,'(%6.2g,%6.2g),(%6.2g,%6.2g)  ', real(reg(1)),imag(reg(1)), real(reg(2)),imag(reg(2)));

% search for good parameters for O2, 2
alpha= 1:5:100; beta= 1:5:100;
iterv= zeros(length(alpha)*length(beta),1);
tv= zeros(length(alpha)*length(beta),1);
idx= 0;
for ia= 1:length(alpha)
    for ib= 1:length(beta)
        idx= idx + 1;
        reg= [(alpha(ia)*(1-1i)^2*beta(ib)-k2)/(beta(ib)*(1-1i)+alpha(ia)*(1-1i)),...
               1/(beta(ib)*(1-1i)+alpha(ia)*(1-1i))]; 
                   % special form 2 of Robin
        tdd= tic;
        [~,~,~,iter]= ...
            nos(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,GammaD,GammaR,nx,ny,nz,...
            Nx,Ny,Nz,ntheta,nphi,1,2,2,reg,randinit);
        telapsed= toc(tdd);
        iterv(idx)= iter(2);
        tv(idx)= telapsed;
    end
end
[~,idx]= min(iterv);
fprintf(fid1,'%3d  ',iterv(idx));
fprintf(fid2,'%6.1f  ',tv(idx));
[ib,ia]= ind2sub([length(beta),length(alpha)], idx);
reg= [(alpha(ia)*(1-1i)^2*beta(ib)-k2)/(beta(ib)*(1-1i)+alpha(ia)*(1-1i)),...
               1/(beta(ib)*(1-1i)+alpha(ia)*(1-1i))];
fprintf(fid3,'(%6.2g,%6.2g),(%6.2g,%6.2g)  ', real(reg(1)),imag(reg(1)), real(reg(2)),imag(reg(2)));

% T0
tdd= tic;
[~,~,~,iter,~]= ...
    nos(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,GammaD,GammaR,nx,ny,nz,...
        Nx,Ny,Nz,ntheta,nphi,1,2,0,-1i*sqrt(k2),randinit);
telapsed= toc(tdd);
fprintf(fid1,'%3d  ',iter(2));
fprintf(fid2,'%6.1f  ',telapsed);

% T2
tdd= tic;
[~,~,~,iter,~]= ...
    nos(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,GammaD,GammaR,nx,ny,nz,...
        Nx,Ny,Nz,ntheta,nphi,1,2,2,[-1i*sqrt(k2),-1i/2/sqrt(k2)],randinit);
telapsed= toc(tdd);
fprintf(fid1,'%3d  ',iter(2));
fprintf(fid2,'%6.1f  ',telapsed);

% OD
tdd= tic;
[~,~,~,iter,~]= ...
    schwarzsubs(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR, ...
           Nx,Ny,Nz,olp,ntheta,nphi,randinit);
telapsed= toc(tdd);
fprintf(fid1,'%3d   ',iter(2));
fprintf(fid2,'%6.1f  ',telapsed);

% OO0
tdd= tic;
[~,~,~,iter,~,reg]= ...
    oossubs(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR, ...
           Nx,Ny,Nz,olp,ntheta,nphi,0,[],randinit);
telapsed= toc(tdd);
fprintf(fid1,'%3d    ',iter(2));
fprintf(fid2,'%6.1f  ',telapsed);
fprintf(fid3,'(%6.2g,%6.2g)  ', real(reg),imag(reg));

% search for good parameters for OO0
p= 1:5:100;
iterv= zeros(length(p),1); tv= zeros(length(p),1);
for idx= 1:length(p)
    if ispd
        reg= p(idx);
    else
        reg= p(idx)*(1-1i);
    end
    tdd= tic;
    [~,~,~,iter]= ...
        oossubs(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR, ...
        Nx,Ny,Nz,olp,ntheta,nphi,0,reg,randinit);
    telapsed= toc(tdd);
    iterv(idx)= iter(2);
    tv(idx)= telapsed;
end
[~,idx]= min(iterv);
fprintf(fid1,'%3d   ',iterv(idx));
fprintf(fid2,'%6.1f  ',tv(idx));
fprintf(fid3,'(%6.2g,%6.2g)  ', real(reg),imag(reg));

% OO2
tdd= tic;
[~,~,~,iter,~,reg]= ...
    oossubs(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR, ...
           Nx,Ny,Nz,olp,ntheta,nphi,2,[],randinit);
telapsed= toc(tdd);
fprintf(fid1,'%3d     ',iter(2));
fprintf(fid2,'%6.1f  ',telapsed);
fprintf(fid3,'(%6.2g,%6.2g),(%6.2g,%6.2g)  ', real(reg(1)),imag(reg(1)), real(reg(2)),imag(reg(2)));

% search for good parameters for OO2, 1
alpha= 1:5:100; beta= 1:5:100;
iterv= zeros(length(alpha)*length(beta),1);
tv= zeros(length(alpha)*length(beta),1);
idx= 0;
for ia= 1:length(alpha)
    for ib= 1:length(beta)
        idx= idx + 1;
        reg= [(-alpha(ia)*1i*beta(ib)-k2)/(beta(ib)-alpha(ia)*1i),1/(beta(ib)-alpha(ia)*1i)]; 
                   % special form 1 of Robin
        tdd= tic;
        [~,~,~,iter]= ...
           oossubs(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR, ...
                   Nx,Ny,Nz,olp,ntheta,nphi,2,reg,randinit);
        telapsed= toc(tdd);
        iterv(idx)= iter(2);
        tv(idx)= telapsed;
    end
end
[~,idx]= min(iterv);
fprintf(fid1,'%3d     ',iterv(idx));
fprintf(fid2,'%6.1f  ',tv(idx));
[ib,ia]= ind2sub([length(beta),length(alpha)], idx);
reg= [(-alpha(ia)*1i*beta(ib)-k2)/(beta(ib)-alpha(ia)*1i),1/(beta(ib)-alpha(ia)*1i)];
fprintf(fid3,'(%6.2g,%6.2g),(%6.2g,%6.2g)  ', real(reg(1)),imag(reg(1)), real(reg(2)),imag(reg(2)));

% search for good parameters for OO2, 2
alpha= 1:5:100; beta= 1:5:100;
iterv= zeros(length(alpha)*length(beta),1);
tv= zeros(length(alpha)*length(beta),1);
idx= 0;
for ia= 1:length(alpha)
    for ib= 1:length(beta)
        idx= idx + 1;
        reg= [(alpha(ia)*(1-1i)^2*beta(ib)-k2)/(beta(ib)*(1-1i)+alpha(ia)*(1-1i)),...
               1/(beta(ib)*(1-1i)+alpha(ia)*(1-1i))]; 
                   % special form 2 of Robin
        tdd= tic;
        [~,~,~,iter]= ...
           oossubs(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR, ...
                   Nx,Ny,Nz,olp,ntheta,nphi,2,reg,randinit);
        telapsed= toc(tdd);
        iterv(idx)= iter(2);
        tv(idx)= telapsed;
    end
end
[~,idx]= min(iterv);
fprintf(fid1,'%3d   ',iterv(idx));
fprintf(fid2,'%6.1f  ',tv(idx));
[ib,ia]= ind2sub([length(beta),length(alpha)], idx);
reg= [(alpha(ia)*(1-1i)^2*beta(ib)-k2)/(beta(ib)*(1-1i)+alpha(ia)*(1-1i)),...
               1/(beta(ib)*(1-1i)+alpha(ia)*(1-1i))];
fprintf(fid3,'(%6.2g,%6.2g),(%6.2g,%6.2g)  ', real(reg(1)),imag(reg(1)), real(reg(2)),imag(reg(2)));

% OT0
tdd= tic;
[~,~,~,iter,~]= ...
    oossubs(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR, ...
           Nx,Ny,Nz,olp,ntheta,nphi,0,-1i*sqrt(k2),randinit);
telapsed= toc(tdd);
fprintf(fid1,'%3d   ',iter(2));
fprintf(fid2,'%6.1f  ',telapsed);

% OT2
tdd= tic;
[~,~,~,iter,~]= ...
    oossubs(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR, ...
           Nx,Ny,Nz,olp,ntheta,nphi,2,[-1i*sqrt(k2),-1i/2/sqrt(k2)],randinit);
telapsed= toc(tdd);
fprintf(fid1,'%3d  ',iter(2));
fprintf(fid2,'%6.1f  ',telapsed);

fprintf(fid1,'\n');
fprintf(fid2,'\n');
fprintf(fid3,'\n');
end

fclose('all');
%diary off;

end % --- end of helmholtz() 