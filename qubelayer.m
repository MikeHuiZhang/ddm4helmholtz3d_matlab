function  [cnt,IX,IY,IZ]= qubelayer(nx,ny,nz,layerfaces)
% nx,ny,nz partition of the qube 
% IX vector of the x positions of the boundary nodes of the qube

cnt= 0; 
num= (ny+1)*(nz+1);
cnt= cnt + (layerfaces(1)+layerfaces(2))*num;
num= (nx+1-layerfaces(1)-layerfaces(2))*(nz+1);
cnt= cnt + (layerfaces(3)+layerfaces(4))*num;
num= (nx+1-layerfaces(1)-layerfaces(2))*(ny+1-layerfaces(3)-layerfaces(4));
cnt= cnt + (layerfaces(5)+layerfaces(6))*num;
if nargout==1
    return;
end
IX= zeros(cnt,1); IY= zeros(cnt,1); IZ= zeros(cnt,1); cnt= 0;
% left and right
num= (ny+1)*(nz+1);
if layerfaces(1)
    [IY(cnt+1:cnt+num),IZ(cnt+1:cnt+num)]= ndgrid(1:ny+1,1:nz+1);
    IX(cnt+1:cnt+num)= 1;
    cnt= cnt + num;
end
if layerfaces(2)
    [IY(cnt+1:cnt+num), IZ(cnt+1:cnt+num)]= ndgrid(1:ny+1,1:nz+1);
    IX(cnt+1:cnt+num)= nx+1;
    cnt= cnt + num;
end
% front and back
num= (nx+1-layerfaces(1)-layerfaces(2))*(nz+1); 
if layerfaces(3)
    [IX(cnt+1:cnt+num),IZ(cnt+1:cnt+num)]= ndgrid(1+layerfaces(1):nx+1-layerfaces(2),1:nz+1);
    IY(cnt+1:cnt+num)= 1;
    cnt= cnt + num;
end
if layerfaces(4)
    [IX(cnt+1:cnt+num),IZ(cnt+1:cnt+num)]= ndgrid(1+layerfaces(1):nx+1-layerfaces(2),1:nz+1);
    IY(cnt+1:cnt+num)= ny+1;
    cnt= cnt + num;
end
% bottom  and top
num= (nx+1-layerfaces(1)-layerfaces(2))*(ny+1-layerfaces(3)-layerfaces(4));
if layerfaces(5)
    [IX(cnt+1:cnt+num),IY(cnt+1:cnt+num)]= ndgrid(1+layerfaces(1):nx+1-layerfaces(2),1+layerfaces(3):ny+1-layerfaces(4));
    IZ(cnt+1:cnt+num)= 1;
    cnt= cnt + num;
end
if layerfaces(6)
    [IX(cnt+1:cnt+num),IY(cnt+1:cnt+num)]= ndgrid(1+layerfaces(1):nx+1-layerfaces(2),1+layerfaces(3):ny+1-layerfaces(4));
    IZ(cnt+1:cnt+num)= nz+1;
    cnt= cnt + num;
end
