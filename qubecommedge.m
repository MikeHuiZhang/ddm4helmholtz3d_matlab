function [sfnodes,sendf,sfsend,ngf,senodes,sende,sesend,nge,...
    svnodes,sendv,svsend,ngv, sfelems,seelems,svelems]= ...
    qubecommedge(GammaD,nx,ny,nz,Nx,Ny,Nz,ol)
%   [sfnodes,sendf,sfsend,senodes,sende,sesend,svnodes,sendv,sfelems,seelems,svelems]
% = qubecommedge(GammaD,GammaR,nx,ny,nz,Nx,Ny,Nz,ol)
% construct 'communication edges' for qube partition with overlapping ol
% sfnodes{s}: faces interior nodes of subdomain s, excluding GammaD and
%             GammaR, pointing to global nodes indices 
% sendf(s):   point to the end position of subdomain faces interior nodes 
%             in product faces interior nodes (sfnodes{1},...sfnodes{Ns})
% sfsend(:,s):  with six entries, point to the end position of 1~6 faces interior
%             nodes in sfnodes{s}, if m-th face is on the outer boundary, set 
%             sfsend{s}(m)= sfsend{s}(m-1)
% ngf:        the total number of all subdomains face interior nodes
% senodes{s}: edges interior nodes of subdomain s, exluding GammaD but 
%             including those on GammaR, point to global indices
% sesend(s):
% sesend(:,s):  with twelve entries, 
% nge:
% svnodes{s}:   excluding GammaD but including those on GammaR
% sendv(s):  
% svsend(:,s):  with eight entries,
% ngv:

% sfelems{s,m}: solid elements that contain m-th face of sfnodes{s}, 
%               pointing to global elements indices
% seelems{s,m}: solid elements
% svelems{s,m}: solide elements
snx= nx/Nx;  sny= ny/Ny;  snz= nz/Nz; Ns= Nx*Ny*Nz; 
sfnodes= cell(Ns,1);  sendf= zeros(Ns,1);  sfsend= zeros(6,Ns);  ngf= 0;
senodes= cell(Ns,1);  sende= zeros(Ns,1);  sesend= zeros(12,Ns); nge= 0;
svnodes= cell(Ns,1);  sendv= zeros(Ns,1);  svsend= zeros(8,Ns);  ngv= 0;
for s= 1:Ns
   [sx,sy]= ind2sub([Nx,Ny*Nz],s); % 3d position of this subdomain
   [sy,sz]= ind2sub([Ny,Nz],sy);
   % count size for sfnodes{s}
   numfnodes= 0;
   if 1~=sx
       numfnodes= numfnodes + (sny + (sy<Ny)*ol + (sy>1)*ol - 1)* ...
           ( snz + (sz<Nz)*ol + (sz>1)*ol - 1 );
   end
   sfsend(1,s)= numfnodes;
   if Nx~=sx
       numfnodes= numfnodes + (sny + (sy<Ny)*ol + (sy>1)*ol - 1)* ...
           ( snz + (sz<Nz)*ol + (sz>1)*ol - 1 );
   end
   sfsend(2,s)= numfnodes;
   if 1~=sy
       numfnodes= numfnodes + (snx + (sx<Nx)*ol + (sx>1)*ol - 1)* ...
           ( snz + (sz<Nz)*ol + (sz>1)*ol - 1 );
   end
   sfsend(3,s)= numfnodes;
   if Ny~=sy
       numfnodes= numfnodes + (snx + (sx<Nx)*ol + (sx>1)*ol - 1)* ...
           ( snz + (sz<Nz)*ol + (sz>1)*ol - 1 );
   end
   sfsend(4,s)= numfnodes;
   if 1~=sz
       numfnodes= numfnodes + (snx + (sx<Nx)*ol + (sx>1)*ol - 1)* ...
           ( sny + (sy<Ny)*ol + (sy>1)*ol - 1 );
   end
   sfsend(5,s)= numfnodes;
   if Nz~=sz
       numfnodes= numfnodes + (snx + (sx<Nx)*ol + (sx>1)*ol - 1)* ...
           ( sny + (sy<Ny)*ol + (sy>1)*ol - 1 );
   end
   sfsend(6,s)= numfnodes;
   sfnodes{s}= zeros(numfnodes,1);
   ngf= ngf + numfnodes;
   sendf(s)= ngf;
   numfnodes= 0;
   % face 1
   IX= (sx-1)*snx-(sx>1)*ol+1;                       % 3d position 
   iy= ((sy-1)*sny-(sy>1)*ol+2):(sy*sny+(sy<Ny)*ol); % of global nodes
   iz= ((sz-1)*snz-(sz>1)*ol+2):(sz*snz+(sz<Nz)*ol);
   [IY,IZ]= ndgrid(iy,iz);  numfs= length(iy)*length(iz);
   clear iy iz; 
   if 1~=sx
      sfnodes{s}(numfnodes+1:numfnodes+numfs)= sub2ind([nx+1,ny+1,nz+1],IX*ones(numfs,1),IY(:),IZ(:));
      numfnodes= numfnodes + numfs;
   end
   % face 2
   IX= sx*snx+(sx<Nx)*ol+1;
   if Nx~=sx
      sfnodes{s}(numfnodes+1:numfnodes+numfs)= sub2ind([nx+1,ny+1,nz+1],IX*ones(numfs,1),IY(:),IZ(:));
      numfnodes= numfnodes + numfs;
   end
   clear IX IY IZ;
   % face 3
   ix= ((sx-1)*snx-(sx>1)*ol+2):(sx*snx+(sx<Nx)*ol);   % 3d position 
   IY= (sy-1)*sny-(sy>1)*ol+1;                         % of global nodes
   iz= ((sz-1)*snz-(sz>1)*ol+2):(sz*snz+(sz<Nz)*ol);
   [IX,IZ]= ndgrid(ix,iz);  numfs= length(ix)*length(iz);
   clear ix iz; 
   if 1~=sy
      sfnodes{s}(numfnodes+1:numfnodes+numfs)= sub2ind([nx+1,ny+1,nz+1],IX(:),IY*ones(numfs,1),IZ(:));
      numfnodes= numfnodes + numfs;
   end
   % face 4
   IY= sy*sny+(sy<Ny)*ol+1;  
   if Ny~=sy
      sfnodes{s}(numfnodes+1:numfnodes+numfs)= sub2ind([nx+1,ny+1,nz+1],IX(:),IY*ones(numfs,1),IZ(:));
      numfnodes= numfnodes + numfs;
   end
   clear IX IY IZ;
   % face 5
   ix= ((sx-1)*snx-(sx>1)*ol+2):(sx*snx+(sx<Nx)*ol);
   iy= ((sy-1)*sny-(sy>1)*ol+2):(sy*sny+(sy<Ny)*ol);
   IZ= (sz-1)*snz-(sz>1)*ol+1;
   [IX,IY]= ndgrid(ix,iy);  numfs= length(ix)*length(iy);
   clear ix iy;
   if 1~=sz
      sfnodes{s}(numfnodes+1:numfnodes+numfs)= sub2ind([nx+1,ny+1,nz+1],IX(:),IY(:),IZ*ones(numfs,1));
      numfnodes= numfnodes + numfs;
   end
   % face 6
   IZ= sz*snz+(sz<Nz)*ol+1;
   if Nz~=sz
      sfnodes{s}(numfnodes+1:numfnodes+numfs)= sub2ind([nx+1,ny+1,nz+1],IX(:),IY(:),IZ*ones(numfs,1));
   end
   clear IX IY IZ numfnodes numfs;
   % count size of senodes{s}
   numenodes= 0;
   c1= sx>1 || isempty(find(GammaD==1,1));
   c2= sx<Nx || isempty(find(GammaD==2,1));
   c3= sy>1 || isempty(find(GammaD==3,1));
   c4= sy<Ny || isempty(find(GammaD==4,1));
   c5= sz>1 || isempty(find(GammaD==5,1));
   c6= sz<Nz || isempty(find(GammaD==6,1));
   % % z-direction edges
   numes= snz + (sz<Nz)*ol + (sz>1)*ol - 1;
   if ( c1 ) && ( c3 ) && ~( 1==sx && 1==sy )
       numenodes= numenodes + numes;
   end
   sesend(1,s)= numenodes;
   if ( c2 ) && ( c3 ) && ~( Nx==sx && 1==sy )
       numenodes= numenodes + numes;
   end
   sesend(2,s)= numenodes;
   if ( c1 ) && ( c4 ) && ~( 1==sx && Ny==sy )
       numenodes= numenodes + numes;
   end
   sesend(3,s)= numenodes;
   if ( c2 ) && ( c4 ) && ~( Nx==sx && Ny==sy)
       numenodes= numenodes + numes;
   end
   sesend(4,s)= numenodes;
   % % y-direction edges
   numes= sny + (sy<Ny)*ol + (sy>1)*ol - 1;
   if ( c1 ) && ( c5 ) && ~( 1==sx && 1==sz )
       numenodes= numenodes + numes;
   end
   sesend(5,s)= numenodes;
   if ( c2 ) && ( c5 ) && ~( Nx==sx && 1==sz )
       numenodes= numenodes + numes;
   end
   sesend(6,s)= numenodes;
   if ( c1 ) && ( c6 ) && ~( 1==sx && Nz==sz )
       numenodes= numenodes + numes;
   end
   sesend(7,s)= numenodes;
   if ( c2 ) && ( c6 ) && ~( Nx==sx && Nz==sz )
       numenodes= numenodes + numes;
   end
   sesend(8,s)= numenodes;
   % % x-direction edges
   numes= snx + (sx<Nx)*ol + (sx>1)*ol - 1;
   if ( c3 ) && ( c5 ) && ~( 1==sy && 1==sz )
       numenodes= numenodes + numes;
   end
   sesend(9,s)= numenodes;
   if ( c4 ) && ( c5 ) && ~( Ny==sy && 1==sz )
       numenodes= numenodes + numes;
   end
   sesend(10,s)= numenodes;
   if ( c3 ) && ( c6 ) && ~( 1==sy && Nz==sz )
       numenodes= numenodes + numes;
   end
   sesend(11,s)= numenodes;
   if ( c4 ) && ( c6 ) && ~( Ny==sy && Nz==sz )
       numenodes= numenodes + numes;
   end
   sesend(12,s)= numenodes;
   senodes{s}= zeros(numenodes,1);
   nge= nge + numenodes;
   sende(s)= nge;
   % construction of senodes{s}
   numenodes= 0;
   % % z-direction edges
   numes= snz + (sz<Nz)*ol + (sz>1)*ol - 1;  
   IZ= ((sz-1)*snz-(sz>1)*ol+2):(sz*snz+(sz<Nz)*ol); IZ= IZ.';
   if ( c1 ) && ( c3 ) && ~( 1==sx && 1==sy )
       IX= (sx-1)*snx-(sx>1)*ol+1;
       IY= (sy-1)*sny-(sy>1)*ol+1; 
       senodes{s}(numenodes+1:numenodes+numes)= sub2ind([nx+1,ny+1,nz+1],IX*ones(numes,1),IY*ones(numes,1),IZ);
       numenodes= numenodes + numes;
   end
   if ( c2 ) && ( c3 ) && ~( Nx==sx && 1==sy )
       IX= sx*snx+(sx<Nx)*ol+1;
       IY= (sy-1)*sny-(sy>1)*ol+1; 
       senodes{s}(numenodes+1:numenodes+numes)= sub2ind([nx+1,ny+1,nz+1],IX*ones(numes,1),IY*ones(numes,1),IZ);
       numenodes= numenodes + numes;
   end
   if ( c1 ) && ( c4 ) && ~( 1==sx && Ny==sy )
       IX= (sx-1)*snx-(sx>1)*ol+1;
       IY= sy*sny+(sy<Ny)*ol+1; 
       senodes{s}(numenodes+1:numenodes+numes)= sub2ind([nx+1,ny+1,nz+1],IX*ones(numes,1),IY*ones(numes,1),IZ);
       numenodes= numenodes + numes;
   end
   if ( c2 ) && ( c4 ) && ~( Nx==sx && Ny==sy)
       IX= sx*snx+(sx<Nx)*ol+1;
       IY= sy*sny+(sy<Ny)*ol+1; 
       senodes{s}(numenodes+1:numenodes+numes)= sub2ind([nx+1,ny+1,nz+1],IX*ones(numes,1),IY*ones(numes,1),IZ);
       numenodes= numenodes + numes;
   end
   % % y-direction edges
   numes= sny + (sy<Ny)*ol + (sy>1)*ol - 1;
   IY= ((sy-1)*sny-(sy>1)*ol+2):(sy*sny+(sy<Ny)*ol);  IY= IY.';
   if ( c1 ) && ( c5 ) && ~( 1==sx && 1==sz )
       IX= (sx-1)*snx-(sx>1)*ol+1;
       IZ= (sz-1)*snz-(sz>1)*ol+1;
       senodes{s}(numenodes+1:numenodes+numes)= sub2ind([nx+1,ny+1,nz+1],IX*ones(numes,1),IY,IZ*ones(numes,1));
       numenodes= numenodes + numes;
   end
   if ( c2 ) && ( c5 ) && ~( Nx==sx && 1==sz ) 
       IX= sx*snx+(sx<Nx)*ol+1;
       IZ= (sz-1)*snz-(sz>1)*ol+1;
       senodes{s}(numenodes+1:numenodes+numes)= sub2ind([nx+1,ny+1,nz+1],IX*ones(numes,1),IY,IZ*ones(numes,1));
       numenodes= numenodes + numes;
   end
   if ( c1 ) && ( c6 ) && ~( 1==sx && Nz==sz )
       IX= (sx-1)*snx-(sx>1)*ol+1;
       IZ= sz*snz+(sz<Nz)*ol+1;
       senodes{s}(numenodes+1:numenodes+numes)= sub2ind([nx+1,ny+1,nz+1],IX*ones(numes,1),IY,IZ*ones(numes,1));
       numenodes= numenodes + numes;
   end
   if ( c2 ) && ( c6 ) && ~( Nx==sx && Nz==sz )
       IX= sx*snx+(sx<Nx)*ol+1;
       IZ= sz*snz+(sz<Nz)*ol+1;
       senodes{s}(numenodes+1:numenodes+numes)= sub2ind([nx+1,ny+1,nz+1],IX*ones(numes,1),IY,IZ*ones(numes,1));
       numenodes= numenodes + numes;
   end
   % % x-direction edges
   numes= snx + (sx<Nx)*ol + (sx>1)*ol - 1;
   IX= ((sx-1)*snx-(sx>1)*ol+2):(sx*snx+(sx<Nx)*ol);  IX= IX.';
   if ( c3 ) && ( c5 ) && ~( 1==sy && 1==sz )
       IY= (sy-1)*sny-(sy>1)*ol+1;
       IZ= (sz-1)*snz-(sz>1)*ol+1;
       senodes{s}(numenodes+1:numenodes+numes)= sub2ind([nx+1,ny+1,nz+1],IX,IY*ones(numes,1),IZ*ones(numes,1));
       numenodes= numenodes + numes;
   end
   if ( c4 ) && ( c5 ) && ~( Ny==sy && 1==sz )
       IY= sy*sny+(sy<Ny)*ol+1; 
       IZ= (sz-1)*snz-(sz>1)*ol+1;
       senodes{s}(numenodes+1:numenodes+numes)= sub2ind([nx+1,ny+1,nz+1],IX,IY*ones(numes,1),IZ*ones(numes,1));
       numenodes= numenodes + numes;
   end
   if ( c3 ) && ( c6 ) && ~( 1==sy && Nz==sz )
       IY= (sy-1)*sny-(sy>1)*ol+1;
       IZ= sz*snz+(sz<Nz)*ol+1;
       senodes{s}(numenodes+1:numenodes+numes)= sub2ind([nx+1,ny+1,nz+1],IX,IY*ones(numes,1),IZ*ones(numes,1));
       numenodes= numenodes + numes;
   end
   if ( c4 ) && ( c6 ) && ~( Ny==sy && Nz==sz )
       IY= sy*sny+(sy<Ny)*ol+1; 
       IZ= sz*snz+(sz<Nz)*ol+1;
       senodes{s}(numenodes+1:numenodes+numes)= sub2ind([nx+1,ny+1,nz+1],IX,IY*ones(numes,1),IZ*ones(numes,1));
   end
   clear IX IY IZ numenodes numes;
   % count size of svnodes{s}
   numvnodes= 0;
   if ( c1 ) && ( c3 ) && ( c5 ) && ~( 1==sx && 1==sy && 1==sz ) 
       numvnodes= numvnodes + 1;
   end
   svsend(1,s)= numvnodes;
   if ( c2 ) && ( c3 ) && ( c5 ) && ~( Nx==sx && 1==sy && 1==sz ) 
       numvnodes= numvnodes + 1;
   end
   svsend(2,s)= numvnodes;
   if ( c2 ) && ( c4 ) && ( c5 ) && ~( Nx==sx && Ny==sy && 1==sz )
       numvnodes= numvnodes + 1;
   end
   svsend(3,s)= numvnodes;
   if ( c1 ) && ( c4 ) && ( c5 ) && ~( 1==sx && Ny==sy && 1==sz )
       numvnodes= numvnodes + 1;
   end
   svsend(4,s)= numvnodes;
   if ( c1 ) && ( c3 ) && ( c6 ) && ~( 1==sx && 1==sy && Nz==sz ) 
       numvnodes= numvnodes + 1;
   end
   svsend(5,s)= numvnodes;
   if ( c2 ) && ( c3 ) && ( c6 ) && ~( Nx==sx && 1==sy && Nz==sz ) 
       numvnodes= numvnodes + 1;
   end
   svsend(6,s)= numvnodes;
   if ( c2 ) && ( c4 ) && ( c6 ) && ~( Nx==sx && Ny==sy && Nz==sz )
       numvnodes= numvnodes + 1;
   end
   svsend(7,s)= numvnodes;
   if ( c1 ) && ( c4 ) && ( c6 ) && ~( 1==sx && Ny==sy && Nz==sz )
       numvnodes= numvnodes + 1;
   end
   svsend(8,s)= numvnodes;
   ngv= ngv + numvnodes;
   sendv(s)= ngv;
   % construction of vertices svnodes{s}
   numvnodes= 0;
   if ( c1 ) && ( c3 ) && ( c5 ) && ~( 1==sx && 1==sy && 1==sz )
       IX= (sx-1)*snx-(sx>1)*ol+1;
       IY= (sy-1)*sny-(sy>1)*ol+1;
       IZ= (sz-1)*snz-(sz>1)*ol+1;
       svnodes{s}(numvnodes+1)= sub2ind([nx+1,ny+1,nz+1],IX,IY,IZ);
       numvnodes= numvnodes + 1;
   end
   if ( c2 ) && ( c3 ) && ( c5 ) && ~( Nx==sx && 1==sy && 1==sz ) 
       IX= sx*snx+(sx<Nx)*ol+1;
       IY= (sy-1)*sny-(sy>1)*ol+1;
       IZ= (sz-1)*snz-(sz>1)*ol+1;
       svnodes{s}(numvnodes+1)= sub2ind([nx+1,ny+1,nz+1],IX,IY,IZ);
       numvnodes= numvnodes + 1;
   end
   if ( c2 ) && ( c4 ) && ( c5 ) && ~( Nx==sx && Ny==sy && 1==sz )
       IX= sx*snx+(sx<Nx)*ol+1;
       IY= sy*sny+(sy<Ny)*ol+1;
       IZ= (sz-1)*snz-(sz>1)*ol+1;
       svnodes{s}(numvnodes+1)= sub2ind([nx+1,ny+1,nz+1],IX,IY,IZ);
       numvnodes= numvnodes + 1;
   end 
   if ( c1 ) && ( c4 ) && ( c5 ) && ~( 1==sx && Ny==sy && 1==sz )
       IX= (sx-1)*snx-(sx>1)*ol+1;
       IY= sy*sny+(sy<Ny)*ol+1;
       IZ= (sz-1)*snz-(sz>1)*ol+1;
       svnodes{s}(numvnodes+1)= sub2ind([nx+1,ny+1,nz+1],IX,IY,IZ);
       numvnodes= numvnodes + 1;
   end
   if ( c1 ) && ( c3 ) && ( c6 ) && ~( 1==sx && 1==sy && Nz==sz ) 
       IX= (sx-1)*snx-(sx>1)*ol+1;
       IY= (sy-1)*sny-(sy>1)*ol+1;
       IZ= sz*snz+(sz<Nz)*ol+1;
       svnodes{s}(numvnodes+1)= sub2ind([nx+1,ny+1,nz+1],IX,IY,IZ);
       numvnodes= numvnodes + 1;
   end
   if ( c2 ) && ( c3 ) && ( c6 ) && ~( Nx==sx && 1==sy && Nz==sz ) 
       IX= sx*snx+(sx<Nx)*ol+1;
       IY= (sy-1)*sny-(sy>1)*ol+1;
       IZ= sz*snz+(sz<Nz)*ol+1;
       svnodes{s}(numvnodes+1)= sub2ind([nx+1,ny+1,nz+1],IX,IY,IZ);
       numvnodes= numvnodes + 1;
   end
   if ( c2 ) && ( c4 ) && ( c6 ) && ~( Nx==sx && Ny==sy && Nz==sz )
       IX= sx*snx+(sx<Nx)*ol+1;
       IY= sy*sny+(sy<Ny)*ol+1;
       IZ= sz*snz+(sz<Nz)*ol+1;
       svnodes{s}(numvnodes+1)= sub2ind([nx+1,ny+1,nz+1],IX,IY,IZ);
       numvnodes= numvnodes + 1;
   end
   if ( c1 ) && ( c4 ) && ( c6 ) && ~( 1==sx && Ny==sy && Nz==sz )
       IX= (sx-1)*snx-(sx>1)*ol+1;
       IY= sy*sny+(sy<Ny)*ol+1;
       IZ= sz*snz+(sz<Nz)*ol+1;
       svnodes{s}(numvnodes+1)= sub2ind([nx+1,ny+1,nz+1],IX,IY,IZ);
   end
   clear IX IY IZ numvnodes;
end
if nargout <= 12
    return;
end
sfelems= cell(Ns,6);
seelems= cell(Ns,12);
svelems= cell(Ns,8);
for s= 1:Ns
    [sx,sy]= ind2sub([Nx,Ny*Nz],s); % 3d position of this subdomain
    [sy,sz]= ind2sub([Ny,Nz],sy);
    % solid elements in subdomain s that are near faces 
    % % face 1
    iy= ((sy-1)*sny-(sy>1)*ol+1):(sy*sny+(sy<Ny)*ol);
    iz= ((sz-1)*snz-(sz>1)*ol+1):(sz*snz+(sz<Nz)*ol);
    [IY,IZ]= ndgrid(iy,iz);  numfe= length(iy)*length(iz);
    clear iy iz;
    if sx>1
        IX= (sx-1)*snx-(sx>1)*ol+1;
        sfelems{s,1}= sub2ind([nx,ny,nz],IX*ones(numfe,1),IY(:),IZ(:));
    end
    % % face 2   
    if sx<Nx
        IX= sx*snx+(sx<Nx)*ol;
        sfelems{s,2}= sub2ind([nx,ny,nz],IX*ones(numfe,1),IY(:),IZ(:));
    end
    clear IX IY IZ;
    % % face 3
    ix= ((sx-1)*snx-(sx>1)*ol+1):(sx*snx+(sx<Nx)*ol);
    iz= ((sz-1)*snz-(sz>1)*ol+1):(sz*snz+(sz<Nz)*ol);
    [IX,IZ]= ndgrid(ix,iz); numfe= length(ix)*length(iz);
    clear ix iz;
    if sy>1
        IY= (sy-1)*sny-(sy>1)*ol+1;
        sfelems{s,3}= sub2ind([nx,ny,nz],IX(:),IY*ones(numfe,1),IZ(:));
    end
    % % face 4
    if sy<Ny
        IY= sy*sny+(sy<Ny)*ol;
        sfelems{s,4}= sub2ind([nx,ny,nz],IX(:),IY*ones(numfe,1),IZ(:));
    end
    clear IX IY IZ;
    % % face 5
    ix= ((sx-1)*snx-(sx>1)*ol+1):(sx*snx+(sx<Nx)*ol);
    iy= ((sy-1)*sny-(sy>1)*ol+1):(sy*sny+(sy<Ny)*ol);
    [IX,IY]= ndgrid(ix,iy); numfe= length(ix)*length(iy);
    clear ix iy;
    if sz>1
        IZ= (sz-1)*snz-(sz>1)*ol+1;
        sfelems{s,5}= sub2ind([nx,ny,nz],IX(:),IY(:),IZ*ones(numfe,1));
    end
    % % face 6
    if sz<Nz
        IZ= sz*snz+(sz<Nz)*ol;
        sfelems{s,6}= sub2ind([nx,ny,nz],IX(:),IY(:),IZ*ones(numfe,1));
    end
    clear IX IY IZ;
    % solid elements containing edges nodes of subdomain s
    c1= sx>1 || isempty(find(GammaD==1,1));
    c2= sx<Nx || isempty(find(GammaD==2,1));
    c3= sy>1 || isempty(find(GammaD==3,1));
    c4= sy<Ny || isempty(find(GammaD==4,1));
    c5= sz>1 || isempty(find(GammaD==5,1));
    c6= sz<Nz || isempty(find(GammaD==6,1));
    % % z-direction
    IZ= ((sz-1)*snz-(sz>1)*ol+1):(sz*snz+(sz<Nz)*ol);  IZ= IZ.';
    numee= length(IZ);
    if c1 && c3 && ~( 1==sx && 1==sy )
       IX= (sx-1)*snx-(sx>1)*ol+1;
       IY= (sy-1)*sny-(sy>1)*ol+1;
       seelems{s,1}= sub2ind([nx,ny,nz],IX*ones(numee,1),IY*ones(numee,1),IZ);
    end
    if c2 && c3 && ~( Nx==sx && 1==sy )
       IX= sx*snx+(sx<Nx)*ol;
       IY= (sy-1)*sny-(sy>1)*ol+1;
       seelems{s,2}= sub2ind([nx,ny,nz],IX*ones(numee,1),IY*ones(numee,1),IZ);
    end
    if c1 && c4 && ~( 1==sx && Ny==sy )
       IX= (sx-1)*snx-(sx>1)*ol+1;
       IY= sy*sny+(sy<Ny)*ol;
       seelems{s,3}= sub2ind([nx,ny,nz],IX*ones(numee,1),IY*ones(numee,1),IZ);
    end
    if c2 && c4 && ~( Nx==sx && Ny==sy )
       IX= sx*snx+(sx<Nx)*ol;
       IY= sy*sny+(sy<Ny)*ol;
       seelems{s,4}= sub2ind([nx,ny,nz],IX*ones(numee,1),IY*ones(numee,1),IZ);
    end
    % % y-direction
    IY= ((sy-1)*sny-(sy>1)*ol+1):(sy*sny+(sy<Ny)*ol);  IY= IY.';
    numee= length(IY);
    if c1 && c5 && ~( 1==sx && 1==sz )
        IX= (sx-1)*snx-(sx>1)*ol+1;
        IZ= (sz-1)*snz-(sz>1)*ol+1;
        seelems{s,5}= sub2ind([nx,ny,nz],IX*ones(numee,1),IY,IZ*ones(numee,1));
    end
    if c2 && c5 && ~( Nx==sx && 1==sz )
        IX= sx*snx+(sx<Nx)*ol;
        IZ= (sz-1)*snz-(sz>1)*ol+1;
        seelems{s,6}= sub2ind([nx,ny,nz],IX*ones(numee,1),IY,IZ*ones(numee,1));
    end
    if c1 && c6 && ~( 1==sx && Nz==sz )
        IX= (sx-1)*snx-(sx>1)*ol+1;
        IZ= sz*snz+(sz<Nz)*ol;
        seelems{s,7}= sub2ind([nx,ny,nz],IX*ones(numee,1),IY,IZ*ones(numee,1));
    end
    if c2 && c6 && ~( Nx==sx && Nz==sz )
        IX= sx*snx+(sx<Nx)*ol;
        IZ= sz*snz+(sz<Nz)*ol;
        seelems{s,8}= sub2ind([nx,ny,nz],IX*ones(numee,1),IY,IZ*ones(numee,1));
    end
    % % x-direction
    IX= ((sx-1)*snx-(sx>1)*ol+1):(sx*snx+(sx<Nx)*ol);  IX= IX.';
    numee= length(IX);
    if c3 && c5 && ~( 1==sy && 1==sz )
        IY= (sy-1)*sny-(sy>1)*ol+1;
        IZ= (sz-1)*snz-(sz>1)*ol+1;
        seelems{s,9}= sub2ind([nx,ny,nz],IX,IY*ones(numee,1),IZ*ones(numee,1));
    end
    if c4 && c5 && ~( Ny==sy && 1==sz )
        IY= sy*sny+(sy<Ny)*ol;
        IZ= (sz-1)*snz-(sz>1)*ol+1;
        seelems{s,10}= sub2ind([nx,ny,nz],IX,IY*ones(numee,1),IZ*ones(numee,1));
    end
    if c3 && c6 && ~( 1==sy && Nz==sz )
        IY= (sy-1)*sny-(sy>1)*ol+1;
        IZ= sz*snz+(sz<Nz)*ol;
        seelems{s,11}= sub2ind([nx,ny,nz],IX,IY*ones(numee,1),IZ*ones(numee,1));
    end
    if c4 && c6 && ~( Ny==sy && Nz==sz )
        IY= sy*sny+(sy<Ny)*ol;
        IZ= sz*snz+(sz<Nz)*ol;
        seelems{s,12}= sub2ind([nx,ny,nz],IX,IY*ones(numee,1),IZ*ones(numee,1));
    end
    % solid elements containing vertices nodes of subdomain s
    IZ= (sz-1)*snz-(sz>1)*ol+1;
    if ( c1 ) && ( c3 ) && ( c5 ) && ~( 1==sx && 1==sy && 1==sz )
        IX= (sx-1)*snx-(sx>1)*ol+1;
        IY= (sy-1)*sny-(sy>1)*ol+1;
        svelems{s,1}= sub2ind([nx,ny,nz],IX,IY,IZ);
    end
    if ( c2 ) && ( c3 ) && ( c5 ) && ~( Nx==sx && 1==sy && 1==sz )
        IX= sx*snx+(sx<Nx)*ol;
        IY= (sy-1)*sny-(sy>1)*ol+1;
        svelems{s,2}= sub2ind([nx,ny,nz],IX,IY,IZ);
    end
    if ( c2 ) && ( c4 ) && ( c5 ) && ~( Nx==sx && Ny==sy && 1==sz )
        IX= sx*snx+(sx<Nx)*ol;
        IY= sy*sny+(sy<Ny)*ol;
        svelems{s,3}= sub2ind([nx,ny,nz],IX,IY,IZ);
    end
    if ( c1 ) && ( c4 ) && ( c5 ) && ~( 1==sx && Ny==sy && 1==sz )
        IX= (sx-1)*snx-(sx>1)*ol+1;
        IY= sy*sny+(sy<Ny)*ol;
        svelems{s,4}= sub2ind([nx,ny,nz],IX,IY,IZ);
    end
    IZ= sz*snz+(sz<Nz)*ol;
    if ( c1 ) && ( c3 ) && ( c6 ) && ~( 1==sx && 1==sy && Nz==sz )
        IX= (sx-1)*snx-(sx>1)*ol+1;
        IY= (sy-1)*sny-(sy>1)*ol+1;
        svelems{s,5}= sub2ind([nx,ny,nz],IX,IY,IZ);
    end
    if ( c2 ) && ( c3 ) && ( c6 ) && ~( Nx==sx && 1==sy && Nz==sz )
        IX= sx*snx+(sx<Nx)*ol;
        IY= (sy-1)*sny-(sy>1)*ol+1;
        svelems{s,6}= sub2ind([nx,ny,nz],IX,IY,IZ);
    end
    if ( c2 ) && ( c4 ) && ( c6 ) && ~( Nx==sx && Ny==sy && Nz==sz )
        IX= sx*snx+(sx<Nx)*ol;
        IY= sy*sny+(sy<Ny)*ol;
        svelems{s,7}= sub2ind([nx,ny,nz],IX,IY,IZ);
    end
    if ( c1 ) && ( c4 ) && ( c6 ) && ~( 1==sx && Ny==sy && Nz==sz )
        IX= (sx-1)*snx-(sx>1)*ol+1;
        IY= sy*sny+(sy<Ny)*ol;
        svelems{s,8}= sub2ind([nx,ny,nz],IX,IY,IZ);
    end
    clear IX IY IZ;
end
